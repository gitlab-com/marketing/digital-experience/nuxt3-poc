import eslintPluginYml from 'eslint-plugin-yml';
import { dataPropertiesPlugin } from './custom-eslint/dex-data-properties.js';
import withNuxt from './.nuxt/eslint.config.mjs';
import customRulePlugin from './custom-eslint/dex-custom-plugin.js';
import prettier from 'eslint-config-prettier';
import eslintPluginVueScopedCSS from 'eslint-plugin-vue-scoped-css';
import vueEslintParser from 'vue-eslint-parser';

export default withNuxt(
  {
    rules: {
      '@stylistic/semi': 'off',
      'vue/no-v-html': 'off',
      'vue/require-default-prop': 'off',
      'no-console': ['error', { allow: ['info', 'warn', 'error'] }],
      'vue/html-self-closing': 'off',
      'vue/no-multiple-template-root': 'off',
      'vue/valid-v-show': 'error',
      '@stylistic/quote-props': 'off',
      '@stylistic/member-delimiter-style': 'off',
      '@stylistic/brace-style': 'off',
      '@stylistic/quotes': 'off',
      '@stylistic/arrow-parens': 'off',
      'vue/valid-v-for': 'error',
      '@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
      '@typescript-eslint/no-explicit-any': 'warn',
      ...prettier.rules,
    },
  },
  ...eslintPluginVueScopedCSS.configs['flat/recommended'],
  {
    rules: {
      'vue-scoped-css/no-unused-selector': 'off',
      'vue-scoped-css/enforce-style-type': 'error',
      'vue-scoped-css/require-v-deep-argument': 'error',
    },
  },
  ...eslintPluginYml.configs['flat/recommended'],
  ...eslintPluginYml.configs['flat/prettier'],
  {
    files: ['content/**/*.yaml', 'content/**/*.yml'],
    rules: {
      'yml/key-name-casing': ['error', { camelCase: true }],
    },
  },
  {
    files: ['content/**/*.yaml', 'content/**/*.yml'],
    ignores: [
      'content/shared/**',
      'content/en-us/blog/**', // Remove when blog migration work begins and dummy content is removed
      'content/en-us/blog/authors/**',
      'content/en-us/blog/categories/**',
    ],
    plugins: { dex: customRulePlugin },
    rules: {
      'dex/check-yml-page-file-properties': 'error',
      'dex/check-yml-for-config-properties': 'error',
    },
  },
  {
    files: [
      '**/fr-fr/**/*.yml',
      '**/ja-jp/**/*.yml',
      '**/de-de/**/*.yml',
      '**/es/**/*.yml',
      '**/it-it/**/*.yml',
      '**/pt-br/**/*.yml',
    ],
    rules: {
      'yml/no-irregular-whitespace': 'off',
    },
  },
  {
    files: ['**/*.vue'],
    ignores: ['node_modules/', 'dist/', 'assets/icons/**.vue'],
    languageOptions: {
      parser: vueEslintParser,
      parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
        extraFileExtensions: ['.vue'],
      },
    },
    plugins: {
      'dex-data-properties': dataPropertiesPlugin,
    },
    rules: {
      'dex-data-properties/require-data-ga-attributes': 'error',
    },
  },
);
