<script setup lang="ts">
import { computed } from 'vue';
import type { BaseCopy, BaseImage, BaseLink, BaseVideo, BaseIcon } from '@/types/base';

interface ButtonProps extends BaseLink {
  config: BaseLink['config'] & {
    variant?: string;
  };
}

interface IconProps extends BaseIcon {
  config: BaseIcon['config'] & {
    variant?: string;
    color?: string;
  };
}

interface ImageProps extends BaseImage {
  imageMobile: BaseImage;
  caption: string;
  config: BaseImage['config'] & {
    verticalCentered: string;
    svgFilterStyle: string;
  };
}

interface CopyMedia extends BaseCopy {
  button?: ButtonProps;
  secondaryButton?: ButtonProps;
  icon?: IconProps;
  image?: ImageProps;
  video?: BaseVideo;
  config?: {
    hideHorizontalRule?: boolean;
    headerIntoSeperateColumn?: boolean;
    metadataId?: string;
    invertColumns?: boolean;
  };
}

const props = defineProps<CopyMedia>();

const containsMedia = computed(() => {
  return !!props.image || !!props.video;
});

const orderedSlots = computed(() => {
  const slots = containsMedia.value ? ['type', 'media'] : ['type'];
  return props.config?.invertColumns ? slots : slots.slice().reverse();
});

if (props.config?.headerIntoSeperateColumn && containsMedia.value) {
  throw createError({
    statusCode: 500,
    statusMessage: 'Invalid configuration: `headerIntoSeperateColumn` and `containsMedia` should not both be true.',
  });
}
</script>

<template>
  <section class="slp-mb-64">
    <span
      v-if="config?.metadataId"
      :id="config?.metadataId"
      class="slp-position-absolute"
      :data-slippers-scroll="config?.metadataId"
    >
    </span>
    <SlpContainer>
      <SlpColumn v-if="config?.headerIntoSeperateColumn" class="copy">
        <SlpTypography
          v-if="header && config?.headerIntoSeperateColumn"
          tag="h2"
          variant="heading3-bold"
          class="header slp-mb-16"
        >
          {{ header }}
        </SlpTypography>
      </SlpColumn>
      <template v-for="(slotName, index) in orderedSlots" :key="index">
        <template v-if="slotName === 'type'">
          <SlpColumn class="copy">
            <SlpTypography
              v-if="header && !config?.headerIntoSeperateColumn"
              tag="h2"
              variant="heading3-bold"
              class="header slp-mb-16"
            >
              {{ header }}
            </SlpTypography>
            <SlpTypography v-if="subTitle" tag="h4" variant="heading4">{{ subTitle }} </SlpTypography>
            <div v-if="text" class="copy copy__body">
              <div class="slp-mb-32" v-html="$md.render(text)" />
            </div>
            <SlpButton
              v-if="button"
              class="slp-mr-8 button"
              :href="button.config.href"
              :data-ga-name="button.config.dataGaName"
              :data-ga-location="button.config.dataGaLocation"
              :variant="button.config.variant"
            >
              {{ button.text }}
            </SlpButton>
            <SlpButton
              v-if="secondaryButton"
              class="slp-mr-8 button"
              :href="secondaryButton.config.href"
              :data-ga-name="secondaryButton.config.dataGaName"
              :data-ga-location="secondaryButton.config.dataGaLocation"
              :variant="secondaryButton.config.variant"
            >
              {{ secondaryButton.text }}
            </SlpButton>
          </SlpColumn>
        </template>
        <template v-else-if="slotName === 'media'">
          <SlpColumn v-if="containsMedia">
            <CommonVideo v-if="video" :video-src="video.config.url" :title="header" autoplay />
            <figure v-else-if="image" :class="{ centered: image.config.verticalCentered }">
              <picture>
                <source media="(min-width: 768px)" :srcset="image.config.url" />
                <source
                  media="(max-width: 767px)"
                  :srcset="image.imageMobile ? image.imageMobile.config.url : image.config.url"
                />
                <img
                  class="media__image"
                  :src="image.config.url"
                  :alt="image.altText || ''"
                  :style="{ filter: image.config.svgFilterStyle }"
                  loading="lazy"
                />
              </picture>
              <figcaption v-if="image.caption" v-html="$md.render(image.caption)"></figcaption>
            </figure>
          </SlpColumn>
        </template>
      </template>
      <CommonHorizontalRule v-if="!config?.hideHorizontalRule" class="slp-hr--copy-media" />
    </SlpContainer>
  </section>
</template>
<style lang="scss" scoped>
.header {
  display: flex;
  align-items: center;
  gap: 16px;
}

.slp-hr--copy-media {
  margin: 64px 0;
  @media (min-width: $breakpoint-lg) {
    width: 88%;
  }
}

.copy {
  &:deep(h3) {
    margin-bottom: $spacing-8;
  }

  &:deep(h5) {
    font-size: calc($text-heading-5 - 0.25rem);
  }

  &:deep(h4) {
    font-size: $text-heading-5;
  }

  &:deep(h4),
  &:deep(h5) {
    margin-top: $spacing-32;
    margin-bottom: $spacing-16;
  }

  &:deep(blockquote) {
    margin-left: $spacing-32;
  }

  h4 {
    color: $color-text-300;
    font-size: calc($text-heading-5-mobile - 0.1875rem);
    font-weight: 400;
    line-height: 24px;
    margin-bottom: 16px;

    @media (min-width: $breakpoint-lg) {
      font-size: calc($text-heading-5 + 0.0625rem);
    }
  }

  &__body {
    grid-column: 1 / -1;
    color: $color-text-300;

    &:deep(p) {
      margin-bottom: $spacing-16;
      font-size: $text-body-2;
    }

    &:deep(li) {
      padding-bottom: $spacing-8;
      font-size: $text-body-2;
    }

    &:deep(ul) {
      list-style: initial;
      padding: initial;
    }

    & :deep(p a) {
      color: $color-text-link-100;
    }
  }
}

.media__image {
  display: block;
  width: 100%;

  @media (max-width: $breakpoint-md) {
    padding-bottom: $spacing-32;
    padding-top: $spacing-32;
  }
}

.image-caption {
  font-style: italic;
}

video {
  display: block;
  margin: 0 auto;
  max-width: 100%;
  border: 1px solid #f2f2f2;
  height: 315px;
  width: 560px;
}

.button {
  color: #7759c2;
  margin-left: -$spacing-16;
}
</style>
