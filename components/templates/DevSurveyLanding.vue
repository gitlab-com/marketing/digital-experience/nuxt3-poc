<script lang="ts" setup>
import { ExternalLink } from 'slippers-ui/icons';
import { ref } from 'vue';
import { useAOS } from '@/composables/useAOS';
const { locale } = useI18n();
const { cookieValue, setCookieValue } = useGetCookieValue('2024-gitlab-devsecops-survey');

const props = defineProps({
  content: {
    type: Object,
    required: true,
  },
});

const basePath = `shared/${locale.value}/developer-survey/2024/data/`;
const gated = ref(true);

useAOS();

await Promise.all(
  props.content.sections.map(async (section) => {
    await Promise.all(
      section.subsections.map(async (subsection) => {
        if (subsection.config.resultsFile) {
          const resultsFilePath = `${basePath}${subsection.config.resultsFile}`;
          const resultsData = await queryContent(resultsFilePath).findOne();

          subsection.data = resultsData.data;
        }
      }),
    );
  }),
);

onMounted(() => {
  if (cookieValue.value) {
    gated.value = false;
  }
});

const onFormSubmit = () => {
  gated.value = false;
  setCookieValue('2024-gitlab-devsecops-survey', true);
  if (window.innerWidth <= 768) {
    // This is needed to help with the content shift on mobile once the form is filled out
    const firstH3 = document.querySelector('h3');
    if (firstH3) {
      firstH3.scrollIntoView({ behavior: 'smooth' });
    }
  }
};
</script>

<template>
  <div class="developer-survey">
    <Devsurvey2024Intro :hero="content.hero" :intro="content.intro" />
    <div v-if="gated" class="gate">
      <Devsurvey2024Section tabindex="-1" :data="content.sections[0]" style="margin-bottom: -100px" />
      <section class="form-container">
        <SlpContainer>
          <SlpRow>
            <SlpColumn :cols="6" class="text-wrapper">
              <SlpTypography variant="body3-bold" tag="p" class="slp-mb-8">
                {{ content.form.subtext }}
              </SlpTypography>
              <SlpTypography variant="heading2-bold" tag="h2" class="slp-mb-8">
                {{ content.form.header }}
              </SlpTypography>
              <SlpTypography variant="body2" tag="div" class="text-block">
                <span v-html="$md.render(content.form.text)" />
              </SlpTypography>
            </SlpColumn>
            <SlpColumn :cols="6" class="form-wrapper">
              <div :class="`form-wrapper__inner ${locale == 'en-us' ? 'english' : ''}`">
                <CommonMktoForm v-bind="{ config: content.form.config }" @submit="onFormSubmit" />
                <CommonOneTrustWarning />
              </div>
            </SlpColumn>
          </SlpRow>
        </SlpContainer>
      </section>
    </div>
    <div v-else>
      <Devsurvey2024Section
        v-for="section in content.sections"
        :key="section.title"
        :data="section"
        :inverse-layout="section.config.inverse"
      />
      <section v-if="content.allReports" class="previous-links">
        <SlpContainer>
          <SlpTypography variant="heading3-bold" tag="h2" class="slp-mb-32">
            {{ content.allReports.header }}
          </SlpTypography>
          <div class="links">
            <a
              v-for="link in content.allReports.reports"
              :key="link.label"
              :data-ga-name="`${link.label} report`"
              data-ga-location="body"
              :href="link.config.href"
              class="link-button"
            >
              <div class="link">
                <SlpIcon :icon="ExternalLink" size="lg" hex-color="#232150" />
                <SlpTypography class="link-text" variant="heading5-bold">{{ link.label }}</SlpTypography>
              </div>
            </a>
          </div>
        </SlpContainer>
      </section>
    </div>
  </div>
</template>

<style lang="scss" scoped>
$primary-survey-text-color: #232150;
$checkmark-svg: 'data:image/svg+xml,%3Csvg%20width%3D%2220%22%20height%3D%2220%22%20viewBox%3D%220%200%2020%2020%22%20fill%3D%22none%22%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%3E%3Crect%20width%3D%2220%22%20height%3D%2220%22%20rx%3D%2210%22%20fill%3D%22%236FDAC9%22/%3E%3Cpath%20d%3D%22M4.5%2010.325L7.95%2013.775L15.5%206.22498%22%20stroke%3D%22%232F2A6B%22%20stroke-width%3D%222.2%22%20stroke-miterlimit%3D%2210%22/%3E%3C/svg%3E';

.developer-survey {
  overflow: clip;
  background: #f2f1f5;

  &:deep(.slp-container) {
    position: relative;
  }
}

.form-wrapper {
  z-index: 5;
  color: $primary-survey-text-color;

  &_container {
    background: $color-surface-800 !important;
    border: none !important;
    padding: 0 !important;
  }

  &__inner {
    background: $color-surface-800;
    padding: 24px;
    border-radius: 8px;
    box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.1);

    h2 {
      font-family: 'GitLab Mono';
      font-size: 14px;
      line-height: 22px;
    }

    :deep(details) {
      text-align: center;
    }

    // Override MktoForm component styles
    :deep(ul),
    :deep(ol) {
      padding-left: 1.5em;
    }

    :deep(li) {
      list-style-type: disc;
      margin-bottom: 8px;
    }
    &.english {
      &:deep(.mktoForm) {
        .mktoButtonWrap {
          button.mktoButton,
          .mktoButtonRow .mktoButtonWrap button {
            color: #171321 !important;
            font-size: 0 !important;
            position: relative;
            min-height: 52px;
            min-width: 140px;

            &:before {
              content: 'Unlock the full report below' !important;
              font-size: 18px;
              color: #fff !important;
              position: absolute;
              width: 100%;
              height: 100%;
              top: 30%;
              right: 0%;
            }

            &:hover:before {
              color: #171321 !important;
            }
          }
        }
      }
    }

    &:deep(.mktoForm) {
      @media (min-width: $breakpoint-lg) {
        display: grid;
        grid-template-columns: 1fr 1fr;
        column-gap: 30px;

        .mktoFormRow:nth-of-type(11) {
          grid-column: 1 / 3;
        }

        .mktoButtonRow {
          margin: 24px 0;
          grid-column: 1 / 3;
        }
      }

      .mktoRequiredField label.mktoLabel,
      .mktoFieldWrap label {
        margin-bottom: 4px;
        color: $primary-survey-text-color !important;
      }

      #LblCountry {
        margin-top: 0 !important;
      }
    }
  }
}

.gate {
  position: relative;

  &:deep(.section) {
    position: relative;

    &:before {
      content: '';
      position: absolute;
      bottom: 68px;
      left: 0;
      right: 0;
      height: 300px;
      background: linear-gradient(to bottom, rgba(255, 255, 255, 0), #f8f8f8 100%);
      z-index: 10;

      @media (max-width: $breakpoint-md) {
        bottom: 60px;
      }
    }
    .subsection {
      display: none;
    }
  }
}

.text-wrapper {
  padding-right: 64px;

  @media (max-width: $breakpoint-md) {
    padding-right: 0;
  }
}

.form-container {
  color: $primary-survey-text-color !important;
  width: 100%;
  position: relative;
  padding: 96px 0;
  background: linear-gradient(179deg, #f8f8f8 0.88%, #d7fefe 99.07%);

  &:before {
    @media (min-width: $breakpoint-lg) {
      content: '';
      background: url('/images/developer-survey/2024/2024-survey-hero-bg-pills.png');
      background-size: contain;
      background-repeat: no-repeat;
      position: absolute;
      bottom: 0;
      left: 20%;
      z-index: 3;
      width: 3000px;
      height: 600px;
    }
  }

  @media (max-width: $breakpoint-md) {
    padding: 64px 0 96px 0;
  }

  .text-block {
    :deep(p) {
      margin-bottom: 16px;
    }

    :deep(strong) {
      font-weight: $font-weight-bold;
    }

    :deep(li) {
      margin-bottom: 8px;
      padding-left: 30px;
      position: relative;
    }

    :deep(li)::before {
      content: url($checkmark-svg);
      position: absolute;
      left: 0;
      top: 0;
      width: 20px;
      height: 20px;
      z-index: 5;
    }
  }
}

.previous-links {
  background: $color-surface-100;
  padding: 96px 0;

  .links {
    display: flex;
    gap: 24px;
    flex-wrap: wrap;

    .link-button {
      border-radius: 4px;
      border: 1px solid #232150;
      display: block;
      padding: 0;
      background: $color-surface-50;
      transition: all 0.2s ease-in-out;

      @media (max-width: $breakpoint-md) {
        flex: 1 0 150px;
      }

      .link {
        padding: $spacing-16 $spacing-32;
        display: flex;
        justify-content: center;
        color: #232150;
      }
      span {
        margin-left: $spacing-16;
        margin-top: 5px;
      }

      &:hover {
        background: #232150;

        .link {
          color: #fdf1dd;
        }
      }
    }
  }
}
</style>
