<script lang="ts" setup>
import { ref, computed, onMounted, onBeforeUnmount } from 'vue';
import { useI18n } from 'vue-i18n';

interface Filter {
  title: string;
  config?: {
    isOpen?: boolean;
  };
  items: [{ name: string }];
}

interface ResultsRadio {
  header: string;
  filters: [Filter];
  config?: {
    variant2?: boolean;
  };
  data: Record<string, { yoyLabel?: string; results: { item: { label: string; percentage: number; yoy?: number } }[] }>;
}

const props = defineProps({
  data: {
    type: Object as () => ResultsRadio,
    required: true,
  },
});

const convertToSnakeCase = (text: string): string => {
  return text
    .toLowerCase()
    .replace(/[\u{1F1E6}-\u{1F1FF}]\s?/gu, '') // Removes flag emojis and the space after them
    .replace(/[\u{1F30D}-\u{1F30F}]\s?/gu, '') // Removes globe/world emojis and the space after them
    .replace(/[’!@#$%^&*()_+={}:;"'<>,.?|~`[\]]/g, '') // Removes special characters
    .replace(/[-/]/g, '_') // Replaces hyphens and slashes with underscores
    .replace(/\s+/g, '_'); // Replaces spaces with underscores
};

const selectedFilter = ref<string | null>(convertToSnakeCase(props.data.filters[0].items[0].name));
const allResults = ref(null);
const openAccordion = ref<string | null>(null);
const observer = ref<IntersectionObserver | null>(null);
const isIntersecting = ref<boolean>(false);
const initialAnimation = ref<boolean>(false);
const isFilterChange = ref<boolean>(false);
const isNotEnglish = ref<boolean | null>(null);

const { locale } = useI18n();

if (locale.value.toLowerCase() === 'de-de' || locale.value.toLowerCase() == 'fr-fr') {
  isNotEnglish.value = true;
}

const toggleAccordion = (id: string) => {
  openAccordion.value = openAccordion.value === id ? null : id;
};

const getFilteredResults = (data: ResultsRadio, selectedFilter: string | null) => {
  if (selectedFilter) {
    const filterKey = selectedFilter;
    const filteredData = data.data[filterKey];
    return filteredData ? filteredData : { results: [] };
  }
  return { results: [] };
};

const filteredResults = computed(() => {
  return getFilteredResults(props.data, selectedFilter.value);
});

const setUpObserver = () => {
  if (observer.value) {
    observer.value.disconnect();
  }
  const resultsElement = allResults.value;

  if (!resultsElement) {
    return;
  }

  observer.value = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          isIntersecting.value = true;
          isFilterChange.value = false; // Reset flag on intersection
          animateCounters();
        } else {
          isIntersecting.value = false;
        }
      });
    },
    {
      root: null,
      rootMargin: '0px',
      threshold: 0.1,
    },
  );

  observer.value.observe(resultsElement);
};

const animateCounters = () => {
  if (!isIntersecting.value && !isFilterChange.value) {
    return;
  }

  filteredResults.value.results.forEach((result) => {
    const number = document.querySelector(`[data-ref="result-${toKebabCase(result.item.label)}"]`) as HTMLElement;
    const bar = document.querySelector(`[data-ref="${toKebabCase(result.item.label)}-bar"]`) as HTMLElement;
    if (number && bar) {
      animateCounter(number, result.item.percentage);
      animateBar(bar, result.item.percentage);
      initialAnimation.value = true;
    }
  });
};

const animateBar = (element: HTMLElement, percentage: number) => {
  if (window.matchMedia('(prefers-reduced-motion: reduce)').matches) {
    element.style.width = `${percentage}%`;
    initialAnimation.value = true;
    return;
  }
  const currentWidth = 0;
  const targetWidth = percentage;

  const widthDifference = targetWidth - currentWidth;
  const increment = widthDifference / 45;
  let current = currentWidth;

  const interval = setInterval(() => {
    current += increment;
    if ((increment > 0 && current >= targetWidth) || (increment < 0 && current <= targetWidth)) {
      current = targetWidth;
      clearInterval(interval);
    }
    element.style.width = `${Math.round(current)}%`;
  }, 10);
};

const animateCounter = (element: HTMLElement, percentage: number) => {
  if (isFilterChange.value || window.matchMedia('(prefers-reduced-motion: reduce)').matches) {
    element.innerText = `${percentage}${isNotEnglish.value ? ' ' : ''}%`;
    initialAnimation.value = true;
    return;
  }

  let current = 0;
  const increment = percentage / 65;

  const interval = setInterval(() => {
    current += increment;
    if (current >= percentage) {
      current = percentage;
      clearInterval(interval);
    }
    element.innerText = `${Math.round(current)}${isNotEnglish.value ? ' ' : ''}%`;
  }, 20);
};

onMounted(() => {
  setUpObserver();
});

onBeforeUnmount(() => {
  if (observer.value) {
    observer.value.disconnect();
  }
});
</script>

<template>
  <div class="survey-results-radio">
    <SlpTypography tag="h4" variant="heading5-bold" class="slp-mb-24">
      {{ data.header }}
    </SlpTypography>
    <div v-for="filter in data.filters" :key="filter.title" class="accordion-container">
      <CommonAccordion
        :data="{
          config: {
            id: `${data.header}-${filter.title}`,
            analytics: {
              dataGaName: filter.title.toLowerCase(),
              dataGaLocation: data.header.toLowerCase(),
            },
          },
        }"
        :is-open="openAccordion === `${data.header}-${filter.title}`"
        @toggle-controller="toggleAccordion(`${data.header}-${filter.title}`)"
      >
        <template #header>
          <div class="filter-accordion-title">
            {{ filter.title }}
          </div>
        </template>
        <template #content>
          <div class="filter-accordion-content" :class="filter.items.length <= 6 ? 'nowrap' : ''">
            <div v-for="item in filter.items" :key="item.name" class="filter">
              <input
                :id="`${data.header}-${item.name}`"
                v-model="selectedFilter"
                type="radio"
                :value="convertToSnakeCase(item.name)"
                :data-ga-name="`${convertToSnakeCase(item.name)} - ${filter.title.toLowerCase()}`"
                :data-ga-location="data.header.toLowerCase()"
              />
              <label
                :for="`${data.header}-${item.name}`"
                :data-ga-name="`${convertToSnakeCase(item.name)} - ${filter.title.toLowerCase()}`"
                :data-ga-location="data.header.toLowerCase()"
                >{{ item.name }}</label
              >
            </div>
          </div>
        </template>
      </CommonAccordion>
    </div>

    <div ref="allResults" class="all-results slp-mt-24">
      <div
        v-for="result in filteredResults.results"
        :key="result.item.label"
        class="result-bar"
        :class="data.config?.variant2 ? 'variant-two ' : ''"
      >
        <div class="result-bar__container">
          <div
            :data-ref="`${toKebabCase(result.item.label)}-bar`"
            class="result-bar__bar"
            :style="{
              width: !initialAnimation ? '0%' : result.item.percentage + '%',
            }"
          ></div>
        </div>
        <div class="result-bar__content">
          <div class="result-bar__label">{{ result.item.label }}</div>
          <div class="result-bar__percentage">
            <span :data-ref="`result-${toKebabCase(result.item.label)}`" class="result-bar__percent">
              {{ result.item.percentage }}{{ isNotEnglish && ' ' }}%
            </span>
            <span v-if="result.item.yoy" class="result-bar__yoy">
              {{ result.item.yoy }}{{ isNotEnglish && ' ' }}%
              {{ filteredResults.yoyLabel }}
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</template>

<style lang="scss" scoped>
.survey-results-radio {
  .accordion-container {
    .accordion {
      border-bottom: 1px solid #232150;

      &:deep(button) {
        color: #232150 !important;
        @media (min-width: $breakpoint-md) {
          padding: 12px 0;
        }
      }
      &:deep(.slp-icon) {
        color: #232150 !important;
      }

      &:deep(.accordion__icon) {
        height: 12px;
        width: 12px;
        &:before,
        &:after {
          background-color: #232150;
          height: 12px;
          width: 1.5px;
        }
      }
    }

    &:first-of-type {
      .accordion {
        border-top: 1px solid #232150;
      }
    }
  }
}

.result-bar {
  margin-bottom: 16px;

  &.variant-two {
    .result-bar__bar {
      background-color: #fc6d26;
    }
  }

  &__container {
    background-color: $color-surface-50;
    border: 1px solid #171321;
    border-radius: 10px;
    overflow: hidden;
    padding: 4px;
    position: relative;
    height: 20px;
    width: 100%;
    display: inline-block;
    vertical-align: middle;
    margin-bottom: 8px;
  }

  &__bar {
    border-radius: 10px;
    background-color: #5943b6;
    height: 100%;
    width: 0%;
    transition:
      width 1s ease,
      background-color 1s ease;

    @media (prefers-reduced-motion: reduce) {
      transition: none;
    }
  }

  &__content {
    display: flex;
    align-items: flex-start;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: nowrap;
    width: 100%;
  }

  &__label {
    font-weight: bold;
    margin-right: 10px;
    flex-shrink: 1;
    max-width: 60%;
    font-size: 14px;
    line-height: 22px;
  }

  &__percentage {
    display: flex;
    align-items: center;
  }

  &__percent {
    padding: 4px;
  }

  &__percent,
  &__yoy {
    font-family: 'GitLab Mono', monospace;
    font-size: 14px;
    line-height: 22px;
    font-weight: $font-weight-bold;
  }

  &__yoy {
    background-color: #2f2a6b;
    color: #fdf1dd;
    padding: 2px 8px;
    border-radius: 999px;
    margin-left: 4px;
    text-align: center;
    min-width: 68px;
  }
}

.filter-accordion-title {
  color: #232150;
  font-size: 16px;
  font-weight: $font-weight-bold;
  line-height: 24px;
}

.filter-accordion-content {
  display: flex;
  padding-bottom: 12px;
  flex-wrap: wrap;

  &.nowrap {
    display: block;
  }

  .filter {
    flex: 0 0 50%;
    padding: 4px 0;
    display: flex;
    align-items: flex-start;
    justify-content: flex-start;

    input[type='radio'] {
      display: block;
      accent-color: #232150;
      color: #232150;
      margin-top: 5px;
    }

    label {
      font-size: 14px;
      line-height: 22px;
      display: block;
      color: #232150;
      font-weight: bold;
      cursor: pointer;
      margin-left: 4px;
    }
  }
}
</style>
