seo:
  title: 'GitLab Duo'
  description: "La suite de capacités assistées par l'IA qui alimente vos workflows"
config:
  enableAnimations: true
content:
  - componentName: GitlabDuoFullWidthHero
    componentContent:
      title: "Créez des logiciels plus sécurisés plus rapidement grâce à l'IA tout au long du cycle du développement logiciel"
      primaryButton:
        text: 'Essayez gratuitement'
        config:
          href: '/fr-fr/solutions/gitlab-duo-pro/sales/'
          dataGaName: 'get started'
          dataGaLocation: 'hero'
      image:
        altText: 'GitLab Duo Logo Image'
        config:
          src: '/images/logos/duo-logo.svg'
      banner:
        text: "GitLab Duo Enterprise est maintenant disponible. Créez des logiciels plus sécurisés plus rapidement grâce à l'IA tout au long du cycle du développement logiciel."
        config:
          href: '/fr-fr/solutions/gitlab-duo-pro/sales/'
          dataGaName: 'gitlab duo pill'
          dataGaLocation: 'hero'
      config:
        theme: dark
  - componentName: GitlabDuoVideoModalLoop
    componentContent:
      config:
        videoSrc: 'https://player.vimeo.com/Vidéo/945979565?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
        videoMP4: '/images/gitlab-duo/hero.mp4'
        videoWebm: '/images/gitlab-duo/hero.webm'
        thumbnailSrc: '/images/gitlab-duo/duo-thumbnail.jpg'
        theme: dark
  - componentName: CommonStickyNav
    componentContent:
      links:
        - text: 'Pourquoi GitLab Duo ?'
          config:
            href: '#benefits'
            dataGaName: 'benefits'
            dataGaLocation: 'sticky nav'
        - text: 'Fonctionnalités'
          config:
            href: '#features'
            dataGaName: 'features'
            dataGaLocation: 'sticky nav'
        - text: 'Tarifs'
          config:
            href: '#pricing'
            dataGaName: 'pricing'
            dataGaLocation: 'sticky nav'
        - text: 'FAQ'
          config:
            href: '#faq'
            dataGaName: 'faq'
            dataGaLocation: 'sticky nav'
        - text: 'Ressources'
          config:
            href: '#resources'
            dataGaName: 'resources'
            dataGaLocation: 'sticky nav'
      config:
        dark: true
  - componentName: GitlabDuoAccordionAndTextCol
    componentContent:
      textBlock:
        header: "L'IA tout au long du cycle du développement logiciel"
        content: "De la planification et du codage à la sécurisation et au déploiement, GitLab Duo est la seule solution d'IA qui assiste les développeurs à chaque étape de leur workflow."
      accordion:
        - header: 'Une IA respectueuse de la confidentialité'
          content: "Avec GitLab Duo, vous contrôlez les utilisateurs, les projets et les groupes qui peuvent utiliser des fonctionnalités d'IA. En outre, le code et les données propriétaires de votre pipeline ne sont pas utilisés pour entraîner des modèles d'IA."
          config:
            icon: AiVulnerabilityResolution
        - header: "Une amélioration de l'expérience développeur"
          content: "Offrez à vos développeurs une plateforme unique qui intègre le modèle d'IA le plus adapté pour chaque cas d'utilisation sur l'ensemble du workflow, de la compréhension du code à la correction des failles de sécurité."
          config:
            icon: ValuesThree
        - header: "Nous nous engageons en faveur d'une IA transparente"
          content: 'L''IA doit être transparente pour que les dirigeants d''entreprise et leurs équipes lui fassent confiance. Le [Centre pour la transparence de l''IA](/ai-transparency-center/){data-ga-name="ai transparency center" data-ga-location="body"} de GitLab détaille notre approche éthique et transparente des fonctionnalités alimentées par l''IA.'
          config:
            icon: Eye
      config:
        theme: dark
        id: 'benefits'
  - componentName: GitlabDuoFeatures
    componentContent:
      videoFeatures:
        - textBlock:
            header: 'Optimisez votre productivité grâce à une assistance intelligente lors du codage'
            text: "Écrivez du code sécurisé plus rapidement à l'aide des suggestions de code alimentées par l'IA, disponibles dans plus de 20 langues dans votre IDE préféré. Automatisez les tâches répétitives et accélérez vos cycles de développement"
            eyebrow:
              text: 'Code'
              config:
                icon: AiCodeSuggestions
          video:
            title: 'Code Suggestion Vidéo'
            thumbnail: '/images/gitlab-duo/code-suggestions.png'
            videoMP4: '/images/gitlab-duo/code-suggestions.mp4'
            videoWebm: '/images/gitlab-duo/code-suggestions.webm'
          config:
            reverse: true
            theme: dark
        - textBlock:
            header: "Votre assistant d'IA à chaque étape du développement"
            text: 'Bénéficiez de conseils en temps réel tout au long du cycle de développement logiciel. Générez des tests, expliquez le code et réusinez-le efficacement, et discutez directement dans votre IDE ou votre interface Web.'
            eyebrow:
              text: 'Recherche'
              config:
                icon: AiGitlabChat
          video:
            title: 'Chat Vidéo'
            thumbnail: '/images/gitlab-duo/chat.png'
            videoMP4: '/images/gitlab-duo/chat.mp4'
            videoWebm: '/images/gitlab-duo/chat.webm'
          config:
            theme: dark
        - textBlock:
            header: 'Résolvez rapidement les problèmes de votre pipeline CI/CD'
            text: "Gagnez du temps lorsque vous devez dépanner un job CI/CD en échec grâce à l'analyse des causes profondes assistée par l'IA. Profitez de suggestions de correctifs et restez concentré sur les tâches cruciales."
            eyebrow:
              text: 'Dépannage'
              config:
                icon: AiRootCauseAnalysis
          video:
            title: 'Root Cause Analysis Vidéo'
            thumbnail: '/images/gitlab-duo/root-cause-analysis.png'
            videoMP4: '/images/gitlab-duo/root-cause-analysis.mp4'
            videoWebm: '/images/gitlab-duo/root-cause-analysis.webm'
          config:
            reverse: true
            theme: dark
        - textBlock:
            header: "Protégez votre code grâce à une sécurité alimentée par l'IA"
            text: "Comprenez les vulnérabilités et corrigez-les plus efficacement. Profitez d'explications détaillées sur les failles de sécurité et de merge requests générées automatiquement pour réduire les risques de sécurité."
            eyebrow:
              text: 'Sécurisation'
              config:
                icon: AiVulnerabilityBug
          video:
            title: 'Vulnerability Vidéo'
            thumbnail: '/images/gitlab-duo/vulnerability.png'
            videoMP4: '/images/gitlab-duo/vulnerability.mp4'
            videoWebm: '/images/gitlab-duo/vulnerability.webm'
          config:
            theme: dark
        - textBlock:
            header: 'Mesurez le ROI de votre investissement en IA'
            text: "Suivez l'efficacité de l'IA en temps réel. Constatez les améliorations concrètes en matière de durée de cycle et de fréquences de déploiement, en quantifiant votre retour sur investissement."
            eyebrow:
              text: 'Mesure'
              config:
                icon: AiValueStreamForecast
          video:
            title: 'AI Vidéo'
            thumbnail: '/images/gitlab-duo/ai.png'
            videoMP4: '/images/gitlab-duo/ai.mp4'
            videoWebm: '/images/gitlab-duo/ai.webm'
          config:
            reverse: true
            theme: dark
      banner:
        header: 'GitLab nommée Leader dans le Magic Quadrant™ 2024 de Gartner® dédié aux assistants IA pour le code'
        logo:
          config:
            altText: code source image
            src: '/images/gitlab-duo/gartner-white.svg'
        image:
          config:
            altText: code source image
            src: '/images/gitlab-duo/duo-gartner.png'
        button:
          text: 'Lire le rapport'
          config:
            href: '/fr-fr/gartner-mq-ai-code-assistants/'
            dataGaName: 'Read the report'
            dataGaLocation: 'body'
      config:
        theme: dark
        id: 'features'
  - componentName: GitlabDuoCategories
    componentContent:
      header: "Fonctionnalités alimentées par l'IA tout au long du cycle du développement logiciel"
      categories:
        - name: 'Pour le développement de fonctionnalités'
          config:
            key: 'developing'
            icon: 'PackageAlt2'
          cards:
            - header: 'Chat'
              description: 'Il traite et génère du texte et du code au fil de la conversation. Il vous aide à identifier rapidement les informations utiles à partir de longs blocs de texte dans les tickets, les epics, le code et la documentation GitLab.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiGitlabChat'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo_chat/index.html'
            - header: 'Explication du code'
              description: "Cette fonctionnalité vous aide à comprendre le code en l'expliquant en langage naturel."
              ctaText: 'En savoir plus'
              config:
                icon: 'AiRootCauseAnalysis'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/index.html/#code-explanation'
            - header: 'Suggestions de code'
              description: 'Cette fonctionnalité aide les développeurs à écrire du code sécurisé plus efficacement et à accélérer les durées de cycle en prenant en charge les tâches de code répétitives et routinières.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiGitSuggestions'
                href: 'https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html'
            - header: 'GitLab Duo intégré à la CLI'
              description: 'Découvrez ou rappelez les commandes Git quand vous en avez besoin.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiGitSuggestions'
                href: 'https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/index.html/#gitlab-duo-commands'
            - header: 'Génération de tests'
              description: 'Cette fonctionnalité automatise les tâches répétitives et aide à détecter les bogues rapidement.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiTestsInMr'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/#test-generation'
        - name: 'Pour la sécurisation des applications'
          config:
            key: 'securing'
            icon: 'SecureAlt2'
          cards:
            - header: 'Explication des vulnérabilités'
              description: 'Cette fonctionnalité vous aide corriger les vulnérabilités plus efficacement, à améliorer vos compétences et à écrire du code plus sécurisé.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiVulnerabilityBug'
                href: 'https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html/#explaining-a-vulnerability'
            - header: 'Résolution des vulnérabilités'
              description: 'Cette fonctionnalité génère une merge request contenant les modifications requises pour corriger une vulnérabilité.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiVulnerabilityResolution'
                href: 'https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html/#vulnerability-resolution'
        - name: 'Pour une collaboration facilitée'
          config:
            key: 'facilitating'
            icon: 'PlanAlt2'
          cards:
            - header: "Tableau de bord d'impact de l'IA"
              description: 'Constatez les améliorations en temps réel en matière de durée de cycle et de fréquences de déploiement'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiValueStreamForecast'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/#ai-impact-dashboard'
            - header: 'Résumé des discussions'
              description: "Cette fonctionnalité aide à diffuser le contenu de longues conversations pour vous assurer que tout le monde est sur la même longueur d'onde."
              ctaText: 'En savoir plus'
              config:
                icon: 'AiIssue'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/index.html/#discussion-summary'
            - header: 'Résumé de revue de code'
              description: 'Cette fonctionnalité facilite le transfert des merge requests entre les auteurs et les relecteurs, en aidant ces derniers à comprendre efficacement les suggestions.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiSummarizeMrReview'
                href: 'https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html/#summarize-a-code-review'
            - header: 'Résumé des merge requests'
              description: "Cette fonctionnalité communique efficacement l'impact des modifications de votre merge request."
              ctaText: 'En savoir plus'
              config:
                icon: 'AiMergeRequest'
                href: 'https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html'
            - header: 'Génération de descriptions de tickets'
              description: 'Cette fonctionnalité génère des descriptions de ticket.'
              ctaText: 'En savoir plus'
              config:
                icon: 'AiGenerateIssueDescription'
                href: 'https://docs.gitlab.com/ee/user/project/merge_requests/duo_in_merge_requests.html/#summarize-a-code-review'
        - name: 'Pour le dépannage avancé'
          config:
            key: 'troubleshooting'
            icon: 'MonitorAlt2'
          cards:
            - header: 'Analyse des causes profondes'
              description: "Cette fonctionnalité vous aide à déterminer la cause profonde d'un échec de pipeline et d'une compilation CI/CD qui a échoué."
              ctaText: 'En savoir plus'
              config:
                icon: 'AiRootCauseAnalysis'
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/index.html/#root-cause-analysis'
      disclaimer: "Une licence GitLab Ultimate permet de tester certaines fonctionnalités à titre expérimental ou en version bêta, conformément à l'[Accord de test GitLab](https://handbook.gitlab.com/handbook/legal/testing-agreement/). Lorsqu'une fonctionnalité d'IA passe du statut de version bêta à la disponibilité générale, les clients disposant d'une licence GitLab Premium ou GitLab Ultimate peuvent continuer à utiliser les fonctionnalités de GitLab Duo Pro en achetant le module d'extension GitLab Duo Enterprise."
  - componentName: GitlabDuoVideoCard
    componentContent:
      header: 'Découvrez GitLab Duo Workflow'
      eyebrow: 'Et après ?'
      text: "La prochaine génération de développement piloté par l'IA. GitLab Workflow est un agent intelligent, toujours disponible, qui surveille, optimise et sécurise les projets de manière autonome, permettant ainsi aux développeurs de se concentrer sur l'innovation."
      link:
        text: "Lire l'article de blog"
        config:
          href: '/fr-fr/blog/2024/06/27/meet-gitlab-duo-workflow-the-future-of-ai-driven-development/'
          dataGaName: 'gitlab duo workflow blog post'
          dataGaLocation: 'body'
      video:
        config:
          src: 'https://player.vimeo.com/video/1059060959'
          thumbnail: '/images/gitlab-duo/duo-workflow.jpg'
      config:
        theme: dark
  - componentName: CommonPricingCardsGrid
    componentContent:
      config:
        id: 'pricing'
        theme: 'dark'
  - componentName: GitlabDuoFaq
    componentContent:
      header: 'Frequently Asked Questions'
      showAll: Show All
      hideAll: Hide All
      accordion:
        - header: 'Quels sont les langages de programmation pris en charge dans les suggestions de code ?'
          content: 'Les meilleurs résultats des suggestions de code sont attendus pour les langages que les [API Vertex AI Codey Google](https://cloud.google.com/vertex-ai/generative-ai/docs/code/code-models-overview#supported_coding_languages) prennent directement en charge : C++, C#, Go, Google SQL, Java, JavaScript, Kotlin, PHP, Python, Ruby, Rust, Scala, Swift et TypeScript. '
          config:
            darkMode: true
        - header: 'Quels modèles de langage GitLab Duo utilise-t-il ?'
          content: "Notre couche d'abstraction nous permet d'alimenter les fonctionnalités d'IA en utilisant le modèle adapté au cas d'utilisation. Explorez les modèles de langage utilisés pour chaque fonctionnalité GitLab Duo [ici.](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/supported_extensions.html)"
          config:
            darkMode: true
        - header: "Mon code sera-t-il utilisé pour entraîner des modèles d'IA ?"
          content: 'GitLab ne forme pas de modèles d''IA générative basés sur des données privées (non publiques). Les fournisseurs avec lesquels nous travaillons n''utilisent pas non plus de données privées pour entraîner les modèles d''IA. [En savoir plus ici](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#training-data){data-ga-name="learn more about ai training models" data-ga-location="faq"}.'
          config:
            darkMode: true
        - header: 'GitLab Duo est-il un noyau ouvert ?'
          content: 'Oui. Le modèle commercial à noyau ouvert de GitLab nous permet de créer en collaboration avec notre clientèle, qui peut apporter de nouvelles capacités à notre produit.'
          config:
            darkMode: true
        - header: 'Comment utiliser la production générée par GitLab Duo ?'
          content: 'La production générée par GitLab Duo peut être utilisée à votre discrétion. Si une réclamation de tiers découle de votre utilisation de la production générée par GitLab Duo, GitLab interviendra et vous défendra.'
          config:
            darkMode: true
        - header: 'Comment les clients contrôlent-ils les utilisateurs qui peuvent accéder aux fonctionnalités de GitLab Duo Pro ?'
          content: 'Les contrôles au niveau de l''organisation permettent aux administrateurs d''attribuer des sièges de licence à des utilisateurs spécifiques dans une nouvelle UI d''administration. Les suggestions de code peuvent être activées ou désactivées par les utilisateurs directement dans les paramètres de l''extension IDE. Les fonctionnalités d''IA expérimentales/bêta, telles que GitLab Chat, sont contrôlées par un paramètre de groupe principal. [En savoir plus ici](https://docs.gitlab.com/ee/user/gitlab_duo/index.html#experimental-features){data-ga-name="learn more about experiemental ai features" data-ga-location="faq"}'
          config:
            darkMode: true
        - header: 'GitLab Duo Pro est-il disponible sur les instances GitLab non connectées/à connectivité limitée ?'
          content: 'Non. Pour accéder aux suggestions de code et au chat, les clients autogérés nécessitent une connectivité Internet et une licence cloud.'
          config:
            darkMode: true
        - header: "Les prompts fournies dans un commentaire (dans le fichier source) peuvent-elles être dans une langue autre que l'anglais ?"
          content: "Nous ne limitons pas intentionnellement le langage naturel, mais nous nous attendons à ce que l'anglais fournisse les meilleurs résultats. Des expérimentations ont montré que rédiger les prompts dans une langue autre que l'anglais fournit des suggestions de code adaptées dans cette langue (qui respectent la syntaxe de la langue en question et les mots-clés réservés)."
          config:
            darkMode: true
        - header: 'Comment les suggestions de code assurent-elles la protection de la propriété intellectuelle et la confidentialité des données ?'
          content: 'Pour tout savoir sur la protection de la propriété intellectuelle et la confidentialité des données dans les suggestions de code, cliquez [ici](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html#data-privacy){data-ga-name="data privacy and ip protection" data-ga-location="faq"}. L''utilisation des suggestions de code et de GitLab Chat est régie par l''[Accord de test GitLab](https://about.gitlab.com/handbook/legal/testing-agreement/){data-ga-name="gitlab testing agreement" data-ga-location="faq"} tant qu''elle demeure gratuite et que GitLab Chat est encore en version bêta. Pour en savoir plus sur [l''utilisation des données avec les suggestions de code, cliquez ici](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name="coe suggestions data usage" data-ga-location="faq"}. Pour [l''utilisation des données de GitLab Chat, cliquez ici.](https://docs.gitlab.com/ee/user/gitlab_duo/data_usage.html){data-ga-name="chat data usage" data-ga-location="faq"}'
          config:
            darkMode: true
      config:
        theme: dark
        id: 'faq'
  - componentName: CommonResourcesContainer
    componentContent:
      header: Learn more about GitLab Duo
      tabs:
        - name: 'Vidéos'
          items:
            - header: 'Découvrez GitLab Duo'
              type: 'Vidéo'
              image:
                altText: 'Découvrez GitLab Duo'
                config:
                  src: '/images/gitlab-duo/meet-git-lab-duo-thumbnail.png'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/855805049?title=0&byline=0&portrait=0&badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Suggestions de code'
              type: 'Vidéo'
              image:
                altText: 'Suggestions de code'
                config:
                  src: '/images/gitlab-duo/code-suggestions-thumbnail.png'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/894621401?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Duo Chat'
              type: 'Vidéo'
              image:
                altText: 'Duo Chat'
                config:
                  src: '/images/gitlab-duo/chat-thumbnail.png'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/927753737?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Résumé de revue de code'
              type: 'Vidéo'
              image:
                altText: 'Résumé de revue de code'
                config:
                  src: '/images/gitlab-duo/code-review-summary-thumbnail.png'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/929891003?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Résumé des discussions'
              type: 'Vidéo'
              image:
                altText: 'Résumé des discussions'
                config:
                  src: '/images/gitlab-duo/discussion-summary-thumbnail.jpg'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/928501915?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Explication des vulnérabilités'
              type: 'Vidéo'
              image:
                altText: 'Explication des vulnérabilités'
                config:
                  src: '/images/gitlab-duo/vulnerability-explanation-thumbnail.jpg'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/930066123?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Explication du code'
              type: 'Vidéo'
              image:
                altText: 'Explication du code'
                config:
                  src: '/images/gitlab-duo/code-explanation-thumbnail.jpg'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/930066090?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
            - header: 'Suggestion de relecteurs'
              type: 'Vidéo'
              image:
                altText: 'Suggestion de relecteurs'
                config:
                  src: '/images/gitlab-duo/suggested-reviewers-thumbnail.png'
              link:
                text: 'Regarder maintenant'
                config:
                  href: 'https://player.vimeo.com/Vidéo/930066108?badge=0&autopause=0&player_id=0&app_id=58479/'
                  dataGaName: 'Regarder maintenant'
                  dataGaLocation: 'resources'
                  modal: true
          config:
            key: 'videos'
        - name: 'Blogs'
          items:
            - header: "Comprendre et résoudre les vulnérabilités avec GitLab Duo alimenté par l'IA"
              type: 'Blog'
              config:
                icon: 'Blog'
              image:
                altText: "Comprendre et résoudre les vulnérabilités avec GitLab Duo alimenté par l'IA"
                config:
                  src: '/images/gitlab-duo/git-lab-duo-blog-image-1.png'
              link:
                config:
                  href: '/blog/2024/02/21/understand-and-resolve-vulnerabilities-with-ai-powered-gitlab-duo/'
                  dataGaName: "Comprendre et résoudre les vulnérabilités avec GitLab Duo alimenté par l'IA"
                  dataGaLocation: 'resources'
            - header: "Mesurer l'efficacité de l'IA au-delà des métriques de productivité des développeurs"
              type: 'Blog'
              config:
                icon: 'Blog'
              image:
                altText: "Mesurer l'efficacité de l'IA au-delà des métriques de productivité des développeurs"
                config:
                  src: '/images/gitlab-duo/git-lab-duo-blog-image-2.png'
              link:
                config:
                  href: '/blog/2024/02/20/measuring-ai-effectiveness-beyond-developer-productivity-metrics/'
                  dataGaName: "Mesurer l'efficacité de l'IA au-delà des métriques de productivité des développeurs"
                  dataGaLocation: 'resources'
            - header: "Un nouveau rapport sur les outils assistés par l'IA souligne les enjeux croissants pour le DevSecOps"
              type: 'Blog'
              config:
                icon: 'Blog'
              image:
                altText: "Un nouveau rapport sur les outils assistés par l'IA souligne les enjeux croissants pour le DevSecOps"
                config:
                  src: '/images/gitlab-duo/git-lab-duo-blog-image-3.png'
              link:
                config:
                  href: '/blog/2024/02/14/new-report-on-ai-assisted-tools-points-to-rising-stakes-for-devsecops/'
                  dataGaName: "Un nouveau rapport sur les outils assistés par l'IA souligne les enjeux croissants pour le DevSecOps"
                  dataGaLocation: 'resources'
          config:
            key: 'blogs'
      config:
        theme: dark
        id: 'resources'
  - componentName: CommonNextSteps
