seo:
  title: Pourquoi utiliser GitLab?
  description: 10 raisons pour lesquelles les entreprises choisissent GitLab, la plateforme DevSecOps alimentée par l'IA la plus complète proposée sous la forme d'une application unique
content:
  - componentName: WhyGitlabHero
    componentContent:
      tagline: 10 raisons pour lesquelles
      title: les entreprises choisissent GitLab
      description: |
        GitLab est la plateforme DevSecOps alimentée par l'IA la plus complète.
      secondaryButton:
        text: Essayer Ultimate gratuitement
        config:
          dataGaName: free trial
          dataGaLocation: hero
          href: 'https://gitlab.com/-/trial_registrations/new?glm_source=localhost/why-gitlab&glm_content=default-saas-trial/'
      image:
        altText: graphic of numbers incrementing from 01 to 04
        config:
          src: /images/why-gitlab/one-two-three-four.webp
  - componentName: WhyGitlabTenReasons
    componentContent:
      items:
        - navigationTitle: 'Un logiciel plus rapide'
          title: Une durée de cycle 7 fois plus courte
          description: |
            GitLab aide les entreprises à gérer les complexités croissantes du développement, de la sécurisation et du déploiement de logiciels. En réduisant la multiplication de la chaîne d'outils, les équipes sur GitLab consacrent moins de temps à la maintenance des outils, ce qui équivaut à des cycles 7 fois plus rapides, à une meilleure productivité des développeurs, à une réduction des dépenses logicielles et à un gain de temps à mettre au profit des fonctionnalités qui distinguent votre entreprise.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/carfax.png
              config:
                icon: Quote
              text: "« Nous dépensions trop de temps et d'argent dans l'achat et l'assistance de notre chaîne d'outils, qui comptait 12 outils. Nous devions minimiser la maintenance et l'assistance de la chaîne d'outils autant que possible afin que nos équipes puissent se concentrer sur la création de nouvelles fonctionnalités et pas seulement sur la prise en charge de tous ces différents outils. Indirectement, c'est un avantage pour l'ensemble de l'entreprise. C'est tout à fait ça : comment être le plus efficace possible pour proposer des fonctionnalités aux clients. »"
              source: Mark Portofe, Director of Platform Engineering, CARFAX
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/carfax/
                  dataGaName: 01 carfax
                  dataGaLocation: body
        - navigationTitle: "Alimenté par l'IA"
          title: IA intégrée tout au long du cycle de vie du développement logiciel
          description: |
            Améliorez l'efficience et réduisez les durées de cycle à l'aide de l'IA tout au long du cycle de vie du développement logiciel. GitLab Duo, nos workflows alimentés par l'IA, soutiennent les équipes à chaque étape, de la création de code et des tests de sécurité, à la documentation et à la gouvernance.
          quotes:
            - logo:
                fallbackText: 2023 Global DevSecOps Report
              config:
                icon: AiCodeSuggestions
              text: L'IA devrait aider les développeurs à accomplir toutes leurs tâches de développement et de déploiement de logiciels, et pas seulement à créer du code plus rapidement. Alors qu'un quart du temps des développeurs est consacré à la création de code, 75 % du temps est consacré à toutes les autres tâches telles que le test, la sécurisation et l'analyse des logiciels.
              source: "- L'IA dans le développement de logiciels"
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/developer-survey/
                  dataGaName: 02 developer survey
                  dataGaLocation: body
        - navigationTitle: 'IA respectueuse de la confidentialité'
          title: IA respectueuse de la confidentialité et utilisation du modèle adapté au cas d'utilisation
          description: |
            L'IA peut être complexe. Chez GitLab, notre approche est simple : votre code reste votre code, il n'est pas utilisé pour l'entraînement ou la mise au point de nos propres modèles. Les suggestions de code n'utilisent pas de code client, et comme GitLab n'est lié commercialement à aucun fournisseur de grand modèle de langage (LLM), nous utilisons le modèle adapté à votre cas d'utilisation. Choisissez facilement ce qui vous convient ou changez lorsqu'un modèle ne correspond plus à votre stratégie commerciale ou technologique.
          quotes:
            - logo:
                fallbackText: 2023 Global DevSecOps Report
              config:
                icon: Devsecops
              text: La confidentialité est importante. 95 % des cadres dirigeants interrogés déclarent que la confidentialité et la protection de la propriété intellectuelle sont importantes lors de l'évaluation d'un outil ou d'une fonctionnalité d'IA. 79 % des répondants se disent préoccupés par le fait que les outils d'IA aient accès à des informations privées ou à la propriété intellectuelle.
              source: "- L'IA dans le développement de logiciels"
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/developer-survey/
                  dataGaName: 03 developer survey
                  dataGaLocation: body
        - navigationTitle: 'Sécurité'
          title: L'automatisation et la gouvernance de la sécurité à chaque étape
          description: |
            GitLab active la sécurité à grande échelle, offrant une gouvernance à l'échelle de la plateforme pour sécuriser la chaîne d'approvisionnement logicielle. Les garde-fous automatisés de GitLab assurent la sécurité de l'environnement de production. Les fonctionnalités de sécurité de GitLab permettent aux clients de définir des stratégies et des règles granulaires qui automatisent la conformité, ce qui vous permet de sécuriser la chaîne d'approvisionnement logicielle. Notre automatisation de la sécurité permet à vos développeurs de réduire au maximum les tâches manuelles répétitives afin qu'ils puissent se concentrer sur le travail approfondi et générateur de valeur. Nos garde-fous de gouvernance assurent également à l'équipe de sécurité que les développeurs suivent les meilleures pratiques dans toute l'entreprise.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/hackerone-logo-white.svg
              config:
                icon: SpeedGauge
              text: L'équipe d'ingénieurs de HackerOne a économisé quatre à cinq heures par jour et par ingénieur en consolidant le travail précédemment consacré aux tests de déploiement.
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/hackerone/
                  dataGaName: 04 hackerone
                  dataGaLocation: body
            - logo:
                config:
                  src: /images/customer_logos/dunelm.svg
              config:
                icon: MonitorPipeline
              text: Dunelm avait besoin d'une plateforme qui puisse compiler des pipelines de manière transparente et qui intègre la sécurité dès le départ. En utilisant GitLab pour améliorer les processus de sécurité tout au long du cycle de vie du développement logiciel, Dunelm a rendu les déploiements 7 fois plus rapides.
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/dunelm/
                  dataGaName: dunelm
                  dataGaLocation: body
        - navigationTitle: 'Conformité'
          title: Conformité et auditabilité de bout en bout
          description: |
            Les outils propres à la sécurité qui ne sont pas utilisés de manière cohérente ne parviennent pas à protéger les organisations et leurs clients. La solution de gouvernance complète de GitLab impose les conditions requises à tous les projets, séparant les équipes de développeur et de sécurité/conformité. À l'aide de notre éditeur de stratégie, vous pouvez personnaliser les règles d'approbation pour répondre aux besoins de conformité de votre entreprise et réduire les risques.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/lockheed-martin-white.svg
              config:
                icon: ShieldCheckLight
              text: La conformité est un problème critique pour Lockheed Martin. L'utilisation du framework de conformité de GitLab pour imposer l'automatisation et la qualité des logiciels afin de rendre les releases et la gestion des dépendances plus efficaces a conduit à des compilations de pipelines 80 fois plus rapides, avec 90 % de temps en moins consacré à la maintenance du système.
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/lockheed-martin/
                  dataGaName: 05 lockheed martin
                  dataGaLocation: body
        - navigationTitle: 'Déploiement'
          title: Déploiement flexible
          description: |
            Les entreprises modernes ont besoin de flexibilité de déploiement, en particulier les organisations ayant des exigences complexes en matière de sécurité, de conformité et de réglementation. Choisissez parmi le SaaS sur site, multilocataire ou <a href='/fr-fr/dedicated/'>GitLab Dedicated</a>, notre solution SaaS à locataire unique entièrement gérée. Avec GitLab Dedicated, nous nous occupons de la gestion et du déploiement de votre plateforme DevSecOps, ce qui permet de réduire les coûts opérationnels tout en vous apportant le contrôle et la conformité de l'auto-hébergement. Également inclus : isolation des données complètes et du code source, résidence des données et mise en réseau privé.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/natwestgroup.svg
              config:
                icon: Quote
              text: « NatWest Group adopte GitLab Dedicated pour offrir à nos ingénieurs une plateforme d'ingénierie cloud commune, qui permet de livrer de nouveaux produits à nos clients rapidement, fréquemment et en toute sécurité grâce à des tests automatisés de haute qualité, une infrastructure à la demande et un déploiement direct. Cela améliorera considérablement la collaboration ainsi que la productivité des développeurs et libérera la créativité grâce à un "canal unique" pour le développement de logiciels. »
              source: Adam Leggett, Platform Lead - Engineering Platforms, NatWest Group
        - navigationTitle: 'Insights'
          title: Métriques de bout en bout et visibilité sur l'ensemble du cycle de livraison de logiciels
          description: |
            La clé pour créer un développement logiciel axé sur des chaînes de valeur ? Les métriques. Cette approche tire parti des personnes, des processus et de la technologie pour passer de l'idée à la valeur client avec la durée de cycle la plus rapide possible. Avec un magasin de données unifié, les équipes sur GitLab peuvent mesurer l'efficience, la productivité et d'autres métriques clés en un seul endroit. Obtenez une vue globale de tout, de l'adoption de DevOps à la productivité des développeurs, en passant par la détection des vulnérabilités, la qualité des logiciels, l'innovation et plus encore.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/chorus.svg
              config:
                icon: MonitorTest
              text: L'équipe de Chorus remercie GitLab de l'avoir aidée à améliorer son analyse du cycle des fonctionnalités. Grâce aux résultats des tests, aux examens de sécurité, aux tests de performance, au code climate et à tout ce qui figure dans les requêtes de fusion, Chorus a pu agir rapidement.
              button:
                text: En savoir plus
                config:
                  href: '/customers/chorus/'
                  dataGaName: 07 chorus
                  dataGaLocation: body
        - navigationTitle: 'Planification'
          title: Livraison Agile en entreprise intégrée
          description: |
            Les organisations ont besoin que la planification des logiciels et des produits soit intégrée au reste du cycle de développement plutôt que traitée comme une activité à part. La livraison Agile en entreprise de GitLab est intégrée à la plateforme DevSecOps que les équipes de développement, de sécurité et d'opérations utilisent pour développer et déployer des logiciels, créant ainsi une expérience plus efficace et améliorant le délai de mise sur le marché.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/iron-mountain-logo-white.svg
              config:
                icon: Quote
              description: Iron Mountain considère GitLab comme un élément important de l'architecture d'entreprise et des plateformes permettant de mettre en œuvre des méthodes Agile et contribuant à l'évolution de l'entreprise vers DevOps. Le SaaS GitLab Ultimate effectue la maintenance, afin que les développeurs puissent se concentrer sur le développement, réduisant ainsi le temps d'intégration de 20 heures par projet et réalisant des économies d'environ 150 000 $ par an.
              text: « GitLab nous a fourni les bases et la plateforme nécessaires pour mettre en place notre Scaled Agile Framework. Nous sommes désormais en mesure de collaborer au sein de nos équipes informatiques dans l'ensemble de l'entreprise et avec nos principales parties prenantes. »
              source: Hayelom Tadesse, Vice President of Enterprise Technology, Iron Mountain
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/iron-mountain/
                  dataGaName: 08 iron mountain
                  dataGaLocation: body
        - navigationTitle: 'Aucun blocage fournisseur'
          title: Une stratégie multi-cloud sans blocage fournisseur
          description: |
            Choisir une plateforme DevSecOps doit signifier choisir les clouds qui correspondent à votre stratégie commerciale et technologique. Étant donné que GitLab n'est lié commercialement à aucun fournisseur de cloud spécifique, vous pouvez réduire les risques liés à votre stratégie multi-cloud et éviter de dépendre d'un seul fournisseur.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/bendigo.svg
              config:
                icon: Quote
              description: Avec GitLab, Bendigo and Adelaide Bank a adopté la technologie cloud et a automatisé les processus manuels. Lors de la migration vers GitLab, l'équipe a déplacé 1 500 projets, plus de 30 organisations, 500 utilisateurs et 50 Go de données en moins de quatre semaines.
              text: « GitLab nous aide avec les déploiements multi-cloud. Nous pouvons déployer des runners dans n'importe quelle infrastructure, et nous les utilisons actuellement pour les déployer sur AWS et GCP. Le déploiement vers le cloud a été simple. Depuis un an que nous utilisons GitLab, nous sommes en bonne position pour atteindre notre objectif de passer au cloud. »
              source: Caio Trevisan, Head of DevOps Enablement, Bendigo and Adelaide Bank
              button:
                text: En savoir plus
                config:
                  href: /fr-fr/customers/bab/
                  dataGaName: 09 bendigo and adelaide bank
                  dataGaLocation: body
        - navigationTitle: Noyau ouvert
          title: Une plateforme DevSecOps à noyau ouvert que vous pouvez personnaliser en fonction de vos besoins
          description: |
            Meilleure collaboration, innovation plus rapide. Personnalisez votre plateforme DevSecOps avec le noyau ouvert de GitLab et créez ainsi une solution qui répond vraiment à vos besoins, tout en façonnant l'avenir de notre feuille de route et de DevSecOps.
          quotes:
            - logo:
                config:
                  src: /images/customer_logos/ubs.svg
              config:
                icon: Quote
              text: « Dans un modèle open source, chaque fois qu'il y avait un interruption, un problème ou un besoin d'assistance, nous pouvions contacter GitLab et keur demander de nous aider. Y a-t-il un moyen d'améliorer cela ? » Cette réelle valeur ajoutée est une des raisons pour lesquelles nous avons choisi GitLab. »
              source: Rick Carey, Group Chief Technology Officer, UBS
              button:
                text: En savoir plus
                config:
                  href: /blog/2021/08/04/ubs-gitlab-devops-platform/
                  dataGaName: '10 ubs '
                  dataGaLocation: body
  - componentName: CommonNextSteps
