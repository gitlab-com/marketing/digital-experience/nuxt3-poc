seo:
  title: 'Apprendre Python : tout savoir sur ce langage de programmation'
  description: >-
    Python est de plus en plus populaire, et pour de bonnes raisons. Retrouvez
    toutes les informations essentielles pour faire vos premiers pas avec
    Python.
  ogTitle: 'Apprendre Python : tout savoir sur ce langage de programmation'
  ogDescription: >-
    Python est de plus en plus populaire, et pour de bonnes raisons. Retrouvez
    toutes les informations essentielles pour faire vos premiers pas avec
    Python.
  noIndex: false
  ogImage: images/blog/hero-images/python.jpg
  ogUrl: https://about.gitlab.com/blog/beginner-guide-python-programming
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/beginner-guide-python-programming
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Apprendre Python : tout savoir sur ce langage de programmation",
            "author": [{"@type":"Person","name":"GitLab"}],
            "datePublished": "2021-10-21",
          }

content:
  title: 'Apprendre Python : tout savoir sur ce langage de programmation'
  description: >-
    Python est de plus en plus populaire, et pour de bonnes raisons. Retrouvez
    toutes les informations essentielles pour faire vos premiers pas avec
    Python.
  authors:
    - GitLab
  heroImage: images/blog/hero-images/python.jpg
  date: '2021-10-21'
  body: >
    Vous vous passionnez pour la programmation informatique et souhaitez en
    apprendre davantage sur Python ? Vous souhaitez savoir par où commencer pour
    maîtriser les bases de Python ? Découvrez dans cet article les concepts de
    base de ce langage. 


    ## Comment débuter sur Python ?


    Python est un langage de programmation puissant et facile à apprendre, à
    utiliser et à déployer, dont l'utilisation est très répandue dans la
    création d'applications web et de bureau. Python facilite également
    l'analyse de données et l'exécution de tâches
    [DevOps](https://about.gitlab.com/fr-fr/solutions/devops-platform/
    "Qu'est-ce que DevOps ? "). C'est un langage de programmation gratuit, open
    source et orienté objet, utilisé pour écrire des scripts simples, tout comme
    des programmes complexes. Parmi les près de 700 langages de programmation,
    Python se distingue comme l'un des plus accessibles pour les débutants.


    ### Installer Python


    Avant d'aborder les bases de Python, il est essentiel de télécharger et
    d'installer Python sur votre ordinateur de bureau ou votre ordinateur
    portable. Python fonctionne sur plusieurs plateformes, dont Linux, Windows
    et Mac. Il est préinstallé sur la plupart des systèmes Mac et Linux, mais il
    est préférable de télécharger la dernière version depuis le [site officiel
    de Python](https://www.python.org/downloads/ "Site officiel de Python").


    Pour vérifier la version actuelle de Python sur votre système, ouvrez la
    ligne de commande et tapez « python -V ».


    ![Invite de commande
    Python](https://about.gitlab.com/images/blogimages/python1.png){: .shadow}


    Si votre version est obsolète, téléchargez l'installation 32 ou 64 bits à
    partir du site Web, selon la configuration de votre système.


    Il existe d’autres alternatives pour télécharger le programme d’installation
    : sur Windows, vous pouvez l'installer directement à partir de Microsoft.
    Sur Linux, vous pouvez l'installer à l'aide du gestionnaire de paquets. Sur
    macOS, vous pouvez télécharger Python à partir de
    [Homebrew](https://brew.sh/ "Homebrew").


    Une fois le programme téléchargé, exécutez-le et confirmez l'installation du
    fichier. Vous pouvez dès lors commencer à utiliser Python. 


    Voici un exemple d'installation de Python pour Windows.


    ![Installation de Python pour Windows
    ](https://about.gitlab.com/images/blogimages/python2.png){: .shadow}


    ### Exécuter Python dans l'invite de commande


    Pour vérifier que Python est installé et fonctionne correctement sous
    Windows, ouvrez l'invite de commande et saisissez « python », ce qui lancera
    l'interpréteur. Vous pouvez y exécuter directement des codes Python. Par
    exemple, saisissez « 2*5+1 » et appuyez sur « Entrée ». Vous verrez « 11 »
    comme résultat. Saisissez « Quit () » pour quitter l'interpréteur.


    ![Interpréteur
    Python](https://about.gitlab.com/images/blogimages/python3.png){: .shadow}


    ### Exécuter Python dans l'IDE


    Maintenant que la dernière version de Python est installée, vous êtes prêt à
    commencer à programmer en Python. Pour écrire des scripts ou des programmes
    longs en Python, utilisez l'environnement de développement et
    d'apprentissage intégré
    ([IDLE](https://docs.python.org/fr/3/library/idle.html "IDLE Python")) de
    Python.


    Lancez l'IDLE puis, dans le menu déroulant sélectionnez « Fichier », puis «
    Nouveau fichier », ce qui ouvre une nouvelle fenêtre d'édition. Vous avez à
    présent deux fenêtres sur votre écran : un *shell* Python et un fichier sans
    titre.


    ![shell Python et fichier sans
    titre](https://about.gitlab.com/images/blogimages/python4.png){: .shadow}


    Le *shell* Python est un environnement REPL, abréviation de «
    *read-eval-print-loop* » (boucle de lecture-évaluation-affichage). Il
    exécute des extraits de code, généralement une instruction à la fois. Par
    exemple, en répétant le même calcul « 2*5+1 » que nous avons fait dans
    l'invite de commande, vous pouvez constater qu'un *shell* Python peut
    fonctionner comme une calculatrice.


    ![Débuter sur Python avec une opération de
    calcul](https://about.gitlab.com/images/blogimages/python5.png){: .shadow}


    La fenêtre sans titre est une fenêtre d'édition de texte permettant d'écrire
    des programmes complets. Le *shell* affiche son résultat. Par exemple, le
    premier programme conventionnel de Python pour les débutants consiste à
    afficher le texte « Hello World! ». Veillez à sauvegarder l'éditeur de texte
    avant de l'exécuter en appuyant sur « F5 ».


    ![Affichage du texte Hello World! sur
    Python](https://about.gitlab.com/images/blogimages/python61.png){: .shadow}


    ## Les bases de Python


    Nous savons que vous avez hâte de commencer à écrire de longs scripts pour
    des jeux et des sites Web, mais il vous reste encore un long chemin à
    parcourir pour y arriver. Comme pour l’apprentissage de tout autre langage,
    vous devez d'abord comprendre les bases de Python.


    La fonction __print()__, comme nous l’avons vu dans l'exemple « Hello World!
    », affiche une valeur dans la fenêtre de résultat. Une valeur est la chose
    la plus élémentaire qu'un programme utilise. Il peut s'agir d'une chaîne de
    caractères, d'une valeur numérique ou de tout autre objet Python. Tout objet
    entre guillemets simples ou doubles est appelé chaîne de caractères
    (string).


    Par exemple, le « Hello World! » affiché dans le programme ci-dessus est
    également une chaîne de caractères. Les valeurs numériques telles que 4 et
    4.5 sont respectivement des nombres entiers et des nombres flottants. Vous
    pouvez transformer un nombre entier ou un nombre flottant en une chaîne de
    caractères, et vice versa en utilisant les fonctions intégrées __int()__,
    __float()__ et __str()__.


    ![Image illustrant les fonctions str(), int() et float() de
    Python.](https://about.gitlab.com/images/blogimages/python7.png){: .shadow}


    ## Le vocabulaire de Python


    Python est un des langages de programmation les plus simples. Il est facile
    à lire et à comprendre. Contrairement aux langages humains, Python possède
    un vocabulaire restreint, et des mots-clés réservés ayant une signification
    particulière. Les termes ne faisant pas partie de ce vocabulaire réservé
    n'ont de sens que pour vous et sont appelés variables. Ces 35 mots-clés
    réservés sont les suivants :


    ![Les 35 mots-clés réservés à connaître pour apprendre
    Python](https://about.gitlab.com/images/blogimages/python8.png){: .shadow}


    Veillez à utiliser ces mots-clés dans leur but spécifique pour éviter
    d'induire l'interpréteur Python en erreur et de provoquer une erreur de
    syntaxe.


    ### Nommer les variables


    Vous aurez parfois besoin de stocker des valeurs dans votre code pour les
    récupérer plus tard, ce que vous pouvez faire en leur donnant des noms
    symboliques, appelés variables. Comme illustré ci-dessous, nous demandons à
    Python de stocker 5 et 6 avec les labels respectifs x et y, puis de les
    récupérer plus tard pour trouver leur somme. 


    ![Stockage des variables dans
    Python](https://about.gitlab.com/images/blogimages/python9.png){: .shadow}


    Il existe des règles pour choisir le nom d'une variable ; le non-respect de
    ces règles entraînera une erreur de syntaxe. 


    Voici quelques règles obligatoires :

    - Le nom peut contenir des lettres et des chiffres, mais il ne peut pas
    commencer par un chiffre.

    - Un tiret bas peut être utilisé dans le nom pour séparer plusieurs mots.

    - Les symboles spéciaux comme @#$ sont interdits et ne doivent pas
    apparaître dans le nom.

    - Les mots-clés Python ne doivent pas être utilisés comme noms de variables.


    ### Comprendre les opérateurs et les opérandes 


    Python utilise des symboles spéciaux appelés « opérateurs » pour représenter
    les calculs mathématiques de base. Les valeurs auxquelles ces opérateurs
    sont appliqués sont appelées opérandes. Les symboles utilisés comme
    opérateurs pour la soustraction, l'addition, la division, la multiplication
    et l'exponentiation sont respectivement -, +, /, * et **.


    ![Apprendre à coder en Python : symboles pour les
    opérateurs](https://about.gitlab.com/images/blogimages/python10.png){:
    .shadow}


    L'opérateur modulo (%) génère le reste du premier opérande divisé par le
    second opérande. Il est utile pour vérifier si un nombre est divisible par
    un autre et pour extraire le ou les chiffres les plus à droite d'un nombre.


    ![Opérateur
    modulo](https://about.gitlab.com/images/blogimages/python11.png){: .shadow}


    ### Utiliser les expressions


    Une combinaison de valeurs, de variables et d'opérateurs est appelée une
    expression. Une expression saisie dans le *shell* est évaluée et la réponse
    est affichée. Cependant, dans un script, une expression ne fait rien par
    elle-même.


    Python utilise la convention mathématique PEMDAS pour les opérateurs, ce qui
    signifie que P pour Parenthèses a la priorité la plus élevée, puis
    Exponentiation, Multiplication et Division, qui ont la même priorité.
    L'Addition et la Soustraction viennent ensuite et ont également la même
    priorité. Les opérateurs qui ont la même préférence sont également évalués
    de gauche à droite.


    ![Apprendre python :
    PEMDAS](https://about.gitlab.com/images/blogimages/python12.png){: .shadow}


    Les opérateurs Addition et Multiplication fonctionnent aussi avec les
    chaînes de caractères pour la concaténation et la répétition d'une chaîne : 


    ![Opérateurs d'addition et de
    multiplication](https://about.gitlab.com/images/blogimages/python13.png){:
    .shadow}


    Python permet également d'obtenir la valeur d'une variable de l'utilisateur
    via son clavier. Cela peut être fait en utilisant une fonction intégrée
    appelée __input__.


    ![Les bases de python : fonction
    input](https://about.gitlab.com/images/blogimages/python14.png){: .shadow}


    ## Débutez sur Python en écrivant votre premier programme


    Il est maintenant temps d'écrire votre premier programme simple en utilisant
    tout ce que vous avez appris dans cet article. Écrivez un script qui prend
    deux nombres en entrée (input) et les additionne. Faites-le de vous-même,
    puis consultez le code ci-dessous pour comparer votre travail.


    ![Écrire un premier programme sur
    Python](https://about.gitlab.com/images/blogimages/python15.png){: .shadow}


    Félicitations ! Vous venez d'écrire votre premier programme.


    Apprendre Python est facile. Nous vous avons simplement aidé à acquérir les
    notions de base nécessaires pour maîtriser les bases de Python. C’est à vous
    de jouer maintenant ! 


    Photo par <a
    href="https://unsplash.com/@davidclode?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">David
    Clode</a> sur Unsplash.
  category: DevSecOps
  tags:
    - DevOps
    - careers
    - tutorial
config:
  slug: beginner-guide-python-programming
  featured: false
  template: BlogPost
