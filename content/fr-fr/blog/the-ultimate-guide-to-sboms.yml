seo:
  title: 'SBOM (Software Bill of Materials) : notre guide complet'
  description: >-
    Découvrez ce qu'est une nomenclature logicielle et pourquoi elle fait
    désormais partie intégrante du développement logiciel. Lisez notre guide
    complet.
  ogTitle: 'SBOM (Software Bill of Materials) : notre guide complet'
  ogDescription: >-
    Découvrez ce qu'est une nomenclature logicielle et pourquoi elle fait
    désormais partie intégrante du développement logiciel. Lisez notre guide
    complet.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(8).png
  ogUrl: https://about.gitlab.com/blog/the-ultimate-guide-to-sboms
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/the-ultimate-guide-to-sboms
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "SBOM (Software Bill of Materials) : notre guide complet",
            "author": [{"@type":"Person","name":"Sandra Gittlen"}],
            "datePublished": "2022-10-25",
          }

content:
  title: 'SBOM (Software Bill of Materials) : notre guide complet'
  description: >-
    Découvrez ce qu'est une nomenclature logicielle et pourquoi elle fait
    désormais partie intégrante du développement logiciel. Lisez notre guide
    complet.
  authors:
    - Sandra Gittlen
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(8).png
  date: '2022-10-25'
  body: >
    Une nomenclature logicielle (ou SBOM) est une liste complète des composants
    d’un logiciel qui facilite la compréhension du réseau complexe de
    bibliothèques, d'outils et de processus utilisés tout au long du cycle de
    développement. Associées à des outils de gestion des vulnérabilités, les
    SBOM ne se contentent pas de révéler les vulnérabilités potentielles des
    logiciels, mais ouvrent également la voie à une stratégie d'atténuation des
    risques. 


    Découvrez dans ce guide tout ce que vous devez savoir sur les SBOM, ainsi
    que le rôle central qu'elles jouent dans une stratégie
    [DevSecOps](https://about.gitlab.com/fr-fr/topics/devsecops/ "Qu'est-ce que
    DevSecOps ?") multifacette.   


    ## Qu'est-ce qu'une SBOM ?


    Une SBOM est un inventaire imbriqué ou une [liste des ingrédients qui
    constituent les composants logiciels](https://www.cisa.gov/sbom# "Définition
    d'une SBOM"). Les SBOM comprennent des informations critiques sur les
    bibliothèques, outils et processus utilisés pour développer, construire et
    déployer un artefact logiciel. 


    Bien que les SBOM soient souvent créées avec des logiciels autonomes, des
    plateformes DevSecOps comme GitLab intègrent la génération de SBOM dans un
    workflow DevSecOps.


    ![Sécurité de la chaîne d'approvisionnement
    SDLC](//images.ctfassets.net/r9o86ar0p03f/7fXeaSh0rB5qBTA1XIhnm9/558eed7ce3711b0eeedf5e2e1e0f97bf/supply_chain_security_sdlc.png)


    ### Pourquoi les SBOM sont-elles importantes ?


    Aujourd'hui, le secteur du développement logiciel a tendance à prioriser
    rapidité et efficacité pour la livraison de logiciels. Cependant, cela
    induit parfois des risques de sécurité via l'intégration de code moins
    sécurisé provenant de dépôts open source ou de paquets propriétaires.


    Selon le rapport « Open Source Security and Risk Analysis » de Synopsys
    (2024), qui a analysé plus de 1 000 bases de code commerciales issues de 17
    industries différentes en 2023, 96 % de ces bases contenaient du code open
    source et 84 % de celles évaluées pour les risques contenaient des
    vulnérabilités.


    À la lecture de ce rapport, il devient clair qu'intégrer du code provenant
    de dépôts inconnus augmente le risque de failles exploitables par les
    hackers.


    Un exemple connu, l'[attaque de
    SolarWinds](https://www.techtarget.com/whatis/feature/SolarWinds-hack-explained-Everything-you-need-to-know
    "Attaque de SolarWinds") en 2020, a été déclenchée par l'activation d'une
    injection malveillante de code dans un paquet utilisé par le produit Orion
    de SolarWinds. Cette dernière n'a pas été sans conséquences puisqu'elle a
    fortement affecté les clients de l’ensemble de la chaîne d'approvisionnement
    logicielle.


    Cette attaque, comme bien d'autres (par exemple, la vulnérabilité logj4) ont
    mis en lumière l'importance d'effectuer une analyse approfondie des
    dépendances des applications, y compris des conteneurs et de
    l'infrastructure, afin d'évaluer les [risques potentiels tout au long de la
    chaîne d'approvisionnement
    logicielle](https://about.gitlab.com/blog/2022/08/30/the-ultimate-guide-to-software-supply-chain-security/
    "Sécurité de la chaîne d'approvisionnement logicielle").


    Le coût lié à l'identification et à la correction d’une faille de sécurité
    est un facteur important à ne pas négliger. Mais au-delà de l'aspect
    financier, une attaque sur la chaîne d'approvisionnement logicielle peut
    gravement affecter la réputation d'une entreprise.


    Non seulement les SBOM permettent de limiter les risques d'attaques (et donc
    de préserver votre réputation), mais elles contribuent également à optimiser
    les coûts liés à la gestion des vulnérabilités : 


    - En offrant un aperçu de vos dépendances,

    - En identifiant les vulnérabilités ainsi que les licences non conformes aux
    protocoles de sécurité internes à votre entreprise. 


    ## Quels sont les types de normes d'échange de données SBOM ?


    Les SBOM fonctionnent de manière optimale lorsque la génération et
    l'interprétation d'informations telles que le nom, la version, ou encore
    l’empaqueteur, peuvent être automatisées. Cette automatisation est optimale
    si toutes les parties prenantes impliquées utilisent un format standard
    d'échange de données. 


    Il existe deux principaux types de normes d'échange de données SBOM
    utilisées aujourd'hui :


    - [OWASP CycloneDX](https://cyclonedx.org/capabilities/sbom/ "OWASP
    CycloneDX")

    - [SPDX](https://spdx.dev/ "SPDX")


    GitLab utilise CycloneDX pour la génération de SBOM, car cette norme est :


    - prescriptive (marque un ensemble de normes objectives),

    - facile à comprendre et à prendre en main, 

    - capable de simplifier les relations complexes, 

    - extensible pour prendre en charge et s'adapter à des cas spécifiques.


    De plus, il existe des outils open source comme cyclonedx-cli et cdx2spdx
    qui peuvent être utilisés pour convertir des fichiers CycloneDX au format
    SPDX si nécessaire.


    ## Quels sont les avantages de l'association des SBOM avec la gestion des
    vulnérabilités logicielles


    Les SBOM sont très utiles pour les équipes DevSecOps et les utilisateurs de
    logiciels, et ce, pour plusieurs raisons :

     - Elles permettent une approche standardisée pour comprendre quels composants logiciels supplémentaires sont présents dans une application et où ils sont déclarés.
     - Elles offrent une visibilité continue sur l'historique de création d'une application, y compris des détails sur les origines du code tiers et les dépôts hôtes.
    - Elles fournissent un haut niveau de transparence en matière de sécurité,
    tant pour le code développé en interne que pour les logiciels open source
    utilisés.

    - Les détails fournis par les SBOM permettent aux [équipes
    DevOps](https://about.gitlab.com/fr-fr/topics/devops/build-a-devops-team/
    "Construire une équipe DevOps") d'identifier les vulnérabilités, d'évaluer
    les risques, puis de les atténuer. 

    - Les SBOM fournissent la transparence désormais attendue par les acheteurs
    d'applications.


    ## GitLab et les SBOM dynamiques


    Pour que les SBOM soient les plus efficaces possibles, les entreprises
    doivent être en mesure :


    - de les générer automatiquement,

    - de les connecter à des outils d'analyse de sécurité des applications,

    - d’intégrer les vulnérabilités et les licences dans un tableau de bord
    (pour faciliter la compréhension et la prise de décision),

    - de les mettre à jour en continu.


    Sachez que GitLab prend en charge tous ces objectifs. 


    ![Illustration de la gestion dynamique des
    SBOM](//images.ctfassets.net/r9o86ar0p03f/5zBLpR93LXmOmQjXixxiA/4851bbd356d0c0fc35f98bb63146c1a6/Screenshot_2024-05-03_at_10.53.28_AM.png)


    ### Générer et gérer des SBOM à grande échelle


    Pour se conformer aux réglementations ainsi qu'aux politiques internes de
    votre entreprise, il est essentiel de disposer de SBOM capables de couvrir
    tant les logiciels open source que les logiciels tiers et propriétaires.


    Il est également nécessaire de mettre en place un processus optimisé afin de
    créer, fusionner, valider et approuver les SBOM. C'est une condition
    essentielle à la bonne gestion des SBOM, et ce, pour chaque composant et
    version de produit.


    Comment la plateforme GitLab peut-elle vous aider dans la génération de SBOM
    ? 


    - La fonctionnalité « [Liste des
    dépendances](https://docs.gitlab.com/ee/user/application_security/dependency_list/
    "Liste des dépendances de GitLab") » rassemble les données connues sur les
    vulnérabilités et les licences dans une vue unique disponible au sein de
    l'interface utilisateur de GitLab. 

    - Des informations sur le graphe des dépendances sont générées dans le cadre
    du rapport d'analyse des dépendances. Cela permet aux utilisateurs d'obtenir
    une vue d'ensemble complète sur les dépendances et les vulnérabilités
    potentielles au sein de leurs projets ou groupes de projets.  

    - Un artefact au format JSON CycloneDX peut être produit dans le pipeline
    CI. Cette API propose une approche plus nuancée et personnalisée de la
    génération de SBOM. 

    - Les SBOM sont exportables depuis l'interface utilisateur, un pipeline ou
    un projet spécifique, ou via l'API GitLab.


    ### Ingérer et fusionner des SBOM


    En ingérant des SBOM tiers, GitLab offre un haut degré de transparence en
    matière de sécurité, que ce soit pour le code développé par des tiers que
    pour les logiciels open source utilisés. 


    GitLab vous permet d'utiliser un job
    [CI/CD](https://about.gitlab.com/fr-fr/topics/ci-cd/ "Qu'est-ce que le CI/CD
    ? ") pour fusionner simplement plusieurs SBOM CycloneDX. En s'appuyant sur
    les détails spécifiques à l'implémentation dans les métadonnées CycloneDX de
    chaque SBOM (comme l'emplacement des fichiers de compilation et de
    verrouillage), GitLab élimine les doublons. Ces données sont également
    enrichies automatiquement avec des informations sur les licences et les
    vulnérabilités des composants à l'intérieur de la nomenclature logicielle. 


    ### Accélérer l'atténuation des risques pour une meilleure gestion des SBOM


    Pour pouvoir créer rapidement des produits de haute qualité, les équipes de
    développement ont besoin de données exploitables pour identifier et corriger
    les vulnérabilités critiques des applications. GitLab contribue à sécuriser
    votre chaîne d'approvisionnement logicielle en [analysant les
    vulnérabilités](https://docs.gitlab.com/ee/user/application_security/secure_your_application.html
    "Comment sécuriser son application ? ") dans le code source, dans les
    conteneurs, les dépendances et les applications en cours d'exécution.


    GitLab offre une couverture complète pour aider dans la gestion des
    vulnérabilités (vulnerability management) grâce à sa large panoplie de
    scanners de sécurité : 


    - des tests statiques de sécurité des applications
    ([SAST](https://docs.gitlab.com/ee/user/application_security/sast/ "SAST"))

    - des tests dynamiques de sécurité des applications
    ([DAST](https://docs.gitlab.com/ee/user/application_security/dast/ "DAST"))

    - l'analyse des conteneurs

    - l'analyse de la composition logicielle
    ([SCA](https://about.gitlab.com/direction/secure/composition-analysis/software-composition-analysis/
    "SCA")) 


    Pour aider les équipes de développement et d'ingénierie en sécurité à mieux
    comprendre et corriger les vulnérabilités, GitLab met à disposition de ses
    utilisateurs la fonctionnalité d'explication des vulnérabilités de [GitLab
    Duo](https://about.gitlab.com/fr-fr/gitlab-duo/ "GitLab Duo "). Cette
    fonctionnalité alimentée par l'IA fournit une explication sur une
    vulnérabilité spécifique, comment elle peut être exploitée et, surtout,
    fournit une recommandation sur la manière de la corriger. Combinée à la
    fonctionnalité de résolution des vulnérabilités de GitLab Duo, cette
    fonctionnalité permet aux équipes DevSecOps de rapidement identifier,
    analyser et corriger les vulnérabilités de leurs applications.


    La plateforme prend également en charge la création de nouvelles règles (et
    le [respect de leur
    application](https://docs.gitlab.com/ee/administration/compliance.html))
    basées sur les vulnérabilités nouvellement détectées.


    ### Analyser les SBOM en continu 


    L’analyse continue des vulnérabilités de GitLab déclenche une analyse sur
    tous les projets où l'analyse des conteneurs, l'analyse des dépendances, ou
    les deux, sont activées indépendamment d'un pipeline.


    Lorsque de nouvelles CVE (Common Vulnerabilities and Exposures) sont
    signalées, les utilisateurs n'ont pas besoin de relancer leurs pipelines
    pour obtenir les dernières informations.


    En effet, l'équipe de recherche sur les vulnérabilités de GitLab les ajoute
    à la base de données des avis ([GitLab Advisory
    Database](https://docs.gitlab.com/ee/user/application_security/gitlab_advisory_database/
    "GitLab Advisory Database")) pour être intégrées dans la liste des
    vulnérabilités connues de GitLab. Cela rend la SBOM de GitLab véritablement
    dynamique. 


    ### Renforcer la confiance dans les SBOM


    GitLab permet aux entreprises nécessitant une [fonctionnalité de
    conformité](https://about.gitlab.com/fr-fr/solutions/compliance/ "La
    conformité des logiciels avec GitLab") de [générer des attestations pour
    tous les artefacts de
    compilation](https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
    "Sécuriser la chaîne d'approvisionnement logicielle grâce à l'attestation
    automatisée") produits par le GitLab Runner. Le processus est sécurisé, car
    il est réalisé directement par le [GitLab
    Runner](https://docs.gitlab.com/runner/ "GitLab Runner"), sans qu'il y ait
    de transfert de données vers un service externe.


    ## L'avenir des fonctionnalités SBOM de GitLab


    Les attaques fréquentes contre de grands fournisseurs de logiciels et sur
    les environnements open source obligent le monde du développement à
    renforcer ses défenses contre les menaces extérieures. Pour cette raison, la
    sécurité de la chaîne d'approvisionnement des logiciels est aujourd'hui un
    sujet central dans l'industrie de la cybersécurité et du logiciel. 


    Bien que les SBOM évoluent rapidement, des préoccupations subsistent
    néanmoins. Par exemple : 


    - la manière et la fréquence dont elles sont générées

    - leur stockage

    - la combinaison de plusieurs SBOM pour des applications complexes

    - leur analyse et utilisation pour améliorer la santé des applications.


    GitLab a fait des SBOM une partie intégrante de sa stratégie en matière de
    [chaîne d'approvisionnement
    logicielle](https://about.gitlab.com/direction/supply-chain/ "Chaîne
    d'approvisionnement logicielle") et continue d'améliorer ses capacités SBOM
    en permanence, en intégrant régulièrement de nouvelles fonctionnalités.


    Parmi ces améliorations, nous retrouvons entre autres : l'automatisation des
    attestations, la signature numérique des artefacts de compilation et la
    prise en charge des SBOM générées en externe.


    GitLab a également mis en place un [modèle de maturité
    SBOM](https://handbook.gitlab.com/handbook/security/security-assurance/dedicated-compliance/sbom-plan/
    "Modèle de maturité SBOM") (SBOM Model Maturity) au sein de sa plateforme.
    Ce modèle inclut des étapes comme la génération automatique de SBOM,
    l’approvisionnement en SBOM depuis l'environnement de développement,
    l’analyse des SBOM pour les artefacts et la promotion de la signature
    numérique des SBOM.


    ## Commencez à utiliser les SBOM


    La demande en SBOM est déjà très forte.


    Aux États-Unis, les agences gouvernementales recommandent, voire exigent des
    fournisseurs et des développeurs de logiciels fédéraux, et même des
    communautés open source qu'ils créent des SBOM. 


    A l'échelle européenne, ce sujet est traité au sein du [règlement sur la
    cyberrésilience
    (CRA)](https://digital-strategy.ec.europa.eu/fr/news/cyber-resilience-act-enters-force-make-europes-cyberspace-safer-and-more-secure
    "règlement sur la cyberrésilience"). Quant à la France, c'est l'[Agence
    nationale de la sécurité des systèmes d'information](https://cyber.gouv.fr/
    "Agence nationale de la sécurité des systèmes d'information") (ANSSI) qui
    tranche sur ce type de sujets. 


    Ainsi, que ce soit aux États-Unis, en France, mais aussi ailleurs dans le
    monde, les agences liées à la sécurité informatique informent sur les
    menaces grandissantes qui pèsent sur le monde du développement logiciel et
    numérique plus globalement. 


    Pour anticiper ces exigences, consultez les fonctionnalités SBOM de
    l'édition GitLab Ultimate disponibles dans la [plateforme DevSecOps de
    GitLab](https://gitlab.com/-/trials/new "Essai gratuit de la plateforme
    GitLab ").


    ## FAQ sur les SBOM


    ### Qu'est-ce qu'une SBOM ?


    Une SBOM est un inventaire détaillé qui répertorie tous les composants,
    bibliothèques et outils utilisés pour créer, construire et déployer des
    logiciels. Cette liste exhaustive inclut des informations essentielles sur
    les origines du code. Cela favorise une compréhension plus approfondie de la
    composition d'une application et de ses vulnérabilités potentielles.


    ### Pourquoi les SBOM sont-elles si importantes ?


    Les SBOM sont essentielles pour plusieurs raisons. Elles fournissent :


    - __Un aperçu des dépendances.__ Elles permettent de comprendre ce qui
    compose votre logiciel, et donc de mieux identifier et atténuer les risques
    associés aux composants tiers.

    - __Une sécurité renforcée.__ Avec une visibilité détaillée des composants
    de leurs applications, les entreprises peuvent rapidement identifier leurs
    vulnérabilités et prendre des mesures pour les résoudre.

    - __Une conformité réglementaire.__ De plus en plus, les réglementations et
    les meilleures pratiques recommandent ou exigent une SBOM pour l'empaquetage
    de logiciels, en particulier dans le secteur public.

    - __Un développement rationalisé.__ Les équipes de développement peuvent
    s'appuyer sur les SBOM pour obtenir des informations sur les bibliothèques
    et les composants utilisés, ce qui permet de gagner du temps et de réduire
    les erreurs dans le cycle de développement logiciel. 


    ### Quelles normes sont utilisées pour l'échange de données SBOM ?


    Il existe deux normes prédominantes pour l'échange de données SBOM : 


    -
    [CycloneDX](https://docs.gitlab.com/ee/user/compliance/license_scanning_of_cyclonedx_files/
    "CycloneDX") : connue pour être particulièrement intuitive, CycloneDX
    simplifie les relations complexes entre les composants logiciels et prend en
    charge des cas d'utilisation spécifiques. 

    - [SPDX](https://docs.gitlab.com/ee/raketasks/spdx.html "SPDX") : un autre
    framework largement utilisé pour l'échange de données SBOM. SPDX fournit des
    informations détaillées sur les composants au sein de l'environnement
    logiciel. 


    En raison de sa nature normative et de son adaptabilité, GitLab utilise
    spécifiquement CycloneDX pour sa génération de SBOM.


    ### Quelle est l’approche de GitLab en matière de SBOM ?


    GitLab met l'accent sur la création de SBOM dynamiques qui peuvent être : 


    - Générées automatiquement : garantir des informations à jour sur la
    composition logicielle

    - Intégrées avec des outils : se connecter à des outils d’analyse de
    vulnérabilités, pour une meilleure gestion des risques. 

    - Faciles à gérer : prendre en charge l'ingestion et la fusion des SBOM pour
    une analyse complète 

    - Analysées en continu : offrir une analyse continue des projets pour
    détecter les nouvelles vulnérabilités au fur et à mesure de leur apparition


    ### Comment puis-je commencer à implémenter les SBOM dans mon entreprise ?


    Pour les entreprises prêtes à adopter les SBOM, l'édition [GitLab
    Ultimate](https://about.gitlab.com/fr-fr/pricing/ultimate/ "GitLab
    Ultimate") met à disposition des utilisateurs une plateforme robuste pour
    générer et gérer des SBOM au sein d'un workflow DevSecOps. En tirant parti
    des outils de GitLab, les équipes de développement peuvent garantir la
    conformité logicielle, renforcer la sécurité et optimiser les pratiques de
    développement.


    La demande croissante de SBOM reflète l’importance grandissante accordée à
    la sécurité logicielle et à l'intégrité de la chaîne d'approvisionnement. En
    intégrant les fonctionnalités SBOM, les entreprises peuvent mieux se
    protéger contre les vulnérabilités et se conformer aux nouvelles
    réglementations en vigueur.


    > [Essayez GitLab Ultimate gratuitement pendant 30
    jours.](https://about.gitlab.com/free-trial/devsecops/ "Essai gratuit de
    GitLab Ultimate")


    *Avertissement : cet article de blog contient des informations relatives aux
    produits, fonctionnalités et caractéristiques à venir. Il est important de
    noter que les informations contenues dans cet article de blog ne sont
    fournies qu'à titre informatif. Veuillez ne pas vous fier à ces informations
    à des fins d'achat ou de planification. Comme pour tout projet, les éléments
    mentionnés dans cet article (et les pages qui y sont liées) sont
    susceptibles de changer ou d'être retardés. Le développement, la sortie et
    le calendrier de tout produit ou fonctionnalité restent à la seule
    discrétion de GitLab.*
  category: Sécurité
  tags:
    - security
    - DevSecOps
    - performance
    - open source
    - public sector
config:
  slug: the-ultimate-guide-to-sboms
  featured: false
  template: BlogPost
