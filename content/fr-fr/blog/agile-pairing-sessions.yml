seo:
  title: >-
    Pair programming : coder en binôme pour progresser et améliorer sa
    productivité
  description: >-
    Dans cet article, découvrez le pair programming, pourquoi vous devez coder
    en binôme et comment éviter les erreurs principales lorsqu'on se lance.
  ogTitle: >-
    Pair programming : coder en binôme pour progresser et améliorer sa
    productivité
  ogDescription: >-
    Dans cet article, découvrez le pair programming, pourquoi vous devez coder
    en binôme et comment éviter les erreurs principales lorsqu'on se lance.
  noIndex: false
  ogImage: images/blog/hero-images/incrementalcodedevelopment.jpg
  ogUrl: https://about.gitlab.com/blog/agile-pairing-sessions
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/agile-pairing-sessions
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Pair programming : coder en binôme pour progresser et améliorer sa productivité",
            "author": [{"@type":"Person","name":"Suri Patel"}],
            "datePublished": "2019-08-20",
          }

content:
  title: >-
    Pair programming : coder en binôme pour progresser et améliorer sa
    productivité
  description: >-
    Dans cet article, découvrez le pair programming, pourquoi vous devez coder
    en binôme et comment éviter les erreurs principales lorsqu'on se lance.
  authors:
    - Suri Patel
  heroImage: images/blog/hero-images/incrementalcodedevelopment.jpg
  date: '2019-08-20'
  body: >-
    Coder en binôme aide les équipes de développement à progresser plus
    rapidement et à améliorer la livraison des projets. Découvrez dans cet
    article le fonctionnement du pair programming, comment coder en binôme et
    les meilleures astuces pour tirer parti de cette méthode Agile. 


    ## Qu’est-ce que le pair programming ? 


    Le pair programming (ou programmation en binôme) est une approche Agile du
    développement logiciel qui implique deux développeurs et/ou développeuses
    travaillant sur le même poste de travail. L'un, appelé le conducteur
    (driver), rédige le code pendant que l'autre, appelé le navigateur
    (navigator) le commente et le révise en temps réel. Les sessions de pair
    programming peuvent accélérer les projets agiles, car les binômes
    travaillent ensemble pour trouver les meilleures solutions aux différentes
    problématiques rencontrées. Plutôt que de travailler en silos, les membres
    de l'équipe s'associent pour partager leurs connaissances et avancer plus
    rapidement. 


    > [Essayez GitLab Ultimate gratuitement pendant 30
    jours.](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/blog&glm_content=default-saas-trial
    "Essai gratuit de GitLab pendant 30 jours.") 


    ## Comment débuter une session de pair programming ?


    La clé d'une session de pair programming réussie repose sur une
    communication fluide et la création d'un planning afin d'éviter de
    rencontrer des problèmes lors de la réalisation du projet. 


    Voici quatre éléments à considérer avant de vous lancer dans une session de
    programmation en binôme : 


    - Ayez une compréhension commune sur la notion du « prêt à être lancé » pour
    le projet en question. Concertez-vous ainsi que toute partie prenante
    impliquée, comme le Product Owner, afin de déterminer clairement à quel
    moment donner le feu vert au projet.

    - Planifiez le projet étape par étape. Définissez comment vous allez coder,
    effectuer la revue de code et tester le projet. Pensez aussi aux
    potentielles aides extérieures dont vous pourriez avoir besoin pour mener le
    projet à bien.

    - Faites une liste de tous les obstacles potentiels que vous pourriez
    rencontrer lors de l'accomplissement du projet et essayez de trouver des
    solutions potentielles. Vous pouvez le faire ensemble, à l’écrit ou
    oralement, ou séparément avant de partager vos idées. Quelle que soit la
    manière, cette étape est importante.

    - Trouvez un accord sur la technologie à utiliser pour le projet. De
    l’ordinateur idéal en passant par une connexion Wi-Fi fiable, il faut vous
    doter de tous les outils nécessaires à la réussite du projet.


    ## 5 astuces pour réussir sa session de pair programming


    Pour tirer pleinement parti de vos sessions de programmation en binôme,
    voici cinq bonnes pratiques à suivre : 


    - __Communiquer sans interruption.__ Dans une session de pair programming,
    la communication est primordiale, que vous ayez déjà collaboré ou non avec
    votre binôme. Il est normal que deux personnes aient des idées et des
    opinions différentes lors d’un projet. Pour éviter que le projet et le
    binôme n'en pâtissent, gardez une communication ouverte et fréquente. 

    - __Alterner les rôles.__ Aucun des deux ne doit être le seul conducteur ou
    navigateur. Pensez à échanger les rôles régulièrement afin de garder un œil
    frais et de continuer à produire un travail de qualité.

    - __Prendre des pauses.__ Rome ne s'est pas construite en un jour, le codage
    non plus. Veillez donc à faire des pauses régulières pour ne pas vous
    épuiser.

    - __Utiliser les bons outils.__ Souvent, le pair programming se fait à
    distance. Même si le dialogue reste virtuel, pensez à échanger par
    visioconférence pour mieux collaborer tout au long du projet. 

    - __Demander de l'aide.__ Vous ne comprenez pas une partie du projet ?
    N'hésitez pas à demander de l'aide. Il vaut mieux en demander au cours de la
    session de programmation qu'au moment de la validation finale.


    ## Quels sont les avantages du pair programming ?


    Collaborer directement avec un développeur ou une développeuse booste le
    moral et diversifie la journée de travail de chacun. En travaillant en
    binôme, il est possible d’apprendre de nouvelles pratiques de codage, de
    nouveaux workflows ou encore de nouvelles façons d'aborder un problème pour
    favoriser l’innovation, améliorer l’efficacité et réduire la rétention
    d'information.


    Les développeurs juniors profitent de l’expérience de leurs collègues
    seniors pour acquérir de solides connaissances. De leur côté, les
    développeurs expérimentés apprennent à enseigner leurs méthodes et à
    utiliser leur pensée critique pour trouver des solutions. 


    Quel que soit le niveau d'expérience, tout le monde peut bénéficier des
    sessions de pair programming, puisqu'il n'existe pas de bonne ou de mauvaise
    façon de programmer. Le développement de logiciels est un effort à multiples
    facettes dans lequel l'imagination et la créativité jouent un rôle
    fondamental. Les connaissances, l'expérience et les styles d'apprentissage
    de chacun déterminent la manière d'appréhender les liens entre des éléments
    de code et un système en place. En binôme, il est possible de discuter de
    ces perspectives et de déterminer la meilleure approche.


    ## Quels sont les inconvénients du pair programming ?


    Coder en binôme peut sembler être la solution à tous les problèmes, mais ce
    n’est pas toujours le cas. 


    Lorsqu’il comprend à quel point le pair programming peut les aider, un
    binôme peut être tenté d’unir ses forces un peu trop souvent. Pour des
    tâches simples, des changements mineurs et précis, coder en binôme s'avère
    inutile.


    De plus, si deux développeurs ou développeuses commencent tout juste à coder
    ensemble, ils doivent faire preuve de patience et persévérer pour former un
    excellent binôme. Ce qui n'est pas toujours facile, même pour les
    développeurs expérimentés. Pensez à débriefer après vos sessions de pair
    programming afin de comprendre ce qui a bien ou moins bien fonctionné et
    comment vous pouvez vous améliorer lors des prochaines sessions. 


    ## Le pair programming chez GitLab


    Chez [GitLab](https://about.gitlab.com/fr-fr/ "Site Web de GitLab"), nous
    aimons coder en binôme. La plupart du temps, les sessions de pair
    programming s'effectuent sur le même poste de travail, mais en tant
    qu'[entreprise en 100 %
    télétravail](https://about.gitlab.com/fr-fr/company/all-remote/ "GitLab est
    une entreprise en 100% télétravail "), nous avons trouvé des moyens pour
    faire en sorte que cela fonctionne.  


    Au sein de l'équipe d'ingénierie, [l'équipe Frontend, a par exemple
    expérimenté la programmation en
    binôme](https://gitlab.com/gitlab-org/frontend/general/-/issues/12 "Exemple
    de pair programming chez GitLab"). L'équipe a utilisé VSCode Live Share à
    plusieurs reprises, mais privilégie l'envoi de correctifs et les
    conversations en groupe.


    *« Le meilleur format jusqu'à présent reste le suivant : quelqu'un envoie
    une demande de binôme dans le canal Slack #frontend_pairs, des personnes
    manifestent leur intérêt, nous bloquons une plage horaire dans le
    calendrier, puis nous organisons une session pour programmer en groupe. »*
    Paul Slaughter, Staff Fullstack Engineer.


    Toutes les équipes de développement entendent parler de l'importance de
    l'accélération. Cela peut devenir décourageant, particulièrement lorsque
    l'équipe est confrontée à des problèmes complexes. La prochaine fois que
    vous rencontrez des difficultés à rédiger du code, envisagez une session de
    pair programming pour résoudre les problèmes à deux. 


    *« Coder en binôme est ressenti différemment par chaque personne.
    Privilégiez toujours la communication, le partage des informations et la
    collaboration entre les équipes. »* Paul Slaughter.  


    Maintenant que vous en savez plus sur le pair programming, il est temps de
    vous lancer ! 


    > [Essayez GitLab Ultimate gratuitement pendant 30
    jours.](https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/blog&glm_content=default-saas-trial
    "Essai gratuit de GitLab pendant 30 jours. ")
  category: Informations clés
  tags:
    - agile
    - collaboration
    - DevOps
    - workflow
config:
  slug: agile-pairing-sessions
  featured: false
  template: BlogPost
