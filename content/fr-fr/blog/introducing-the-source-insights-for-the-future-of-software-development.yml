seo:
  title: "The Source\_: une mine d'informations sur l'avenir du développement logiciel"
  description: >-
    Découvrez des stratégies de développement logiciel transformatrices et des
    conseils d'experts sur les technologies émergentes.
  ogTitle: "The Source\_: une mine d'informations sur l'avenir du développement logiciel"
  ogDescription: >-
    Découvrez des stratégies de développement logiciel transformatrices et des
    conseils d'experts sur les technologies émergentes.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(1).png
  ogUrl: >-
    https://about.gitlab.com/blog/introducing-the-source-insights-for-the-future-of-software-development
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/introducing-the-source-insights-for-the-future-of-software-development
  schema: "\n                        {\n        \"@context\": \"https://schema.org\",\n        \"@type\": \"Article\",\n        \"headline\": \"The Source\_: une mine d'informations sur l'avenir du développement logiciel\",\n        \"author\": [{\"@type\":\"Person\",\"name\":\"Chandler Gibbons\"}],\n        \"datePublished\": \"2024-10-29\",\n      }\n                  "
content:
  title: "The Source\_: une mine d'informations sur l'avenir du développement logiciel"
  description: >-
    Découvrez des stratégies de développement logiciel transformatrices et des
    conseils d'experts sur les technologies émergentes.
  authors:
    - Chandler Gibbons
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(1).png
  date: '2024-10-29'
  body: "Le développement logiciel moderne transforme la façon dont les entreprises créent, proposent et développent de la valeur commerciale. Les équipes doivent être en mesure d’élaborer des solutions rapidement et efficacement, tout en gérant les menaces de sécurité croissantes, les technologies émergentes et les exigences de conformité de plus en plus complexes.\n\nAujourd'hui, GitLab lance [The Source](https://about.gitlab.com/fr-fr/the-source/), une nouvelle publication qui couvre l'évolution du développement logiciel en tant que moteur de la réussite commerciale. Nous partageons régulièrement des informations clés sur l'avenir du développement logiciel, étayées par les recherches et les analyses originales de nos experts métier et de nos leaders d'opinion.\n\nThe Source vous permet par exemple de trouver des réponses aux questions suivantes : \n* Comment les dirigeants peuvent-ils mesurer le retour sur investissement de l'IA tout au long du cycle de développement logiciel\_?  \n* Quelle est la meilleure façon de garantir la sécurité et la conformité tout au long de la chaîne d'approvisionnement logicielle\_?\n* Quels gains de productivité les équipes peuvent-elles réaliser grâce à la consolidation de leur plateforme et de leur chaîne d'outils\_?\n\nVoici un aperçu du contenu que vous pouvez d'ores et déjà retrouver sur The Source : \n\n**Mesurer l'impact de l'IA\_: 4\_étapes indispensables**\n\n«\_L'évaluation de la productivité dans un contexte de développement amélioré par l'IA nécessite une approche plus nuancée que celle reposant sur les indicateurs traditionnels tels que les lignes de code, les validations de code ou l'achèvement des tâches. Elle nécessite une réorientation vers des résultats commerciaux concrets qui permettent d'équilibrer la rapidité de développement, la qualité des logiciels et la sécurité.\_»\n- [Taylor\_McCaslin, expert en IA, présente les 4\_étapes indispensables pour mesurer l'impact de l'IA.](https://about.gitlab.com/fr-fr/the-source/ai/4-steps-for-measuring-the-impact-of-ai/)\n\n**Identifier et remédier aux points de friction liés à la sécurité**\n\n«\_Bien que l'approche DevSecOps promette une meilleure intégration entre l'ingénierie et la sécurité, la présence de frustrations et de déséquilibres montre que des défis subsistent. Ce constat reflète un problème plus large lié à la manière dont les entreprises perçoivent la sécurité, organisent la collaboration entre les équipes, et allouent du temps à cette priorité.\_»\n- [Trouvez le bon équilibre, grâce à l'expertise de Josh\_Lemos, CISO de GitLab.](https://about.gitlab.com/fr-fr/the-source/security/security-its-more-than-culture-addressing-the-root-cause-of-common-security/)\n\n**L'ingénierie de plateforme au service de la réussite des entreprises**\n\n« En normalisant et en optimisant les workflows des équipes de développement, l'ingénierie de plateforme crée des \"golden paths\" optimisés pour les tâches courantes tout en offrant la flexibilité nécessaire pour les tâches plus uniques et spécialisées. »  \n- [Découvrez les bonnes pratiques de Brian\_Wald (GitLab Field CTO) en matière d'ingénierie de plateforme.](https://about.gitlab.com/fr-fr/the-source/platform-and-infrastructure/driving-business-results-with-platform-engineering/)\n\n## The Source, votre partenaire pour la prise de décision\n\nConsultez [The Source](https://about.gitlab.com/fr-fr/the-source/) dès aujourd'hui pour découvrir les dernières informations du secteur, obtenir des réponses en matière de leadership et découvrir les dernières nouveautés du marché à partager avec vos équipes. Vous pouvez également vous abonner à notre newsletter pour recevoir des mises à jour régulières par e-mail. Rejoignez notre communauté de leaders technologiques avant-gardistes et contribuez à façonner l'avenir du développement logiciel."
  category: Actualités
  tags:
    - AI/ML
    - security
    - news
    - DevSecOps
config:
  slug: introducing-the-source-insights-for-the-future-of-software-development
  featured: true
  template: BlogPost
