seo:
  title: >-
    Installation de GitLab : guide pratique pour faire vos premiers pas sur la
    plateforme DevSecOps
  description: >-
    Découvrez dans ce guide pratique tout ce que vous devez savoir sur la
    plateforme DevSecOps de GitLab, ainsi qu’un tutoriel pour installer GitLab
    Self-Managed.
  ogTitle: >-
    Installation de GitLab : guide pratique pour faire vos premiers pas sur la
    plateforme DevSecOps
  ogDescription: >-
    Découvrez dans ce guide pratique tout ce que vous devez savoir sur la
    plateforme DevSecOps de GitLab, ainsi qu’un tutoriel pour installer GitLab
    Self-Managed.
  noIndex: false
  ogImage: images/blog/hero-images/installation-gitlab.jpeg
  ogUrl: https://about.gitlab.com/blog/how-to-install-gitlab-beginners-guide
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/how-to-install-gitlab-beginners-guide
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Installation de GitLab : guide pratique pour faire vos premiers pas sur la plateforme DevSecOps",
            "author": [{"@type":"Person","name":"GitLab France Team"}],
            "datePublished": "2025-01-15",
          }

content:
  title: >-
    Installation de GitLab : guide pratique pour faire vos premiers pas sur la
    plateforme DevSecOps
  description: >-
    Découvrez dans ce guide pratique tout ce que vous devez savoir sur la
    plateforme DevSecOps de GitLab, ainsi qu’un tutoriel pour installer GitLab
    Self-Managed.
  authors:
    - GitLab France Team
  heroImage: images/blog/hero-images/installation-gitlab.jpeg
  date: '2025-01-15'
  body: >
    Si vous travaillez sur des projets de développement logiciel, la plateforme
    DevSecOps de GitLab doit certainement susciter votre intérêt. GitLab, c'est
    un ensemble de fonctionnalités intégrées au sein d'une même plateforme, vous
    permettant d'accélérer votre [livraison de
    logiciels](https://about.gitlab.com/fr-fr/solutions/faster-software-delivery/
    "Livraison de logiciels"), de façon sécurisée et indépendante des
    fournisseurs de services cloud. 


    Pour faire vos premiers pas, ce tutoriel GitLab vous présente le
    fonctionnement global de la plateforme, ainsi que les principales méthodes
    pour [installer GitLab Self-Managed](https://about.gitlab.com/fr-fr/install/
    "Installer GitLab Self-Managed").


    ## Comment fonctionne GitLab ?


    GitLab est bien plus qu'un gestionnaire de dépôt
    [Git](https://about.gitlab.com/fr-fr/blog/2024/10/08/what-is-git/ "Qu'est-ce
    que Git ? "). C'est une plateforme collaborative qui réunit les différentes
    équipes chargées du développement, de la sécurité et des opérations d'une
    entreprise. Elles peuvent collaborer efficacement grâce à une suite de
    fonctionnalités complètes et facilement intégrables à d'autres solutions et
    environnements technologiques. 


    GitLab permet aux entreprises de livrer des logiciels de qualité toujours
    plus rapidement en intégrant au sein de sa plateforme DevSecOps toutes les
    [étapes essentielles du cycle du développement
    logiciel](https://about.gitlab.com/fr-fr/stages-devops-lifecycle/ "Etapes du
    cycle du développement logiciel") : 


    - __La planification :__ organisation, planification, coordination et suivi
    de l'ensemble des projets. 


    - __La création :__ création, consultation, gestion du code et des données
    des projets. 


    - __La vérification :__ maintien des normes de qualité strictes pour le code
    à l’aide de tests et de rapports automatisés. 


    - __L’empaquetage :__ création d'une chaîne d'approvisionnement logicielle
    fiable et cohérente avec la gestion des paquets. 


    - __La sécurité :__ livraison de logiciels sécurisés et conformes avec les
    tests statiques de sécurité des applications (SAST), les tests dynamiques de
    sécurité des applications (DAST), l’analyse de conteneurs et l’analyse des
    dépendances. 


    - __Le déploiement :__ automatisation de la phase de release et de livraison
    de logiciels, raccourcissement du cycle de développement logiciel,
    rationalisation des processus manuels, et plus encore.  


    - __La surveillance :__ réduction de la fréquence et du niveau de gravité
    des incidents. 


    - __La conformité :__ gestion des failles de sécurité, contrôle des
    stratégies de sécurité et de la conformité de votre entreprise.  


    À chaque étape, la [plateforme DevSecOps de
    GitLab](https://about.gitlab.com/fr-fr/platform/ "Plateforme DevSecOps de
    GitLab") permet d'améliorer le workflow des équipes de développement,
    notamment avec l'aide de GitLab Duo, notre suite de fonctionnalités
    alimentées par l'IA. [GitLab Duo](https://about.gitlab.com/fr-fr/gitlab-duo/
    "GitLab Duo") permet de booster la productivité des équipes à toutes les
    étapes du cycle du développement logiciel, de la planification à la
    surveillance, en passant par le code et les tests.


    L'architecture GitLab s'adapte à toutes les configurations, du simple projet
    individuel à des réalisations de grands groupes internationaux. Avant
    d'installer GitLab, choisissez la manière dont vous souhaitez utiliser notre
    plateforme : en SaaS ou Self-Managed. Pour en savoir plus, consultez notre
    [page de tarification](https://about.gitlab.com/fr-fr/pricing/ "Page de
    tarification de GitLab"). 


    Si votre choix se porte sur GitLab Self-Managed, découvrez dans cet article
    ses principales méthodes d’installation. 


    ## Comment installer GitLab Self-Managed ?


    GitLab est une plateforme de développement logiciel flexible. Elle peut être
    installée sur de nombreux environnements et systèmes d'exploitation Linux
    tels que Ubuntu, Debian, AlmaLinux, CentOS 7, et plus encore. En
    environnement Cloud, GitLab est pris en charge par les plateformes majeures
    telles qu'Amazon Web Services, Google Cloud Platform, ou encore Microsoft
    Azure. 


    Parmi tous ces [environnements d'installation de
    GitLab](https://about.gitlab.com/fr-fr/install/ "Installer GitLab
    Self-Managed") disponibles, vous trouverez très certainement celui que vous
    avez l'habitude d'utiliser. Pour cet article, nous focaliserons notre
    attention sur certains d’entre eux.


    ### Installation de GitLab sur Linux


    Le paquet Linux est la méthode d’installation recommandée par GitLab.
    Également appelé Omnibus, ce paquet regroupe tous les services et outils
    nécessaires à l'exécution de GitLab.  


    __Instructions relatives à l’[installation de GitLab sur
    Ubuntu](https://about.gitlab.com/fr-fr/install/#ubuntu "Installation de
    GitLab sur Ubuntu") :__


    1. __Téléchargez votre paquet Linux officiel__ 


    Assurez-vous de télécharger le paquet adapté à votre distribution Linux,
    parmi les [différentes versions
    disponibles](https://packages.gitlab.com/gitlab/gitlab-ee "gitlab-ee").


    2. __Installez et configurez les dépendances nécessaires__


    `sudo apt-get update`


    `sudo apt-get install -y curl openssh-server ca-certificates tzdata perl`


    Puis installez Postfix ou Sendmail pour envoyer vos e-mails de notification.
    Si vous souhaitez utiliser un autre serveur de messagerie électronique,
    ignorez cette étape. 


    `sudo apt-get install -y postfix`


    Pendant l'installation de Postfix, il se peut qu'une fenêtre de
    configuration s'affiche. Dans ce cas, sélectionnez « Site Internet » puis
    appuyez sur « Entrée ». Saisissez le DNS externe de votre serveur dans le
    champ « Nom de l'e-mail » et appuyez sur « Entrée ». Si d'autres fenêtres
    s'affichent, appuyez à nouveau sur « Entrée » pour accepter les paramètres
    par défaut.


    3. __Ajoutez le dépôt de paquets GitLab et installez le paquet__


    `curl
    https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh
    | sudo bash`


    Installez ensuite le paquet GitLab, configurez votre DNS puis remplacez
    https://gitlab.example.com/ par l’URL à laquelle vous voulez accéder à votre
    instance GitLab. Pour les URL avec le protocole « https:// », GitLab vous
    demande automatiquement un certificat Let's Encrypt. Vous pouvez également
    utiliser votre propre certificat ou le protocole « http:// ». Ensuite,
    choisissez entre un mot de passe personnalisé ou généré aléatoirement. 


    `sudo EXTERNAL_URL="https://gitlab.example.com" apt-get install gitlab-ee`


    `# List available versions: apt-cache madison gitlab-ee`


    `# Specifiy version: sudo EXTERNAL_URL="https://gitlab.example.com" apt-get
    install gitlab-ee=16.2.3-ee.0`


    `# Pin the version to limit auto-updates: sudo apt-mark hold gitlab-ee`


    `# Show what packages are held back: sudo apt-mark showhold`


    4. __Accédez au nom d'hôte__


    Si vous optez pour un mot de passe généré aléatoirement, ce dernier sera
    stocké dans « /etc/gitlab/initial_root_password » pendant 24 heures. Pour
    vous connecter, utilisez ce mot de passe avec le nom d'utilisateur « root
    ». 


    5. __Choisissez vos préférences de communication__


    Vous pouvez décider pour quelles raisons et à quelle fréquence vous recevrez
    des nouvelles de GitLab. Inscrivez-vous à la newsletter dédiée à la sécurité
    pour être informés des alertes. 


    Retrouvez en détail l'[installation de GitLab sur
    Ubuntu](https://about.gitlab.com/fr-fr/install/#ubuntu "Installation de
    GitLab sur Ubuntu"), ainsi que les étapes additionnelles recommandées pour
    [finaliser votre
    installation](https://docs.gitlab.com/ee/install/next_steps.html). 


    ### Installation de GitLab sur Docker


    Pour [installer GitLab sur
    Docker](https://docs.gitlab.com/ee/install/docker/installation.html
    "Installer GitLab sur Docker"), assurez-vous d’avoir une installation Docker
    fonctionnelle (autre que Docker pour Windows), un agent de transfert de
    courrier (tel que Postfix ou Sendmail), ainsi qu’un nom d'hôte valide et
    accessible de l'extérieur (différent de `locahost`).


    __Instructions relatives à l'installation de GitLab sur Docker :__


    1. __Configurez le port SSH__ 


    GitLab utilise par défaut le port 22 pour interagir avec Git. Pour utiliser
    un autre port SSH, référez-vous à [notre
    documentation](https://docs.gitlab.com/ee/install/docker/installation.html#configure-the-ssh-port). 


    2. __Créez un répertoire pour les volumes__ 


    Créez un répertoire `sudo mkdir -p /srv/gitlab` pour les fichiers de
    configuration, les journaux et les fichiers de données. Ce répertoire peut
    se trouver dans le répertoire personnel de l'utilisateur (par exemple
    `~/gitlab-docker`), ou dans un répertoire comme `/srv/gitlab`. Si vous
    exécutez Docker avec un utilisateur autre que `root`, accordez les
    permissions appropriées à l'utilisateur pour le nouveau répertoire.


    Configurez ensuite une nouvelle variable d'environnement `$GITLAB_HOME` qui
    définit le chemin vers le répertoire que vous avez créé : `export
    GITLAB_HOME=/srv/gitlab`. 


    3. __Trouvez la version et l'édition de GitLab à utiliser__


    Rattachez votre déploiement à une version spécifique de GitLab. Passez en
    revue les versions disponibles et choisissez celle que vous souhaitez
    utiliser dans la page des balises Docker : [GitLab Enterprise
    Edition](https://hub.docker.com/r/gitlab/gitlab-ee/tags/ "GitLab Enterprise
    Edition") ou [GitLab Community
    Edition](https://hub.docker.com/r/gitlab/gitlab-ce/tags/ "GitLab Community
    Edition").


    4. __Choisissez une méthode d'installation__


    Pour installer GitLab sur Docker, trois options s’offrent à vous : Docker
    Compose, Docker Engine ou Docker Swarm mode. Dans le cadre de cet article,
    nous allons nous intéresser uniquement à Docker Compose qui est la méthode
    d’installation recommandée. 


    [Installez Docker Compose](https://docs.docker.com/compose/install/linux/
    "Installation de Docker Compose"), puis créez un fichier
    `docker-compose.yml`. Par exemple :


    ```

    version: '3.6'

    services:
      gitlab:
        image: gitlab/gitlab-ee:<version>-ee.0
        container_name: gitlab
        restart: always
        hostname: 'gitlab.example.com'
        environment:
          GITLAB_OMNIBUS_CONFIG: |
            # Add any other gitlab.rb configuration here, each on its own line
            external_url 'https://gitlab.example.com'
        ports:
          - '80:80'
          - '443:443'
          - '22:22'
        volumes:
          - '$GITLAB_HOME/config:/etc/gitlab'
          - '$GITLAB_HOME/logs:/var/log/gitlab'
          - '$GITLAB_HOME/data:/var/opt/gitlab'
        shm_size: '256m'
    ```


    Voici un autre exemple de fichier `docker-compose.yml` avec GitLab
    fonctionnant sur des ports HTTP et SSH personnalisés. Notez que les
    variables `GITLAB_OMNIBUS_CONFIG` correspondent à la section `ports` : 


    ```

    version: '3.6'

    services:
      gitlab:
        image: gitlab/gitlab-ee:<version>-ee.0
        container_name: gitlab
        restart: always
        hostname: 'gitlab.example.com'
        environment:
          GITLAB_OMNIBUS_CONFIG: |
            external_url 'http://gitlab.example.com:8929'
            gitlab_rails['gitlab_shell_ssh_port'] = 2424
        ports:
          - '8929:8929'
          - '443:443'
          - '2424:22'
        volumes:
          - '$GITLAB_HOME/config:/etc/gitlab'
          - '$GITLAB_HOME/logs:/var/log/gitlab'
          - '$GITLAB_HOME/data:/var/opt/gitlab'
        shm_size: '256m'
    ```


    Cette configuration est équivalente à l'utilisation de `--publish 8929:8929
    --publish 2424:22`.

    Enfin, dans le même répertoire que `docker-compose.yml`, démarrez GitLab : 


    ```

    docker compose up -d

    ```


    Reportez-vous à la [documentation d'installation de Docker
    ](https://docs.gitlab.com/ee/install/docker/installation.html "Documentation
    d'installation de Docker ")pour en savoir plus sur Docker Compose, Docker
    Engine et Docker Swarm mode. 


    ### Installation de GitLab sur Kubernetes


    Vous pouvez déployer certains composants dans
    [Kubernetes](https://about.gitlab.com/fr-fr/blog/2024/07/25/kubernetes-the-container-orchestration-solution/
    "Qu'est-ce que Kubernetes ? "), avec une architecture de référence Cloud
    Native Hybrid. Pour des raisons de stabilité en production, il n’est pas
    recommandé de déployer les composants stateful, tels que Gitaly, dans
    Kubernetes.


    Cloud Native Hybrid est une alternative et une configuration plus avancée
    par rapport à une architecture de référence standard. L'exécution de
    services dans Kubernetes est complexe. N'utilisez cette configuration que si
    vous avez de solides connaissances et une solide expérience de Kubernetes.
    Vous retrouverez dans la documentation les instructions pour installer
    GitLab suivant la configuration [Cloud Native
    Hybrid](https://docs.gitlab.com/ee/administration/reference_architectures/#cloud-native-hybrid
    "Cloud Native Hybrid"). 


    GitLab propose également une méthode d'intégration via le [chart Helm
    GitLab](https://docs.gitlab.com/charts/ "Chart Helm GitLab"). Pour les
    installations basées sur OpenShift, optez pour l'[opérateur
    GitLab](https://docs.gitlab.com/operator/installation.html "Opérateur
    GitLab"). Les valeurs par défaut du chart Helm GitLab et de l'opérateur
    GitLab ne sont pas recommandées pour une utilisation en production. Avec ces
    valeurs, une instance GitLab est créée où tous les services, y compris les
    données persistantes, sont déployés dans un cluster Kubernetes. Pour plus
    d'informations sur ces limitations, [consultez notre
    documentation](https://docs.gitlab.com/ee/administration/reference_architectures/#stateful-components-in-kubernetes).
    Pour une utilisation en production, il est recommandé de se baser sur
    l’architecture de référence Cloud Native Hybrid. 


    Notre tutoriel touche à sa fin ! Pour connaître toutes les méthodes
    d’installation de GitLab Self-Managed, consultez notre [page
    d’installation](https://about.gitlab.com/fr-fr/install/ "Installation GitLab
    Self-Managed"). 


    #### Ressources utiles


    - [Architectures de référence de
    GitLab](https://docs.gitlab.com/ee/administration/reference_architectures/#available-reference-architectures
    "Architectures de référence de GitLab")

    - [GitLab Environment
    Toolkit](https://gitlab.com/gitlab-org/gitlab-environment-toolkit "GitLab
    Environment Toolkit")


    ## FAQ sur l'installation de GitLab


    ### Sur quel système d'exploitation Linux peut-on installer GitLab ?


    GitLab est disponible sur les environnements Linux suivants : Ubuntu,
    Debian, AlmaLinux, CentOS 7, OpenSUSE Leap, Amazon Linux, et Raspberry Pi.
    Le paquet Debian natif et le paquet Arch Linux sont des méthodes
    d'installation alternatives pour GitLab, mais elles ne sont pas officielles
    et ne sont pas prises en charge.


    ### Comment mettre à jour GitLab ?


    Consultez la [page de mise à jour de
    GitLab](https://about.gitlab.com/fr-fr/update/ "Mettre à jour GitLab") pour
    utiliser notre dernière version. Un tutoriel GitLab est disponible pour les
    différents systèmes d'exploitation, tels que Linux, Kubernetes et Docker. La
    page donne accès à de nombreuses ressources, documentations officielles de
    maintenance et instructions pour une mise à niveau sur une version plus
    puissante.


    ### Comment résoudre un problème d'installation de GitLab ? 


    En cas de question ou de difficulté pour installer GitLab, vous pouvez
    trouver de l'aide en consultant notre
    [documentation](https://docs.gitlab.com/ "Documentation de GitLab"), nos
    [ressources communautaires](https://about.gitlab.com/fr-fr/community/
    "Communauté de GitLab"), notre [forum](https://forum.gitlab.com/ "Forum de
    GitLab") et notre [blog](https://about.gitlab.com/blog/fr-fr/ "Blog de
    GitLab"). Pour des conseils personnalisés sur votre projet, [contactez un
    expert GitLab](https://about.gitlab.com/fr-fr/sales/ "Contacter un expert
    GitLab").
  category: Produit
  tags:
    - DevSecOps platform
    - tutorial
config:
  slug: how-to-install-gitlab-beginners-guide
  featured: false
  template: BlogPost
