seo:
  title: Aide
  description: Obtenez des informations sur l'assistance technique, la mise à jour de votre instance GitLab, les propositions de fonctionnalités et le suivi des bogues.
content:
  - componentName: CommonSimpleHero
    componentContent:
      header: Thèmes du centre d'aide
      config:
        hasHorizontalRule: true
        centered: true
  - componentName: GetHelpMenu
    componentContent:
      navigationItems:
        - text: Assistance du compte
          id: 'account-support'
        - text: Contributions
          id: 'contributing'
        - text: Propositions de fonctionnalités
          id: 'feature-proposals'
        - text: Références
          id: 'references'
        - text: Licences et abonnements
          id: 'licensing-and-subscriptions'
        - text: Bogues reproductibles
          id: 'reproducible-bugs'
        - text: Autres ressources à discuter
          id: 'other-resources-for-discussion'
        - text: Sécurité
          id: 'security'
        - text: Assistance technique
          id: 'technical-support'
        - text: Mises à jour
          id: 'updating'
      slotContent:
        - componentName: CommonCopyMedia
          componentContent:
            header: Assistance du compte
            text: >-
              Si vous n'avez pas reçu votre e-mail de confirmation, vous pouvez demander que vos instructions de confirmation vous soient renvoyées via notre [page de confirmation](https://gitlab.com/users/confirmation/new){data-ga-name="confirmation page" data-ga-location="body"}.
            config:
              metadataId: account-support
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Contributions
            text: >-
              Nous voulons faciliter au maximum la tâche des utilisateurs de GitLab pour qu'ils deviennent des contributeurs de GitLab. C'est pourquoi nous avons créé ce guide de contribution pour vous aider à vous lancer. Vous trouverez des [instructions étape par étape pour contribuer au développement, à la documentation, à la traduction et au design](https://about.gitlab.com/community/contribute/){data-ga-name="issues" data-ga-location="contributing"}.
            config:
              metadataId: contributing
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Propositions de fonctionnalités
            text: >-
              Les propositions de fonctionnalités doivent être soumises dans le  [gestionnaire de suivi des tickets](https://gitlab.com/gitlab-org/gitlab/issues){data-ga-name="issue tracker" data-ga-location="feature proposals"}.

              Veuillez lire les [directives relatives aux contributions liées aux propositions de fonctionnalités](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#feature-proposals) avant toute publication sur le gestionnaire de suivi des tickets et utiliser le modèle de ticket intitulé « Proposition de fonctionnalité ».
            config:
              metadataId: feature-proposals
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Références
            text: >-
              * [Documentation GitLab](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} : pour documenter toutes les applications GitLab.

              * [Forum GitLab](https://forum.gitlab.com/){data-ga-name="forum" data-ga-location="references"} : pour demander directement de l'aide à la communauté.

              * [GitLab University](https://university.gitlab.com/){data-ga-name="university" data-ga-location="body"} : contient une variété de ressources pour se former sur Git et GitLab.

              * [GitLab Cookbook](https://www.packtpub.com/product/gitlab-cookbook/9781783986842) : écrit par Jeroen van Baarsen, membre de la Core Team, c'est le livre le plus complet sur GitLab.

              * [Recettes GitLab](https://gitlab.com/gitlab-org/gitlab-recipes){data-ga-name="recipes" data-ga-location="body"} : une série de guides non officiels pratiques dans le cas de logiciels non empaquetés en conjonction avec GitLab.

              * [Learn Enough Git to Be Dangerous par Michael Hartl](http://www.learnenough.com/git-tutorial) : excellente introduction au contrôle de version et à Git.

              * La deuxième version du [livre Pro Git](http://git-scm.com/book/en/v2) comporte une [section sur GitLab](http://git-scm.com/book/en/v2/Git-on-the-Server-GitLab).

              * Le [livre](http://shop.oreilly.com/product/0636920034520.do)
              d'O'Reilly Media « Git for teams » contient un chapitre sur GitLab. Des [vidéos](http://shop.oreilly.com/product/0636920034872.do?code=WKGTVD) à propos de Git et GitLab sont également disponibles. Il existe également une [vidéo gratuite sur la création d'un compte GitLab](https://www.oreilly.com/library/view/collaborating-with-git/9781491916162/video194077.html).

              * [Chaîne YouTube GitLab](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) : pour regarder des vidéos expliquant les fonctionnalités et options d'installation de GitLab.
            config:
              metadataId: references
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Licences et abonnements
            text: >-
              [FAQ sur les achats, les renouvellements, les paiements et les utilisateurs facturables](/pricing/licensing-faq/){data-ga-name="FAQ about purchasing" data-ga-location="body"}
            config:
              metadataId: licensing-and-subscriptions
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Bogues reproductibles
            text: >-
              Les rapports de bogues doivent être soumis dans le [gestionnaire de suivi des tickets](https://gitlab.com/gitlab-org/gitlab/issues){data-ga-name="issue tracker" data-ga-location="reproducible bugs"}.

              Veuillez lire les [directives relatives aux contributions liées aux propositions de fonctionnalités](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#bugs) avant toute publication sur le gestionnaire de suivi des tickets et utiliser le modèle de ticket intitulé « Bogue ».
            config:
              metadataId: reproducible-bugs
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Autres ressources de discussion gérées par la communauté
            text: >-
              * [Forum communautaire GitLab](https://forum.gitlab.com/){data-ga-name ="forum"data-ga-location ="references"} : le meilleur endroit pour discuter.

              * [Communauté Discord GitLab](https://discord.gg/gitlab) : une communauté Discord pour les contributeurs et pour le soutien à la communauté.

              * [Reddit](https://www.reddit.com/r/gitlab/)

              * [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab) : vous pouvez rechercher des problèmes similaires avant de publier votre ticket, car il y a de fortes chances qu'un autre utilisateur ait rencontré le même problème que vous et ait déjà trouvé une solution.

              * [Canal #gitlab Libera IRC](https://web.libera.chat/#gitlab) : canal Libera pour entrer en contact avec d'autres utilisateurs de GitLab et obtenir de l'aide.
            config:
              metadataId: other-resources-for-discussion
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Sécurité
            text: >-
              * [La page de divulgation responsable décrit](/security/disclosure/){data-ga-name="security disclosure" data-ga-location="body"} comment contacter GitLab pour signaler des vulnérabilités de sécurité et d'autres informations relatives à la sécurité.

              * [La section Sécurité de la documentation](https://docs.gitlab.com/ee/security/){data-ga-name="security documentation" data-ga-location="body"} répertorie les solutions pour sécuriser davantage votre instance GitLab.

              * [La page Confiance et Sécurité](/handbook/engineering/security/security-operations/trustandsafety/#how-to-report-abuse){data-ga-name="trust and safety" data-ga-location="body"} décrit comment contacter GitLab pour signaler des abus sur la plateforme, notamment des pages d'hameçonnage, des logiciels malveillants et des requêtes DMCA.

              * [Le Centre de confiance GitLab](/security/){data-ga-location = "trust center" data-ga-location = "body"} fournit nos FAQ sur la sécurité et la confidentialité, ainsi que le moyen de contacter notre équipe de sécurité.
            config:
              metadataId: security
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Assistance technique
            text: >-
              Pour savoir où obtenir une assistance technique et avec quel niveau de service, veuillez consulter la [page dédiée à l'assistance](/support/){data-ga-name="support" data-ga-location="body"}.
            config:
              metadataId: technical-support
              headerIntoSeperateColumn: true
        - componentName: CommonCopyMedia
          componentContent:
            header: Mises à jour
            text: >-
              * [Page des mises à jour GitLab](/update/){data-ga-name="update" data-ga-location="body"} : ressources et informations pour vous aider à mettre à jour votre instance GitLab.

              * [Politique de maintenance logicielle](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/MAINTENANCE.md){data-ga-name="maintanance policy" data-ga-location="body"} : précise les versions prises en charge.
            config:
              metadataId: updating
              headerIntoSeperateColumn: true
