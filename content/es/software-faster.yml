seo:
  title: 'SEO más rápido en software'
  description: 'GitLab simplifica sus DevSecOps para que pueda centrarse en lo que realmente importa. Más información'
config:
  enableAnimations: true
content:
  - componentName: SoftwareFasterHero
    componentContent:
      hero:
        titleHighlight: 'Desarrollo 7 veces más rápido'
        secondaryButton:
          text: '¿Qué es GitLab?'
          config:
            dataGaName: 'what is gitlab'
            dataGaLocation: 'hero'
      video:
        text: '¿Qué es GitLab?'
        config:
          videoUrl: 'https://player.vimeo.com/video/799236905?h=4eee39a447&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
          videoThumbnail: 'images/software-faster-thumbnail.jpg'
          dataGaName: 'what is gitlab'
          dataGaLocation: 'hero'
      backgroundImage:
        altText: 'Hero image for software faster page.'
        config:
          src: '/images/backgrounds/software-faster-hero.svg'
      config:
        highlightText: true
  - componentName: CommonFeatureCards
    componentContent:
      header: 'Mejor juntos: Los clientes envían el software más rápido con GitLab'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '800'
      caseStudies:
        - header: 'La transición rápida y fluida de Nasdaq a la nube'
          description: 'Nasdaq tiene la visión de convertirse al 100 % en la nube. Se están asociando con GitLab para lograrlo.'
          showcaseImg:
            altText: 'Realistic cloud with up and down arrows'
            config:
              url: '/images/nasdaq-showcase.png'
          logoImg:
            altText: 'Nasdaq logo'
            config:
              url: '/images/logos/nasdaq.svg'
          link:
            text: 'Mire el video'
            config:
              dataGaName: 'nasdaq'
              dataGaLocation: 'body'
              videoUrl: 'https://player.vimeo.com/video/767082285?h=e76c380db4'
              videoThumbnail: 'images/software-faster-thumbnail.jpg'
        - header: 'Logre un tiempo de implementación 5 veces más rápido'
          description: 'Hackerone mejoró el tiempo de pipeline, la velocidad de implementación y la eficiencia del desarrollador con GitLab Ultimate.'
          showcaseImg:
            altText: 'Person with multiple screens of code'
            config:
              url: '/images/hackerone-showcase.png'
          logoImg:
            altText: 'Hackerone logo'
            config:
              url: '/images/logos/hackerone-bw.png'
          link:
            text: 'Más información'
            config:
              href: '/es/customers/hackerone/'
              dataGaName: 'hackerone'
              dataGaLocation: 'body'
        - header: 'Lanzamiento de funcionalidades 144 veces más rápido'
          description: 'Airbus Intelligence mejoró su flujo de trabajo y la calidad del código con CI de aplicación única.'
          showcaseImg:
            altText: 'Plane poking out of hangar'
            config:
              url: '/images/airbus-showcase.png'
          logoImg:
            altText: 'Airbus logo'
            config:
              url: '/images/customer_logos/airbus.png'
          link:
            text: 'Lea su historia'
            config:
              href: '/es/customers/airbus/'
              dataGaName: 'airbus'
              dataGaLocation: 'body'
  - componentName: SoftwareFasterCustomerLogos
    componentContent:
      title:
        text: 'Estas empresas confían en nosotros:'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '1600'
          offset: '200'
      logos:
        - altText: 'Tmobile'
          config:
            src: '/images/customer_logos/deutsche-telekom-color-logo.jpg'
            href: '/es/customers/deutsche-telekom/'
        - altText: 'Goldman Sachs logo'
          config:
            src: '/images/customer_logos/goldman-sachs.svg'
            href: '/es/customers/goldman-sachs/'
        - altText: 'Siemens Color logo'
          config:
            src: '/images/customer_logos/siemens-color-logo.png'
            href: '/es/customers/siemens/'
        - altText: 'Nvidia logo'
          config:
            src: '/images/customer_logos/nvidia-color-logo.svg'
            href: '/es/customers/nvidia/'
        - altText: 'UBS logo'
          config:
            src: '/images/customer_logos/ubs-logo-black.svg'
            href: '/customers/ubs/'
  - componentName: SoftwareFasterFeatureCards
    componentContent:
      hero:
        title: 'GitLab reúne todo el ciclo de vida de desarrollo del software en una única aplicación con tecnología de IA'
        secondaryButton:
          text: 'Más información'
          config:
            href: '/es/platform/'
            dataGaName: 'platform'
            dataGaLocation: 'hero'
        image:
          altText: 'The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).'
          config:
            src: '/images/devsecops-infinity-shield.svg'
      cards:
        - title: 'Mejor información'
          description: 'Visibilidad integral en todo el ciclo de vida de la entrega de software.'
          config:
            icon: 'CaseStudyAlt'
        - title: 'Mayor eficiencia'
          description: 'Asistencia integrada para automatización e integraciones con servicios de terceros.'
          config:
            icon: 'Principles'
        - title: 'Colaboración mejorada'
          description: 'Un flujo de trabajo que une a los equipos de desarrollo, seguridad y operaciones.'
          config:
            icon: 'Roles'
        - title: 'Tiempo hasta que se ofrece valor al usuario más rápido'
          description: 'Mejora continua a través de ciclos de comentarios acelerados.'
          config:
            icon: 'Verification'
  - componentName: CommonCaseStudies
    componentContent:
      charcoalBg: true
      title: 'Llegue a la misma página más rápido: Explore los recursos de DevSecOps'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '500'
      rows:
        - title: 'Informe de la encuesta 2024 de DevSecOps'
          subtitle: 'Le pedimos a más de 5000 profesionales de DevSecOps que compartieran su opinión sobre el estado de la seguridad, la IA y mucho más.'
          button:
            text: 'Lea el informe'
            config:
              href: '/developer-survey/'
              dataGaName: 'developer survey'
              dataGaLocation: 'body'
          image:
            alt: '2024 developer survey image'
            config:
              url: '/images/2024-devsecops-survey.png'
          config:
            icon:
              name: 'DocPencilAlt'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
        - title: 'La cadencia lo es todo: Organizaciones de ingeniería de alto rendimiento para ingenieros de alto rendimiento'
          subtitle: 'El CEO y cofundador de GitLab, Sid Sijbrandij, habla sobre la importancia de la cadencia en las organizaciones de ingeniería.'
          button:
            text: Lea el blog
            config:
              href: '/blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/'
              dataGaName: 'cadence '
              dataGaLocation: 'body'
          image:
            alt: 'People running race in city street'
            config:
              url: '/images/athlinks_running.jpg'
          config:
            icon:
              name: 'Blog'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
  - componentName: CommonNextSteps
