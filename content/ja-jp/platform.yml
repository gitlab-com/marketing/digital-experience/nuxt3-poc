seo:
  title: プラットフォーム
  description: GitLabプラットフォームがどのようにチームの連携とソフトウェアの構築を高速化するのに役立つかについての詳細をご覧ください。
config:
  enableAnimations: true
content:
  - componentName: CommonHero
    componentContent:
      tagline: 最も包括的な
      titleHighlight: AIを活用した
      title: DevSecOpsプラットフォーム
      description: ソフトウェア開発ライフサイクル全体に対応する単一のプラットフォームにより、高品質なソフトウェアの迅速なデリバリーを実現します。
      image:
        altText: The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).
        config:
          src: '/images/loop-shield-duo.svg'
      secondaryButton:
        text: 無料トライアルを開始
        config:
          href: 'https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com/ja-jp/platform&glm_content=default-saas-trial/'
          dataGaName: free trial
          dataGaLocation: hero
      tertiaryButton:
        text: 価格を確認
        config:
          href: /ja-jp/pricing/
          dataGaName: pricing
          dataGaLocation: hero
      config:
        theme: dark
  - componentName: CommonTable
    componentContent:
      columns:
        - name: '計画'
          config:
            href: '/ja-jp/solutions/visibility-measurement/'
          icon:
            altText: 'Calendar'
            config:
              name: 'PlanAlt2'
          features:
            - name: 'DevOpsレポート'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#devops-reports'
            - name: 'DORAメトリクス'
              config:
                href: '/ja-jp/solutions/value-stream-management/dora/'
            - name: 'バリューストリーム管理'
              config:
                href: '/ja-jp/solutions/value-stream-management/'
            - name: 'Pages'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#pages'
            - name: 'Wiki'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#wiki'
            - name: 'ポートフォリオ管理'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#portfolio-management'
            - name: 'チーム計画'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#team-planning'
            - name: 'イシューの説明の生成'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#generate-issue-description'
            - name: 'ディスカッションの概要'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#discussion-summary'
          replacement:
            text: Replacement for
            product: Sentry
        - name: 'ソースコード管理'
          config:
            href: '/ja-jp/solutions/delivery-automation/'
          icon:
            altText: 'Cog'
            config:
              name: 'CogCode'
          features:
            - name: 'リモート開発'
              config:
                href: '/ja-jp/solutions/delivery-automation/#remote-development'
            - name: 'ソースコード管理'
              config:
                href: '/ja-jp/solutions/source-code-management/'
            - name: 'Web IDE'
              config:
                href: '/ja-jp/solutions/delivery-automation/#web-ide'
            - name: 'GitLab CLI'
              config:
                href: '/ja-jp/solutions/delivery-automation/#gitlab-cli'
            - name: 'コードレビューワークフロー'
              config:
                href: '/ja-jp/solutions/delivery-automation/#code-review-workflow'
            - name: 'コード提案'
              config:
                href: '/ja-jp/solutions/delivery-automation/#code-suggestions'
            - name: 'コードの説明'
              config:
                href: '/ja-jp/solutions/delivery-automation/#code-explanation'
            - name: 'コードレビューの概要'
              config:
                href: '/ja-jp/solutions/delivery-automation/#code-review-summary'
            - name: 'テスト生成'
              config:
                href: '/ja-jp/solutions/delivery-automation/#test-generation'
            - name: 'マージリクエストテンプレート作成'
              config:
                href: 'https://docs.gitlab.com/ee/user/gitlab_duo/index.html/#merge-request-template-population'
          replacement:
            text: Replacement for
            product: GitHub
        - name: '継続的インテグレーション'
          config:
            href: '/ja-jp/solutions/delivery-automation/'
          icon:
            altText: 'Automation'
            config:
              name: 'AutomatedCodeAlt'
          features:
            - name: '秘密管理'
              config:
                href: '/ja-jp/solutions/delivery-automation/#secrets-management'
            - name: 'Review Apps'
              config:
                href: '/ja-jp/solutions/delivery-automation/#review-apps'
            - name: 'コードテストとカバレッジ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#code-testing-and-coverage'
            - name: 'マージトレイン'
              config:
                href: '/ja-jp/solutions/delivery-automation/#merge-trains'
            - name: '推奨レビュアー'
              config:
                href: '/ja-jp/solutions/delivery-automation/#suggested-reviewers'
            - name: 'マージリクエストの概要'
              config:
                href: '/ja-jp/solutions/delivery-automation/#merge-request-summary'
            - name: '主要因分析'
              config:
                href: '/ja-jp/solutions/delivery-automation/#root-cause-analysis'
            - name: 'ディスカッションの概要'
              config:
                href: '/ja-jp/solutions/delivery-automation/#discussion-summary'
          replacement:
            text: Replacement for
            product: Jenkins
        - name: 'セキュリティ'
          config:
            href: '/ja-jp/solutions/security-compliance/'
          icon:
            altText: 'Lock'
            config:
              name: 'SecureAlt2'
          features:
            - name: 'コンテナのスキャン'
              config:
                href: '/ja-jp/solutions/security-compliance/#container-scanning'
            - name: 'ソフトウェアコンポジション解析 (SCA) '
              config:
                href: '/ja-jp/solutions/security-compliance/#software-composition-analysis'
            - name: 'APIセキュリティ'
              config:
                href: '/ja-jp/solutions/security-compliance/#api-security'
            - name: 'カバレッジファジング'
              config:
                href: '/ja-jp/solutions/security-compliance/#fuzz-testing'
            - name: 'DAST'
              config:
                href: '/ja-jp/solutions/security-compliance/#dast'
            - name: 'Code Quality'
              config:
                href: '/ja-jp/solutions/security-compliance/#code-quality'
            - name: 'シークレット検出'
              config:
                href: '/ja-jp/solutions/security-compliance/#secret-detection'
            - name: 'SAST'
              config:
                href: '/ja-jp/solutions/security-compliance/#sast'
            - name: '脆弱性の説明'
              config:
                href: '/ja-jp/solutions/security-compliance/#vulnerability-explanation'
            - name: '脆弱性の修正'
              config:
                href: '/ja-jp/solutions/security-compliance/#vulnerability-resolution'
          replacement:
            text: Replacement for
            product: Snyk
        - name: 'コンプライアンス'
          icon:
            altText: 'Shield'
            config:
              name: 'ProtectAlt2'
          config:
            href: '/ja-jp/solutions/security-compliance/'
          features:
            - name: 'リリースエビデンス'
              config:
                href: '/ja-jp/solutions/security-compliance/#release-evidence'
            - name: 'コンプライアンス管理'
              config:
                href: '/ja-jp/solutions/security-compliance/#compliance-management'
            - name: '監査イベント'
              config:
                href: '/ja-jp/solutions/security-compliance/#audit-events'
            - name: 'ソフトウェア部品表'
              config:
                href: '/ja-jp/solutions/security-compliance/#software-bill-of-materials'
            - name: '依存管理'
              config:
                href: '/ja-jp/solutions/security-compliance/#dependency-management'
            - name: '脆弱性管理'
              config:
                href: '/ja-jp/solutions/security-compliance/#vulnerability-management'
            - name: 'セキュリティポリシー管理'
              config:
                href: '/ja-jp/solutions/security-compliance/#security-policy-management'
        - name: 'アーティファクトレジストリ'
          config:
            href: '/ja-jp/solutions/delivery-automation/'
          icon:
            altText: 'Code'
            config:
              name: 'PackageAlt2'
          features:
            - name: '仮想レジストリ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#virtual-registry'
            - name: 'コンテナレジストリ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#container-registry'
            - name: 'Helm Chartレジストリ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#helm-chart-registry'
            - name: 'パッケージレジストリ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#package-registry'
          replacement:
            text: Replacement for
            product: JFrog
        - name: '継続的デリバリー'
          icon:
            altText: 'Continuous'
            config:
              name: 'ContinuousDeliveryAlt'
          config:
            href: '/ja-jp/solutions/delivery-automation/'
          features:
            - name: 'リリースオーケストレーション'
              config:
                href: '/ja-jp/solutions/delivery-automation/#release-orchestration'
            - name: 'Infrastructure as Code'
              config:
                href: '/ja-jp/solutions/delivery-automation/#infrastructure-as-code'
            - name: '機能フラグ'
              config:
                href: '/ja-jp/solutions/delivery-automation/#feature-flags'
            - name: '環境管理'
              config:
                href: '/ja-jp/solutions/delivery-automation/#environment-management'
            - name: 'デプロイ管理'
              config:
                href: '/ja-jp/solutions/delivery-automation/#deployment-management'
            - name: 'Auto DevOps'
              config:
                href: '/ja-jp/solutions/delivery-automation/'
          replacement:
            text: Replacement for
            product: Harness
        - name: 'オブザーバビリティ'
          icon:
            altText: 'Monitor'
            config:
              name: 'MonitorAlt2'
          config:
            href: '/ja-jp/solutions/visibility-measurement/'
          features:
            - name: 'サービスデスク'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#service-desk'
            - name: 'オンコールのスケジュール管理'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#oncall-schedule-management'
            - name: 'インシデント管理'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#incident-management'
            - name: 'エラー追跡'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#error-tracking'
            - name: 'プロダクト分析の可視化'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#product-analytics-visualization'
            - name: 'バリューストリーム予測'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#value-stream-forecasting'
            - name: 'AIプロダクト分析'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#ai-product-analytics'
            - name: 'Metrics'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#metrics'
            - name: 'Distributed Tracing'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#distributed-tracing'
            - name: 'Logs'
              config:
                href: '/ja-jp/solutions/visibility-measurement/#logs'
          replacement:
            text: Replacement for
            product: Sentry
  - componentName: PlatformDevSecOpsTabs
    componentContent:
      header:
        highlighted: 一元化されたプラットフォームにより
        text: デベロッパー、セキュリティ担当者、運用チームをサポート
      image:
        altText: code source image
        config:
          src: '/images/one-platform.svg'
      tabs:
        - tabButtonText: 開発
          config:
            dataGaName: development
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: AI駆動のワークフロー
                content: 計画やコード作成からテスト、セキュリティ、モニタリングまで、ソフトウェア開発ライフサイクルのすべての段階で、AIの助けを借りて、すべてのユーザーの効率性を高め、サイクルタイムを短縮します。
                config:
                  darkMode: true
                primaryCtas:
                  - text: GitLab Duo
                    config:
                      href: '/ja-jp/gitlab-duo/'
                      dataGaName: GitLab Duo
                      dataGaLocation: body
                secondaryCtaHeader: 'See it in action:'
                secondaryCtas:
                  - text: GitLab Duo
                    config:
                      href: 'https://player.vimeo.com/video/855805049/'
                      dataGaName: GitLab Duo
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: コード提案
                    config:
                      href: 'https://player.vimeo.com/video/894621401/'
                      dataGaName: Code Suggestions
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: チャット
                    config:
                      href: 'https://player.vimeo.com/video/927753737/'
                      dataGaName: Chat
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: 単一のアプリケーション
                content: GitLabでは、DevSecOpsのあらゆる機能が統合データストアを備えた単一のアプリケーションにまとまっているため、すべてが一元化されています。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLabでのDORAメトリクスの使用に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/892023781/'
                      dataGaName: DORA metrics - User Analytics
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLabのバリューストリームダッシュボードに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/819308062?h=752d064728&badge=0&autopause=0&player_id=0&app_id=58479/'
                      dataGaName: GitLab's Value Streams Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: デベロッパーの生産性向上
                content: 単一アプリケーションとして提供されるGitLabは、優れたユーザーエクスペリエンスを通じてサイクルタイムを改善し、頭の切り替えがなるべく発生しないようにします。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLabのポートフォリオ管理に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925629920/'
                      dataGaName: GitLab's Portfolio Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLabのOKR管理に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925632272/'
                      dataGaName: GitLab's OKR Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLabのイシューへのデザインのアップロードに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925633691/'
                      dataGaName: Design Uploads to GitLab issues
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: より進化した自動化
                content: GitLabには、信頼できる自動化ツールが備わっています。豊富な機能を活用できるため、認知負荷や不必要な単調作業をなくせます。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLabのCDの概要に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/892023715/'
                      dataGaName: GitLab's CD Overview
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: エラー追跡に関するドキュメント
                    config:
                      href: 'https://docs.gitlab.com/ee/operations/error_tracking.html'
                      dataGaName: Error tracking documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: インシデント管理に関するドキュメント
                    config:
                      href: https://docs.gitlab.com/ee/operations/incident_management/
                      dataGaName: Incident management documentation
                      dataGaLocation: body
                      iconName: Docs
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: 「戦略をスコープとコードに結びつけるというGitLabのビジョンは非常に優れています。 そのビジョンを実現するために、GitLabが積極的にプラットフォームの開発と改善に取り組んでいることに感謝しています
                metrics:
                  - number: 15万ドル
                    text: 年間のおおよその削減コスト
                  - number: 20時間
                    text: プロジェクト単位で節約した時間数
                author:
                  name: Jason Monoharan
                  title: テクノロジー部門副社長
                  company: Iron Mountain
                cta:
                  text: 事例を読む
                  config:
                    href: '/ja-jp/customers/iron-mountain/'
                    dataGaName: iron mountain case study
                    dataGaLocation: body
                config:
                  logo: /images/customer_logos/iron-mountain-logo-white.svg
                  headshot: /images/headshots/jason-monoharan-headshot.png
          featuredCta:
            boxText:
              title: GitLab DuoでAIの力を
              highlight: 活用しましょう
              text: 詳細はこちら
            config:
              href: '/ja-jp/gitlab-duo/'
              dataGaName: GitLab Duo
              dataGaLocation: body
        - tabButtonText: セキュリティ
          config:
            dataGaName: security
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: ビルトインセキュリティなので追加は不要
                content: GitLabのセキュリティ機能 (DAST、ファジング、コンテナのスキャン、APIスクリーニングなど) は、エンドツーエンドで統合されています。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: 動的アプリケーションセキュリティテスト (DAST) に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925635707/'
                      dataGaName: Dynamic Application Security Testing (DAST) video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: コンテナのスキャンに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925676815/'
                      dataGaName: Container scanning video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: APIセキュリティとWeb APIファジングに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925677603/'
                      dataGaName: API security and web API Fuzzing video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: コンプライアンスと的確なポリシー管理
                content: GitLabは、チーム間の職務分掌を実現する包括的なガバナンスソリューションを提供します。GitLabのポリシーエディターを使用すると、各組織のコンプライアンス要件に合わせてカスタマイズされた承認ルールを適用できるため、リスクが軽減されます。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: コンプライアンス管理に関するドキュメント
                    config:
                      href: '/ja-jp/solutions/security-compliance/#compliance-management'
                      dataGaName: Compliance Management documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: GitLabのコンプライアンスフレームワークに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925679314/'
                      dataGaName: GitLab's Compliance Frameworks
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLabの要求事項管理に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925679982/'
                      dataGaName: GitLab's Requirements Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: セキュリティの自動化
                content: GitLabの高度な自動化ツールは、コードの脆弱性を確実かつ自動的にスキャンすると同時に、ガードレールを実装して迅速な開発を実現します。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLabのセキュリティダッシュボードに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/925680640/'
                      dataGaName: GitLab's Security Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: GitLabはデベロッパーのワークフローに統合されており、おかげでセキュリティ上の欠陥を早期に発見できています。 エンジニアがコードをGitLab CIにプッシュすると、数多くある段階的な監査ステップで即座にフィードバックを受け取ることができ、セキュリティの脆弱性の有無を確認できます。さらに、独自の新しいステップを構築して特定のセキュリティの問題に特化したテストを行うことも可能です"
                metrics:
                  - number: '7.5倍'
                    text: パイプラインの処理時間を短縮
                  - number: '4時間'
                    text: エンジニア1人あたりの開発時間を短縮
                author:
                  name: Mitch Trale氏
                  title: Head of Infrastructure
                  company: Hackerone
                cta:
                  text: 事例を読む
                  config:
                    href: '/ja-jp/customers/hackerone/'
                    dataGaName: hackerone case study
                    dataGaLocation: body
                config:
                  logo: /images/customer_logos/hackerone-logo-white.svg
          featuredCta:
            boxText:
              title: CIパイプラインにセキュリティスキャンを追加する方法を
              highlight: ご覧ください
              text: デモを開始
            config:
              dataGaName: ci pipeline
              dataGaLocation: body
              modal: true
              iconName: LaptopVideo
            demo:
              subtitle: CI/CDパイプラインにセキュリティスキャンを追加
              config:
                demoHref: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
                videoFallbackHref: https://player.vimeo.com/video/892023806
              scheduleButton:
                text: Schedule a custom demo
                config:
                  href: '/ja-jp/sales/'
                  dataGaName: demo
                  dataGaLocation: body
        - tabButtonText: オペレーション
          config:
            dataGaName: operations
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: エンタープライズ規模のワークロードにスケールアウト可能
                content: GitLabには、ダウンタイムがほぼなしで管理およびアップグレードできる機能が備わっており、あらゆる規模のエンタープライズを無理なくサポートします。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: Infrastructure as Code (IaC) に関するドキュメント
                    config:
                      href: https://docs.gitlab.com/ee/user/infrastructure/iac/
                      dataGaName: Infrastructure as code (IaC) documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: インシデント管理に関するドキュメント
                    config:
                      href: https://docs.gitlab.com/ee/operations/incident_management/
                      dataGaName: Incident management documentation
                      dataGaLocation: body
                      iconName: Docs
              - header: 他に類を見ないメトリクスの可視化
                content: GitLabの統一されたデータストアにより、ソフトウェア開発ライフサイクル全体の分析を1か所で行うことができるため、ほかの製品を後から統合する必要がなくなります。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLabでのDORAメトリクスの使用に関する動画
                    config:
                      href: 'https://player.vimeo.com/video/892023781/'
                      dataGaName: DORA metrics - User Analytics
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLabのバリューストリームダッシュボードに関する動画
                    config:
                      href: 'https://player.vimeo.com/video/819308062/'
                      dataGaName: GitLab's Value Streams Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: クラウドネイティブ、マルチクラウド、既存のシステムに対応
                content: GitLabは包括的なDevSecOpsプラットフォームを提供するため、異なるインフラストラクチャ環境を使用している場合も、同じ生産性メトリクスを使用し、同じガバナンスを適用できます。
                config:
                  darkMode: true
                secondaryCtas:
                  - text: マルチクラウドに関するドキュメント
                    config:
                      href: /ja-jp/topics/multicloud/
                      dataGaName: Multicloud documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: GitOpsに関するドキュメント
                    config:
                      href: /ja-jp/solutions/gitops/
                      dataGaName: GitOps documentation
                      dataGaLocation: body
                      iconName: Docs
              - header: 総所有コストの削減
                secondaryCtas:
                  - text: Lockheed Martin社の事例
                    description: '世界最大の防衛関連企業が、GitLabでツールチェーンの縮小、製造プロセスの高速化、セキュリティの向上を行っている事例をご覧ください。'
                    config:
                      href: /ja-jp/customers/lockheed-martin/
                      dataGaName: Lockheed Martin case study
                      dataGaLocation: body
                      iconName: Docs
                  - text: CARFAX社の事例
                    description: 'CARFAX社がGitLabを使用して、自社のDevSecOpsツールチェーンの縮小、セキュリティの向上を実現した方法についてご覧ください。'
                    config:
                      href: /ja-jp/customers/carfax/
                      dataGaName: CARFAX case study
                      dataGaLocation: body
                      iconName: Docs
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: 開発効率とデリバリー効率が87%以上向上し、結果として2,300 万ドル以上ものコストの削減を実現できています。 各企業はGitLabを利用することで、DevOpsライフサイクル全体の各フェーズの処理時間を大幅に短縮できました
                metrics:
                  - number: 2億50万ドル
                    text: 3年間で得られた利益
                  - number: 427%
                    text: 総投資収益率 (ROI )
                cta:
                  text: 事例を読む
                  config:
                    href: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                    dataGaName: resources study forrester
                    dataGaLocation: body
                config:
                  logo: /images/logos/forrester-logo.svg
          featuredCta:
            boxText:
              title: 現在のツールチェーンの費用を
              highlight: 確認しましょう
              text: ROI計算ツールを試す
            config:
              href: /ja-jp/calculator/
              dataGaName: try our roi calculator
              dataGaLocation: body
  - componentName: CommonVideoSpotlight
    componentContent:
      title: 開発速度を上げたいですか？今すぐツールチェーンを統合しましょう
      benefits:
        - text: コラボレーションの改善
        - text: 管理者の負担の軽減
        - text: セキュリティの向上
        - text: 総保有コストの削減
        - text: シームレスなスケーリング
      cta: |
        **どこから始めればよいかわからない場合は、**
        弊社のセールスチームがお手伝いします。
      button:
        text: セールスチームに問い合わせる
        config:
          externalUrl: /sales/
          dataGaName: sales
          dataGaLocation: body
      video:
        buttonText: 詳細はこちら
        config:
          src: https://player.vimeo.com/video/799236905?h=4eee39a447
          dataGaName: Learn More
          dataGaLocation: body
          thumbnail: images/platform/platform-video-thumbnail.jpg
  - componentName: CommonRecognition
    componentContent:
      title: 業界のトップ企業から信頼されるGitLab
      subtitle: GitLabは、DevOpsカテゴリ全体でG2リーダーとしてランク付けされています。
      badges:
        - altText: G2 - Winter 2024 - Leader
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/b3632635c9e3bf13a2d02ada6374029b/DevOpsPlatforms_Leader_Leader.png
        - altText: G2 - Winter 2024 - Easiest to Use
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/17EE9DHTQpLyGbJAQslSEk/e04bb1e6e71d895af94d10ffb85471e3/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
        - altText: G2 - Winter 2024 - Users love us
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/1SNrMzYNTrOvEnOuNucAMD/be22fc71855a96d9c05627c7085148a9/users-love-us.png
        - altText: G2 - Winter 2024 - Best Usability
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/3UhXS8fSwKieddbliKZEGn/9bd6e04223769aa0869470befd78dc62/ValueStreamManagement_BestUsability_Total.png
        - altText: G2 - Winter 2024 - Leader Europe
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/7AODDnwkskODEsylwXkBys/908eb94a40d8ab2f64bdcacbd18f35a3/DevOpsPlatforms_Leader_Europe_Leader.png
        - altText: G2 - Winter 2024 - Leader Enterprise
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/4xk6CmKqUndyuir0m4rF5b/b069fcc665b3b11c2a017fb36aa55130/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
        - altText: G2 - Winter 2024 - Momentum Leader
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/7jG9DylTxCnElENurGpocF/4241ca192ba6ec8c1d73d07c5a065d76/DevOpsPlatforms_MomentumLeader_Leader.png
        - altText: G2 - Winter 2024 - Leader Americas
          config:
            src: https://images.ctfassets.net/xz1dnu24egyd/I7XhKHEf8rB7o1zYwYyBf/0b62ae05ecb749e83cd453de13972482/DevOps_Leader_Americas_Leader.png
      cards:
        - description: '『2024 Gartner® Magic Quadrant™ for DevOps Platforms』でGitLabがリーダーの1社として認定'
          image:
            altText: gartner logo
            config:
              src: /images/logos/gartner.svg
          button:
            text: レポートを読む
            config:
              href: '/ja-jp/gartner-magic-quadrant/'
              dataGaName: gartner
              dataGaLocation: analyst
        - description: '『Forrester Wave™: 2023年度第二四半期統合ソフトウエアデリバリープラットフォーム』においてGitLabが唯一のリーダーに認定'
          image:
            altText: forrester logo
            config:
              src: /images/logos/forrester-logo.svg
          button:
            text: レポートを読む
            config:
              href: 'https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023/'
              dataGaName: forrester
              dataGaLocation: analyst
  - componentName: PlatformPricingBlurb
    componentContent:
      icon:
        altText: 'Calendar'
        config:
          name: 'PlanAlt2'
      title: '成長を続けるチームに最適な料金プランを見る'
      button:
        text: 'GitLab Premiumを選ぶ理由'
        config:
          href: /ja-jp/pricing/premium/
          dataGaName: 'why gitlab premium'
          dataGaLocation: 'body'
      shimmerButton:
        text: 'GitLab Ultimateを選ぶ理由'
        config:
          href: /ja-jp/pricing/ultimate/
          dataGaName: 'why gitlab ultimate'
          dataGaLocation: 'body'
  - componentName: CommonNextSteps
