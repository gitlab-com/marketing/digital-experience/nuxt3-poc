seo:
  title: 'DevOpsからDevSecOpsへ: IT Week 2024 春イベントレポート【前編】'
  description: 2024年4月末に東京ビッグサイトで開催されたJapan IT Weekで実施したセミナーの内容を下敷きに、GitLabの最新情報をお伝えします。
  ogTitle: 'DevOpsからDevSecOpsへ: IT Week 2024 春イベントレポート【前編】'
  ogDescription: 2024年4月末に東京ビッグサイトで開催されたJapan IT Weekで実施したセミナーの内容を下敷きに、GitLabの最新情報をお伝えします。
  noIndex: false
  ogImage: images/blog/hero-images/_NYG2319.jpg
  ogUrl: https://about.gitlab.com/blog/event-report-japan-it-week-spring-1
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/event-report-japan-it-week-spring-1
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "DevOpsからDevSecOpsへ: IT Week 2024 春イベントレポート【前編】",
            "author": [{"@type":"Person","name":"GitLab Japan Team"}],
            "datePublished": "2024-07-04",
          }

content:
  title: 'DevOpsからDevSecOpsへ: IT Week 2024 春イベントレポート【前編】'
  description: 2024年4月末に東京ビッグサイトで開催されたJapan IT Weekで実施したセミナーの内容を下敷きに、GitLabの最新情報をお伝えします。
  authors:
    - GitLab Japan Team
  heroImage: images/blog/hero-images/_NYG2319.jpg
  date: '2024-07-04'
  body: >-
    *今回から2回に分けて、2024年4月末に東京ビッグサイトで開催されたJapan IT
    Weekで実施したセミナーの内容を下敷きに、GitLabの最新情報をお伝えします。前編では、DevSecOpsの最新状況とGitLabの価値について、パートナー様やお客様のご意見を取り入れながら紹介していきます。*


    ## DevOpsをより高品質にするPlatform Engineering


    アプリケーション開発のやり方は、テクノロジーの進化とともに変化し、最適化されていきます。変化のスピードは2000年代に入ると加速し、近年はより高まっているように感じられるのは、テクノロジーの進化と多様化が進んでいるためでしょう。その中で、いくつも最新ワードが流行し、消費されることになりますが、時代を象徴する言葉は永く私達の記憶に刻まれることになるでしょう。


    いま、注目を集めているワードは、どのようなものでしょう。[SB C&S株式会社](https://cas.softbank.jp/)の佐藤
    梨花氏は、「Platform Engineering」を取り上げます。Platform
    Engineeringは、開発プラットフォームの硬直化を防ぐ概念。開発効率の高いプラットフォームをいち早く取り入れ、モダン化できる状況を整えることで、開発生産性と開発品質をどちらも向上させようとする考え方です。DevOpsを信頼性の高いものとして安定的に運用するために必要なものとして有名なのはSREですが、SREとは別のアプローチでDevOpsをより高速に回し、最適化し続けるものがPlatform
    Engineeringであると言えばわかりやすいでしょうか。


    Platform
    Engineeringは、GitLabと相性が良いことも好印象です。[CI/CD](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)にSAST/DASTを組み込んだ自動化はもちろん、イシューとエピック、Wikiによる情報共有、そしてプロジェクトの状況を可視化するアナライズ機能など、GitLabはPlatform
    Engineeringの成功に必要なさまざまな機能を備えています。VSM（Value Stream
    Mapping）を使った成果測定手法も確立しつつあり、これからも注目されることになるでしょう。


    DevOpsをうまく回すための手法が、最新のワードとして取り上げられるのは興味深い事実です。DevOpsは、すでに「当たり前の存在」になったと言えるのかもしれません。[アジャイル開発](https://about.gitlab.com/ja-jp/solutions/agile-delivery/)と親和性が高いことで普及が後押しされたという側面はありますが、DevOpsの概念であるDevelopment＝開発とOperation＝運用が連携／協力して、フレキシブルかつスピーディに業務を遂行することで得られる価値が多くの企業で実証されたことも重要なポイントでしょう。


    ![DSC2328_202404JapanITWeekYukiMurakami](//images.ctfassets.net/r9o86ar0p03f/15x5bZZ51rWj8dtBGDNhmn/cbe083d140c266b94672175bf492d70f/_DSC2328.jpg)

    *GitLab合同会社 営業本部 エンタープライズ営業部 シニア アカウントエグゼクティブ 村上 裕紀*


    DevOpsというワードが生まれた当初は、あくまでもコンセプトであり考え方にすぎなかったのですが、それを実現するツール群が提供されるようになったことで、そのコンセプトを実際のプロセスに適用できるようになりました。DevOpsの登場で、「できるだけ早く多くの機能を実装したい」という開発者たちと、「できるだけ安全かつ継続的に運用したい」という運用チームの立場の違いを吸収し、両者が連携して効率的にプロジェクトを進められるようになったのです。


    この流れに乗ってGitLabも進化を続けています。[2023年にガートナーのマジック・クアドラント](https://about.gitlab.com/blog/2023/06/07/gitlab-leader-gartner-magic-quadrant-devops-platforms/)において、DevOpsプラットフォームのリーダーに位置づけられ、フォレスター・リサーチのThe
    Forrester
    Waveでは唯一のリーダーポジションを得ています。開発、テスト、Deployのプロセスを自動化できるだけでなく、DevOpsの全プロセスを網羅する機能群を提供し、それらが有機的に結びついた開発生産性の高いプラットフォームとなっていることが評価されました。


    ## DevOpsにセキュリティを組み込むDevSecOpsの価値


    DevSecOpsという最新のコンセプトは、[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)をDevOpsのプロセスに組み込むものです。[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)／コンプライアンスの重要性が高まり、“説明可能な”[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)が求められるいま、多くの日本企業が注目し、アーリーアダプターが成果を出し始めています。そして、GitLabはすでにDevOpsを超えてDevSecOpsを実現する機能を提供しています。


    「DevSecOpsを実現するツール」を調べると、多くの選択肢が目に止まるかもしれません。実際に、DevSecOpsを実現するために、活用できるツールにはさまざまなものがあります。ただ、その中でGitLabは唯一、「DevSecOpsをやりたければGitLabがあれば良い」という統合ソリューションを提供できるのです。開発者、セキュリティチーム、運用チームを一連のプロセスの中で統合できるのはGitLabだけです。


    「複数のツールを組み合わせてDevSecOpsをDIYで作る」やり方も可能ではありますが、その場合「DIYしたDevSecOpsの運用コスト」が必要になります。GitLabならそれが必要ないことは大きなメリットで、開発、テスト、Deployという一連のプロセスを自動化し、[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)を含めた開発プロセスすべてを管理できます。


    さらに、[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)機能として最も重視される脆弱性の早期検知においても、強力なエンジンを提供しています。この機能はテストプロセスの後に組み込まれ、開発してすぐテストするというDevOpsの流れをそのまま持ち込めば、開発者は自分の書いたコードや使用するライブラリに脆弱性が含まれるかどうかをすぐに理解することができます。計画段階においてはイシューに要件を付加しておけば、どのレベルの[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)が必要かどうかを振り返って判断することもでき、レビュー時に再確認することも容易です。自動化と人手による検証をどちらも可能にし、開発からリリースに至る全工程に[セキュリティ](https://about.gitlab.com/ja-jp/solutions/security-compliance/)を持ち込み、必要なところは自動化できるようになるのです。

    ![DSC1407_202404JapanITWeekYoheiKawase](//images.ctfassets.net/r9o86ar0p03f/4XAozestf5eeOBfLV1bZ5c/e6359ba0ea88507684cff60fdb46efcc/_DSC1407.jpg)

    *GitLab合同会社 カスタマーサクセス本部 カスタマーサクセスマネジメント部 シニアカスタマーサクセスマネージャー 川瀬 洋平*


    ひとつのプラットフォームでDevSecOpsを実現することで、ツールを連携させて運用するコストはなくなり、ライセンスコストも最小化します。GitLabのユーザーインタフェースは直感的で評価は高く、開発生産性は大きく向上します。自動化により、開発プロセスはスピードアップします。GitLabが外部委託した調査では、ユーザー企業が収益を生み出すアプリケーション開発プロセスおいて427％のROIを実現し、6か月未満で投資を回収できたことが明らかになりました。


    さらに、GitLabでは、DevSecOpsプロセスに[AI](https://about.gitlab.com/ja-jp/gitlab-duo/)を取り入れ、より強力にプロセスの効率化を支援しています。[AI](https://about.gitlab.com/ja-jp/gitlab-duo/)の進化、およびその使い方の熟成が進めば、数字として表面化する成果もより優れたものになると期待されています。


    ## CI/CDでリリース1回あたりの作業時間を9分の1に


    最後に、日本ですでにDevSecOpsに取り組んでいる事例を紹介します。ブースセミナーに登壇した[株式会社キャラウェブ](https://www.chara-web.co.jp/)
    クラウドパートナーグループ 副部長 中山
    桂一氏は、GitLabの導入支援を実施する立場からのコメントをいただき、[株式会社ジークス](https://www.zyyx.jp/) 新規事業開発室
    久保 仁詩氏には自社での活用状況についてお話いただいています。


    ジークスの久保氏が最も顕著な成果を得られたと感じているのは、[CI/CD](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)による自動化です。作業時間の短縮だけでなく、人為的ミスをなくすことにもCI/CDは役立っています。


    ![DSC1773_202404 Japan IT Week Kubo san from
    ZYYX](//images.ctfassets.net/r9o86ar0p03f/7AsW1r09LpmLdOa5A3v5Ng/442e9b79da6a86555c1360ca96c0788f/_DSC1773.jpg)

    *株式会社ジークス 新規事業開発室 久保 仁詩氏*


    リリースプロセスでは、まず手順書を準備し、モジュールを作成。ターミナルに接続してコマンドを実行する作業をサーバ台数分繰り返す必要がありました。これらをすべて開発者の手作業で実施していたころには、3人がそれぞれ30分をかける必要のあるプロセスで、実行コマンドの入力（手順書のコピー＆ペースト）ミスやリリース漏れなどのリスクがありました。


    [CI/CD](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)を導入すれば、これらのプロセスはすべて自動化されます。その結果、導入後には、1人の開発者が実行結果を確認するためにわずか10分の時間をかけるだけで済むようになりました。ジークスでは、スモールスタートで効果を確認し、[CI/CD](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)を採用するプロジェクトを拡大。現在はモバイル領域にも[CI/CD](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)を適用し、月間平均80時間に相当する業務効率化を達成しました。さらに、リリースタイミングは月次や週次ではなく、1日に5回できるようになりました。


    キャラウェブの中山氏は、GitLabで開発プロセスを管理する価値は大きいと紹介してくれました。具体的には、マージリクエストの際に、自動的に脆弱性検査とライセンスチェック、コード品質検査を実施することで手戻りは最小限に抑えられます。9種のセキュリティテストを同時に走らせ、静的スキャンに加えて動的な脆弱性スキャンも実施することができます。マージ後に再度セキュリティテストを実行することで、安心できるセキュリティレベルを保証することも可能です。


    ![DSC1811_202404JapanITWeekNakayamasan](//images.ctfassets.net/r9o86ar0p03f/FAoI7p6GeYbnDexcutSTo/11f5722b17b8278916e93b4e74bc6ea9/_DSC1811.jpg)

    *株式会社キャラウェブ クラウドパートナーグループ 副部長 中山 桂一氏*


    <br>

    ブースセミナーにパートナー様とお客様が登壇してくださったように、日本でもDevSecOpsは徐々に浸透してきています。アーリーアダプターからはすでに数多くの成功者が生まれています。これからもDevSecOpsの発展とGitLabにご注目ください。


    <br><br>

    ＜[後編を読む：DevSecOpsで人材問題は解決できるか](https://about.gitlab.com/ja-jp/blog/2024/07/18/event-report-japan-it-week-spring-2/)＞
  category: DevSecOps
  tags:
    - events
    - inside GitLab
    - DevSecOps
    - DevSecOps platform
config:
  slug: event-report-japan-it-week-spring-1
  featured: true
  template: BlogPost
