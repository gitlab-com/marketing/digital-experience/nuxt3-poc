seo:
  title: GitLab 17.8リリース
  description: GitLab 17.8でリリースした最新機能をご紹介します。
  ogTitle: GitLab 17.8リリース
  ogDescription: GitLab 17.8でリリースした最新機能をご紹介します。
  noIndex: false
  ogImage: >-
    images/blog/hero-images/product-gl17-blog-release-cover-17-8-0093-1800x945-fy25.png
  ogUrl: https://about.gitlab.com/blog/gitlab-17-8-release
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-17-8-release
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab 17.8リリース",
            "author": [{"@type":"Person","name":"GitLab Japan Team"}],
            "datePublished": "2025-01-16",
          }

content:
  title: GitLab 17.8リリース
  description: GitLab 17.8でリリースした最新機能をご紹介します。
  authors:
    - GitLab Japan Team
  heroImage: >-
    images/blog/hero-images/product-gl17-blog-release-cover-17-8-0093-1800x945-fy25.png
  date: '2025-01-16'
  body: >-
    **コンテナリポジトリのセキュリティが向上したGitLab 17.8をリリース**


    このたび、GitLab
    17.8のリリースを発表しました。このリリースでは、コンテナリポジトリのセキュリティ強化、リリース関連のデプロイの一覧表示、機械学習モデル検証の追跡、GitLab
    Dedicated向けLinuxホステッドランナーなど、さまざまな機能が追加されました！  


    これらの機能は、今回のリリースに含まれる60件以上の改善点のほんの一部です。この記事では、お役に立つアップデートをすべてご紹介していますので、ぜひ最後までお読みください。  


    GitLab
    17.8には、GitLabコミュニティのユーザーから121件ものコントリビュートがありました。ありがとうございました！GitLabは[誰もがコントリビュートできる](https://about.gitlab.com/community/contribute/)プラットフォームであり、今回のリリースも、ユーザーのみなさまのご協力なくしては実現しませんでした。  

    来月のリリースで予定されている内容を先取りするには、17.9リリースのキックオフビデオも視聴できる[今後のリリースページ](https://about.gitlab.com/direction/kickoff/)をご覧ください。  


    > [GitLab
    17.8のリリースでは、コンテナリポジトリのセキュリティが向上しました！クリックしてSNSで共有しましょう！](http://twitter.com/share?text=GitLab+17.8+released+with+improved+container+repository+security&url=https://about.gitlab.com/releases/2025/01/16/gitlab-17-8-released/&hashtags=)


    ## 今月の[MVP](https://about.gitlab.com/community/mvp/)は[Océane
    Legrand](https://gitlab.com/oceane_scania)さんと[Juan Pablo
    Gonzalez](https://gitlab.com/ScanianJP)さんが受賞


    MVPには、誰もが[GitLabコミュニティのコントリビューターをMVPに推薦できます](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/490)。現在の候補者を応援したり、他の誰かをノミネートしてみませんか🙌


    [Océane Legrand](https://gitlab.com/oceane_scania)さんは共同開発プログラムを通じて、Juan
    Pablo
    Gonzalezさんと協力しながら、Conanのパッケージレジストリの機能セットを強化する取り組みを主導してきました。お二人は、一般公開（GA）に向けた機能の準備、またConanバージョン2のサポート実装に重点的に取り組んできました。お二人の例は、GitLabのパッケージレジストリ機能を大幅に改善する上で、共同開発プログラムがいかに有効であるかを示しています。  


    LegrandさんとGonzalezさんは、GitLabのコントリビューターサクセスチームでシニアフルスタックエンジニアを務める[Raimund
    Hook](https://gitlab.com/stingrayza)によって推薦されました。Hookは、お二人が連携しながら粘り強く取り組み、Conanパッケージレジストリの機能改善を継続的に進めた点に注目しました。お二人の功績はGitLabの価値観を体現するものであり、GitLabプラットフォーム上でConanを利用する全ユーザーに恩恵をもたらします。


    Scania社のフルスタックデベロッパーであるOcéane
    Legrandさんは、AWS上のセルフホスト型GitLabインスタンスの保守作業を担っています。Legrandさんは「私がオープンソースで取り組んでいる作業は、GitLabとScaniaの両方に影響を与えています。共同開発プログラムを通じてコントリビュートすることで、Rubyやバックグラウンドマイグレーションの経験など、新たなスキルを習得できました。Scaniaの所属チームでアップグレード作業中に問題が発生した際、共同開発プログラムですでに同じ問題を経験していたため、トラブルシューティングを手伝うことができました」と述べています。  


    GitLabの共同開発プログラムについて詳しくは[こちら](https://about.gitlab.com/community/co-create/)をご覧ください。これらのプログラムでは、GitLabのお客様が、当社製品チームやエンジニアリングチームと直接連携しながら新機能の開発や既存機能の改善に取り組んでいます。


    ## GitLab 17.8のリリースに含まれる主な改善点


    ### 保護されたコンテナリポジトリによるセキュリティ強化


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    本リリースでは、保護されたコンテナリポジトリが導入されました。この新機能は、コンテナイメージを管理する際のセキュリティと制御の課題を解決することを目的に追加されました。組織は、機密性の高いコンテナリポジトリへの不正アクセス、意図せぬ変更、細かい制御が設定できない、コンプライアンスの維持の難しさといった課題に苦労することがよくあります。このソリューションは、厳格なアクセス制御、プッシュ、プル、管理の操作権限を詳細に設定できるようにし、、GitLab
    CI/CDパイプラインとの統合をシームレスにすることで、セキュリティを強化します。


    保護されたコンテナリポジトリの導入で、セキュリティ侵害リスクや過失によって重要な資産が変更されるリスクが軽減されます。また、開発速度とセキュリティの両方を維持しながら、ワークフローを効率化できます。コンテナレジストリの全体的なガバナンスが向上されるほか、組織のニーズに基づいて重要なコンテナ資産が保護されていることが分かるため、安心感も得られます。


    この機能と[保護パッケージ](https://gitlab.com/groups/gitlab-org/-/epics/5574)は、`gerardo-navarro`さんとシーメンス社のみなさまによるコミュニティへのコントリビュートにより実現しました。この場を借りて、GitLabに多大なるコントリビュートをしてくださったNavarroさんをはじめ、シーメンス社のみなさまに感謝申し上げます！この変更に対するNavarroさんとシーメンス社の方々のコントリビュートについて、詳しくは[こちらの動画](https://www.youtube.com/watch?v=5-nQ1_Mi7zg)をご確認ください。Navarroさんが、外部のコントリビューターとしてGitLabにコントリビュートした経験から得た洞察やベストプラクティスを紹介してくれています。


    [ドキュメント](https://docs.gitlab.com/ee/user/packages/container_registry/container_repository_protection_rules.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/480385)


    <img src="https://about.gitlab.com/images/17_8/protected_containers.png">


    ### リリース関連のデプロイの一覧表示


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    GitLabでは、これまでもGitタグを元にしたリリースの作成およびデプロイの追跡をサポートしてきました。しかし、これらの情報が異なる場所に分散していたため、紐づけるのが困難でした。本リリースより、リリース関連のデプロイがすべてリリースページに直接表示されるようになりました。これにより、リリースマネージャーは、リリースのデプロイ先や、どの環境がデプロイ待ちであるかといったステータスを素早く確認できます。既存のデプロイページではタグ付けされたデプロイのリリースノートを表示されますが、この機能は既存のデプロイページの統合を補完するものです。


    この場を借りて、GitLabに両機能をコントリビュートしてくれた[Anton
    Kalmykov](https://gitlab.com/antonkalmykov)さんに心より感謝します。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/releases/)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/501169)


    <img
    src="https://about.gitlab.com/images/17_8/list_the_deployments_related_to_a_release.png">


    ### 機械学習モデル検証の追跡機能の一般公開


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    機械学習モデルを作成する際、データサイエンティストはモデルの性能向上を目的に、さまざまなパラメーターや設定、特徴量エンジニアリングを試行することがよくあります。データサイエンティストにとって、これらのメタデータや関連するアーティファクトをすべて追跡し、後から実験を再現できるようにするのは容易ではありません。機械学習モデル検証の追跡により、パラメータ、メトリクス、アーティファクトをGitLabに直接記録できるため、後から簡単にアクセスできるだけでなく、すべての実験データをGitLab環境内で保持できるようになりました。この機能は、データ表示の強化、権限設定の強化、GitLabとのより緊密な統合、バグ修正といった改善が加えられ、本リリースより一般提供されています。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/ml/experiment_tracking/)  

    [エピック](https://gitlab.com/groups/gitlab-org/-/epics/9341)


    <iframe width="560" height="315"
    src="https://www.youtube.com/embed/jkZq3SYm7a8?si=AaBF71InBSRhZWZa"
    title="YouTube video player" frameborder="0" allow="accelerometer; autoplay;
    clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
    referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


    ### GitLab Dedicated向けLinuxホステッドランナーが限定的に利用可能に


    SaaS：Ultimate<br>

    Self-Managed：-


    本リリースより、GitLab Dedicated向けLinuxホステッドランナーが限定的に利用可能になりました。


    ランナーのフリート管理作業は複雑になりがちで、デベロッパーの要求に応じてすべてのCI/CDジョブをスケールするには、豊富な経験が必要です。

    GitLab
    Dedicated向けのホステッドランナーでは、CI/CDジョブ用に徹底管理されたランナーを活用できます。そのため、独自のランナーインフラストラクチャを管理せずに済むほか、ランナーには、GitLab
    Dedicatedと同等のセキュリティ、柔軟性、効率性が確保されます。

    ホステッドランナーは、CI/CDのニーズに合わせて自動的にスケールし、ピーク時や大規模プロジェクトにおいてパフォーマンスを最適化します。今回の限定リリースでは、2～32
    vCPU、8～128 GBのメモリを搭載した、さまざまなサイズのLinuxランナーをご利用いただけます。


    限定リリース期間中にGitLab Dedicated向けホステッドランナーをご利用になりたい場合は、GitLabの担当者までお問い合わせください。


    [ドキュメント](https://docs.gitlab.com/ee/administration/dedicated/hosted_runners.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509142)


    <img src="https://about.gitlab.com/images/17_8/runner_dedicated.png">


    ### macOS大規模M2 Proホステッドランナーが利用可能に（ベータ版）


    SaaS：Premium、Ultimate<br>

    Self-Managed：-


    M2 Proの性能を、モバイルDevOpsチームが活用できるようになりました。  

    M1ランナーの最大2倍、x86-64 macOSランナーの最大6倍の性能を誇るM2
    Proランナーを使用することで、開発チームによるアプリケーションのビルドとデプロイの作業速度が向上します。


    このランナーは、GitLab
    CI/CDに完全統合され、オンデマンドで利用可能です。これにより、Appleエコシステム向けアプリケーションの作成、テスト、およびデプロイがより迅速かつシームレスになります。


    `.gitlab-ci.yml`ファイルのタグに`saas-macos-large-m2pro`を指定して、新しいM2
    Proランナーをぜひお試しください。


    [ドキュメント](https://docs.gitlab.com/ee/ci/runners/hosted_runners/macos.html)  

    [エピック](https://gitlab.com/groups/gitlab-org/ci-cd/shared-runners/-/epics/19)


    <img src="https://about.gitlab.com/images/17_8/rocket_m2pro.png">


    ## GitLab 17.8のリリースに含まれるその他の改善点


    ### イシューまたはマージリクエスト内における複数のto-doアイテムの追跡


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    単一のイシューまたはマージリクエスト内で、複数のディスカッションやメンションを追跡できるようになりました。この新機能は、メンションやアクションごとに個別のto-doアイテムを表示させて、重要な更新やリクエストを見逃さないようサポートします。この機能強化により、作業をより効果的に管理し、チームのニーズにより効率的に対応できるようになります。


    [ドキュメント](https://docs.gitlab.com/ee/user/todos.html#multiple-to-do-items-per-object)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/28355)


    ### エピックの祖先を把握しやすい表示に


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    祖先ウィジェットの設計が見直されたことで、[エピックの階層](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-other-items)を確認しやすくなりました。各エピックの上部に、パンくずリストのようなナビゲーションガイドで目立つように表示されます。1つ上の親と一番上の親を一目で確認し、エピック間の関係を素早く把握できます。これにより、プロジェクト構造の概要を分かりやすく管理し、関連するエピック間を簡単に移動できます。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-other-items)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509920)


    <img src="https://about.gitlab.com/images/17_8/epic_ancestors.png">


    ### エピックから親を追加


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    イシューの場合と同様に、直接エピックから親を追加することで、エピック階層を簡単に管理できるようになりました。このプロセスの効率化により、作業をより柔軟に整理できるようになり、エピック間の関係を迅速に構築し、プロジェクト構造を分かりやすく保つことができます。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-other-items)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509923)


    <img src="https://about.gitlab.com/images/17_8/epic_parent.png">


    ### エピック、イシュー、目標の子アイテムにイテレーションフィールドを表示


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    プランナーはエピックの詳細を閲覧する際、どの子イシューがイテレーション（スプリント）で計画されていて、どれがまだ計画されていないかを確認できなければなりません。この新機能により、定義されたすべての作業がスプリントで計画されているかどうか、チームがより簡単に確認できるようになりました。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/iterations)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/510005)


    <img
    src="https://about.gitlab.com/images/17_8/show_iteration_field_on_items_within_the_work_items_child_widget.png">


    ### エピックのWebhook


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    エピックのWebhookを使用することで、ワークフローの自動化が強化されるだけでなく、エピックで変更が発生するたびに、お好みのツールでリアルタイムの更新を受け取ることができます。お使いの他のサービスとGitLabを統合すると、コラボレーションを強化し、プロジェクトの進捗を常に把握できます。また、アプリケーション間を何度も移動する必要がなくなり、プロセスが効率化されます。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509928)


    ### GitLab Community Editionでパイプラインの制限を適用可能に


    SaaS：-<br>

    Self-Managed：Free、Premium、Ultimate


    管理者は、GitLab Community
    Edition（CE）にCI/CDに関する制限を適用して、パイプラインリソースの使用を制御できるようになりました。これまで、この機能はGitLab
    Enterpriseエディションでのみ利用可能でした。


    [ドキュメント](https://docs.gitlab.com/ee/administration/settings/continuous_integration.html#set-cicd-limits)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/287669)


    ### Kubernetes用ダッシュボードでのポッド検索


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    大規模なデプロイの場合、Kubernetes用ダッシュボードで特定のポッドを見つけるのには時間がかかる可能性があります。新たに検索バーが追加され、ポッドの名前を指定して素早く絞り込めるようになりました。利用可能なすべてのポッドが検索対象に含まれます。また、ステータスフィルターと組み合わせて、モニタリングやトラブルシューティングが必要なポッドを特定することも可能です。


    [ドキュメント](https://docs.gitlab.com/ee/ci/environments/kubernetes_dashboard.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/508010)


    ### シークレット検出の実行時に修正手順が表示されるように


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    漏洩した認証情報を使用して攻撃者がシステムに侵入するリスクを最小限に抑えるには、公開されてしまったシークレットに素早く対処しなければなりません。正しく修正するには、認証情報のローテーションや不正アクセスできる可能性がある箇所の調査など、単にシークレットを削除するだけでなく、さまざまな手順を行う必要があります。本リリースから、システムの安全性を保つために、シークレット検出を実行すると、検出されたシークレットのタイプごとに具体的な修正手順が表示されるようになりました。この修正手順を参考にすることで、情報漏洩に体系的に対処し、セキュリティ侵害のリスクを軽減できます。パイプラインが完了すると、検出されたすべての脆弱性に関する修正手順が表示されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/secret_detection/)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/505757)


    ### `override_ci`戦略が一元化されたワークフロールールの適用に対応


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    パイプライン実行ポリシーの`override_ci`戦略が、`include:project`の利用時に、プロジェクト設定で定義されたジョブだけでなく、ポリシー内で定義されたジョブに対しても、ポリシーの実施を支援するワークフロールールの使用をサポートするようになりました。ポリシー内でワークフロールールを定義することで、プロジェクトでのブランチパイプラインの使用を防ぐルールを設定するなど、特定のルールに基づいてパイプライン実行ポリシーによって実行されるジョブをフィルタリングできます。


    ポリシー内で定義されたジョブのみを対象とするワークフロールールを切り離して使用するには、ポリシーによってグローバルにルールを定義せずに、ジョブに対してルールを定義するのがおすすめです。もしくは、別の`include`フィールドを用いて、ジョブやルールをグループ化することもできます。


    これまでは`override_ci`戦略を使用すると、パイプライン実行ポリシーで定義されたジョブにのみ、ワークフロールールを適用できました。


    `Inject_ci`戦略に変更はありません。ワークフロールールは、プロジェクトのワークフロールールには影響を及ぼさず、ポリシーのジョブが実行されるタイミングを制御するためだけに利用できます。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html#override_project_ci)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/512123)


    ### パイプライン実行ポリシーで`skip_ci`を設定可能に


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    パイプライン実行ポリシー（PEP）に新たな設定オプションを導入され、より柔軟に`[skip
    ci]`ディレクティブを処理できるようになりました。この機能によって、重要なセキュリティおよびコンプライアンスのチェックを確実に実行しつつ、パイプラインの実行をバイパスする必要がある、特定の自動化されたプロセス（セマンティックリリースなど）に対応できます。


    この機能を使用するには、パイプライン実行ポリシーのYAML設定で`skip_ci`を`allowed:
    false`に設定するか、ポリシーエディターで「**ユーザーがパイプラインをスキップできないようにする**」を有効にします。次に、`[skip
    ci]`の使用を許可するユーザーまたはサービスアカウントを指定します。`skip_ci`設定内で例外として除外されない限り、デフォルトでは、すべてのユーザーがパイプライン実行ジョブをスキップできません。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/policies/pipeline_execution_policies.html#skip_ci-type)  

    [イシュー](https://gitlab.com/groups/gitlab-org/-/epics/15647)


    <img
    src="https://about.gitlab.com/images/17_8/service-account-exception-skip_ci-pep.png">


    ### マージリクエスト承認ポリシーで複数の異なる承認アクションをサポート


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    これまでマージリクエスト承認ポリシーはポリシーごとに1つの承認ルールしかサポートしておらず、「OR」条件を用いて複数の承認者を指定する場合も1セットしか設定できませんでした。結果として、さまざまなロール、個々の承認者、または別々のグループから成る、階層化されたセキュリティ承認の実装は非常に困難でした。


    今回の更新により、マージリクエスト承認ポリシーごとに最大5つの承認ルールを作成できるようになったことから、より柔軟で堅牢な承認ポリシーの設定が可能になりました。ルールごとに異なる承認者やルールを指定でき、各ルールは個別に評価されます。たとえば、セキュリティチームは、グループAとグループBからそれぞれ1名の承認者、もしくは特定のロールと特定のグループから1名の承認者を必要とするような複雑な承認ワークフローを定義できます。これにより、機密性の高いワークフローにおけるコンプライアンスと制御の強化が実現できます。


    この機能強化の使用例を以下にご紹介します。


    * __異なるロールによる承認__：デベロッパーロールおよびメンテナーロールによる承認  

    * __ロールおよびグループによる承認__：デベロッパーまたはメンテナーロールによる承認と、セキュリティグループのメンバーによる承認  

    * __異なるグループによる承認__： Pythonエキスパートグループのメンバーによる承認と、セキュリティグループのメンバーによる承認


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/policies/merge_request_approval_policies.html)


    [イシュー](https://gitlab.com/groups/gitlab-org/-/epics/12319)


    <img
    src="https://about.gitlab.com/images/17_8/multiple-distinc-approvers-nov-19.png">


    ### GitLab MLOps Pythonクライアント（ベータ版）


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    データサイエンティストや機械学習エンジニアは主に作業環境としてPythonを使用していますが、機械学習ワークフローをGitLabのMLOps機能と統合するには、頭の切り替えが必要となるだけでなく、GitLabのAPI構造を理解しなければなりません。結果として、開発プロセスに摩擦が生じ、実験の追跡、モデルアーティファクトの管理、チームメンバーとのコラボレーションの速度に悪影響を及ぼす可能性があります。


    新しいGitLab MLOps
    Pythonクライアントでは、GitLabのMLOps機能にシームレスにアクセスできる、Pythonに適したインターフェイスを利用できます。これにより、データサイエンティストは、Pythonスクリプトやノートブックから直接GitLabの[実験追跡](https://docs.gitlab.com/ee/user/project/ml/experiment_tracking/)機能や[モデルレジストリ](https://docs.gitlab.com/ee/user/project/ml/model_registry/)機能を利用できるようになりました。クライアントでは以下の機能を利用できます。


    * **GitLab内での実験の追跡**：GitLab内で行われる機械学習実験を簡単に追跡できます。  

    * **モデルレジストリの統合**：GitLabのモデルレジストリでモデルの登録および管理ができます。  

    * **実験の管理**：クライアントから直接実験を作成し、管理できます。  

    * **追跡の実行**：トレーニングの実行とモニタリングが簡単にできます。


    この統合により、データサイエンティストはモデル開発に集中しながら、機械学習ライフサイクルのメタデータをGitLabに自動的に取り込むことが可能になりました。Pythonクライアントは既存の機械学習ワークフローとシームレスに連携し、設定は最小限で済むため、データサイエンスコミュニティにとってGitLabのMLOps機能がより身近な存在となります。


    幅広いPythonとデータサイエンスコミュニティのみなさまからのコントリビュートをお待ちしています。[プロジェクトのリポジトリ](https://gitlab.com/gitlab-org/modelops/mlops/gitlab-mlops)から、ぜひ直接フィードバックをお寄せください。


    [ドキュメント](https://gitlab.com/gitlab-org/modelops/mlops/gitlab-mlops)  

    [イシュー](https://gitlab.com/groups/gitlab-org/-/epics/16193)


    ### エピックの色をカスタマイズ可能


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    既存の値やカスタムのRGB値または16進コードを含む、拡張されたカラーオプションを使用して、エピックをより柔軟に分類できるようになりました。この視覚的なカスタマイズの強化により、エピックをスクワッド、会社のイニシアチブ、または階層レベルに簡単に関連付けることができ、ロードマップやエピックボードでの作業の優先順位付けや整理が楽になります。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#epic-color)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509924)


    <img
    src="https://about.gitlab.com/images/17_8/customizable_colors_for_epics.png">


    ### エピックのヘルスステータス


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    エピックのヘルスステータス機能が新たに追加され、プロジェクトの進捗状況を共有しやすくなりました。ステータスを「健全」「要注意」「危険」のいずれかに設定することで、エピックの健全性が可視化され素早く把握できるようになります。これにより、リスクを管理しつつ、プロジェクト全体のステータスを関係者と常に共有できるようになりました。


    管理者は、[エピックの新しい外観](https://docs.gitlab.com/ee/user/group/epics/epic_work_items.html)を有効にする必要があります。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#health-status.)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509922)


    <img src="https://about.gitlab.com/images/17_8/epic_health_status.png">


    ### GitLab Pagesでのプライマリドメインへのリダイレクト


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    GitLab
    Pagesでプライマリドメインを設定し、カスタムドメインからのリクエストをすべてプライマリドメインに自動的にリダイレクトできるようになりました。訪問者がどのURLからサイトにアクセスしても、指定したドメインにリダイレクトされるため、SEOランキングを維持し、一貫したブランド体験を提供するのに役立ちます。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/pages/#primary-domain)  

    [エピック](https://gitlab.com/groups/gitlab-org/-/epics/15000)


    ### エピックに費やした時間の追跡


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    エピック内で直接時間をトラッキングして、プロジェクトの時間管理をより細かくコントロールできるようになりました。この新機能を使用すると、プロジェクトに費やされた時間を細分化して記録できるため、スプリントやマイルストーンを進める中で、進捗をモニタリングし、スケジュールを遵守し、予算を管理するのに役立ちます。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/time_tracking.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509930)


    <img src="https://about.gitlab.com/images/17_8/epic_time_tracking.png">


    ### ロールを使用してプロジェクトメンバーをGitLabコードオーナーとして定義


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    ロールをGitLabコードオーナーとして`CODEOWNERS`ファイルに設定できるようになりました。これにより、ロールベースの技能と承認をより効率的に管理できるようになりました。個別のユーザーを列挙したり、グループを作成したりする代わりに、以下の構文を使用できます。


    * `@@developers`：デベロッパーロールが付与されたすべてのユーザーを参照  

    * `@@maintainers`：メンテナーロールが付与されたすべてのユーザーを参照  

    * `@@owners`：オーナーロールが付与されたすべてのユーザーを参照


    たとえば、`* @@maintainers`を追加すると、リポジトリにおけるすべての変更に対して、メンテナーによる承認が必要になります。  

    これにより、プロジェクトにおいてチームメンバーの参加、離脱、またはロールの変更があった場合でも、GitLabコードオーナーを簡単に管理できます。GitLabが指定されたロールを持つすべてのユーザーを自動的に`CODEOWNERS`ファイルに反映するため、手動でファイルを更新することなく、常に最新の状態を維持できます。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/codeowners/reference.html#add-a-role-as-a-code-owner)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/282438)


    ### 保護パッケージを使用して依存関係を守る


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    本リリースでは、PyPIの保護パッケージが新たにサポートされました。こちらは、GitLabパッケージレジストリのセキュリティと安定性を強化することを目的として設計された新機能です。急速に変化するソフトウェア開発の現場では、パッケージを誤って変更または削除してしまった場合、開発プロセス全体に混乱が生じる可能性があります。保護パッケージを使用すると、意図せぬ変更を防いで最も重要な依存関係を保護できます。


    GitLab
    17.8からは、保護ルールを作成してPyPIパッケージを保護します。保護ルールの条件に合致したパッケージは、指定されたユーザーのみが更新または削除できます。この機能を使用すると、手動による監視の必要性を減らすことにより、意図せぬ変更の防止、規制要件に関連するコンプライアンスの強化、ワークフローの効率化を実現できます。


    [ドキュメント](https://docs.gitlab.com/ee/user/packages/package_registry/package_protection_rules.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/323971)


    <img src="https://about.gitlab.com/images/17_8/protected_pypi_packages.png">


    ### Kubernetes用ダッシュボードで一時停止中のFluxの調整を表示


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    これまではKubernetes用ダッシュボードでFluxの調整（Flux
    reconciliation）を一時停止しても、一時停止状態であることを示す明確な指標がありませんでした。本リリースでは、既存のステータス指標に新たに「一時停止」が追加され、Fluxの調整が中断された状態であることが明示されるようになり、デプロイの状態に関する可視性が向上しました。


    [ドキュメント](https://docs.gitlab.com/ee/ci/environments/kubernetes_dashboard.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/501339)


    ### Webhookイベントのサポート対象に脆弱性を追加


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    脆弱性に関連するアクションに対してイベントを生成するWebhookインテグレーションが導入されました。これにより、自動化や外部リソースとの統合が可能になります。たとえば、脆弱性の発生時や脆弱性ステータスの変更時にイベントが生成されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html#vulnerability-events)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/366770)


    <img
    src="https://about.gitlab.com/images/unreleased/vulnerabiltiy-webhook.jpg">


    ### 脆弱性が修正されたコミットの特定


    SaaS：Ultimate<br>

    Self-Managed：Ultimate


    これまでは、脆弱性が検出されなくなった場合に、その脆弱性がいつ、どこで修正されたかを確認できませんでした。本リリースより、脆弱性が修正されたコミットSHAへのリンクが表示されるようになったため、トレーサビリティが向上したほか、修正プロセスに関する詳細なインサイトも取得可能になりました。これにより、セキュリティチームと開発チームが連携してより効果的に脆弱性を管理しやすくなりました。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/#vulnerability-resolution)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/372799)


    <img
    src="https://about.gitlab.com/images/17_8/commit-link-vulnerability.png">


    ### スケジュールされたスキャン実行パイプラインの並行処理を管理


    SaaS：Premium、Ultimate<br>

    Self-Managed：Ultimate


    グローバルスケジュール型スキャン実行ポリシーのスケーラビリティを向上させるために、スキャン実行ポリシーに時間枠を設定する機能が新たに導入されました。`time_window`プロパティでポリシーによって新規スケジュールが作成および実行される期間を定義し、最適なパフォーマンスを確保します。


    新たに追加されたプロパティを使用するには、YAMLモードを使用してポリシーを更新し、[`time_window`スキーマ](https://docs.gitlab.com/ee/user/application_security/policies/scan_execution_policies.html#time_window-schema)に従います。スケジュールが実行される時間枠は秒単位で指定できます。たとえば、24時間の時間枠を設定する場合は`86400`と指定します。次に、`distribution:
    random`フィールドおよび値を指定すると、定義された時間枠でスケジュールがランダムに実行されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/policies/scan_execution_policies.html#concurrency-control)  

    [エピック](https://gitlab.com/groups/gitlab-org/-/epics/13997)


    ### コンプライアンスセンターの「フレームワーク」レポートタブのUIパフォーマンスをスケーリング


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    GitLab
    17.8では、バックエンドを改良し、コンプライアンスセンターで一貫した応答性と高速な動作を実現しました。たとえコンプライアンスセンターの「**フレームワーク**」レポートタブに数千件のコンプライアンスフレームワークがある場合でも、この性能は維持されます。


    さらに、より詳細な情報を求めて「**フレームワーク**」タブで任意のフレームワークをクリックすると、右側のポップアップメニューに、そのフレームワークに関連付けられているプロジェクト情報が1,000件まで表示されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/compliance/compliance_center/compliance_frameworks_report.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/477394)


    ### グループのプロジェクト作成権限にオーナーロールを追加


    SaaS：Free、Premium、Ultimate<br>

    Self-Managed：Free、Premium、Ultimate


    **プロジェクトの作成許可**設定を使用すると、プロジェクトを作成できる対象者をグループ内の特定のロールに制限できます。本リリースから、オーナーロールがオプションに追加され、新規プロジェクトを作成できる対象者をグループに対してオーナーロールを持つユーザーに制限できるようになりました。このロールは、これまで選択オプションに含まれていませんでした。


    この場を借りて、コミュニティにコントリビュートしてくださった[@yasuk](https://gitlab.com/yasuk)さんに感謝申し上げます！


    [ドキュメント](https://docs.gitlab.com/ee/user/group/index.html#specify-who-can-add-projects-to-a-group)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/354355)


    ### 削除予定のサブグループとプロジェクトの表示


    SaaS：Premium、Ultimate<br>

    Self-Managed：Premium、Ultimate


    グループを削除対象としてマークする際は、影響を受けるすべてのサブグループとプロジェクトを確認する必要があります。これまでは削除対象としてマークされたグループのみに「削除の保留中」ラベルが表示されており、そのサブグループとプロジェクトには表示されていなかったため、削除予定のコンテンツを特定するのは大変でした。


    本リリースから、グループが削除対象としてマークされると、そのすべてのサブグループとプロジェクトに「削除の保留中」ラベルが表示されるようになりました。これにより、可視性が向上し、グループ階層全体でアクティブなコンテンツと削除予定のコンテンツを素早く見分けることができます。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/#view-groups-pending-deletion)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/457718)


    ## 実験的な機能


    ### VS CodeでのSASTスキャン


    リアルタイムのGitLab SASTスキャンが、実験的な機能としてVS Codeで利用できるようになりました。


    プロジェクトファイルをコミットまたはプッシュする前にVS
    Codeで直接スキャンできるため、セキュリティの脆弱性をこれまでよりも早期に発見して修正できます。SASTスキャンのサイドパネルには、スキャン結果が表示され、コードに変更を加えると更新されます。脆弱性の結果にカーソルを合わせると、詳細な説明が表示されます。またはエディタウィンドウを開いて詳細を確認することも可能です。この機能の利用を開始するには、[こちらのドキュメント](https://docs.gitlab.com/ee/editor_extensions/visual_studio_code/#perform-sast-scanning)をご参照ください。


    この機能は、UltimateプランでGitLab.comをご使用のお客様にご利用いただけます。ぜひ[フィードバック](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/1775)をお寄せください。今後段階的に、本機能を改善していく予定です。


    デモをご覧になりたい場合は、[VS
    CodeでのSASTスキャンの動画](https://www.youtube.com/watch?v=KOYdVdA6ZCs)をご視聴ください。


    ## バグ修正、パフォーマンスの改善、UIの改善


    GitLabでは、ユーザーに可能な限り最高の環境をお届けできるよう尽力しています。リリースのたびに、バグを修正し、パフォーマンスを改善し、UIを向上させるためにたゆまぬ努力を続けています。GitLabでは、100万人を超えるGitLab.comユーザーをはじめ、GitLabのプラットフォームをご利用のすべての方々に、スムーズでシームレスな体験をお届けすることを約束します。


    以下のリンクをクリックして、17.8のバグ修正、パフォーマンスの強化、UIの改善についてすべてご覧ください。


    *
    [バグ修正](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=type%3A%3Abug&or%5Blabel_name%5D%5B%5D=workflow%3A%3Acomplete&or%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&or%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&milestone_title=17.8)  

    *
    [パフォーマンスの改善](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=bug%3A%3Aperformance&or%5Blabel_name%5D%5B%5D=workflow%3A%3Acomplete&or%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&or%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&milestone_title=17.8)  

    * [UIの改善](https://papercuts.gitlab.com/?milestone=17.8)


    *監修：知念 梨果 [@rikachinen](https://gitlab.com/rikachinen)* <br>

    *（GitLab合同会社 カスタマーサクセス本部 カスタマーサクセスエンジニア）*


    ### 過去の日本語リリース情報


    - [GitLab
    17.9](https://about.gitlab.com/ja-jp/blog/2025/02/20/gitlab-17-9-release/)

    * [GitLab
    17.8](https://about.gitlab.com/ja-jp/blog/2025/01/16/gitlab-17-8-release/)

    * [GitLab
    17.7](https://about.gitlab.com/ja-jp/blog/2024/12/19/gitlab-17-7-release/)

    * [GitLab
    17.6](https://about.gitlab.com/ja-jp/blog/2024/11/21/gitlab-17-6-release/)

    * [GitLab
    17.5](https://about.gitlab.com/ja-jp/blog/2024/10/17/gitlab-17-5-released/)  

    * [GitLab
    17.4](https://about.gitlab.com/ja-jp/blog/2024/09/19/gitlab-17-4-released/)  

    * [GitLab
    17.3](https://about.gitlab.com/ja-jp/blog/2024/08/15/gitlab-17-3-released/)  

    * [GitLab
    17.2](https://about.gitlab.com/ja-jp/blog/2024/07/18/gitlab-17-2-released/)  

    * [GitLab
    17.1](https://about.gitlab.com/ja-jp/blog/2024/08/08/gitlab-17-1-released/)  

    * [GitLab
    16.11](https://about.gitlab.com/ja-jp/blog/2024/06/20/gitlab-16-11-released/)
  category: 製品
  tags:
    - AI/ML
    - releases
    - product
config:
  slug: gitlab-17-8-release
  featured: true
  template: BlogPost
