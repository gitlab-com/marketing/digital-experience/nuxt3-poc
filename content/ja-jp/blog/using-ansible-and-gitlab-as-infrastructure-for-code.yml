seo:
  title: GitLabとAnsibleを使ってIaCを作成する方法
  description: Ansible playbookを使ってIaCを作成します。GitLab CIが持つ力を探求してみてください。
  ogTitle: GitLabとAnsibleを使ってIaCを作成する方法
  ogDescription: Ansible playbookを使ってIaCを作成します。GitLab CIが持つ力を探求してみてください。
  noIndex: false
  ogImage: images/blog/hero-images/gitlab-ansible-cover.png
  ogUrl: >-
    https://about.gitlab.com/blog/using-ansible-and-gitlab-as-infrastructure-for-code
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/using-ansible-and-gitlab-as-infrastructure-for-code
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLabとAnsibleを使ってIaCを作成する方法",
            "author": [{"@type":"Person","name":"Brad Downey"},{"@type":"Person","name":"Sara Kassabian"}],
            "datePublished": "2019-07-01",
          }

content:
  title: GitLabとAnsibleを使ってIaCを作成する方法
  description: Ansible playbookを使ってIaCを作成します。GitLab CIが持つ力を探求してみてください。
  authors:
    - Brad Downey
    - Sara Kassabian
  heroImage: images/blog/hero-images/gitlab-ansible-cover.png
  date: '2019-07-01'
  body: >
    IaCとして管理されるAnsible playbookを実行する際に力を発揮する、GitLab CIの機能を探求してみませんか。


    GitLab
    CIは、[IaC](https://about.gitlab.com/ja-jp/topics/gitops/infrastructure-as-code/)や[GitOps](https://about.gitlab.com/ja-jp/solutions/gitops/)など、さまざまな用途に使用できる強力なツールです。GitLabはツールに依存しないプラットフォームですが、AnsibleはIaCを管理するために、開発者によく使用される言語なので、今回はAnsibleを使用していきます。


    ## Ansibleとは


    Ansibleはオープンソースであり、デプロイ、構成、そしてコンピュータシステムの管理を自動的に行なう際に使用し、構成管理ツールに分類されます。開発者やシステム管理者がAnsibleを使用すると、サーバーの構成やアプリケーションのデプロイ、またネットワークデバイスの管理といった、複雑な、繰り返しの多いプロセスを自動化できます。AnsibleはYAMLを使って、playbookとして知られている宣言型のタスク説明を作成します。作成された宣言型のタスク説明は、コマンドを実行するターゲットホストへのSSH接続を通じてAnsibleが行なう、望まれるシステム状態を説明してくれます。


    ## デモ：GitLab CI と Ansible


    [GitLab
    CI](https://about.gitlab.com/ja-jp/solutions/continuous-integration/)の特に優れている機能のひとつは、ローカルの端末等に依存ライブラリをインストールしなくても
    [Ansible
    playbook](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html)（外部サイト）のコードを編集しデプロイできることです。デモでご紹介するプロジェクトは、セキュリティポリシーに従って毎月すべてのデバイスのSNMP文字列を更新する必要がありますが、これはGitLabがホストする
    [GitLab.com](https://about.gitlab.com/ja-jp/pricing/)で簡単に行なうことができます。  


    まずはじめに、Ansible playbookを開いてください。ここには次の4つのタスクがあります。


    * ルーターの情報を収集  

    * バージョンを表示  

    * シリアル番号を表示  

    * SNMPを構成


    このデモでは、SNMPの文字列の構成方法に焦点を当てています。構成は簡単な一連のステップに従うことで完了できます。


    ## はじめに：イシューボード


    GitLabではプロジェクトに関する計画はすべて同じやり方、[イシューの起票](https://handbook.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/getting-started/101/#issue)から始まります。そのためGitLabのワークフローの最初のステップでは、[ansible-demoプロジェクト](https://gitlab.com/bdowney/ansible-demo)内のイシューボードを確認します。[ansible-demoイシューボード](https://gitlab.com/bdowney/ansible-demo/-/boards)を見ると、すでに
    [すべてのルーターのSNMP文字列の変更](https://gitlab.com/bdowney/ansible-demo/issues/4)に関連するイシューがあることがわかります。イシューの中に、SNMP文字列を毎月ローテーションさせ、読み取り専用と読み書き両方の場合には、異なる文字列を使用する必要があることを記述したGitLabセキュリティポリシーのWikiへのリンクがあります。


    ![Security
    policies](https://about.gitlab.com/images/blogimages/ansible_screenshots/security_policies_1A.png){:
    .shadow.medium.center}

    SNMP 文字列用の GitLab セキュリティポリシー

    {: .note.text-center}


    GitLabのセキュリティポリシーによると、SNMP文字列を毎月更新する必要があります。  


    次に、[2つのルーターのデモ](https://gitlab.com/bdowney/ansible-demo/blob/master/ci-cd-demo/ci.yml)でSNMP文字列を設定するコマンドが、イシューで概説されているGitLabのセキュリティポリシーに従っていることを確認します。


    ![Ansible SNMP
    change](https://about.gitlab.com/images/blogimages/ansible_screenshots/ansible_snmp_change_2.png){:
    .shadow.medium.center}

    SNMP 文字列設定のためのコマンド

    {: .note.text-center}


    SNMP文字列を設定するコマンドは、Ansible playbookに記載されています。


    次に、イシューに戻り、イシューをご自身にアサインしてください。右サイドバーのラベルを`to-do`から`doing`に切り替えるか、または、イシューボードの列をドラッグして移動することもできます。


    ## マージリクエストを作成する


    次のステップでは、イシューからマージリクエスト (MR)
    を作成します。「Draft」のフラグがMRに付いていることを再度確認してください。これにより、準備ができていないうちにmasterブランチにマージされてしまうことが防止されます。ここでは、SNMP文字列への変更点が少ないので、ローカルの
    IDEでソースコードを編集するのではなく、GitLabの [Web
    IDE](https://docs.gitlab.com/ee/user/project/web_ide/) を使います。  


    * [CI/CD](https://about.gitlab.com/topics/ci-cd/)デモセクションを開きます。  

    * Ansible playbookに移動します。  

    * SNMPセクションを次のように編集します。


    ```

    -snmp-server community New-SNMP-DEMO1 RO


    -snmp-server community Fun-SNMP-RW-STR RW

    ```


    *  [イシュー](https://gitlab.com/bdowney/ansible-demo/issues/1)で説明されている[GitLab
    セキュリティポリシー](https://gitlab.com/bdowney/ansible-demo/wikis/Security-Policies)に従い、ROとRWが異なる文字列として設定されていることに注意してください。


    ## 変更をコミットする


    SNMP文字列がガイドラインに沿って更新されましたので、変更をコミットします。最新のコミットでMRが更新されたことを確認するために、side-by-side比較機能を利用してください。


    ![Commit
    changes](https://about.gitlab.com/images/blogimages/ansible_screenshots/side-by-side_3.png){:
    .shadow.medium.center}

    GitLab Ansible 内でのマージリクエストのサイド・バイ・サイド比較

    {: .note.text-center}


    並べて比較できるツールにより、変更内容が一目でわかります。


    ## マージリクエストの出力


    変更内容をコミットすると、GitLab CIパイプラインが自動的に起動されます。ここでは、次のようなタスクが実行されます。

    構文のチェック

    ドライラン

    ラボ/シミュレーション環境での変更点のテスト

    GitLab CIパイプラインの各ジョブの進捗状況と出力を表示して、SNMPの更新を実行します。


    ![Job
    running](https://about.gitlab.com/images/blogimages/ansible_screenshots/job_running_4.png){:
    .shadow.medium.center}

    GitLabジョブの出力

    {: .note.text-center}


    ジョブからの出力を確認して、シミュレーション環境でSNMPの更新が確実に行なわれたことを確認します。

    このすべてのタスクは、マージリクエスト (MR) 内で実行され、記録されます。


    ![Pipeline](https://about.gitlab.com/images/blogimages/ansible_screenshots/pipeline_5A.png){:
    .shadow.medium.center}

    GitLab CIパイプライン内のチェックマーク

    {: .note.text-center}


    緑色のチェックマークは、GitLab CIパイプラインで各タスクが正常に完了したことを示しています。

    次に、ラボのルーターにログインして、変更内容を確認します。


    ![routers
    snmp](https://about.gitlab.com/images/blogimages/ansible_screenshots/routersnmp_6.png){:
    .shadow.medium.center}

    ルーターのSNMP

    {: .note.text-center}


    読み取り専用（RO）および読み書き（RW）のSNMP文字列の変更点がルーターに反映されています。


    ## マージリクエストのレビュー


    オプションとして、[マージリクエスト（MR）の承認](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)をアクティブ化することもできます。これにより、変更が本番環境に送られる前に、より多くのユーザーが変更点をレビューできるようになります。


    ![approvers](https://about.gitlab.com/images/blogimages/ansible_screenshots/approvers_7.png){:
    .shadow.medium.center}

    GitLabでのSNMP文字列の更新

    {: .note.text-center}


    マージリクエスト（MR）は、masterブランチへのマージを行なう前に、別のユーザーに作業内容をレビューしてもらうように設定することができます。


    ## Masterブランチへのマージ


    テストが完了したら、変更点をmasterブランチにマージします。masterブランチには、本番環境コードが格納されています。

    準備ができたら、`Mark`ボタンをクリックし、次に`Merge`をクリックします。

    「Draft」のステータスを解決すると、MRがマージされ、イシューがクローズされます。

    すると、新しいパイプラインが実行され、追加のステップとして playbookを本番環境で実行する、すべてのテストが実行されます。

    「パイプライン」の画面では、その進捗とログが確認できます。このプロセスが完了したら、本番用ルーターにログインし、SNMPセキュリティ文字列が更新されたことを確認します。


    ## GitLab CIの魔法


    これまで説明してきたさまざまな機能を可能にした魔法は GitLab CIです。GitLab
    CIパイプラインとは、Ansibleコードをテストしてデプロイするために必要なあらゆることを実行する、一連の連続したタスクを意味します。


    GitLab CIは、リポジトリ内に存在する単一のシンプルな [YAML
    ファイル](https://about.gitlab.com/blog/2020/10/01/three-yaml-tips-better-pipelines/)である`.gitlab-ci.yml`で構成されています。


    このデモでは`.gitlab-ci.yml`ファイルが3つのステージで構成されていることがご確認いただけます。

    1. Deploy：Ansibleを使用する AWSに 、２つのルーターのシミュレーションネットワークが作成されます。

    2. Demo：SNMP文字列を変更するplaybook が実行されます。

    3. Destory：２つのルーターのシミュレーションネットワークが破棄されます。


    GitLab CIは、ベースイメージで始まります。この場合、必要なすべての
    Ansibleバイナリと依存ライブラリを含むDockerイメージを使用しています。必要に応じて、それぞれのステージで実行するコマンドと、依存関係を指定します。


    ![More
    code](https://about.gitlab.com/images/blogimages/ansible_screenshots/more_code_9A.png){:
    .shadow.medium.center}

    単純なYAMLファイルには、GitLab CIの3つのフェーズが含まれます。

    {: .note.text-center}


    ![More
    Code](https://about.gitlab.com/images/blogimages/ansible_screenshots/more_code_10A.png){:
    .shadow.medium.center}

    GitLab CIのデモレベル

    {: .note.text-center}


    GitLab CIのデモステージを覗いてみましょう。これはAnsible playbookを実行しているものです。


    今度はパイプラインを見てみましょう。GitLab
    CIを使用すれば、コンピュータにAnsibleの依存関係をインストールすることなく構成管理を実装できることがわかります。これは、IaCを実行するために
    GitLab CIを使用する方法の一例に過ぎません。以下のリンクは、完全版チュートリアルの動画ですので、ぜひご覧ください。


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/M-SgRTKSeOg" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## FAQ - よくある質問


    ### Q: Ansibleとは


    A:
    Ansibleとは、ITプロセスの自動化と構成管理を可能にするオープンソースのツールです。アプリケーションのデプロイ、構成管理、オーケストレーション、プロビジョニングなど、コンピュータシステムの管理を自動的に行なう際に使用します。


    Ansibleを使用すると、サーバーの構成やアプリケーションのデプロイ、またネットワークデバイスの管理といった、複雑な、繰り返しの多いワークフローのオーケストレーションが可能になります。AnsibleはYAMLを使って、playbookとして知られている宣言型のタスク説明を作成します。作成された宣言型のタスク説明は、コマンドを実行するターゲットホストへのSSH接続を通じてAnsibleが行なう、望まれるシステム状態を説明してくれます。詳しくは[こちら](#heading=h.llxgny6efk4z)をご覧ください。


    ### Q: Ansible Playbookとは


    A: Ansible
    Playbookはyamlで書かれたタスクのリストファイルのことで、指定したインベントリーやホストのグループに対して自動的に実行されます。ネットワークインフラ、WindowsサーバーなどITインフラに適用するタスクやコンフィグレーションをこのファイルで定義します。Ansibleタスクは、1つまたは複数タスクがplayとしてグループ化され、それぞれのplayが特定のホストやホストグループに対して実行されます。クラウド管理、ユーザー管理、ネットワーク、セキュリティ、構成管理などがAnsible
    Playbookで管理できます。


    ### Q: GitLab CIとは


    A: GitLab CI（継続的インテグレーション、Continuous
    Integration）とは、GitLab上でコードの変更が行われた際に自動的にテストやビルドを実行するツールで、サードパーティのツールやライブラリを導入しなくても利用できます。GitLab
    CIは、すべてのコード変更を共有ソースコードリポジトリのmainブランチに早い段階で頻繁に統合し、コミットやマージ時に各変更を自動的にテストし、自動的にビルドを開始するプラクティスのことです。継続的インテグレーションを行うことで、エラーやセキュリティの問題をより簡単に、開発プロセスのかなり早い段階で特定し、修正することが可能になります。詳しくは[こちら](https://about.gitlab.com/ja-jp/topics/ci-cd/#what-is-continuous-integration-ci)をご覧ください。


    ### Q: GitLab CIとAnsibleを使用するメリットは何ですか


    A: GitLab CIなら、ローカルの端末等に依存ライブラリをインストールしなくてもAnsible
    playbookのコードを編集しデプロイできます。また、GitLab
    CIパイプラインは、Ansibleコードをテストしてデプロイするために必要なあらゆることを実行する、一連の連続したタスクです。つまり、GitLabでは、リポジトリの管理とCIおよびAnsible
    Towerのワークフロー実行が行なえます。加えられた変更の内容、たとえば、いつ、誰がどのファイルに対して変更を行なったのかも自動で記録されるうえ、マージリクエスト(MR)を使えば、理由や目的などのメモも残せるなど、いろいろなメリットがあります。


    *\*監修：伊藤 俊廷 [@toshitakaito](https://gitlab.com/toshitakaito) 

    （GitLab合同会社 ソリューションアーキテクト本部 スタッフソリューションアーキテクト）*
  category: エンジニアリング
  tags:
    - demo
    - CI/CD
config:
  slug: using-ansible-and-gitlab-as-infrastructure-for-code
  featured: false
  template: BlogPost
