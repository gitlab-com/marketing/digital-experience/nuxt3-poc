seo:
  title: GitLab Duoエンタープライズを提供開始
  description: AIパートナーの登場です。GitLab Duoエンタープライズが、DevSecOpsのライフサイクル全体にどのようなメリットをもたらすかご紹介します。
  ogTitle: GitLab Duoエンタープライズを提供開始
  ogDescription: AIパートナーの登場です。GitLab Duoエンタープライズが、DevSecOpsのライフサイクル全体にどのようなメリットをもたらすかご紹介します。
  noIndex: false
  ogImage: images/blog/hero-images/Untitled-(1800-x-945-px).png
  ogUrl: https://about.gitlab.com/blog/gitlab-duo-enterprise-is-now-available
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-duo-enterprise-is-now-available
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Duoエンタープライズを提供開始",
            "author": [{"@type":"Person","name":"David DeSanto, Chief Product Officer, GitLab"}],
            "datePublished": "2024-09-03",
          }

content:
  title: GitLab Duoエンタープライズを提供開始
  description: AIパートナーの登場です。GitLab Duoエンタープライズが、DevSecOpsのライフサイクル全体にどのようなメリットをもたらすかご紹介します。
  authors:
    - David DeSanto, Chief Product Officer, GitLab
  heroImage: images/blog/hero-images/Untitled-(1800-x-945-px).png
  date: '2024-09-03'
  body: >
    [GitLab
    Duoエンタープライズ](https://about.gitlab.com/ja-jp/gitlab-duo/)は、ソフトウェア開発ライフサイクル全体に適用できるように設計されたエンドツーエンドのAIパートナーです。この強力なAIツール群は、デベロッパーの生産性向上、セキュリティの強化、コラボレーションの効率化、そしてDevSecOpsプロセスの加速を目的として設計されました。


    主な機能は次のとおりです。

    - 25以上のプログラミング言語に対応したインテリジェントなコード支援

    - AIによるセキュリティ脆弱性の詳細情報と解決策の提示テスト生成と根本原因分析の自動化

    - テスト生成と根本原因分析の自動化

    - AI生成のサマリーによるチームコラボレーションの改善

    - AIインパクトダッシュボードによるROIの定量化


    ## GitLab Duoエンタープライズを開発した理由


    組織は、より高品質なソフトウェアを迅速に提供し、顧客価値を高めようとする中で、その進捗を妨げる大きな課題に直面しています。[当社の調査（英語）](http://about.gitlab.com/developer-survey/2024/ai)によると、95%の企業がソフトウェア開発プロセスにおいてAIの導入を検討しているか、すでに使用しています。しかし、55%の回答者が、ソフトウェア開発にAIを使用することにはリスクが伴うと感じています。


    企業が直面する一般的な問題には、デベロッパーのエクスペリエンスや生産性の最適化が不十分であること、セキュリティやコンプライアンスの要求が増加していること、チーム間のコラボレーションが非効率であること、AI技術への投資に対するROIの評価が困難であることが挙げられます。GitLab
    Duoエンタープライズは、これらの課題に真っ向から取り組むために開発され、開発チームに対して安全で効率的かつ強力なAIパートナーを提供します。


    **このブログでは、GitLab
    Duoエンタープライズによって、企業のソフトウェア開発とデプロイにどのようなメリットがもたらされるかをご紹介します。** 


    ## インテリジェントなコード支援を活用してデベロッパーの生産性を高める


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1004252678?h=83f35171b6&amp;badge=0&amp;badge=0&amp?autoplay=1&loop=1&autopause=0&background=1&muted=1"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Code
    Suggestions clip"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>

    <p></p>


    ソフトウェア開発における主なハードルのひとつは、日常的なコーディング作業に時間がかかることです。しかし、次の機能を活用すれば、より重要な作業にすばやく着手できます。


    -
    __コード提案__：25以上のプログラミング言語に対応しています。このAI搭載機能を使用することで、コード作成の高速化、コード品質の向上、および定型作業にかかる時間の短縮を実現できます。


    しかし、新しいコードを作成することだけが目的ではありません。


    - __コードの説明__（GitLab Duoエンタープライズ機能）：デベロッパーが複雑なコードや不慣れなコードをすばやく理解できるよう支援します。


    -
    **コードリファクタリング**：この機能を使用することで、デベロッパーは[既存のコードの改善とモダナイゼーション（英語）](https://about.gitlab.com/blog/2024/08/26/refactor-code-into-modern-languages-with-ai-powered-gitlab-duo/)を行えます。


    -
    __テスト生成__：包括的なユニットテストの作成を自動化します。これにより、デベロッパーがイノベーションを促進する価値の高いタスクに集中できるようになり、結果として開発サイクルの短縮とソフトウェアの品質向上につながります。


    >
    [欧州のテクノロジー企業であるCube社（英語）](https://about.gitlab.com/customers/cube/)が、コード提案やテスト生成、その他のGitLab
    Duo機能を活用して、どのようにスピードと効率を大幅に向上させているかをご確認ください。


    ## チームのコラボレーションとコミュニケーションを強化


    効果的なコラボレーションは、ソフトウェア開発を成功に導くための基盤となります。しかし、長時間にわたる議論、複雑なマージリクエスト、そして時間のかかるコードレビューによって、その実現が妨げられてしまうことも多々あります。GitLab
    Duoエンタープライズなら、次に挙げる一連の要約（サマリー生成）機能とテンプレートツールを使用してこれらの課題に対処できます。


    - __ディスカッションサマリー__：イシュー内の長い議論内容を要約し、チームメンバーがすばやく理解できるよう支援します。

    - __マージリクエストサマリー__：提案された変更の概要を明確かつ簡潔に説明します。

    - __コードレビューサマリー__：レビュープロセスを効率化し、作成者とレビュアー間の引き継ぎをスムーズにします。


    GitLab
    Duoエンタープライズは、より明確なコミュニケーションと迅速な意思決定を促進し、チームがより効率的に仕事をし、より迅速に成果を上げられるよう支援します。


    ## トラブルシューティングとデバッグを効率化


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1004252688?h=fc6c048bfd&amp;badge=0&amp;badge=0&amp?autoplay=1&loop=1&autopause=0&background=1&muted=1"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Root
    Cause Analysis clip"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>

    <p></p>


    開発パイプラインが失敗した場合、プロジェクトタイムラインに大きな影響が生じる可能性があります。ここでは、GitLab
    Duoエンタープライズの__根本原因分析__機能が活躍します。根本原因分析は、ログを自動分析し、失敗の詳細な説明と修正手順を提示することで、トラブルシューティングにかかる時間を大幅に短縮できます。


    このメリットは、単なる時間の節約にとどまりません。[CI/CDビルドの問題解決を高速化](https://about.gitlab.com/ja-jp/blog/2024/06/06/developing-gitlab-duo-blending-ai-and-root-cause-analysis-to-fix-ci-cd/)することで、チームのスピード維持、ダウンタイムの削減、そして最終的には、ソフトウェアアップデートの頻度と信頼度を高めることができます。


    ## 開発ライフサイクル全体のセキュリティを強化


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1004252706?h=73e568b89c&amp;badge=0&amp;badge=0&amp?autoplay=1&loop=1&autopause=0&background=1&muted=1"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="Vulnerability Explanation and Resolution clip"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>

    <p></p>


    サイバーセキュリティの脅威は常に存在しているため、堅牢なアプリケーションセキュリティが必要です。GitLab Duoエンタープライズは、
    __脆弱性の説明機能__ と __脆弱性の修正機能__
    を備えています。これらのAI搭載ツールは、[デベロッパーがセキュリティの脆弱性を完全に理解（英語）](https://about.gitlab.com/blog/2024/07/15/developing-gitlab-duo-use-ai-to-remediate-security-vulnerabilities/)し、提案された修正案が反映されたマージリクエストを自動生成します。


    ## AIの影響を定量化して戦略的意思決定を実現


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1004252663?h=d35106288b&amp;badge=0&amp?autoplay=1&loop=1&autopause=0&background=1&muted=1"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="AI
    Impact Dashboard clip"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>

    <p></p>


    技術投資のROIを示すことは非常に重要です。このニーズを確実に満たすために、GitLab Duoエンタープライズには
    __AIのインパクトダッシュボード__
    が備わっています。この分析ツールは、バリューストリーム分析とDORA4メトリクスに基づいて構築されており、サイクルタイムの改善やデプロイ頻度の向上に関連する具体的なメトリクスを提示します。これにより組織は、開発プロセスへのAI導入がもたらす明確なメリットを定量化できます。


    [AIインパクトダッシュボード](https://about.gitlab.com/ja-jp/blog/2024/05/15/developing-gitlab-duo-ai-impact-analytics-dashboard-measures-the-roi-of-ai/)には、AIの活用が主要な生産性メトリクスとどのように関連しているかのインサイトが表示されます。これにより、経営陣はリソース配分や戦略的な技術投資に関するデータに基づいた意思決定を行えるようになります。


    ## AIが導くDevSecOpsの新時代を迎えましょう


    GitLab Duoエンタープライズの発表に際し、GitLabが初の[Gartner® Magic
    Quadrant™のAIコードアシスタント部門](https://about.gitlab.com/ja-jp/gartner-mq-ai-code-assistants/)のリーダーの1社に選定されたことをご案内します。この評価は、真のビジネスバリューをもたらすAIソリューションを提供するGitLabの取り組みを強調するものです。


    ソフトウェア開発の未来はすでにここにあり、それを支えているのはAIです。GitLabは、DevSecOpsのライフサイクル全体にインテリジェントで拡張性に優れたAIを組み込むサポートを提供し、組織が顧客に成果をより迅速に届けられるよう支援します。


    > [60日間の無料トライアルでGitLab
    Duoエンタープライズを今すぐ始めましょう！](https://about.gitlab.com/solutions/gitlab-duo-pro/sales/?type=free-trial&toggle=gitlab-duo-pro)
  category: AIと機械学習
  tags:
    - AI/ML
    - product
    - DevSecOps platform
    - features
    - news
config:
  slug: gitlab-duo-enterprise-is-now-available
  featured: true
  template: BlogPost
