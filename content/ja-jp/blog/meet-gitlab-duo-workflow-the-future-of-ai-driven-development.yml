seo:
  title: GitLab Duo Workflowの紹介 - AI主導の開発の未来
  description: >-
    自律型AIエージェントであるWorkflowは、チームがソフトウェアを構築してデリバリーする方法に変革をもたらします。Workflowの登場により、GitLabはAI主導のDevSecOpsの実現に向けた強力な最初の一歩を踏み出します。
  ogTitle: GitLab Duo Workflowの紹介 - AI主導の開発の未来
  ogDescription: >-
    自律型AIエージェントであるWorkflowは、チームがソフトウェアを構築してデリバリーする方法に変革をもたらします。Workflowの登場により、GitLabはAI主導のDevSecOpsの実現に向けた強力な最初の一歩を踏み出します。
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(20).png
  ogUrl: >-
    https://about.gitlab.com/blog/meet-gitlab-duo-workflow-the-future-of-ai-driven-development
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/meet-gitlab-duo-workflow-the-future-of-ai-driven-development
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Duo Workflowの紹介 - AI主導の開発の未来",
            "author": [{"@type":"Person","name":"David DeSanto, Chief Product Officer, GitLab"}],
            "datePublished": "2024-06-27",
          }

content:
  title: GitLab Duo Workflowの紹介 - AI主導の開発の未来
  description: >-
    自律型AIエージェントであるWorkflowは、チームがソフトウェアを構築してデリバリーする方法に変革をもたらします。Workflowの登場により、GitLabはAI主導のDevSecOpsの実現に向けた強力な最初の一歩を踏み出します。
  authors:
    - David DeSanto, Chief Product Officer, GitLab
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(20).png
  date: '2024-06-27'
  body: >
    ソフトウェアのコードを、ソフトウェア自らが作成できるとしたらどうでしょう？それは遠い未来でしか実現できないことのように思えますが、絶えず成長する大規模言語モデル（LLM）とGitLabの一元化されたAI搭載のDevSecOpsプラットフォームにより、その未来は間近に迫っています。GitLabは、[GitLab
    17のリリースイベント](https://about.gitlab.com/seventeen/)でGitLab Duo
    Workflowについて発表しました。GitLab Duo
    Workflowは、チームがソフトウェアのビルド、保護、デプロイ、モニタリングする方法に変革をもたらす自律型AIエージェントです。


    GitLab Duo
    Workflowは、ソフトウェア開発ライフサイクルのあらゆる側面の最適化に積極的に貢献する自律的なチームメンバーを作り出し、プロンプトベースで受け身でしかなかったAIアシスタントのこれまでの状況を刷新します。Workflowが特に優れている点は、関連するすべてのデータ、プロジェクト、リポジトリ、ドキュメントをシームレスにつなげるGitLabの統合されたデータストアを活用していることです。これにより、Workflowはインテリジェントで常時稼働するエージェントとして、プロジェクトの常時モニタリング、本番環境で起こりうる問題の予測、自動的な脆弱性の特定・修正、パフォーマンスの最大化を目的としたアプリケーションの最適化、カスタマイズされたリモート開発環境の迅速な構築によるオンボーディングの効率化をすることができます。


    今やAIにより、安全なソフトウェアの開発、メンテナンス、更新、デプロイ、モニタリングの方法が変容し、従来よりも多くのソフトウェアをデリバリーできるようになっています。GitLab
    Duo Workflowは、AI主導のDevSecOpsの実現に向けた強力な第一歩です。当社は、[GitLab
    Duo](https://about.gitlab.com/gitlab-duo/)によって繰り返しの作業を処理し、外からは見えない部分を最適化することで、デベロッパーが高度な問題解決や価値創造に注力できるようにすることを目指しています。


    ## GitLab Duo Workflowのビジョン

    GitLab Duo
    Workflowでは、いくつかの主要なユースケースに重点的に取り組むことで、ソフトウェア開発プロセス全体を自動化および最適化しています。

    ### 1. 開発の自動化


    GitLab Duo
    Workflowは、IDE上で直接、個々のプロジェクトや定義済みの組織プロセスに合わせたタスクを計画や、優先順位付けを支援します。特定の作業アイテム（エピックやイシュー、タスクなど）の要件に基づき、デベロッパーがレビューした上で改良できる実装計画を作成します。その後、Workflowは計画に沿って作業を進め、定義された要件を達成できるように、コードの生成または修正を行います。Workflowではこれらの処理が[GitLabリモート開発ワークスペース](https://about.gitlab.com/blog/2023/06/26/quick-start-guide-for-gitlab-workspaces/)内で行われるため、安全かつ確実にコード変更の記述、評価、テストが実施されます。また、これにより、Workflowが要件を満たすだけでなく、セキュリティスキャンを含むすべてのCIパイプラインテストに通るコードを生成することも保証されます。パイプラインが失敗した場合、Workflowは自動的に問題に対処し、必要に応じてトラブルシューティングを行い、組織の基準を満たす高品質のコードのみが作成され、プロジェクトにコミットされるようにします。


    準備ができたら、Workflowはコード変更をまとめたマージリクエストを自動的に作成し、コードのレビュアーや管理者とのやり取りなど、マージリクエストの承認プロセスを実行します。人間のレビュアーが現在行っているように、Workflowにコードをレビューさせて、マージリクエストにコメントを残させるよう指示することすら可能です。さらに、必要に応じてWorkflowは提案内容を実装することもできます。そして、これはまだ始まりに過ぎません。


    ### 2. インテリジェントな継続的改善

    GitLab Duo
    Workflowは、コードベースをリアルタイムで分析し、効率、パフォーマンス、コスト削減を向上させるためにアーキテクチャの最適化を提案します。さらに、デベロッパーに対して変更を提案したり、サンドボックス環境に変更を自動的に実装することで、スケーラビリティを向上させ、技術的負債に対処するためのコードリファクタリングの機会を積極的に特定します。また、Workflowはクラウドリソースを動的に管理して、オーバープロビジョニングを回避し、アプリケーションが常にパフォーマンス目標を満たせるようにします。


    ### 3. 積極的なセキュリティとコンプライアンス対応

    どのような組織においても、セキュリティとコンプライアンスは最優先事項です。GitLab Duo
    Workflowは、パッチの適用や脆弱なコードのリファクタリングに加え、新たに出現する脅威にリアルタイムで対応するよう、デベロッパーに対して自動的に指示を出します。さらに、アプリケーションと本番環境に関連するセキュリティリスクを継続的に評価し、軽減策の実装を支援します。


    ### 4. パフォーマンスの向上に向けた自己最適化

    GitLab Duo
    Workflowには、継続的な学習と改善を行うために高度なフィードバックループが組み込まれています。モニタリングツールやユーザーとのやり取り、ビジネス成果から得たデータの分析を通じて、アプリケーションのアーキテクチャがビジネスニーズに常に合致するように、コードベースを絶えず改善します。すべてのAIと同様、Workflowは絶えず改善され、組織のパートナーとして成長しながら、自らのミスを見つけて、修正できるようになります。


    <!-- 空白行 -->

    <figure class="video_container">
      <iframe src="https://player.vimeo.com/video/967982166?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allowfullscreen="true" title="GitLab Duo Workflow the future of AI-driven DevSecOps"> </iframe>
    </figure>

    <!-- 空白行 -->


    ## 未来のAIはすでに現実に

    GitLab Duo
    Workflowの登場は、常に人間が指示する必要があったAIから、必要なときにのみ人間の指示を受けて開発のワークフローとプロセスを推進するAIへと移行する、大きな前進を意味します。DevSecOpsライフサイクルをカバーするGitLabの統一されたAI主導のインターフェイスを利用することで、最高水準のセキュリティとコンプライアンスを維持しながら、他に類を見ないスピード、効率性、イノベーションを実現して、新世代のAI搭載型アプリケーションを開発できます。その際に、トレードオフが生じることはありません。


    GitLabでは今後もソフトウェア開発においてAIができることの範囲を広げていきますので、インサイトや最新情報をお見逃しなく。一緒にAI主導のDevSecOpsの未来に進み、チームと組織の可能性を最大限に解き放ちましょう。


    *監修：大井 雄介 [@yoi_gl](https://gitlab.com/yoi_gl)

    （GitLab合同会社 ソリューションアーキテクト本部 本部長）*


    > AI主導のDevSecOpsに関心があり、ぜひプレリリースプログラムに参加して体験してみたいという方は、[GitLab Duo
    Workflowウェイティングリスト](https://forms.gle/5ppRuNVb8LwSPNVJA)にご登録ください。
  category: AIと機械学習
  tags:
    - AI/ML
    - DevSecOps platform
    - DevSecOps
    - news
    - workflow
config:
  slug: meet-gitlab-duo-workflow-the-future-of-ai-driven-development
  featured: true
  template: BlogPost
