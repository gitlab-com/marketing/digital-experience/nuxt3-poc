seo:
  title: 金融サービス業界向け：GitLabの職務分離機能を実装する方法
  description: >-
    金融サービス業界において、GitLabの職務分離機能を活用して安全でコンプライアンスに準拠したソフトウェア開発を実現する方法をご説明します。また、規制フレームワークの遵守を支援する機能も併せてご紹介します。
  ogTitle: 金融サービス業界向け：GitLabの職務分離機能を実装する方法
  ogDescription: >-
    金融サービス業界において、GitLabの職務分離機能を活用して安全でコンプライアンスに準拠したソフトウェア開発を実現する方法をご説明します。また、規制フレームワークの遵守を支援する機能も併せてご紹介します。
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(6).png
  ogUrl: >-
    https://about.gitlab.com/blog/finserv-how-to-implement-gitlabs-separation-of-duties-features
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/finserv-how-to-implement-gitlabs-separation-of-duties-features
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "金融サービス業界向け：GitLabの職務分離機能を実装する方法",
            "author": [{"@type":"Person","name":"Cherry Han"},{"@type":"Person","name":"Gavin Peltz"}],
            "datePublished": "2024-08-13",
          }

content:
  title: 金融サービス業界向け：GitLabの職務分離機能を実装する方法
  description: >-
    金融サービス業界において、GitLabの職務分離機能を活用して安全でコンプライアンスに準拠したソフトウェア開発を実現する方法をご説明します。また、規制フレームワークの遵守を支援する機能も併せてご紹介します。
  authors:
    - Cherry Han
    - Gavin Peltz
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(6).png
  date: '2024-08-13'
  body: >-
    ソフトウェア開発の過程において、特に金融サービスのようなデータの完全性や規制の遵守が不可欠な業界では、強固なセキュリティとコンプライアンス対策が求められます。これらの基準を維持する上で重要な要素の1つが職務分離（SoD）です。SoDは、プロセス全体の管理を1人に割り当てないようにすることで、エラーや不正行為のリスクを軽減するアプローチです。SoDにより、ソフトウェア開発プロセスの完全性を損なう可能性のある外部からの悪意ある行為を防ぎ、ソフトウェアサプライチェーンにおけるリスクを軽減できます。


    ## 金融サービス業界におけるSoDの重要性


    金融サービス業界では、SoDが機密情報の保護やコンプライアンス遵守の確保を実現する上で重要な役割を果たします。SoDは、金融サービス業界において戦略的に以下のようなメリットをがあります。


    *
    **リスク軽減**：職務を複数の役割に分担することで、システムの完全性や規制コンプライアンスの遵守を損なう可能性のあるエラー、不正行為、無許可のアクティビティのリスクを軽減します。

    *
    **責任の強化**：明確な職務分担により、1人の担当者がプロセス全体を一貫して管理することができなくなり、結果として透明性と責任意識が高まります。透明性と責任意識は、ステークホルダーや規制当局との信頼関係を維持する上で不可欠です。

    *
    **規制コンプライアンス**：SoDは、機密性の高い業務が監視と審査の下で行われることを保証する目的で、金融規制によって義務付けられています。これらの基準を遵守することで、ペナルティを回避できるだけでなく、組織の評判も保護されます。

    *
    **運用の強靭性**：意思決定と実行を分散することで、組織は人的ミスや悪意ある行為、および予期せぬ出来事によってもたらされる混乱の影響を受けにくくなります。


    ## GitLabによる職務分離とベストプラクティス

    GitLabでは、DevSecOpsワークフロー全体を対象としたエンドツーエンドの職務分離機能を使用できます。


    ![金融サービスのSoD -
    画像1](//images.ctfassets.net/r9o86ar0p03f/5cfiEUlHPVXSgEngmSItaw/3b59405149f069e3f86fa8378a5b1d8f/image1.png)


    上記の図は、SoDの原則を維持するために、マージリクエスト承認ポリシー、保護機能、ユーザー権限、コンプライアンスフレームワーク、監査イベントといった重要な要素がどのように統合され、連携しているかを示しています。各要素については、後続のセクションで安全かつコンプライアンスに準拠した開発環境の構築方法を解説する中で、詳述します。


    ### マージリクエスト承認ポリシー


    金融サービス業界が直面する課題のひとつに、不正またはチェックされていない変更が統合されるのを防ぐ承認メカニズムの実装があります。ここで役立つのが[マージリクエスト承認ポリシー](https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html)です。このポリシーにより、セキュリティと開発の職務分離が強制され、デベロッパーが脆弱性を含むコード変更を自分で承認したり、開発チームが適切な監視なしにコードを本番環境に直接デプロイしたりすることを防止できます。


    ポリシーを作成する際には、承認者として誰が適切であるかを検討することをおすすめします。承認者には、個人ユーザー、アプリケーションセキュリティチームなどのグループ、あるいはメンテナーといったロールタイプを指定できます。さらに制約を加えたい場合は、以下の主要なポリシー機能の使用をご検討ください。


    -
    作成者による承認を防止：このポリシーにより、マージリクエストの作成者が自身の変更を承認できないようガードレールが設定されます。独立したレビューを求めることで、承認プロセスの客観性と公平性が維持されます。


    -
    コミットを追加したユーザーによる承認を防ぐ：マージリクエストにコミットを追加したユーザーも、承認を行うことができないようにします。これにより、変更に直接関わっていないチームメンバーによって検証が行われる、独立したレビューの原則がさらに強化されます。


    -
    承認ルールの編集を防止：承認プロセスの整合性を維持するため、GitLabではプロジェクトやマージリクエストレベルで承認ルールの編集を管理者が禁止できます。これにより、一度定義された承認ポリシーが無断で変更されたり回避されたりしないよう保証されます。


    -
    承認にユーザーパスワードを要求：追加のセキュリティ対策として、GitLabではマージリクエストを承認するユーザーに対して、パスワードの入力を求めることができます。


    職務分離を明確に維持するため、マージリクエスト承認ポリシーを含むセキュリティポリシーを格納するための専用の[トップレベルグループを別途作成](https://docs.gitlab.com/ee/user/application_security/policies/#enforce-policies-globally-in-gitlab-dedicated-or-your-gitlab-self-managed-instance)することが推奨されます。この設定により、権限を継承するユーザーの数が最小限に抑えられ、ポリシー管理を厳格に制御できます。この専用グループから、目標に合う最上位グループレベルで[セキュリティポリシープロジェクトをリンク](https://docs.gitlab.com/ee/user/application_security/policies/#link-to-a-security-policy-project)することで、ポリシー管理の手間を軽減し、開発環境全体で包括的なカバレッジを確保できます。


    また、ポリシーがデフォルトで有効になっている場合、そのポリシーは関連するリンクされたグループ、サブグループ、および個別のプロジェクト内のすべてのプロジェクトに適用されることにも注意してください。より対象を絞ってポリシーを適用したい場合、GitLabは[コンプライアンスフレームワークラベル](https://docs.gitlab.com/ee/user/group/compliance_frameworks.html)を使用してポリシーの適用範囲を絞り込むことを推奨しています。厳格な規制に対応しているお客様の多くは、「SOX」や「PCI」などの規制要件に対応するコンプライアンスラベルを作成しています。また、このフレームワークへのリンクにより、[ネイティブコンプライアンスセンター](https://docs.gitlab.com/ee/user/compliance/compliance_center/)でさまざまなユースケースに合わせたセキュリティポリシーを管理できるようになります。


    ### コンプライアンスフレームワークと制御


    規制対象の業界に属するお客様は、大規模な組織においてコンプライアンスを維持する上で大きな課題に直面しています。手動のプロセスはエラーが生じやすく、チーム間でポリシーを一貫して適用し続けることが難しい場合もあります。


    GitLabのコンプライアンスフレームワークを使用することで、組織は予防措置の自動化と管理、リスクの体系的な管理、そして規制コンプライアンスのシームレスな適用を実現できます。これらのフレームワークによって、どのようなパイプラインでもセキュリティプロトコルやカスタムジョブを実施できます。


    コンプライアンス設定を組織レベルで保護するために、GitLabではグループまたはプロジェクトのオーナーのみがコンプライアンスフレームワークを追加または削除できるようになっています。この対策により、適切な権限レベルを持たない開発チームやマネージャーによるコンプライアンス設定の変更が防止され、セキュリティがさらに強化されます。なお、メンテナー権限を持つ個人がサブグループを作成できる場合、そのサブグループのオーナーとなりコンプライアンスフレームワークを変更できる点に注意してください。これを防ぐには、権限とグループの設定で[サブグループの作成者](https://docs.gitlab.com/ee/user/group/subgroups/#change-who-can-create-subgroups)を制限してください。


    ## SoDのための権限とロール


    金融サービス業界で職務分離を効果的に実施するには、明確かつ正確なアクセス制御の設定が不可欠です。GitLabには、あらかじめ定義されたロール（ゲスト、レポーター、デベロッパー、メンテナー、オーナーなど）による階層的な[権限モデル](https://docs.gitlab.com/ee/user/permissions.html)が備わっています。各ロールには特定の権限セットが割り当てられており、個人が業務を遂行する際に自身の権限の範囲を逸脱しないようになっています。これにより、利益相反やセキュリティリスクを抑えられます。GitLabでは[最小権限の原則](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/)に従ってロールを割り当てることを推奨しています。


    細かいニーズを持つ組織、特にGitLab
    Ultimateを使用している組織では、[カスタムロール](https://docs.gitlab.com/ee/user/custom_roles.html)を活用することで、さらに柔軟な対応が可能になります。これらのロールを使用することで、各組織の独自のワークフローやコンプライアンス要件に合わせた特定の権限を設定できます。担当者が相反するタスクを実行できなくなるため、SoDの強制に特に役立ちます。


    一般的なユースケースとして、デプロイロールが必要な場合があります。これは、デプロイの権限が必要な個人に対して、コードの編集やプッシュの権限は与えたくない場合に使用できます。こういった要件に対応するために、GitLabは[保護環境](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#protecting-environments)を提供しています。保護環境を使用すると、[ジョブのデプロイ承認を受けたグループを招待](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#deployment-only-access-to-protected-environments)し、ユーザーのロールをレポーターに限定できます。なお、デプロイジョブにはenvironmentキーワードを含める必要があります。この設定により、コードの編集権限がないユーザーがデプロイを実行できるようになり、コンプライアンス要件に準拠できます。


    組織においてロールと権限を慎重に定義し適用することで、安全かつコンプライアンスに準拠した開発環境を構築できます。ユーザー権限を広範に見直したい場合、[こちらのグループメンバーレポート](https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/gitlab-group-member-report)を使用して各ロールのメンバー数を確認し、それに応じた次のステップを検討することが可能です。


    ## 保護機能

    GitLabは、開発プロセスをより厳重にかんりできるようにするために「保護」機能を提供しています。これらの機能は、指定された担当者のみが重要な変更を行えるようにするために使用でき、SoDを維持するためには不可欠です。


    -
    保護ブランチ：保護ブランチでは、誰がプッシュ、マージ、または強制プッシュを実行できるかが制限されます。権限を持つユーザーのみが変更を加えられるように制御でき、特に「main」や「production」のようなブランチで効果的です。

    -
    保護Gitタグ：このタグを使用すると、タグの作成権限を持つユーザーを制御できます。タグの作成後に誤った更新や削除が発生しないよう防止され、バージョン管理の整合性が保たれます。

    -
    保護環境：特定の環境、特に本番環境への無許可のアクセスを防ぐことは必須事項と言えます。保護環境では、適切な権限を持つユーザーのみがデプロイできるため、意図しない変更から環境を保護できます。これは前述のデプロイロール機能と関連しており、コードを編集せずにジョブをデプロイできるようになるため、コンプライアンスとセキュリティを確保できます。

    - 保護パッケージ：パッケージ保護ルールを使用することで、どのユーザーがパッケージに変更を加えられるかを制限できます。

    これらの保護機能はすべて、SoDの原則に沿った安全でコンプライアンスに準拠した開発環境の維持に役立ちます。


    ## 監査イベントとコンプライアンスセンター

    ここまで承認ポリシー、コンプライアンスフレームワーク、ロール、保護機能について説明してきましたが、最後に、GitLabでこれらの実装をどのようにモニタリングおよび監査してコンプライアンスを遵守できるかについて解説します。GitLabの[監査イベント](https://docs.gitlab.com/ee/user/compliance/audit_events.html)では、ユーザーの活動やプロジェクトの変更など、オーナーや管理者向けに詳細なアクティビティ履歴を提供します。このログは、ユーザーアクションを追跡したり、無許可のアクティビティを検出したりする上で不可欠です。組織において[監査イベントストリーミング](https://docs.gitlab.com/ee/user/compliance/audit_event_streaming.html)を使用して監査イベントを外部システムにストリーミングすることで、リアルタイムでの分析やアラートが可能になります。これにより、改変や違反が検出され、迅速に修正できるようになります。


    [GitLabのコンプライアンスセンター](https://docs.gitlab.com/ee/user/compliance/compliance_center/)は、コンプライアンスに関連するアクティビティを一元的に管理およびモニタリングできる場所です。ここではプロジェクトやグループ全体のコンプライアンス状況の概要を確認でき、マージリクエスト承認ルールやその他のポリシーの違反が強調表示されます。管理者は問題に迅速に対処し、あらかじめ定義されたコンプライアンス基準への準拠を確認できます。この一元化されたアプローチにより、コンプライアンス管理が簡素化され、高度なレベルでモニタリングと制御を行えます。


    >
    GitLabのSoDやコンプライアンスに関する考え方について詳しく知りたい方は、[Governステージに関するGitLabの製品方針](https://about.gitlab.com/direction/govern/)
    と[GitLabのコンプライアンスドキュメント](https://docs.gitlab.com/ee/administration/compliance.html)をご覧ください。


    ## その他のリソース


    -
    [GitLabのコンプライアンスとセキュリティポリシー管理で規制基準を遵守](https://about.gitlab.com/blog/2023/08/17/meet-regulatory-standards-with-gitlab/)

    -
    [GitLabを使ったGitLabの構築：セキュリティ認証ポートフォリオを拡充](https://about.gitlab.com/blog/2024/04/04/building-gitlab-with-gitlab-expanding-our-security-certification-portfolio/)

    -
    [オンライン小売業者bol社、GitLabで拡大するコンプライアンスニーズに対応](https://about.gitlab.com/blog/2024/06/12/online-retailer-bol-tackles-growing-compliance-needs-with-gitlab/)
  category: セキュリティ
  tags:
    - security
    - DevSecOps platform
    - product
    - financial services
config:
  slug: finserv-how-to-implement-gitlabs-separation-of-duties-features
  featured: false
  template: BlogPost
