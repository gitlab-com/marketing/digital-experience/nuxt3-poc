seo:
  title: GitLab Duo with Amazon Q：DevSecOpsに自律型AIという新たな選択肢を
  description: >-
    AIを活用したDevSecOpsは、自律型AIエージェントにより、デベロッパーの生産性、アプリケーションのモダナイゼーション、そしてイノベーションを加速させます。
  ogTitle: GitLab Duo with Amazon Q：DevSecOpsに自律型AIという新たな選択肢を
  ogDescription: >-
    AIを活用したDevSecOpsは、自律型AIエージェントにより、デベロッパーの生産性、アプリケーションのモダナイゼーション、そしてイノベーションを加速させます。
  noIndex: false
  ogImage: images/blog/hero-images/Screenshot-2024-11-27-at-4.55.28 PM.png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-duo-with-amazon-q-devsecops-meets-agentic-ai
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-duo-with-amazon-q-devsecops-meets-agentic-ai
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Duo with Amazon Q：DevSecOpsに自律型AIという新たな選択肢を",
            "author": [{"@type":"Person","name":"Emilio Salvador"}],
            "datePublished": "2024-12-03",
          }

content:
  title: GitLab Duo with Amazon Q：DevSecOpsに自律型AIという新たな選択肢を
  description: >-
    AIを活用したDevSecOpsは、自律型AIエージェントにより、デベロッパーの生産性、アプリケーションのモダナイゼーション、そしてイノベーションを加速させます。
  authors:
    - Emilio Salvador
  heroImage: images/blog/hero-images/Screenshot-2024-11-27-at-4.55.28 PM.png
  date: '2024-12-03'
  body: >-
    GitLabは、Amazon Qとの共同開発による新たなソリューション「GitLab Duo with Amazon
    Q」をリリースします。この統合型ソリューションは、GitLabの包括的なAI搭載DevSecOpsプラットフォームとAmazon
    Qの自律型AIエージェントを組み合わせたものです。


    GitLab Duo with Amazon
    Qを活用して強力なAIエージェントを日々のワークフローに直接統合することで、ソフトウェア開発を革新できます。複数のツールを切り替える必要がなくなり、デベロッパーはGitLabの包括的なDevSecOpsプラットフォーム内で、機能開発からコードレビューに至るまでの主要なタスクを加速できます。Amazon
    QのAIエージェントは、インテリジェントなアシスタントとして、要件に基づくコード生成、ユニットテストの作成、コードレビューの実施、Javaアプリケーションのモダナイゼーションなど、時間のかかるタスクを自動化します。この共同ソリューションは、こうした複雑なタスクを処理することで、セキュリティと品質基準を維持しながら、チームがイノベーションに集中できるよう支援します。


    このエンタープライズ向けのデベロッパーエクスペリエンスには次の内容が含まれます。


    * 単一のデータストアを備えたGitLabの統合プラットフォームで、安全なコードのビルド、テスト、パッケージ化、デプロイを自動化できます。

    * Amazon Q Developerによって強化されたGitLab
    Duoで、GitLabプロジェクトのコンテキストを活用し、タスクに基づいて複数ファイルの変更を生成できます。

    * GitLab Duoに統合されたAmazon
    QのAIエージェントで、タスクごとにイシューを更新し、マージリクエストを作成できます。これらはプロジェクト単位で権限が設定されています。


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1033653810?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="GitLab
    Duo and Amazon Q"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ## パートナーシップによるイノベーション：GitLabとAWS


    GitLab Duo with Amazon
    Qは、GitLabとAWS社のエンジニアリングチームの緊密な協力から誕生しました。このパートナーシップは、ソフトウェア開発を革新するために両社の強みを結集したものです。GitLabの統合型DevSecOpsにおける専門知識と、AWS社のクラウドコンピューティング分野でのリーダーシップを融合し、デベロッパーのニーズを理解する革新的なソリューションを実現しました。


    Amazon
    Qの自律型エージェントをGitLabの包括的なAI搭載プラットフォームに統合することで、単なる技術的な連携を超えた体験を実現しました。これにより、AIを活用した直感的な開発が可能になっただけでなく、エンタープライズが求めるセキュリティ、コンプライアンス、信頼性も確保されています。


    業界のアナリストたちは、AI搭載型ソフトウェア開発の進展においてこの統合が持つ重要性を認識しています。


    ***「このコラボレーションにより、GitLabとAWSはそれぞれの強みを組み合わせて、ソフトウェア開発におけるエージェント型AIを実現しています。GitLab
    Duo with Amazon
    Qは、顧客がAIの可能性を最大限に活用できるよう、効果的なユースケースに取り組むとともに重要な課題に対処しています」（IDC社、リサーチマネージャー、Katie
    Norton氏）***


    ***「デベロッパーと彼らが働く組織の両方が、シンプルで統合されたエクスペリエンスにますます関心を持っています。特に、セキュリティとプライバシーが最重要の懸念事項であるAIの時代には、最先端のテクノロジーの力を活用すると同時に、リスクを制御し、分断されたソフトウェアツールチェーンを最小限に抑えたいと考えています。GitLab
    DuoとAmazon
    Qのパートナーシップは、エンドツーエンドのDevSecOpsエクスペリエンスの中でデベロッパーが必要とするツールを提供することを目指しています」（RedMonk社、シニアアナリスト、Rachel
    Stephens氏）***


    ## 4つの主要な顧客メリット


    GitLab Duo with Amazon
    Qは、AI搭載型DevSecOpsと最も充実したクラウドコンピューティング機能を組み合わせ、開発チームを次のように支援します。


    ### 1. 機能開発におけるアイデアからコードへの変換を効率化


    開発チームは、要件をコードに変換する作業に多くの時間を費やすことが多く、その結果、納期の遅延や実装時の不整合が生じることがあります。しかし、新しいクイックアクション`/q
    dev`を使用することで、GitLab Duo with Amazon
    QのAIエージェントを呼び出し、イシューの内容をマージ可能なコードに数分で直接変換できるようになりました。このエージェントは要件を分析し、実装を計画し、最適なマージリクエストを生成します。すべて、チームの開発基準を遵守しながら行われます。さらに、コメントでのフィードバックを活用して迅速にイテレーションを行うことで、アイデアから本番環境での動作可能なコード開発を可能にします。


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1034050110?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="Feature Dev with Rev"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 2. レガシーコードのモダナイゼーションの手間を低減


    従来、Javaアプリケーションのアップグレードには、数週間にわたる慎重な計画、手作業でのコード変更、大規模なテストが必要でした。しかし、新しいクイックアクション`/q
    transform`を使用することで、Javaモダナイゼーションプロセス全体を自動化できます。エージェントは、Java 8やJava
    11のコードベースの分析、包括的なアップグレード計画の作成、Java
    17への移行に必要なマージリクエストのドキュメント化および生成、これらすべてを数時間でなく、わずか数分でこなします。コード変換中のすべての変更が逐次報告されるため、チームは安心して作業を進められ、アプリケーションのセキュリティとパフォーマンスも向上します。


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1034050145?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="QCT"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 3. 品質を損なうことなくコードレビューを加速


    コードレビューはボトルネックになりがちです。フィードバックを得るまでに数日待つ一方で、一貫した基準を維持する必要があります。しかし、クイックアクション`/q
    review`を使用すれば、マージリクエスト内でコード品質やセキュリティに関するフィードバックを即座に得られます。エージェントは、チームの基準に基づいて潜在的な問題を自動的に特定し、改善案を提案します。これにより、レビューサイクルを飛躍的に短縮しつつ、高品質なコードを維持できます。


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1034050136?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Code
    Reviews"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 4. テストの自動化により自信を持ってリリース


    手動でのテスト作成は時間がかかる上、チーム間でテストの範囲にばらつきが生じることがよくあります。しかし、クイックアクション`/q
    test`を使用すれば、アプリケーションのロジックを理解した包括的なユニットテストを自動生成できます。エージェントは重要なパスやエッジケースを徹底的にカバーし、既存のテストパターンに一致させます。この自動化により、チームは問題を早期に発見し、一貫した品質基準を維持しながら、デベロッパーの貴重な時間を節約できます。


    <div style="padding:54.37% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1034050181?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Use
    GitLab Duo with Amazon Q to add tests"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ## エンタープライズ向けのセキュリティとガードレールを標準搭載


    エンタープライズ規模とセキュリティを念頭に設計されたこのソリューションは、GitLabの統合型セキュリティ、コンプライアンス、プライバシー機能とAmazon
    QのAIエージェントを組み合わせ、デベロッパーのワークフローを加速し、組織がより迅速にセキュアなソフトウェアをリリースできるよう支援します。


    統合機能の特長：


    * 開発速度を維持するためのガードレールを標準装備

    * それぞれのレベル（ユーザー、プロジェクト、グループ）でAI搭載機能を管理できるきめ細かい制御性

    * 既存のワークフローに統合可能なエンドツーエンドのセキュリティ


    DevSecOpsチームは、世界で最も広く採用されているクラウドを活用して、開発環境を安全にスケールできます。


    ## 今後の展望


    GitLab Duo with Amazon
    Qは、[2024年5月に発表されたAWS社との既存のインテグレーション（英語）](https://press.aboutamazon.com/2024/4/aws-announces-general-availability-of-amazon-q-the-most-capable-generative-ai-powered-assistant-for-accelerating-software-development-and-leveraging-companies-internal-data)を基盤としており、ソフトウェア開発を変革するという共同のミッションにおいて大きな前進をもたらしました。今回のより高度なAI機能の統合は、AWS社との連携拡大の始まりを示しています。今後もこれらの機能を進化させ、次の点に注力していきます。


    * 開発ライフサイクル全体へのAI機能の拡張

    * デベロッパーの生産性向上

    * エンタープライズ規模の開発ニーズへの対応


    **GitLab Duo with Amazon
    Qは現在、GitLab.orgプロジェクト内の[パブリックブランチ](https://gitlab.com/groups/gitlab-org/-/epics/16059)で利用可能です。この機能のプレビュー版にアクセスし、ソフトウェア開発プロセスをどのように変革できるか詳しく知りたい方は、[当社のウェブサイト](https://about.gitlab.com/partners/technology-partners/aws/#interest)をご覧ください。**


    *監修：小松原 つかさ [@tkomatsubara](https://gitlab.com/tkomatsubara)<br>

    （GitLab合同会社 ソリューションアーキテクト本部 シニアパートナーソリューションアーキテクト）*
  category: AIと機械学習
  tags:
    - news
    - AWS
    - AI/ML
    - DevSecOps platform
    - integrations
config:
  slug: gitlab-duo-with-amazon-q-devsecops-meets-agentic-ai
  featured: true
  template: BlogPost
