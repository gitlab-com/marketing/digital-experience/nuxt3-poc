seo:
  title: GitLab 17.7リリース
  description: GitLab 17.7でリリースした最新機能をご紹介します。
  ogTitle: GitLab 17.7リリース
  ogDescription: GitLab 17.7でリリースした最新機能をご紹介します。
  noIndex: false
  ogImage: >-
    images/blog/hero-images/product-gl17-blog-release-cover-17-7-0093-1800x945-fy25.png
  ogUrl: https://about.gitlab.com/blog/gitlab-17-7-release
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-17-7-release
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab 17.7リリース",
            "author": [{"@type":"Person","name":"GitLab Japan Team"}],
            "datePublished": "2024-12-19",
          }

content:
  title: GitLab 17.7リリース
  description: GitLab 17.7でリリースした最新機能をご紹介します。
  authors:
    - GitLab Japan Team
  heroImage: >-
    images/blog/hero-images/product-gl17-blog-release-cover-17-7-0093-1800x945-fy25.png
  date: '2024-12-19'
  body: >-
    **プランナーユーザーロールを新たに追加したGitLab 17.7をリリース**


    このたび、GitLab
    17.7のリリースを発表しました。今回のリリースでは、プランナーという新たなユーザーロール、脆弱性の自動解決ポリシー、管理者が制御可能なインスタンスのインテグレーションの許可リスト、UIでのアクセストークンローテーションなどが追加されました！


    これらの機能は、今回のリリースに含まれる230件以上の改善点のほんの一部です。この記事では、お役に立つアップデートをすべてご紹介していますので、ぜひ最後までお読みください。


    GitLab
    17.7には、GitLabコミュニティのユーザーから138件ものコントリビュートがありました。ありがとうございました！GitLabは[誰もがコントリビュートできる](https://about.gitlab.com/community/contribute/)プラットフォームであり、今回のリリースはユーザーのみなさまの協力なしには実現しませんでした。  


    来月のリリースで予定されている内容を先取りするには、17.8リリースのキックオフビデオも視聴できる[今後のリリースページ](https://about.gitlab.com/direction/kickoff/)をご覧ください。


    > [GitLab
    17.7のリリースでは、プランナーというユーザーロールが新たに追加されました。クリックしてSNSで共有しましょう！](http://twitter.com/share?text=GitLab+17.7+released+with+new+Planner+user+role&url=https://about.gitlab.com/releases/2024/12/19/gitlab-17-7-released/&hashtags=)


    ## 今月のMost Valuable Person
    [MVP](https://about.gitlab.com/community/mvp/)は[Vedant
    Jain](https://gitlab.com/vedant-jain03)さんが受賞


    MVPには、誰もが[GitLabコミュニティのコントリビューターを推薦](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/490)できます。現在の候補者を応援したり、他の誰かをノミネートしてみませんか。🙌


    Vedantさんは、コミュニティのコントリビューターとして活躍されていて、コントリビュートに対する積極的なアプローチ、デリバリーへのコミットメント、そしてコラボレーションスキルで知られています。フィードバックを受け入れ、それを作業に取り入れ、必要な場合は支援を求めることに長けていて、コントリビュートを完了させるだけでなく、常にGitLabの基準を満たしてくれています。


    Vedantさんのコントリビュートには、[抽象化された作業アイテム属性を単一のリスト／ボードにまとめた](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/172191)プロジェクト管理プロセスの効率化、[作業アイテムのメタデータの並べ替え](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/173033)、[作業アイテムウィジェットの折りたたみ状態を記憶する](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/171228)機能の開発などがあります。また、UIのドキュメントへのリンクを修正し（[1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170633)、[2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/170534)）、製品全体のユーザーエクスペリエンスを改善する重要な取り組みの一環として、テクニカルライティングチームを支援しました。


    GitLabのプロダクトプランニング担当シニアプロダクトマネージャーである[Amanda
    Rueda](https://gitlab.com/amandarueda)は、Vedantさんを推薦し、彼の積極的でコミュニティ指向の考え方について次のように述べました。「Vedantさんは業務を通じて、ユーザーのニーズに対応するだけでなく、コントリビュートにより安定性と信頼性をより一層高めたGitLab環境をともに作り上げてくれています。バグの修正、ユーザビリティの改善、メンテナンスの取り組みにコントリビュートすることで、製品の全体的な品質を向上させる上で重要な役割を担っています。Vedantさんの積極的なアプローチとグループの枠にとらわれないコントリビュートは、イテレーション、顧客とのコラボレーション、継続的な改善というGitLabのコアバリューを体現しており、彼はコミュニティの中でも傑出したコントリビューターと言えます」


    今回の受賞に関して、Vedantさんは次のように述べています。「ご協力いただいたみなさんのおかげです。良い影響を与えることができたことにとても感謝しており、これからもより多くのコントリビュートをしていきたいと思います」


    Vedantさんは、最新のデータチーム向けのアクティブなメタデータプラットフォームであるAtlanのフロントエンドエンジニアであり、Google
    Summer of Code（GSOC）2024のメンターも務めています。


    Vedantさんのコントリビュートを始め、GitLabにコントリビュートしてくださっているオープンソースコミュニティのみなさまに心より感謝します！


    ## GitLab 17.7でリリースされた主な改善点


    ### プランナーという新たなユーザーロール


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    プランナーというロールが新たに導入されました。これにより、[権限](https://docs.gitlab.com/ee/user/permissions.html)を過剰に付与することなく、エピック、ロードマップ、Kanbanボードなどのアジャイルプランニングツールへのアクセスをカスタマイズできます。この変更により、ワークフローを安全に保ち、最小権限の原則を守りながら、コラボレーションをより効果的に行うことができます。


    [ドキュメント](https://docs.gitlab.com/ee/user/permissions.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/482733)


    <img src="https://about.gitlab.com/images/17_7/new_planner_user_role.png">


    ### インスタンス管理者がどのインテグレーションを有効にするか設定可能に 

    SaaS: -  

    Self-Managed: Ultimate


    インスタンス管理者は、GitLabインスタンスで有効にするインテグレーションを許可リストで設定できるようになりました。空欄の許可リストを設定した場合、インスタンス上でインテグレーションは許可されません。許可リストの設定後、デフォルトでは新しいGitLabインテグレーションは許可リストに含まれません。


    以前に有効にしていたインテグレーションを後から許可リストの設定によってブロックした場合、そのインテグレーションは無効になります。これらのインテグレーションを再度許可すると、既存の設定で改めて有効になります。


    [ドキュメント 
    ](https://docs.gitlab.com/ee/administration/settings/project_integration_management.html#integration-allowlist)


    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/500610)


    <img src="https://about.gitlab.com/images/17_7/integrations_allowlist.png">


    ### Direct Transfer によるユーザーのコントリビューションとメンバーシップの新たなマッピングのサポート


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    [Direct
    Transfer](https://docs.gitlab.com/ee/user/group/import/index.html)（直接転送）によりGitLabインスタンス間を移行する場合に、ユーザーのコントリビューションとメンバーシップの新たなマッピング方法を使用できるようになりました。この機能により、インポートプロセスを管理するユーザーと、コントリビューションの再割り当てを受けるユーザーの双方に対する柔軟性と制御性が向上しました。新しいマッピング方法による変更点は以下です。


    *
    インポートが完了した後、移行先のインスタンスの既存のユーザーにメンバーシップとコントリビュートを再割り当てできます。インポートするメンバーシップとコントリビュートはすべて、まずはプレースホルダーユーザーにマッピングされます。すべてのコントリビュートは、移行先のインスタンスで再割り当てするまで、プレースホルダーユーザーに関連付けられて表示されます。  

    * ソースインスタンスと移行先のインスタンスに異なるメールアドレスを持つユーザーのメンバーシップとコントリビュートをマッピングします。


    移行先のインスタンスのユーザーにコントリビュートを再割り当てすると、ユーザーは再割り当てを承認または拒否できます。  

    詳細については、[ユーザーのコントリビュートとメンバーシップのマッピングによる移行の効率化](https://about.gitlab.com/blog/2024/11/25/streamline-migrations-with-user-contribution-and-membership-mapping/)（英語）を参照してください。フィードバックを投稿するには、[イシュー502565](https://gitlab.com/gitlab-org/gitlab/-/issues/502565)にコメントを追加してください。


    [ドキュメント](https://docs.gitlab.com/ee/user/project/import/#user-contribution-and-membership-mapping)  

    [エピック](https://gitlab.com/gitlab-org/gitlab/-/issues/478054)


    <img
    src="https://about.gitlab.com/images/17_7/user_contributions_mapping.png">


    ### 後続のスキャンで見つからない場合、脆弱性を自動的に解決


    SaaS: Ultimate  

    Self-Managed: Ultimate


    GitLabの[セキュリティスキャンツール](https://docs.gitlab.com/ee/user/application_security/#security-scanning-tools)は、アプリケーションコードの既知の脆弱性と潜在的な弱点を特定するのに役立ちます。フィーチャーブランチのスキャンを行うと、新たな弱点や脆弱性が検出されるため、マージする前に修正できます。プロジェクトのデフォルトブランチにすでに脆弱性がある場合、フィーチャーブランチで修正しておけば、次のデフォルトブランチスキャンの実行時に脆弱性が検出されなくなります。どの脆弱性が検出されなくなったかを把握しておくことは有益であるものの、各脆弱性をクローズするには、それぞれ手動で解決済みとしてマークする必要があります。解決すべき脆弱性が多数ある場合、新しいアクティビティフィルターやステータスの一括変更を使用したとしても、時間がかかることがあります。


    本リリースでは、自動スキャンによって検出されなくなった脆弱性を自動的に解決済みに設定したいユーザーのニーズに応えて、新しいポリシータイプである*脆弱性管理ポリシー*を導入します。手順は簡単で、新たに追加された自動解決オプションを使用して新規ポリシーを設定し、適切なプロジェクトに適用するだけです。特定の重大度の脆弱性のみ、または特定のセキュリティスキャナーで検出された脆弱性のみを自動解決するように、ポリシーを設定することも可能です。設定が完了すると、プロジェクトのデフォルトブランチのスキャンが次回行われた際に検出されなかった既存の脆弱性は解決済みとしてマークされます。このアクションにより、脆弱性の情報が更新され、アクティビティノート、アクション発生時のタイムスタンプ、および脆弱性が削除されると判断されたパイプラインが記録されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/policies/vulnerability_management_policy.html)  

    [エピック](https://gitlab.com/groups/gitlab-org/-/epics/5708)  


    <img
    src="https://about.gitlab.com/images/17_7/auto-resolve-when-not-found-in-subsequent-scan.png">


    ### パーソナル、プロジェクト、グループアクセストークンをUIからローテーション可能に


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    UIを使用して、パーソナルアクセストークン、プロジェクトアクセストークン、グループアクセストークンをローテーションできるようになりました。これまでは、UIからトークンを更新するにはAPIを使用する必要がありました。


    この場を借りて、コントリビュートしてくれた[shangsuru](https://gitlab.com/shangsuru)さんに感謝します！


    [ドキュメント](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#revoke-or-rotate-a-personal-access-token)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/241523)


    <iframe width="478" height="269"
    src="https://www.youtube.com/embed/YqK2CF655OE" title="Revoke and Renew a
    GitLab personal access token (PAT) in the UI" frameborder="0"
    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;
    picture-in-picture; web-share"
    referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>


    ### CI/CDコンポーネントの使用状況をプロジェクトをまたいで追跡可能に


    SaaS: Premium、Ultimate  

    Self-Managed: Premium、Ultimate


    中心となるDevOpsチームは多くの場合、パイプライン全体でCI/CDコンポーネントが使用されている場所を追跡して、より適切に管理し、最適化する必要があります。コンポーネントの利用状況を可視化する方法がなければ、古いコンポーネントの使用の特定、採用率の把握、コンポーネントのライフサイクルのサポートなどを行うのは困難です。


    このようなニーズに応えるために、新たにGraphQLクエリを追加し、DevOpsチームが組織のパイプライン全体でコンポーネントが使用されているプロジェクトのリストを表示できるようにしました。この機能により、DevOpsチームは可視化に基づいた分析結果を活用して生産性を向上させ、より良い意思決定を行うことができます。


    [ドキュメント](https://docs.gitlab.com/ee/api/graphql/reference/index.html#cicatalogresourcecomponentusage)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/466575)  

    <img src="https://about.gitlab.com/images/17_7/catalog.png">


    ### Linux Armでホストされる小規模Runnerが全プランで利用可能に


    SaaS: Free、Premium、Ultimate  

    Self-Managed: -


    本リリースから、GitLab.com向けにLinux Armでホストされる小規模Runnerを全プランでご利用いただけるようになりました。この2
    vCPU Arm Runnerは、GitLab
    CI/CDと完全に統合されており、Armアーキテクチャ上でネイティブにアプリケーションをビルドし、テストすることができます。


    当社は、業界最速のCI/CDビルド速度を実現できるよう尽力しており、みなさまのチームがフィードバックサイクルのさらなる短縮に成功し、最終的にソフトウェアをより迅速に提供できるようになることを願っております。


    [ドキュメント](https://docs.gitlab.com/ee/ci/runners/hosted_runners/linux.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/501423)  

    <img src="https://about.gitlab.com/images/17_7/runner_arm.png">  


    ## GitLab 17.7のリリースに含まれるその他の改善点


    ### お好みのテキストエディタをデフォルトに設定


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    本バージョンでは、お好きな方法で編集を行えるようにするために、デフォルトのテキストエディタを設定できるようになりました。この変更により、リッチテキストエディタ、プレーンテキストエディタ、またはデフォルトなしを選択できるようになり、コンテンツの作成方法と編集方法の幅が拡がりました。


    今回のアップデートにより、エディタインターフェイスを個々の好みやチームの標準に合わせられるようになり、よりスムーズなワークフローを実現できます。この機能強化でも、GitLabは従来どおり、すべてのユーザーを考慮した使いやすさ、そしてカスタマイズ性を優先しています。


    [ドキュメント](https://docs.gitlab.com/ee/user/profile/preferences.html#set-the-default-text-editor)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/423104)


    ### GitLab Duo Chatの新しい`/help`コマンド


    SaaS: Premium、Ultimate、Duo Pro、Duo Enterprise  

    Self-Managed: Premium、Ultimate、Duo Pro、Duo Enterprise


    GitLab Duo Chatの様々な強力な機能を見つけましょう！チャットメッセージフィールドに「`/help`」と入力するだけで、Duo
    Chatで利用可能な機能をすべて確認できるようになりました。


    ぜひこの新しいコマンドも試してみて、Duo Chatを使用することでどのように作業をよりスムーズに効率化できるかご確認ください。


    [ドキュメント](https://docs.gitlab.com/ee/user/gitlab_duo_chat/examples.html#gitlab-duo-chat-slash-commands)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/462122)  

    <img
    src="https://about.gitlab.com/images/17_7/new_help_command_in_gitlab_duo_chat.png">


    ### CI/CDジョブからネームスペースとFluxリソースパスを設定可能に


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    Kubernetes用ダッシュボードを使用するには、環境設定からKubernetesとの接続用エージェントを選択し、必要に応じてReconciliationのステータスを追跡するために、ネームスペースとFluxリソースを設定する必要があります。GitLab
    17.6では、CI/CD設定でエージェントの指定が可能になりました。ただし、ネームスペースとFluxリソースを設定するには、引き続きUIを使用するか、APIコールを行う必要がありました。17.7では、`environment.kubernetes.namespace`と`environment.kubernetes.flux_resource_path`を選択したCI/CD構文を使用してダッシュボードの設定をすべて行えるようになりました。


    [ドキュメント](https://docs.gitlab.com/ee/ci/environments/kubernetes_dashboard.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/500164)


    ### KEVによる効率的なリスクの優先順位付け  

    SaaS: Ultimate  

    Self-Managed: Ultimate


    GitLab
    17.7では、悪用が確認された既知の脆弱性（KEV）カタログのサポートを追加しました。[KEVカタログ](https://www.cisa.gov/known-exploited-vulnerabilities-catalog)（英語）は、実際に悪用された共通脆弱性識別子（CVE）のリストがまとめられたもので、米国サイバーセキュリティ・社会基盤安全保障庁（CISA）によって管理されています。KEVを活用すれば、スキャン結果の優先順位付けを改善できるほか、脆弱性によって環境に生じうる影響を評価できます。


    このデータは、GraphQLを介してコンポジション解析ユーザーが利用できます。このデータをGitLab
    UIに表示できるようにする[作業が計画されて](https://gitlab.com/gitlab-org/gitlab/-/issues/427441)います。


    [ドキュメント](https://docs.gitlab.com/ee/api/graphql/reference/#cveenrichmenttype)  

    [イシュー](https://gitlab.com/groups/gitlab-org/-/epics/11912)


    ### 高度なSASTのためのコードフロービューの表示場所を拡大

    SaaS: Ultimate  

    Self-Managed: Ultimate


    脆弱性が表示されている場所であればどこでも、高度なSASTの[コードフロービュー](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html#vulnerability-code-flow)を利用できるようになりました。以下はその一例です。


    *
    [脆弱性レポート](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/)  

    *
    [マージリクエストのセキュリティウィジェット](https://docs.gitlab.com/ee/user/application_security/sast/#merge-request-widget)  

    *
    [パイプラインセキュリティレポート](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/pipeline.html)  

    *
    [マージリクエスト変更ビュー](https://docs.gitlab.com/ee/user/application_security/sast/#merge-request-changes-view)


    GitLab.comでは、新しいビューが有効になっています。GitLab Self-Managedでは、新しいビューはデフォルトでGitLab
    17.7（MR変更ビュー）とGitLab
    17.6（他のすべてのビュー）から有効になっています。サポートされているバージョンと機能フラグの詳細については、[コードフロー機能の可用性](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html#code-flow-feature-availability)を参照してください。


    高度なSASTの詳細については、[本機能の一般提供の発表に関するブログ記事](https://about.gitlab.com/blog/2024/09/19/gitlab-advanced-sast-is-now-generally-available)（英語）をご覧ください。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html#vulnerability-code-flow)  

    [イシュー](https://gitlab.com/groups/gitlab-org/-/epics/13499)  

    <img
    src="https://about.gitlab.com/images/17_7/ast-advanced-sast-code-flow.png">


    ### トークンの有効期限の通知機能の強化


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    これまでは、トークンの有効期限に関するメール通知は、有効期限が切れる7日前にのみ送信されていましたが、本リリースから30日前と60日前にも送信されるようになりました。通知の頻度増加および日付範囲の拡大により、ユーザーはトークンの有効期限をより認識しやすくなりました。


    [ドキュメント](https://docs.gitlab.com/ee/security/tokens/)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/464040)


    ### コンプライアンスセンターのナビゲーションと使いやすさを改善

    SaaS: Premium、Ultimate  

    Self-Managed: Premium、Ultimate


    GitLabでは、グループとプロジェクトの両方で、コンプライアンスセンターのユーザーエクスペリエンスに対する重要な改善を反復的に行っています。  

    GitLab 17.7で行った主な改善は次の2点です。


    *
    コンプライアンスセンターの「**プロジェクト**」タブでグループによるフィルタリングが可能になりました。これにより、ユーザーはこれまでとは別の方法で、適切なプロジェクトとそのプロジェクトに設定されたコンプライアンスフレームワークを適用、絞り込み、検索できるようになりました。  

    *
    プロジェクトのコンプライアンスセンターに「**フレームワーク**」タブが追加されました。ユーザーはこのタブを使用して、特定のプロジェクトに設定されているコンプライアンスフレームワークを検索できます。


    なお、フレームワークの追加や編集は、プロジェクトではなくグループで行われます。


    [ドキュメント](https://docs.gitlab.com/ee/user/compliance/compliance_center/compliance_frameworks_report.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/499183)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/468399)  

    <img
    src="https://about.gitlab.com/images/17_7/navigation-and-usability-improvements-to-compliance-center.png">


    ### オムニバスの改善  

    SaaS: -  

    Self-Managed: Free、Premium、Ultimate


    バグのため、GitLab 17.6以前のバージョンでのFIPS
    LinuxパッケージではシステムLibgcryptは使用されず、標準のLinuxパッケージにバンドルされているのと同じLibgcryptが使用されていました。


    このイシューは、AmazonLinux 2を除くGitLab 17.7のすべてのFIPS Linuxパッケージで修正されています。AmazonLinux
    2のLibgcryptバージョンは、FIPS LinuxパッケージにインストールされているGPGMEおよびGnuPGバージョンと互換性がありません。  


    AmazonLinux 2のFIPS
    Linuxパッケージでは、標準のLinuxパッケージにバンドルされているのと同じLibgcryptが引き続き使用されます。そうでない場合は、GPGMEとGnuPGをダウングレードする必要があります。


    [ドキュメント](https://docs.gitlab.com/omnibus/)


    ### Unicode 15.1の絵文字サポート🦖🍋‍🟩🐦‍🔥


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    17.7より前のバージョンでは、絵文字のサポートは古いUnicode標準に限定されていたため、一部の新しい絵文字は利用できませんでした。


    GitLab 17.7では、Unicode
    15.1のサポートが導入され、最新の絵文字が追加されました。これにより、ティラノサウルス🦖、ライム🍋‍🟩、不死鳥🐦‍🔥などの楽しい絵文字も使えるようになり、最新の絵文字を使用して表現の幅も広がります。


    さらに、今回のアップデートにより絵文字の多様性が強化され、文化、言語、アイデンティティをより適切に表現できるようになりました。プラットフォーム上でのやり取りにおいてすべての人が受け入れられていると感じられるようになります。


    [ドキュメント](https://gitlab-org.gitlab.io/ruby/gems/tanuki_emoji/)  

    [イシュー](https://gitlab.com/gitlab-org/ruby/gems/tanuki_emoji/-/issues/28)


    ### Kubernetes 1.31のサポート


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    本リリースでは、2024年8月にリリースされたKubernetesバージョン1.31のフルサポートが追加されました。Kubernetesにアプリをデプロイすると、接続しているクラスターを最新バージョンにアップグレードし、そのすべての機能を利用できるようになります。


    詳細については、[Kubernetesのサポートポリシーとサポートされているその他のKubernetesバージョン](https://docs.gitlab.com/ee/user/clusters/agent/#supported-kubernetes-versions-for-gitlab-features)を参照してください。


    [ドキュメント](https://docs.gitlab.com/ee/user/clusters/agent/#supported-kubernetes-versions-for-gitlab-features)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/501390)  


    ### `environment.action: access`と`prepare`の設定で`auto_stop_in`タイマーのリセットが可能に


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    これまでは、`action: prepare`、`action: verify`、`action:
    access`ジョブを`auto_stop_in`と組み合わせた場合、タイマーはリセットされませんでした。18.0以降のバージョンでは、`action:
    prepare`と`action: access`を設定すると、タイマーがリセットされ、`action:
    verify`を設定すると、タイマーはそのままとなります。


    現時点では、`prevent_blocking_non_deployment_jobs`機能フラグを有効にすることで、タイマーがリセットされない問題を解消できます。


    複数の破壊的な変更は、`environment.action: prepare | verify |
    access`値の挙動を区別することを目的としています。`environment.action:
    access`キーワードを指定すると、タイマーのリセットを除き、現在の動作に最も近いままとなります。


    将来的に互換性の問題が発生しないように、これらのキーワードの使用方法を見直す必要があります。提案された変更の詳細については、次のイシューを参照してください。


    * [イシュー437132](https://gitlab.com/gitlab-org/gitlab/-/issues/437132)  

    * [イシュー437133](https://gitlab.com/gitlab-org/gitlab/-/issues/437133)  

    * [イシュー437142](https://gitlab.com/gitlab-org/gitlab/-/issues/437142)


    [ドキュメント](https://docs.gitlab.com/ee/ci/yaml/#environmentauto_stop_in)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/437133)


    ### APIを使用してグループ内でシークレットプッシュ保護を有効化


    SaaS: Ultimate  

    Self-Managed: Ultimate


    本リリースでは、[REST
    API](https://docs.gitlab.com/ee/api/group_security_settings.html)と[GraphQL
    API](https://docs.gitlab.com/ee/api/graphql/reference/index.html#mutationsetgroupsecretpushprotection)を介して、グループ内のすべてのプロジェクトでシークレットプッシュ保護を有効にできるようになりました。これにより、プロジェクトごとではなく、グループごとにシークレットプッシュ保護を効率的に有効化できます。プッシュ保護が有効または無効になるたびに、監査イベントが記録されます。


    [ドキュメント](https://docs.gitlab.com/ee/api/group_security_settings.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/502827)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/502828)


    ### 高度なSASTでの検出精度を向上


    SaaS: Ultimate  

    Self-Managed: Ultimate


    次の脆弱性クラスをより正確に検出できるように、高度なSASTを更新しました。


    * C\#：OSコマンド導入とSQL挿入  

    * Go：パストラバーサル  

    *
    Java：コードインジェクション、ヘッダーまたはログへのCRLFインジェクション、クロスサイトリクエストフォージェリ（CSRF）、不適切な証明書の検証、脆弱なデシリアライゼーション、安全でないリフレクション、およびXML外部エンティティ（XXE）インジェクション  

    * JavaScript：コードインジェクション


    また、C\#（ASP.NET）とJava（JSF、HttpServlet）のユーザー入力ソースの検出を改善し、一貫性を保つために重大度レベルを更新しました。


    高度なSASTが各言語で検出できる脆弱性の種類を確認するには、[高度なSASTのカバレッジ](https://docs.gitlab.com/ee/user/application_security/sast/advanced_sast_coverage.html)を参照してください。この改良されたファイルや機能間のスキャンを使用するには、[高度なSASTを有効](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html#enable-advanced-sast-scanning)にしてください。高度なSASTがすでに有効な場合は、新しいルールが[自動的に適用](https://docs.gitlab.com/ee/user/application_security/sast/rules.html#how-rule-updates-are-released)されます。


    [ドキュメント](https://docs.gitlab.com/ee/user/application_security/sast/gitlab_advanced_sast.html)  

    [エピック](https://gitlab.com/groups/gitlab-org/-/epics/14685)  


    ### 認証情報インベントリ内でグループおよびプロジェクトアクセストークンが表示されるように


    SaaS: Ultimate  

    Self-Managed: -


    グループとプロジェクトのアクセストークンが、GitLab.comの資格情報インベントリに表示されるようになりました。これまでは、パーソナルアクセストークンとSSH鍵のみが表示されていました。インベントリ内に新たなトークンタイプが表示されるようになったことで、グループ全体の認証情報をさらに詳しく把握できるようになりました。


    [ドキュメント](https://docs.gitlab.com/ee/user/group/credentials_inventory.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/498333)  


    ### エンタープライズユーザーを一覧表示するAPIエンドポイントを追加


    SaaS: Premium、Ultimate  

    Self-Managed: \-


    グループオーナーは、専用のAPIエンドポイントを使用して、エンタープライズユーザーと関連する属性を一覧表示できるようになりました。


    [ドキュメント](https://docs.gitlab.com/ee/api/group_enterprise_users.html)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/438366)  


    ### アクセストークン用の説明フィールドを追加


    SaaS: Free、Premium、Ultimate  

    Self-Managed: Free、Premium、Ultimate


    パーソナル、プロジェクト、グループ、または代理のアクセストークンを作成する際に、任意でそのトークンの説明を入力できるようになりました。これにより、どこでどのように使用されるかなど、トークンに関する補足情報を入力できます。


    [ドキュメント](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/443819)


    <img src="https://about.gitlab.com/images/17_7/sscs_token_description.png">


    ### カスタムロールからオーナーの基本ロールを削除

    SaaS: Ultimate  

    Self-Managed: Ultimate


    カスタムロールの作成時に、オーナーの基本ロールを利用できなくなりました。カスタムロールは権限を追加していく形で作成するものであり、オーナーの基本ロールを提供することは意味をなさないためです。オーナーの基本ロールを使用している既存のカスタムロールは、この変更の影響を受けません。


    [ドキュメント](https://docs.gitlab.com/ee/user/custom_roles.html#create-a-custom-role)  

    [イシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/474273)


    ## 実験的な機能


    ### SCA Vulnerability Prioritizer


    この実験的な機能の導入は、ユーザーが[依存関係スキャン](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html)または[コンテナスキャン](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html)中に特定された脆弱性への優先順位付けを支援する新たな一歩です。ユーザーは、このCI/CDコンポーネントを\``.gitlab-ci.yml`\`ファイルに含めることができます。これにより、プロジェクト内で見つかった脆弱性の優先順位付けレポートが生成されます。レポートはパイプラインに出力できます。

    このコンポーネントは、GitLab GraphQL APIをクエリし、脆弱性データを取得後、次のように優先順位を付けます。


    1. 悪用が確認された既知の脆弱性（KEV）

    2. 悪用予測スコアリングシステム（EPSS）スコアが高い脆弱性

    3. 重大度の高い脆弱性


    検出および確認された脆弱性のみが表示されます。現在、このコンポーネントではEPSSとKEVデータを使用して、脆弱性の優先順位を付けています。EPSSとKEVデータは、依存関係スキャンとコンテナスキャンを通じて収集されるCVEでのみ利用可能です。詳細については、[Vulnerability
    Prioritizer](https://gitlab.com/components/vulnerability-prioritizer)
    を参照してください。


    フィードバックをお待ちしております。ぜひ[フィードバックイシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509508)からご質問やコメントをお寄せください。


    ### GitLab Self-Managedのカスタム管理者ロール


    管理者はカスタム管理者ロールを使用して、管理者エリアへのきめ細かいアクセスを提供できるようになりました。これにより、タスクを完了するために組織が管理者エリアへの完全なアクセス権をユーザーに付与する必要がなくなります。この機能は実験的に導入されました。詳細については、[カスタム管理者ロール](https://docs.gitlab.com/ee/user/custom_roles.html#custom-admin-roles)を参照してください。


    みなさまからのフィードバックをお待ちしています。ご質問やコメントがある場合、またはGitLabチームとのやり取りをご希望の場合は、こちらの[フィードバックイシュー](https://gitlab.com/gitlab-org/gitlab/-/issues/509376)をご覧ください。


    ## バグ修正、パフォーマンスの改善、UIの改善


    GitLabでは、ユーザーに可能な限り最高の環境をお届けできるよう尽力しています。リリースのたびに、バグを修正し、パフォーマンスを改善し、UIを向上させるためにたゆまぬ努力を続けています。GitLabは、100万人を超えるGitLab.comユーザーをはじめ、GitLabのプラットフォームを利用するすべての人にスムーズでシームレスな体験をお届けすることを約束します。  

    以下のリンクをクリックして、17.7のバグ修正、パフォーマンスの強化、UI改善についてすべてご覧ください。


    *
    [バグ修正](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=type%3A%3Abug&or%5Blabel_name%5D%5B%5D=workflow%3A%3Acomplete&or%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&or%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&milestone_title=17.7)  

    *
    [パフォーマンスの改善](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=closed&label_name%5B%5D=bug%3A%3Aperformance&or%5Blabel_name%5D%5B%5D=workflow%3A%3Acomplete&or%5Blabel_name%5D%5B%5D=workflow%3A%3Averification&or%5Blabel_name%5D%5B%5D=workflow%3A%3Aproduction&milestone_title=17.7)  

    * [UIの改善](https://papercuts.gitlab.com/?milestone=17.7)<br><br>


    *監修：川瀬 洋平 [@ykawase](https://gitlab.com/ykawase)<br>

    （GitLab合同会社 カスタマーサクセス本部 シニアカスタマーサクセスマネージャー）*


    ### 過去の日本語リリース情報


    - [GitLab
    17.9](https://about.gitlab.com/ja-jp/blog/2025/02/20/gitlab-17-9-release/)

    * [GitLab
    17.8](https://about.gitlab.com/ja-jp/blog/2025/01/16/gitlab-17-8-release/)

    * [GitLab
    17.7](https://about.gitlab.com/ja-jp/blog/2024/12/19/gitlab-17-7-release/)

    * [GitLab
    17.6](https://about.gitlab.com/ja-jp/blog/2024/11/21/gitlab-17-6-release/)

    * [GitLab
    17.5](https://about.gitlab.com/ja-jp/blog/2024/10/17/gitlab-17-5-released/)  

    * [GitLab
    17.4](https://about.gitlab.com/ja-jp/blog/2024/09/19/gitlab-17-4-released/)  

    * [GitLab
    17.3](https://about.gitlab.com/ja-jp/blog/2024/08/15/gitlab-17-3-released/)  

    * [GitLab
    17.2](https://about.gitlab.com/ja-jp/blog/2024/07/18/gitlab-17-2-released/)  

    * [GitLab
    17.1](https://about.gitlab.com/ja-jp/blog/2024/08/08/gitlab-17-1-released/)  

    * [GitLab
    16.11](https://about.gitlab.com/ja-jp/blog/2024/06/20/gitlab-16-11-released/)
  category: 製品
  tags:
    - AI/ML
    - releases
    - product
config:
  slug: gitlab-17-7-release
  featured: true
  template: BlogPost
