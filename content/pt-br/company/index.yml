config:
  enableAnimations: true
seo:
  title: Sobre o GitLab
  description: Saiba mais sobre o GitLab e o que nos motiva.
content:
  - componentName: CommonHero
    componentContent:
      title: Sobre o GitLab
      description: Nos bastidores da plataforma DevSecOps
      image:
        altText: ''
        config:
          src: '/images/company/group-of-gitlab-employees.jpg'
          theme: 'rounded'
      config:
        background: purple
        typographyVariants:
          description: heading5
  - componentName: CompanyCopyAbout
    componentContent:
      title: O que fazemos
      description: |
        **Somos a empresa por trás do GitLab, a plataforma de DevSecOps mais abrangente.**

        O que começou em 2011 como um projeto de código aberto para ajudar uma equipe de programadores a colaborar se transformou em uma plataforma utilizada por milhões de pessoas para entregar softwares de forma mais rápida e eficiente, com mais segurança e conformidade.

        Desde o início, acreditamos firmemente em trabalho remoto, código aberto, DevSecOps e iteração. Sempre que acordamos e fazemos logon pela manhã (ou no horário que escolhemos começar nossos dias) para trabalhar com a comunidade do GitLab fazemos isso com o objetivo de oferecer inovações todos os meses para ajudar as equipes a se concentrarem em enviar códigos excelentes com mais rapidez, e não na sua cadeia de ferramentas.
      cta:
        text: Learn more about GitLab
        config:
          href: '/pt-br/why-gitlab/'
          dataGaName: about gitlab
          dataGaLocation: body
  - componentName: CompanyByTheNumbers
    componentContent:
      title: GitLab em números
      asOfDate: Dezembro de 2024
      rows:
        - item:
            - title: |
                Código

                colaboradores
              number: 3,300+
              config:
                col: 4
            - title: |
                Escritórios

                Totalmente remotos desde o início
              number: '0'
              config:
                col: 4
        - item:
            - title: |
                Lançamentos consecutivos

                todos os meses
              number: '133'
              config:
                col: 4
                href: /releases/
                dataGaName: releases
                dataGaLocation: body
            - title: |
                Membros da equipe

                em mais de 60 países
              number: 1,800+
              config:
                col: 4
                href: /company/team/
                dataGaName: team
                dataGaLocation: body
        - item:
            - title: |
                Usuários registrados estimados
              number: Over 30 million
              config:
                col: 8
      ctaBox:
        title: |
          Do planejamento à produção,
          reúna as equipes em uma única aplicação
        cta:
          text: Get free trial
          config:
            href: 'https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com/'
            dataGaName: free trial
            dataGaLocation: body
      customerLogos:
        logos:
          - company: Deutsche Telekom
            altText: Logotipo da T-Mobile
            config:
              src: /images/customer_logos/deutsche-telekom.svg
              url: /customers/deutsche-telekom/
          - company: Goldman Sachs
            altText: Logotipo da Goldman Sachs
            config:
              src: /images/customer_logos/goldman-sachs.svg
              url: /customers/goldman-sachs/
          - company: Siemens
            altText: Logotipo da Siemens
            config:
              src: /images/customer_logos/siemens-color-logo.png
              url: /customers/siemens/
          - company: Nvidia
            altText: Logotipo da Nvidia
            config:
              src: /images/customer_logos/nvidia.svg
              url: /customers/nvidia/
          - company: UBS
            altText: Logotipo da UBS
            config:
              src: /images/customer_logos/ubs.svg
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
  - componentName: CompanyCopyMission
    componentContent:
      title: Missão do GitLab
      description: Nossa missão é permitir que qualquer pessoa possa contribuir e colaborar para a criação dos softwares que movem o mundo.
      cta:
        text: Saiba mais
        config:
          href: /company/mission/
          dataGaName: mission
          dataGaLocation: body
  - componentName: CompanyCardGrid
    componentContent:
      title: Valores do GitLab
      config:
        cardType: common
        backgroundColor: $color-surface-800
      cards:
        - title: Colaboração
          text: Priorizamos tudo aquilo que nos ajuda a trabalhar juntos e de forma eficaz, como assumir uma postura positiva, dizer "obrigado" e "desculpe" e dar feedback no momento certo.
          icon:
            config:
              name: CollaborationAlt4
          button:
            config:
              href: /handbook/values/#collaboration
              dataGaName: Collaboration
              dataGaLocation: body
        - title: Resultados para os clientes
          text: Operamos com um senso de urgência e foco na ação, e fazemos o que prometemos porque existimos para ajudar nossos clientes a conquistar mais.
          icon:
            config:
              name: Increase
          button:
            config:
              href: /handbook/values/#results
              dataGaName: results
              dataGaLocation: body
        - title: Eficiência
          text: Partindo da escolha de soluções chatas e passando a documentar tudo e ser gestores de uma, nós nos esforçamos para progredir rapidamente no que mais importa.
          icon:
            config:
              name: DigitalTransformation
          button:
            config:
              href: /handbook/values/#efficiency
              dataGaName: efficiency
              dataGaLocation: body
        - title: Diversidade, inclusão e pertencimento
          text: Trabalhamos para garantir que pessoas de todas as origens e circunstâncias sintam que façam parte e possam prosperar no GitLab.
          icon:
            config:
              name: Community
          button:
            config:
              href: /handbook/values/#diversity-inclusion
              dataGaName: diversity inclusion
              dataGaLocation: body
        - title: Iteração
          text: Nosso objetivo é fazer a menor coisa viável e valiosa e divulgá-la rapidamente para obter feedback.
          icon:
            config:
              name: ContinuousDelivery
          button:
            config:
              href: /pt/handbook/values/#iteration
              dataGaName: iteration
              dataGaLocation: body
        - title: Transparência
          text: Tudo o que fazemos é público por padrão, desde o manual da nossa empresa até os rastreadores de tíquetes do nosso produto.
          icon:
            config:
              name: OpenBook
          button:
            config:
              href: /handbook/values/#transparency
              dataGaName: transparency
              dataGaLocation: body
  - componentName: CompanyScrollingTimeline
    componentContent:
      title: GitLab ao longo dos anos
      items:
        - text: O projeto GitLab começou com um commit
          year: 2011
        - text: Começamos a lançar uma nova versão do GitLab todo mês, no dia 22
        - text: A primeira versão do GitLab CI é criada
          year: 2012
        - text: O GitLab é incorporado
          year: 2014
        - text: Juntamo-nos ao Y Combinator e publicamos o Manual do GitLab no repositório do nosso site
          year: 2015
        - text: Anunciamos nosso plano diretor e arrecadamos US$ 20 milhões em nossa série B de financiamento
          year: 2016
        - text: 'O GitLab Inc. tornou-se uma empresa de capital aberto no Nasdaq Global Market (NASDAQ: GTLB)'
          year: 2021
  - componentName: CompanyCopyAbout
    componentContent:
      title: Trabalhar no GitLab
      description: |
        Nós nos esforçamos para criar um ambiente totalmente remoto, onde todos os membros da equipe no mundo todo possam ser quem são plenamente, contribuir com o seu melhor, sentir que suas vozes são ouvidas e bem-vindas e realmente priorizar o equilíbrio entre vida pessoal e profissional.

        Se você tem interesse em fazer parte da equipe, convidamos você a [saber mais sobre como trabalhar no GitLab](/jobs/) e se candidatar a qualquer vaga que pareça adequada.
      cta:
        text: View all open roles
        config:
          href: /jobs/all-jobs/
          dataGaName: all jobs
          dataGaLocation: body
  - componentName: CompanyTeamOpsCard
    componentContent:
      title: Equipes melhores. Progresso mais rápido. Mundo melhor.
      description: O TeamOps é a prática exclusiva de pessoal do GitLab que torna o trabalho em equipe uma disciplina objetiva. Foi assim que o GitLab passou de uma startup para uma empresa pública global em uma década. Por meio de uma certificação gratuita e acessível para profissionais, outras empresas podem utilizar o TeamOps para tomar decisões melhores, entregar resultados e fazer nosso mundo avançar.
      images:
        mainImage: /images/company/company-teamops.svg
        titleIcon: /images/logos/teamops.svg
      primaryButton:
        text: Saiba mais sobre o TeamOps
        config:
          href: '/pt-br/teamops/'
          dataGaName: learn more about teamops
          dataGaLocation: body
      secondaryButton:
        text: Obtenha a certificação
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: get certified
          dataGaLocation: body
  - componentName: CompanyCardGrid
    componentContent:
      title: Saiba mais
      config:
        cardType: resource
        backgroundColor: $color-surface-50
      cards:
        - header: Veja o que há de novo no GitLab, em DevOps, segurança e muito mais.
          type: Blog
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Visite o blog
            config:
              href: /blog/
              dataGaName: blog
              dataGaLocation: body
              icon: Blog
        - header: Notícias, comunicados de imprensa, logotipos e recursos de marca do GitLab
          type: Sala de imprensa
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Saiba mais
            config:
              href: /press/press-kit/
              dataGaName: press kit
              dataGaLocation: body
              icon: Announcement
        - header: As informações financeiras e de ações mais recentes para investidores.
          type: Relacionamento com investidores
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Saiba mais
            config:
              href: https://ir.gitlab.com/
              dataGaName: investor relations
              dataGaLocation: body
              icon: Money
  - componentName: CommonNextSteps
