seo:
  title: 'Demo: GitLab + Jira + Jenkins'
  description: >-
    See how you can use our Jira and Jenkins integrations to reduce context
    switching and streamline your workflow.
  ogTitle: 'Demo: GitLab + Jira + Jenkins'
  ogDescription: >-
    See how you can use our Jira and Jenkins integrations to reduce context
    switching and streamline your workflow.
  noIndex: false
  ogImage: images/blog/hero-images/gitlab-jira-jenkins-cover.png
  ogUrl: https://about.gitlab.com/blog/gitlab-workflow-with-jira-jenkins
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-workflow-with-jira-jenkins
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Demo: GitLab + Jira + Jenkins",
            "author": [{"@type":"Person","name":"Joel Krooswyk"}],
            "datePublished": "2018-07-30",
          }

content:
  title: 'Demo: GitLab + Jira + Jenkins'
  description: >-
    See how you can use our Jira and Jenkins integrations to reduce context
    switching and streamline your workflow.
  authors:
    - Joel Krooswyk
  heroImage: images/blog/hero-images/gitlab-jira-jenkins-cover.png
  date: '2018-07-30'
  body: >


    One of the things we love about GitLab is that while it can replace all your
    other software development lifecycle tools [(no, really)](/); it doesn't
    have to. Whether you want to rip and replace everything or use it for one or
    two stages of your workflow, [alongside your existing
    toolset](/partners/technology-partners/integrate/) (for now, or forever),
    we've got you covered.


    One of the things we're most often asked about is how GitLab works together
    with [Jira](/solutions/jira/) for issue tracking, and
    [Jenkins](/solutions/jenkins/) for CI. This could be for one of two reasons:


    1. Your organization is happy with your issue tracking and CI solutions, and
    just want to use GitLab for other features, or

    2. You plan to move to GitLab for your end-to-end software development
    lifecycle, but that's a significant undertaking and it may be less
    disruptive to migrate on a project-by-project basis.


    No matter the reason, what's important is maintaining the context of work
    without having to switch between applications frequently. With these
    integrations you can transition Jira issue states via GitLab, as well as see
    GitLab commits, branches, and merge requests in the Jira development panel.
    You can also view the status of Jenkins pipelines in GitLab to optimize your
    use of GitLab Merge Requests.


    I recorded this demo to show what a workflow using all three would look
    like.



    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/Jn-_fyra7xQ" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->
  category: Company
  tags:
    - CI
    - demo
    - integrations
    - workflow
config:
  slug: gitlab-workflow-with-jira-jenkins
  featured: false
  template: BlogPost
