seo:
  title: Why DevOps collaboration continues to be important
  description: >-
    Modern DevOps isn't just about tech adoption and new processes. DevOps
    collaboration is going to play a key role. Here's why.
  ogTitle: Why DevOps collaboration continues to be important
  ogDescription: >-
    Modern DevOps isn't just about tech adoption and new processes. DevOps
    collaboration is going to play a key role. Here's why.
  noIndex: false
  ogImage: images/blog/hero-images/logoforblogpost.jpg
  ogUrl: >-
    https://about.gitlab.com/blog/why-devops-collaboration-continues-to-be-important
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/why-devops-collaboration-continues-to-be-important
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Why DevOps collaboration continues to be important",
            "author": [{"@type":"Person","name":"Valerie Silverthorne"}],
            "datePublished": "2022-10-25",
          }

content:
  title: Why DevOps collaboration continues to be important
  description: >-
    Modern DevOps isn't just about tech adoption and new processes. DevOps
    collaboration is going to play a key role. Here's why.
  authors:
    - Valerie Silverthorne
  heroImage: images/blog/hero-images/logoforblogpost.jpg
  date: '2022-10-25'
  body: >


    It’s tempting to think the concept of DevOps collaboration is something no
    one needs to talk about anymore. After all, the methodology has been around
    for nearly 15 years, is in widespread use, and has clearly proven to be
    successful at getting safer software out the door faster. Haven’t we figured
    out DevOps collaboration by now?


    The answer is no, at least according to our [2022 Global DevSecOps
    Survey](/developer-survey/) and to industry experts looking at the future of
    DevOps.


    For starters, dev and ops respondents to our survey told us programming
    languages and soft skills like collaboration are going to be most important
    for their careers going forward. DevOps collaboration was the second most
    important skill for sec pros surveyed. These results were far from a
    one-off: In our [2020
    survey](/images/developer-survey/gitlab-devsecops-2021-survey-results.pdf),
    dev, sec, and ops were unanimous that “soft skills,” including DevOps
    collaboration, were the most critical for future careers. In
    [2021](/images/developer-survey/gitlab-devsecops-2021-survey-results.pdf),
    sec and ops continued to prioritize DevOps collaboration for the future,
    while devs opted for AI/ML. 


    This year, we asked over 5,000 survey takers what would be most important to
    their careers, but we didn’t ask *why* it would be so important. A look at
    some recent thought leadership around DevOps collaboration sheds some light.


    According to [an article in SDX
    Central](https://www.sdxcentral.com/articles/analysis/devops-its-about-the-people/2022/07/),
    pundits think collaboration is “critical for DevOps success” today and in
    the future. An [article in Tech
    Beacon](https://techbeacon.com/app-dev-testing/future-devops) goes further,
    suggesting DevOps will embrace business metrics as a measure of success
    going forward, and, as such, will require levels of cross-functional
    collaboration not seen before. 


    In other words, as DevOps expands beyond a technology goal (develop
    software) to a business goal (ensure customer satisfaction or business
    profitability), more teams will be seated at the table. The more people
    involved, the more DevOps collaboration will be critical to the future.


    We’d like to know how DevOps collaboration works on _your_ team. Our
    12-question survey will take you less than four minutes! [Take the
    survey!](/blog/2022/10/12/take-our-survey-on-collaborative-software-development/)
  category: DevSecOps
  tags:
    - DevOps
    - developer survey
    - collaboration
config:
  slug: why-devops-collaboration-continues-to-be-important
  featured: false
  template: BlogPost
