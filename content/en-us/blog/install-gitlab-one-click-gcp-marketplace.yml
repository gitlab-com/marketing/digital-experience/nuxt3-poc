seo:
  title: Install GitLab with a single click from the new GCP Marketplace
  description: >-
    GitLab is now available on the new Google Cloud Platform Marketplace, so you
    can deploy GitLab on Google Kubernetes Engine with a single click!
  ogTitle: Install GitLab with a single click from the new GCP Marketplace
  ogDescription: >-
    GitLab is now available on the new Google Cloud Platform Marketplace, so you
    can deploy GitLab on Google Kubernetes Engine with a single click!
  noIndex: false
  ogImage: images/blog/hero-images/gcp-send-gitlab-large.png
  ogUrl: https://about.gitlab.com/blog/install-gitlab-one-click-gcp-marketplace
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/install-gitlab-one-click-gcp-marketplace
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Install GitLab with a single click from the new GCP Marketplace",
            "author": [{"@type":"Person","name":"William Chia"}],
            "datePublished": "2018-07-18",
          }

content:
  title: Install GitLab with a single click from the new GCP Marketplace
  description: >-
    GitLab is now available on the new Google Cloud Platform Marketplace, so you
    can deploy GitLab on Google Kubernetes Engine with a single click!
  authors:
    - William Chia
  heroImage: images/blog/hero-images/gcp-send-gitlab-large.png
  date: '2018-07-18'
  body: "\nToday, Google Cloud announced its [new Google Cloud Platform (GCP) marketplace](https://cloudplatform.googleblog.com/2018/07/introducing-commercial-kubernetes-applications-in-gcp-marketplace.html) with the ability to deploy applications to your Kubernetes clusters on Google Kubernetes Engine (GKE). We’re proud to make GitLab available in the GCP Marketplace from day one. While you can [install GitLab almost anywhere](/install/), the new GCP Marketpklace app installs with just a single click. It's the easiest way to get your own self-managed GitLab instance up and running.\n\n![Deploy GitLab on Google Cloud Platform](https://about.gitlab.com/images/google-cloud-platform/gcp-send-gitlab-medium.png)\n\n### Not looking to manage your own instance?\n\nFolks who don’t want to take on the overhead of administering their own GitLab instance can [sign up for GitLab.com](https://gitlab.com/users/sign_in). GitLab.com is a SaaS offering that runs the same software as GitLab self-managed, managed by GitLab.\n\nRecently, we announced our [migration from Azure to GCP](/blog/2018/06/25/moving-to-gcp/). This migration is the first step in our goal of running GitLab.com as a cloud native application on Kubernetes. The migration has involved careful planning along with decomposing GitLab into individual services. The lessons learned through our migration have translated directly into our how we are building the GitLab Helm Chart. The work we’ve done to migrate GitLab.com has fueled our ability to offer a solid option for self-managed users to deploy GitLab to Kubernetes.\n\n### Want to deploy your application to Kubernetes?\n\nWith a built-in container registry and [Kubernetes integration](/solutions/kubernetes/), GitLab makes it easier than ever to get started with containers and cloud native development. [Gitlab CI/CD](/topics/ci-cd/) can deploy your application to any Kubernetes cluster.\n\nIf you don’t have a Kubernetes cluster, we’ve got you covered. The easiest way to get set up in using our [GKE Integration](/partners/technology-partners/google-cloud-platform/) and [Auto DevOps](/stages-devops-lifecycle/auto-devops/). It takes just a few clicks to set up, then you have a full deployment pipeline. Just commit your code and GitLab does rest.\n\n![GitLab deploys your app to Google Cloud Platform](https://about.gitlab.com/images/google-cloud-platform/gitlab-send-app-medium.png)\n\n#### Join us at Google Next\n\nNext week on July 24-27 we’ll be at [Google Nex](https://cloud.withgoogle.com/next18/sf/)t in San Francisco, where there’s a lot going on. [Follow GitLab on Twitter](https://twitter.com/gitlab) to stay up to date on announcements from the show. If you’re at the show, stop by booth #S1629 and say hi! We’d love to hear how you are using GitLab and show you how our GKE Integration and Marketplace install work. \_\n\n#### Summary\n\nYou can use GitLab either as a self-managed app or as a service on GitLab.com. Today, we’ve made it easier than ever to install [GitLab with the GCP Marketplace](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab?filter=solution-type:k8s). Additionally, we’ll be moving GitLab.com to GCP and soon afterward to GKE. You can look forward to the increased stability and performance that Kubernetes will bring to GitLab.com. Regardless of whether you are using self-managed GitLab or GitLab.com, GitLab’s Kubernetes integration and GKE integration make it easy to deploy your app to Kubernetes. Stop by Google Next and follow our Twitter feed to get the latest news on using GitLab together with Google Cloud Platform.\n"
  category: Company
  tags:
    - cloud native
    - GKE
    - google
    - kubernetes
config:
  slug: install-gitlab-one-click-gcp-marketplace
  featured: false
  template: BlogPost
