seo:
  title: Expanding GitLab for Startups program
  description: Learn how startups can access the complete DevSecOps platform.
  ogTitle: Expanding GitLab for Startups program
  ogDescription: Learn how startups can access the complete DevSecOps platform.
  noIndex: false
  ogImage: images/blog/hero-images/tanukicover.jpg
  ogUrl: https://about.gitlab.com/blog/expanding-gitlab-for-startups-program
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/expanding-gitlab-for-startups-program
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Expanding GitLab for Startups program",
            "author": [{"@type":"Person","name":"Emilio Salvador"}],
            "datePublished": "2023-03-01",
          }

content:
  title: Expanding GitLab for Startups program
  description: Learn how startups can access the complete DevSecOps platform.
  authors:
    - Emilio Salvador
  heroImage: images/blog/hero-images/tanukicover.jpg
  date: '2023-03-01'
  body: "\nGitLab is committed to supporting the startup community, and we are excited to announce the expansion of our [GitLab for Startups program](/solutions/startups/). Our goal has always been to help businesses of all sizes streamline their development processes and improve collaboration, and we recognize the unique challenges that startups face.\_\n\nStarting on March 15, 2023, our GitLab for Startups program will include qualifying startups backed by external funding. Based on funding level and years in the program, qualifying startups are able to utilize our complete DevSecOps platform at a highly discounted price or possibly at no cost for the first year. The program will continue supporting eligible startups with deep discounts in the second year.\_\_\n\nWith GitLab, startups are able to reduce software development cycles, reduce time to market, and improve product quality. By adopting a single application for the entire DevSecOps lifecycle, startups can develop software faster and focus more on growing their customer base, increasing revenue, and differentiating their product in the market.\n\n![Chart listing eligibility requirements](https://about.gitlab.com/images/blogimages/gitlabforstartupscriteria.png)\n\nQualifying startups can submit the application form on our [GitLab for Startups page](/solutions/startups/join/) starting on March 15, 2023. For additional questions regarding this offer, please see our FAQ section on the GitLab for Startups page or feel free to reach out to us at startups@gitlab.com.\n"
  category: News
  tags:
    - DevSecOps
    - startups
    - news
config:
  slug: expanding-gitlab-for-startups-program
  featured: false
  template: BlogPost
