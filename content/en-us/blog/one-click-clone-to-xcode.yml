seo:
  title: Announcing GitLab one-click clone to Xcode
  description: >-
    GitLab's Xcode integration allows you to clone repos to Xcode with a single
    click!
  ogTitle: Announcing GitLab one-click clone to Xcode
  ogDescription: >-
    GitLab's Xcode integration allows you to clone repos to Xcode with a single
    click!
  noIndex: false
  ogImage: images/blog/hero-images/apple-xcode-cover.jpg
  ogUrl: https://about.gitlab.com/blog/one-click-clone-to-xcode
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/one-click-clone-to-xcode
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Announcing GitLab one-click clone to Xcode",
            "author": [{"@type":"Person","name":"William Chia"}],
            "datePublished": "2018-06-06",
          }

content:
  title: Announcing GitLab one-click clone to Xcode
  description: >-
    GitLab's Xcode integration allows you to clone repos to Xcode with a single
    click!
  authors:
    - William Chia
  heroImage: images/blog/hero-images/apple-xcode-cover.jpg
  date: '2018-06-06'
  body: >


    Yesterday at WWDC, [Apple announced our Xcode integration with
    GitLab](https://twitter.com/gitlab/status/1003764673454342144), which makes
    it easier to work with your Xcode projects hosted in GitLab.


    Projects that contain a `.xcodeproj` or `.xcworkspace` file can now be
    cloned in Xcode using the new **Open in Xcode** button in GitLab. When
    viewing Xcode projects in the GitLab interface, the button will be available
    in GitLab next to the Git URL for cloning your project.


    ![Open in Xcode
    Button](https://about.gitlab.com/images/blogimages/open-in-xcode-button.png){:
    .shadow.medium.center}


    The button is available on GitLab.com, and will be available to self-managed
    GitLab instances in GitLab 11.0 from June 22, 2018. The button works with
    Xcode 9 and above.


    Be on the lookout as we have even more enhancements on the way with GitLab
    11.0.
  category: Company
  tags:
    - integrations
    - news
config:
  slug: one-click-clone-to-xcode
  featured: false
  template: BlogPost
