seo:
  title: 'ICYMI: Key AI and security insights from our developer community'
  description: >-
    Our latest LinkedIn Live highlights the hottest trends in AI, security,
    DevSecOps, and more. Also get a taste of the GitLab community contributions
    that are making an impact.
  ogTitle: 'ICYMI: Key AI and security insights from our developer community'
  ogDescription: >-
    Our latest LinkedIn Live highlights the hottest trends in AI, security,
    DevSecOps, and more. Also get a taste of the GitLab community contributions
    that are making an impact.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(6).png
  ogUrl: >-
    https://about.gitlab.com/blog/icymi-key-ai-and-security-insights-from-our-developer-community
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/icymi-key-ai-and-security-insights-from-our-developer-community
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "ICYMI: Key AI and security insights from our developer community",
            "author": [{"@type":"Person","name":"Fatima Sarah Khalid"}],
            "datePublished": "2024-12-05",
          }

content:
  title: 'ICYMI: Key AI and security insights from our developer community'
  description: >-
    Our latest LinkedIn Live highlights the hottest trends in AI, security,
    DevSecOps, and more. Also get a taste of the GitLab community contributions
    that are making an impact.
  authors:
    - Fatima Sarah Khalid
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(6).png
  date: '2024-12-05'
  body: >
    In our [November LinkedIn Live
    broadcast](https://www.linkedin.com/feed/update/urn:li:activity:7265408726696697857),
    we brought together field CTOs, developer advocates, and community leaders
    to discuss industry trends and showcase features making a difference in
    developer workflows.


    Here are 5 key highlights:


    ### 1. AI adoption trends from the field

    Our field CTOs shared insights on how organizations are embracing AI across
    their development workflows. For instance, Field CTO Cherry Han highlighted
    how financial organizations are thinking beyond individual developer tools.


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1035388263?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Ai
    Adoption Trends from the Field"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    <br></br>

    Andrew Hasker, Field CTO for Asia Pacific and Japan, offered valuable
    perspective on AI adoption.


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1035388277?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="From
    Field CTOs"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 2. Security coverage that makes a difference


    Staff Developer Advocate Fernando Diaz demonstrated how GitLab's security
    scanners cover the complete application lifecycle, showing how easy it is to
    implement [comprehensive security
    scanning](https://about.gitlab.com/solutions/security-compliance/) with just
    a few lines of code.


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1035388297?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="Security Coverage"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 3. AI-powered language migration made simple

    In an impressive demonstration, Senior Technical Marketing Manager Cesar
    Saavedra showed how GitLab Duo can assist in migrating applications between
    programming languages.


    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1036170482?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="AI-Powered Language Migration Made Simple"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 4. Making DevSecOps work smarter


    Developer Advocate Abubakar Siddiq Ango showcased how GitLab's triage
    features can automate routine tasks.

    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1035388290?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Making
    DEvOps Work Smarter"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ### 5. Community contributions making an impact


    Director of Contributor Success Nick Veenhof shared how community
    contributions are shaping GitLab's development:

    <div style="padding:56.25% 0 0 0;position:relative;"><iframe
    src="https://player.vimeo.com/video/1035395211?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
    frameborder="0" allow="autoplay; fullscreen; picture-in-picture;
    clipboard-write"
    style="position:absolute;top:0;left:0;width:100%;height:100%;"
    title="Community Contributions Making an Impact"></iframe></div><script
    src="https://player.vimeo.com/api/player.js"></script>


    ## Watch on-demand


    [Watch the complete broadcast
    recording](https://www.linkedin.com/feed/update/urn:li:activity:7265408726696697857)
    for step-by-step demonstrations and insights from our experts. Also, be sure
    to [follow GitLab on LinkedIn](https://www.linkedin.com/company/gitlab-com)
    to stay up to date on our monthly broadcasts and get insights into our
    platform, DevSecOps, and software development.
  category: DevSecOps
  tags:
    - AI/ML
    - security
    - community
    - tutorial
    - webcast
config:
  slug: icymi-key-ai-and-security-insights-from-our-developer-community
  featured: false
  template: BlogPost
