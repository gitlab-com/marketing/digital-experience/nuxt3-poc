seo:
  title: >-
    Use Streaming Audit Events to connect your technology stack with GitLab and
    Pipedream
  description: >-
    Automation lets your DevSecOps teams have logic in place for how to handle
    events as they come in.
  ogTitle: >-
    Use Streaming Audit Events to connect your technology stack with GitLab and
    Pipedream
  ogDescription: >-
    Automation lets your DevSecOps teams have logic in place for how to handle
    events as they come in.
  noIndex: false
  ogImage: images/blog/hero-images/gl15.jpg
  ogUrl: >-
    https://about.gitlab.com/blog/use-streaming-audit-events-to-connect-your-technology-stack-with-gitlab-and-pipedream
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/use-streaming-audit-events-to-connect-your-technology-stack-with-gitlab-and-pipedream
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Use Streaming Audit Events to connect your technology stack with GitLab and Pipedream",
            "author": [{"@type":"Person","name":"Sam Kerr"}],
            "datePublished": "2022-06-27",
          }

content:
  title: >-
    Use Streaming Audit Events to connect your technology stack with GitLab and
    Pipedream
  description: >-
    Automation lets your DevSecOps teams have logic in place for how to handle
    events as they come in.
  authors:
    - Sam Kerr
  heroImage: images/blog/hero-images/gl15.jpg
  date: '2022-06-27'
  body: "\n\nGitlab recently released [Streaming Audit Events](https://docs.gitlab.com/ee/administration/audit_event_streaming.html) to provide you real-time visibility into what happens inside your GitLab groups and projects. Whenever something happens, an event will be sent to the HTTPS destination of your choice. This is a great way to understand immediately when something has changed and if there is an action that needs to be taken.\n\nThese events are often used to drive automation to update GitLab in response to certain actions, such as creating a new issue to onboard a team member when an account is added to a group, or to restore the correct value of a [merge request approval setting](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html) if it is changed. We know that many users want to combine the streaming audit events with other data sets and tools they already work with. Taking automatic action in response to audit events happening can help ensure your GitLab groups and projects are always in the correct, compliant state.\n\n## Pipedream simplifies the streaming audit event process\n\nDriving automation off of these events or combining the events with other data sets means the destination which will receive the events needs to be running and have logic in place for how to handle the events as they come in. This normally would require setting up and maintaining a server with high availability to receive events as they happen, run any automation scripts, and then process the events if they needed to be sent to another tool or combined with another data set. This is tricky to do right and an extra step that takes time. \n\nEnter our partner, [Pipedream](https://pipedream.com/).\_\n\nPipedream lets you connect APIs, remarkably fast. This includes the new streaming audit events from GitLab. When you select the GitLab New Audit Events trigger in a Pipedream workflow, Pipedream will automatically register an HTTPS endpoint for audit events in your GitLab group:\n\n![Pipedream registration process](https://about.gitlab.com/images/blogimages/pipedreamscreenshot.png){: .shadow}\n\nFrom there, Pipedream allows you to transform the data, forward it to any other tools using Pipedream’s [prebuilt actions](https://pipedream.com/docs/workflows/steps/actions/), or write any custom automation [with code](https://pipedream.com/docs/code/) (i.e., Node.js, Python, Go, or Bash).\n\n## Getting started with Pipedream and GitLab\n\nThe video below shows an example of how to use GitLab streaming audit events and Pipedream together to automatically alert your security team if a sensitive project setting is changed. This is powerful because it ensures that your security teams can immediately take action when a change occurs and understand why it happened.\n\n<!-- blank line -->\n<figure class=\"video_container\">\n  <iframe src=\"https://www.youtube.com/embed/ggzoUMEsjjU\" frameborder=\"0\" allowfullscreen=\"true\"> </iframe>\n</figure>\n<!-- blank line -->\n\nThis is just one example of what you can do with Pipedream and GitLab together. Pipedream allows you to use any [GitLab API](https://docs.gitlab.com/ee/api/) in response to an audit event: You can change the setting to its original value, add comments to issues, kick off pipelines, and more. You can also trigger any action in any of the [700+ other apps](https://pipedream.com/apps) that it has built-in integrations with.\n\n## Open source integration means everyone can contribute\n\nPipedream and GitLab are both strong believers in open source. The integration is publicly available at the Pipedream [repository](https://github.com/PipedreamHQ/pipedream), and contributions are welcome! We are excited to see what sort of workflows you create with Pipedream and GitLab together.\n\n## Final thoughts\n\nIn this post, we talked about the power of GitLab’s [Streaming Audit Events](https://docs.gitlab.com/ee/administration/audit_event_streaming.html) to give you immediate visibility into your groups and projects and how Pipedream makes it easy to build and automate workflows based on those audit events. This was just a preview of what is possible though, as you can use the entire GitLab API within Pipedream in response to audit events or interact with other tools supported by Pipedream.\n\nWe are excited to see the workflows you build with GitLab and Pipedream together. We showed how you can create a GitLab issue to alert the security team when settings are changed, but the sky is the limit - you might also create issues when new user accounts are created to onboard new team members, automatically restore changed settings, or forward data to a security information and event management, a.k.a. SIEM, system. With Pipedream and Gitlab, you can automatically take the actions necessary when things change to ensure you remain secure and compliant.\n"
  category: Security
  tags:
    - DevOps
    - integrations
    - security
config:
  slug: >-
    use-streaming-audit-events-to-connect-your-technology-stack-with-gitlab-and-pipedream
  featured: false
  template: BlogPost
