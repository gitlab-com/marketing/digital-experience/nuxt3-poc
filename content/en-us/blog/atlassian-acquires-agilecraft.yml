seo:
  title: What’s your plan?
  description: ' GitLab integrates planning every step of the way'
  ogTitle: What’s your plan?
  ogDescription: ' GitLab integrates planning every step of the way'
  noIndex: false
  ogImage: images/blog/hero-images/planpost.jpg
  ogUrl: https://about.gitlab.com/blog/atlassian-acquires-agilecraft
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/atlassian-acquires-agilecraft
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "What’s your plan?",
            "author": [{"@type":"Person","name":"GitLab"}],
            "datePublished": "2019-03-18",
          }

content:
  title: What’s your plan?
  description: ' GitLab integrates planning every step of the way'
  authors:
    - GitLab
  heroImage: images/blog/hero-images/planpost.jpg
  date: '2019-03-18'
  body: >+


    Today’s acquisition of AgileCraft by Atlassian brings up an interesting
    discussion: 

    What’s the role of planning in today’s fast-moving software development
    lifecycle?


    In DevOps, planning can’t be an after-thought or something only thought
    about at 

    the beginning. [Planning needs to be
    agile](https://about.gitlab.com/solutions/agile-delivery/), 

    and integrated into what’s happening every day in the modern software shop. 

    Like a quote from the Beatles song, “Life is what happens to you while
    you’re busy making other plans.”


    GitLab has democratized planning, making it an integral part of the
    software 

    development workflow, with out-of-the-box project management, kanban
    boards, 

    epics, time-tracking, and agile portfolio management - with 

    [much more to come](https://about.gitlab.com/direction/plan/). 

    More importantly, though, GitLab’s planning features are intimately linked
    to 

    all of the other [stages of software
    development](https://about.gitlab.com/stages-devops-lifecycle/). 

    Developers, architects, and product managers can plan and re-plan together, 

    collaboratively and concurrently, with full visibility to the entire cycle.


    Don’t get us wrong - AgileCraft is a deep, well-thought out enterprise
    planning 

    tool. But with GitLab, in addition to planning, you get an entire software
    development lifecycle tool out of the box.

  category: Company
  tags:
    - agile
    - news
    - workflow
config:
  slug: atlassian-acquires-agilecraft
  featured: false
  template: BlogPost
