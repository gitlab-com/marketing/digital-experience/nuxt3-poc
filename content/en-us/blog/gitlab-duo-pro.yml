seo:
  title: ' GitLab Duo Pro combines Code Suggestions, Chat, and organizational controls'
  description: >-
    AI-powered code completion, code generation, chat, and per-user assignment
    come to the GitLab DevSecOps Platform as a secure add-on.
  ogTitle: ' GitLab Duo Pro combines Code Suggestions, Chat, and organizational controls'
  ogDescription: >-
    AI-powered code completion, code generation, chat, and per-user assignment
    come to the GitLab DevSecOps Platform as a secure add-on.
  noIndex: false
  ogImage: images/blog/hero-images/gitlabduo.png
  ogUrl: https://about.gitlab.com/blog/gitlab-duo-pro
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-duo-pro
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": " GitLab Duo Pro combines Code Suggestions, Chat, and organizational controls",
            "author": [{"@type":"Person","name":"Hillary Benson"},{"@type":"Person","name":"Dave Steer"}],
            "datePublished": "2024-01-17",
          }

content:
  title: ' GitLab Duo Pro combines Code Suggestions, Chat, and organizational controls'
  description: >-
    AI-powered code completion, code generation, chat, and per-user assignment
    come to the GitLab DevSecOps Platform as a secure add-on.
  authors:
    - Hillary Benson
    - Dave Steer
  heroImage: images/blog/hero-images/gitlabduo.png
  date: '2024-01-17'
  body: >
    > __This blog has been updated to include GitLab Duo Chat (in Beta) as part
    of the GitLab Duo Pro add-on.__


    Last year, we launched GitLab Duo Code Suggestions into general availability
    as the cornerstone of our GitLab Duo suite of AI capabilities that power
    DevSecOps workflows. We received incredibly positive feedback from our
    customers on the ease of use and effectiveness of Code Suggestions' code
    completion and code generation capabilities. 


    Now, we are introducing [GitLab Duo
    Pro](https://about.gitlab.com/solutions/gitlab-duo-pro/sales/), a new
    package that brings together Code Suggestions, Chat, and organizational
    control capabilities, ensuring that teams can take advantage of AI exactly
    where they need it throughout the software development lifecycle.


    > [Get started with GitLab Duo
    Pro](https://about.gitlab.com/solutions/gitlab-duo-pro/sales/) 


    GitLab Duo Pro, which is available to Ultimate and Premium customers as an
    add-on, boosts developer efficiency and effectiveness by decreasing the time
    required to write and understand code. With GitLab Duo Pro, organizations
    can set up their DevSecOps teams for success by giving them the AI-assisted
    tools they need to develop secure code faster, improve collaboration, and
    reduce the security and compliance risks of AI adoption.


    GitLab Duo Pro features:

    - Code Suggestions: AI-powered code completion and code generation

    - Chat (Beta): Provides real-time guidance on coding, refactoring, and test
    generation

    - Privacy-first approach: Your code stays your code — GitLab does not use it
    for training or fine-tuning AI models

    - Organizational controls: GitLab Duo Pro enables organizations to have
    greater control over AI by limiting AI usage to approved users only


    GitLab Duo Pro is available across SaaS, self-managed, and Dedicated
    deployments.


    ## What is GitLab Duo Pro?


    With GitLab Duo Pro, developers can access [Code
    Suggestions](https://about.gitlab.com/solutions/code-suggestions/) to
    generate blocks of code from single- and multi-line comments as well as
    comment blocks. Code Suggestions also autocompletes lines of code from a few
    typed characters — improving cycle times by securely taking care of
    repetitive, routine coding tasks.


    GitLab Duo Pro also includes
    [Chat](https://about.gitlab.com/blog/2023/11/09/gitlab-duo-chat-beta/)
    (Beta), which is helpful for technical and non-technical users across the
    entire software development lifecycle. Chat assists in explaining unfamiliar
    code, suggesting and generating tests, and simplifying code. You can also
    use Chat to write code from scratch interactively. Whether you are
    onboarding to GitLab or you are already an expert, learning how to use
    GitLab is streamlined with Chat.


    Organizational controls are provided in GitLab Duo Pro for better management
    of AI capabilities, including per-user assignment, so only approved users
    can use AI.


    GitLab Duo Pro is built with privacy as a critical foundation. Private,
    non-public customer code stored in GitLab is not used for training or
    fine-tuning AI models. Learn about [data usage when using GitLab Duo Pro
    Code
    Suggestions](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html#code-suggestions-data-usage).


    ## Get GitLab Duo Pro today


    GitLab Duo Pro is currently available at a special introductory price of $9
    USD per user/month to Ultimate and Premium customers. Beginning on February
    1, 2024, GitLab Duo Pro will be available for $19 USD per user/month.
    [Contact us today](https://about.gitlab.com/solutions/gitlab-duo-pro/sales/)
    to get started with GitLab Duo Pro.
  category: AI/ML
  tags:
    - AI/ML
    - DevSecOps platform
    - DevSecOps
config:
  slug: gitlab-duo-pro
  featured: true
  template: BlogPost
