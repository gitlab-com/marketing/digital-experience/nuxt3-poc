seo:
  title: GitLab receives Ally Technology Partner Award for Operational Excellence
  description: >-
    Financial firm recognizes GitLab for its ability to deliver lean, automated,
    and streamlined business models that drive simplified and resilient
    solutions for Ally and its customers.
  ogTitle: GitLab receives Ally Technology Partner Award for Operational Excellence
  ogDescription: >-
    Financial firm recognizes GitLab for its ability to deliver lean, automated,
    and streamlined business models that drive simplified and resilient
    solutions for Ally and its customers.
  noIndex: false
  ogImage: images/blog/hero-images/tanukilifecycle.png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-receives-ally-technology-partner-award-for-operational-excellence
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-receives-ally-technology-partner-award-for-operational-excellence
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab receives Ally Technology Partner Award for Operational Excellence",
            "author": [{"@type":"Person","name":"Sandra Gittlen"}],
            "datePublished": "2024-06-18",
          }

content:
  title: GitLab receives Ally Technology Partner Award for Operational Excellence
  description: >-
    Financial firm recognizes GitLab for its ability to deliver lean, automated,
    and streamlined business models that drive simplified and resilient
    solutions for Ally and its customers.
  authors:
    - Sandra Gittlen
  heroImage: images/blog/hero-images/tanukilifecycle.png
  date: '2024-06-18'
  body: >-
    Earlier this month, Ally Financial, a leading digital financial services
    company, awarded GitLab its Ally Technology Partner Award for Operational
    Excellence, citing the fundamental role GitLab and the GitLab DevSecOps
    platform play for Ally and its customers.


    "This award is meant to recognize partners who help us ensure the resiliency
    of our solutions and who are committed to not just providing us products but
    helping us to operationalize and maintain those products on a sustained
    basis," said Spencer Cremers, CIO of Enterprise Technology Operations at
    Ally Financial. "GitLab is a critical toolset that is fundamental to our
    day-to-day operations." 


    Ally began migrating to GitLab in recent years and now has a large number of
    applications that have fully adopted DevSecOps principles. GitLab enables
    Ally to carry out thousands of builds per day across all environments and
    deploy numerous builds into Production every week.


    GitLab was also lauded for helping support Ally's operational goals. "GitLab
    also provides tremendous operational support when we need service or
    responses on a short-notice basis," Cremers said.


    He added that GitLab is helping the company explore "virtualized development
    environments for a more efficient and predictable space for developers to
    learn," as well as security tools to shift security left in the software
    development lifecycle.


    This is the second year GitLab has won an [Ally Technology Partner
    Award](https://www.ally.com/tech/partnering-to-drive-transformation-2nd-annual-ally-technology-partner-awards/).
    In 2023, the first year these awards were given, the financial firm
    recognized GitLab for "[Velocity with
    Quality](https://www.ally.com/tech/recognizing-delivery-in-ecosystem-ally-technology-partner-awards/)"
    for excellent speed to market, responsiveness, and flexibility, allowing
    Ally to deliver value to customers quickly. 


    > Learn [how Ally uses the GitLab DevSecOps
    Platform](https://about.gitlab.com/customers/ally/) to achieve some big
    wins, including a 55% increase in deployment velocity and $300k yearly cost
    savings.
  category: News
  tags:
    - partners
    - news
    - DevSecOps platform
config:
  slug: gitlab-receives-ally-technology-partner-award-for-operational-excellence
  featured: false
  template: BlogPost
