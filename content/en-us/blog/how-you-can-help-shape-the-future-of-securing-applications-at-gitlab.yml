seo:
  title: How you can help shape the future of securing applications with GitLab
  description: >-
    We want to provide the best experience in keeping your application safe
    after your code is in production.
  ogTitle: How you can help shape the future of securing applications with GitLab
  ogDescription: >-
    We want to provide the best experience in keeping your application safe
    after your code is in production.
  noIndex: false
  ogImage: >-
    images/blog/hero-images/how-you-can-help-shape-the-future-of-securing-applications-at-gitlab.jpg
  ogUrl: >-
    https://about.gitlab.com/blog/how-you-can-help-shape-the-future-of-securing-applications-at-gitlab
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/how-you-can-help-shape-the-future-of-securing-applications-at-gitlab
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How you can help shape the future of securing applications with GitLab",
            "author": [{"@type":"Person","name":"Emily von Hoffmann"}],
            "datePublished": "2019-11-14",
          }

content:
  title: How you can help shape the future of securing applications with GitLab
  description: >-
    We want to provide the best experience in keeping your application safe
    after your code is in production.
  authors:
    - Emily von Hoffmann
  heroImage: >-
    images/blog/hero-images/how-you-can-help-shape-the-future-of-securing-applications-at-gitlab.jpg
  date: '2019-11-14'
  body: >
    This blog post was originally published on the [GitLab Unfiltered
    blog](/blog/categories/unfiltered/). It was reviewed and republished on
    2019-12-09.


    As part of our vision to deliver the entire DevOps lifecycle in a single
    application, we’re designing an experience that will allow security
    professionals to collaborate directly with developers. We need your help to
    make it the best it can be!


    Our newest product stage is Protect, and it’s an exciting time as we
    continue to define our [strategy and roadmap](/direction/govern/). The
    Protect UX team’s goal is to provide the best experience in keeping your
    application safe after your code is in production. This includes all
    features that help you defend your applications and cloud infrastructure by
    giving you the ability to identify, catalogue, manage, and remediate
    threats, vulnerabilities, and risks.


    Some of the new categories we’re planning for in 2020 include Runtime
    Application Self Protection, Threat Detection, User Entity and Behavioral
    Analytics and [more](/handbook/product/categories/#protect-section).


    We have a ton of UX research planned to help us learn more about this new
    category, and we hope you consider adding your voice.


    ### Our users' jobs to be done


    From what we know so far, the Protect user is responsible for maintaining
    the security of their company’s environments and applications. They seem to
    have a wide variety of job titles, including security analyst and SecOps
    engineer.


    We aim to understand our different users’ motivations and goals by
    identifying their primary [jobs to be
    done](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done). For the
    Protect user, these include things like:


    > When I make sure my company’s applications aren’t vulnerable to bad
    actors, I want to monitor the traffic coming to my application and detect
    the possibility of an attack (SQL injection attempts, XSS attempts,
    vulnerability scanners, etc.) so I can know what parts of the application I
    need to protect better.


    ### Our recruiting challenge


    Perhaps because we’re best known for our origins in source code management,
    we usually have an abundance of participants who fit our software developer
    persona when we’re recruiting for studies. Newer personas like our Protect
    users have been more elusive by comparison — we’ve attempted studies where
    we couldn’t find a single human to speak with.


    This is a real problem for us, as we believe strongly in evidence-based
    design. We want to build for your actual wants and needs as opposed to our
    assumptions about them.


    ### How you can help


    If any of this sounds like you, please sign up to our research program,
    [GitLab First Look](/community/gitlab-first-look/)! When you join, you can
    indicate exactly which product areas and types of research you’re interested
    in. We’ll send you invitations to participate when you match with studies.


    Questions? Reach out to me on [twitter](https://twitter.com/EmvonHoffmann).


    [Sam Kerr](/company/team/#stkerr) and [Tali Lavi](/company/team/#tlavi)
    contributed to this post.


    Cover image by [Rashid Khreiss](https://unsplash.com/@rush_intime) on
    [Unsplash](https://unsplash.com).
  category: Company
  tags:
    - UX
    - inside GitLab
config:
  slug: how-you-can-help-shape-the-future-of-securing-applications-at-gitlab
  featured: false
  template: BlogPost
