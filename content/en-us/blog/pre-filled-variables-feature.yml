seo:
  title: How pre-filled CI/CD variables will make running pipelines easier
  description: >-
    Learn more about this future release and how pre-filled variables will save
    time and reduce errors.
  ogTitle: How pre-filled CI/CD variables will make running pipelines easier
  ogDescription: >-
    Learn more about this future release and how pre-filled variables will save
    time and reduce errors.
  noIndex: false
  ogImage: images/blog/hero-images/logoforblogpost.jpg
  ogUrl: https://about.gitlab.com/blog/pre-filled-variables-feature
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/pre-filled-variables-feature
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How pre-filled CI/CD variables will make running pipelines easier",
            "author": [{"@type":"Person","name":"Chrissie Buchanan"}],
            "datePublished": "2020-12-02",
          }

content:
  title: How pre-filled CI/CD variables will make running pipelines easier
  description: >-
    Learn more about this future release and how pre-filled variables will save
    time and reduce errors.
  authors:
    - Chrissie Buchanan
  heroImage: images/blog/hero-images/logoforblogpost.jpg
  date: '2020-12-02'
  body: >

    [CI/CD variables](/topics/ci-cd/) are a useful way to customize pipelines
    based on their environment. But what if you need to override a variable, or
    what if you need to run a pipeline manually? These scenarios can create
    problems.


    *   What if you don’t know what variables/values to put in?

    *   What happens if you make a mistake?


    Having to enter variables and values manually is tedious and prone to error.
    Also, a user may not know all the different variables/values they need to
    enter. In GitLab 13.7, we’re introducing a feature that helps to solve these
    problems by generating pre-filled variables from your `.gitlab-ci.yml.` file
    when you run a pipeline.


    ### What are CI/CD variables?


    [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) are dynamic
    values assigned to environments. These environment variables affect the way
    running processes behave on an operating system. Variables allow teams to
    customize jobs in GitLab CI/CD.


    There are two places where teams can define variables:


    *   The `.gitlab-ci.yml.` file

    *   The GitLab Runner `config.toml.` file


    CI/CD variables can be very useful, but what if you need to override a
    variable or manually run a pipeline? You might do this if the results of a
    pipeline (for example, a code build) are required outside the normal
    operation of the pipeline. Teams may also opt for manual deployments to
    production and need to stop the pipeline early. Running a pipeline manually
    isn’t unusual, but [defining
    variables](https://docs.gitlab.com/ee/ci/variables/where_variables_can_be_used.html)
    and entering them in a manual pipeline hasn’t always been a totally smooth
    process.


    First, teams need to run a pipeline/job manually and then navigate into the
    overview. Then, they have to select all the required variables from a
    drop-down menu on the “Run Pipeline” page. If developers don’t know all the
    required variables by heart, they will need to check their references and
    switch back and forth. If there are numerous key/value pairs to enter, this
    can be especially tedious. 


    ### What are pre-filled variables?


    In 13.7, we’re introducing a feature that will streamline this process. Now
    the “Run pipeline” form will [generate pre-filled
    variables](https://gitlab.com/gitlab-org/gitlab/-/issues/30101) for your
    pipeline based on the variable definitions in your `.gitlab-ci.yml` file.
    The response to this feature from the GitLab community was enthusiastic.


    ![pre-filled variables
    issue](https://about.gitlab.com/images/blogimages/pre-filled-variables.png)

    People are excited about pre-filled variables!


    ### The benefits of pre-filled variables


    Having variables pre-filled is all about increasing efficiency and reducing
    the small frustrations that make jobs more difficult than they need to be. 


    Pre-filled variables will *reduce:*


    *   Frustration with scrolling dropdown values

    *   Friction with choosing the wrong values

    *   Re-running and debugging pipelines due to wrong values

    *   Errors and click actions


    ![Run Pipeline](https://about.gitlab.com/images/blogimages/Run-pipeline.gif)

    Pre-filled variables in action


    For teams that manually deploy to production, pre-filled variables will make
    it easier during that review step so that everyone with permissions can
    manually trigger the deployment pipeline. If the reviewer needs to make an
    exception they can override a variable, if necessary. 


    Pre-filled variables will help teams save time, reduce errors, and make the
    manual pipeline process a bit smoother. Do you think we're missing something
    or have ways that we can streamline the process even further? Leave a
    comment in [the issue](https://gitlab.com/gitlab-org/gitlab/-/issues/30101)
    and let us know what you think. Everyone can contribute.


    ### Other future GitLab CI releases


    Pre-filled variables are only one CI feature in the works. We release [new
    features](/upcoming-releases/) on the 22nd of every month, and everyone can
    contribute to these [public](/handbook/values/#public-by-default) issues. 


    ## More on CI/CD


    - [Want a more effective CI/CD pipeline? Try our pro
    tips](/blog/2020/07/29/effective-ci-cd-pipelines/)

    - [Webcast: 7 CI/CD hacks](/webcast/7cicd-hacks/)

    - [How to use GitLab’s CI/CD pipeline
    templates](/blog/2020/09/23/get-started-ci-pipeline-templates/)


    Cover image by [Gerrie van der
    Walt](https://unsplash.com/photos/m3TYLFI_mDo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
    on
    [Unsplash](https://unsplash.com/search/photos/pipes?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
  category: Insights
  tags:
    - CI/CD
    - features
config:
  slug: pre-filled-variables-feature
  featured: false
  template: BlogPost
