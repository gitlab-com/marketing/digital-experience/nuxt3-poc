seo:
  title: How GitLab supports the FedRAMP authorization journey
  description: >-
    This comprehensive guide dives into the FedRAMP certification process,
    explaining how GitLab offers guidance and best practices for configuration
    and compliance.
  ogTitle: How GitLab supports the FedRAMP authorization journey
  ogDescription: >-
    This comprehensive guide dives into the FedRAMP certification process,
    explaining how GitLab offers guidance and best practices for configuration
    and compliance.
  noIndex: false
  ogImage: images/blog/hero-images/AdobeStock_479904468-(1).jpeg
  ogUrl: >-
    https://about.gitlab.com/blog/how-gitlab-supports-the-fedramp-authorization-journey
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/how-gitlab-supports-the-fedramp-authorization-journey
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How GitLab supports the FedRAMP authorization journey",
            "author": [{"@type":"Person","name":"Christian Nnachi"}],
            "datePublished": "2024-08-07",
          }

content:
  title: How GitLab supports the FedRAMP authorization journey
  description: >-
    This comprehensive guide dives into the FedRAMP certification process,
    explaining how GitLab offers guidance and best practices for configuration
    and compliance.
  authors:
    - Christian Nnachi
  heroImage: images/blog/hero-images/AdobeStock_479904468-(1).jpeg
  date: '2024-08-07'
  body: >-
    The Federal Risk and Authorization Management Program (FedRAMP) is a U.S.
    government program that standardizes security assessment, authorization, and
    continuous monitoring for cloud products and services. Achieving FedRAMP
    authorization allows cloud service providers (CSPs) to offer their services
    to federal agencies, ensuring that these services meet stringent security
    and privacy requirements.


    In this article, you'll learn how to GitLab can help guide you on your
    FedRAMP authorization journey, including:

    * the key steps of the FedRAMP certification process

    * highlights of GitLab’s role in supporting FedRAMP requirements

    * best practices for configuration and compliance


    By leveraging GitLab’s features and adhering to recommended practices,
    organizations can streamline their path to FedRAMP authorization and ensure
    secure and compliant software development.


    ## Key requirements and compliance levels


    FedRAMP categorizes security requirements into [three levels based on the
    impact of
    data](https://www.fedramp.gov/understanding-baselines-and-impact-levels/)
    being handled:


    * **Low:** Impact on operations, assets, or individuals is limited.

    * **Moderate:** Impact on operations, assets, or individuals is serious.

    * **High:** Impact on operations, assets, or individuals is severe or
    catastrophic.


    ## Security and privacy controls from NIST 800-53


    FedRAMP's security controls are derived from the [National Institute of
    Standards and Technology (NIST) Special Publication
    800-53](https://csrc.nist.gov/pubs/sp/800/53/r5/upd1/final). Key areas
    include:


    * **Vulnerability scanning and patching SLAs:** Regular scanning and timely
    patching of vulnerabilities.

    * **Secure software supply chain:** Ensuring that the software and its
    components are secure.

    * **Change management:** Restricting unauthorized software or system changes
    through merge request (MR) approval rules.


    ## Importance of FedRAMP for organizations


    For CSPs, achieving FedRAMP authorization is crucial for doing business with
    federal agencies. Authorized services are listed on the [FedRAMP
    Marketplace](https://marketplace.fedramp.gov/products), enhancing their
    visibility and credibility.


    ## Steps to achieve FedRAMP certification


    The FedRAMP process is evolving, and a [new
    roadmap](https://www.fedramp.gov/2024-03-28-a-new-roadmap-for-fedramp/) has
    been introduced. To stay up to date on the latest changes, [subscribe to
    General Service Administration (GSA)
    list](https://public.govdelivery.com/accounts/USGSA/subscriber/new).


    ### Walkthrough of the certification process


    #### 1\. **Preparation and readiness**


    * **Preparation**
      * Understand FedRAMP requirements and prepare documentation.
    * **Readiness assessment**
      * CSPs can pursue the optional FedRAMP Ready designation by working with an accredited Third-Party Assessment Organization (3PAO). The 3PAO conducts a readiness assessment and documents the CSP's capability to meet federal security requirements in the Readiness Assessment Report (RAR).
    * **Pre-authorization**
      * CSPs formalize partnerships with an agency as outlined in the FedRAMP Marketplace: Designations for Cloud Service Providers.
      * CSPs prepare for the authorization process by making necessary technical and procedural adjustments to meet federal security requirements and prepare the required security deliverables for authorization.

    #### 2\. **Authorization package submission and assessment**


    * **Authorization package submission**
      * Historically: Submit the assessment package to the FedRAMP Joint Authorization Board (JAB) or a federal agency sponsor.
      * [**New process**](https://www.fedramp.gov/2024-03-28-a-new-roadmap-for-fedramp/)**:** Submit to the FedRAMP Board within the GSA, replacing the JAB. The process integrates Agile principles and uses threat-based analysis for control selection and implementation.
    * **Full security assessment**
      * The 3PAO conducts an independent audit of the CSP's system. Before this, the CSP should complete the System Security Plan (SSP) and have it reviewed and approved by the agency customer.
      * The 3PAO develops the Security Assessment Plan (SAP) with input from the authorizing agency. After testing, the 3PAO creates a Security Assessment Report (SAR) detailing their findings and providing a recommendation for FedRAMP Authorization.
    * **Agency authorization process**
      * The agency reviews the security authorization package, including the SAR, and may require CSP remediation.
      * The agency performs a risk analysis, accepts the risk, and issues an Authority to Operate based on its risk tolerance, with the option to implement, document, and test customer-responsible controls either before or after the ATO issuance.

    #### 3\. **Post-authorization and continuous monitoring**


    * **Continuous monitoring**
      * The continuous monitoring phase involves post-authorization activities to maintain FedRAMP-compliant security authorization.
    * **New tool**
      * [**automate.fedramp.gov**](https://www.fedramp.gov/2024-07-11-new-website-launch-automate-fedramp-gov/)**:** Provides detailed technical documentation, best practices, and guidance for creating and managing digital authorization packages with Open Security Controls Assessment Language ([OSCAL](https://pages.nist.gov/OSCAL/)). It supports a digital-first approach, offering faster documentation updates, enhanced user experience, and community collaboration.

    Detailed steps are available on the [FedRAMP Agency Authorization
    page](https://www.fedramp.gov/agency-authorization/). 


    ### Common challenges and pitfalls


    1. **Vulnerability management:** Ensuring timely and effective vulnerability
    management.

    2. **System boundaries:** Clearly defining and documenting system
    boundaries.

    3. **Software security practices:** Implementing and maintaining robust
    software security practices.

    4. **FIPS 140-2 cryptography:** Ensuring cryptographic modules are FIPS
    140-2 compliant (details available in [GitLab's FIPS Compliance
    documentation](https://docs.gitlab.com/ee/development/fips_compliance.html)).


    ## Role of self-managed GitLab in FedRAMP compliance


    ### Supporting FedRAMP requirements


    Self-managed GitLab can play a critical role in achieving FedRAMP compliance
    by providing tools and features that support secure code development and
    deployment within FedRAMP authorization boundaries.


    ### Specific features of GitLab aligned with FedRAMP standards


    1\. **Security configuration**


    You can configure [CI/CD
    pipelines](https://docs.gitlab.com/ee/topics/build_your_application.html) to
    continuously test code while it ships and simultaneously enforce security
    policies. GitLab includes a suite of security tools that you can incorporate
    into the development of customer applications, including but not limited to:


    * [Security
    configuration](https://docs.gitlab.com/ee/user/application_security/configuration/index.html)

    * [Container
    scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/index.html)

    * [Dependency
    scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html)

    * [Static application security
    testing](https://docs.gitlab.com/ee/user/application_security/sast/index.html)

    * [Infrastructure as code (IaC)
    scanning](https://docs.gitlab.com/ee/user/application_security/iac_scanning/index.html)

    * [Secret
    detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/index.html)

    * [Dynamic application security testing
    (DAST)](https://docs.gitlab.com/ee/user/application_security/dast/index.html)

    * [API
    fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html)

    * [Coverage-guided fuzz
    testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/index.html)


    2\. **Access control and authentication**


    Access management in a GitLab deployment varies for each customer. GitLab
    offers extensive documentation on deployments using both identity providers
    and GitLab's native authentication configurations. It is crucial to evaluate
    your organization's specific requirements before deciding on an
    authentication approach for your GitLab instance.


    3\. **[Identity
    providers](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html#identity-providers)**


    To comply with FedRAMP requirements, ensure your existing identity provider
    is FedRAMP-authorized and listed on the FedRAMP Marketplace, and for
    requirements like personal identity verification (PIV), use an identity
    provider rather than relying on native authentication in self-managed
    GitLab.


    4\. **[Native GitLab user authentication
    configurations](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html#native-gitlab-user-authentication-configurations)**


    GitLab enables administrators to monitor users with different levels of
    sensitivity and access requirements.


    5\. [**Audits and
    accountability**](https://docs.gitlab.com/ee/administration/audit_event_streaming/)


    GitLab provides a wide array of security events and streaming capabilities
    for comprehensive logging and monitoring that can be routed to a Security
    Information and Event Management (SIEM) solution.


    * [Event
    types](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html#event-types)


    6\. **Incident response**


    After configuring audit events, it's crucial to monitor them. GitLab offers
    [tools](https://docs.gitlab.com/ee/operations/incident_management/index.html)
    for alert management, incident tracking, and status reporting through a
    centralized interface, allowing you to compile system alerts from SIEM or
    other security tools, triage incidents, and keep stakeholders informed.


    *
    [alerts](https://docs.gitlab.com/ee/operations/incident_management/alerts.html)

    *
    [incidents](https://docs.gitlab.com/ee/operations/incident_management/incidents.html)

    * [on-call
    schedules](https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html)

    * [status
    page](https://docs.gitlab.com/ee/operations/incident_management/status_page.html)


    7\. **Configuration management**


    At its core, GitLab meets [configuration
    management](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html#configuration-management-cm)
    needs with robust CI/CD pipelines, approval workflows, and change control,
    primarily using issues and MRs to manage changes.


    8\. **Federal Information Processing Standard (FIPS) compliance**


    GitLab supports [FIPS
    compliance](https://docs.gitlab.com/ee/development/fips_compliance.html) by
    offering versions that use FIPS-validated cryptographic modules such as
    OpenSSL, BoringSSL, or other CMVP-validated modules. This ensures that
    cryptographic operations meet FIPS requirements, making it suitable for use
    in environments that require high levels of security compliance, such as
    those seeking FedRAMP authorization. Additionally, GitLab's documentation
    provides detailed instructions for installing and configuring FIPS-compliant
    deployments, including a hybrid approach using omnibus and cloud native
    components.


    9\. [**NIST 800-53 R5 security and privacy controls management project
    template**](https://gitlab.com/gitlab-org/project-templates/nist_80053r5)


    The project template helps track and manage compliance with NIST 800-53 R5
    using GitLab issues, based on [NIST 800-53R5
    specifications](https://csrc.nist.gov/pubs/sp/800/53/r5/upd1/final). It
    includes pre-configured issues, issue boards, and a notional example
    pipeline to run tests using OpenSCAP (OSCAP) and update issues with
    artifacts and labels, creating a controls management project within GitLab.
    This template centralizes compliance efforts, automates control testing, and
    facilitates a seamless workflow for both project teams and auditors.


    ## Best practices for using GitLab in the FedRAMP process


    ### Recommended configurations and setups


    To align self-managed GitLab with NIST 800-53 controls and FedRAMP
    requirements, consider the following best practices:


    1. **Security hardening:** Follow GitLab’s [security hardening
    guidance](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html).

    2. **Access control:** Implement role-based access control (RBAC) and
    enforce [the principle of least
    privilege](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/).

    3. **CI/CD pipelines:** Configure pipelines to include security testing and
    approval stages.

    4. **Audit logging:** Enable comprehensive audit logging and integrate with
    a SIEM system.

    5. **Backup and recovery:** Establish robust backup and recovery processes.


    ### NIST 800-53 compliance


    GitLab provides various compliance features to help automate critical
    controls and workflows. Administrators should work with customer solutions
    architects to configure GitLab instances to meet applicable [NIST 800-53
    controls](https://docs.gitlab.com/ee/security/hardening_nist_800_53.html).


    ## Start your FedRAMP compliance journey


    Achieving FedRAMP authorization is a complex but strategic process for CSPs
    looking to provide services to federal agencies. Self-managed GitLab offers
    a comprehensive suite of tools and features that can support this journey,
    ensuring secure and compliant software development and operations. By
    following best practices and leveraging GitLab’s capabilities, organizations
    can navigate the challenges of FedRAMP compliance and successfully achieve
    authorization.


    > Learn more about [GitLab's solutions for the public
    sector](https://about.gitlab.com/solutions/public-sector/).
  category: Security
  tags:
    - tutorial
    - public sector
    - DevSecOps
config:
  slug: how-gitlab-supports-the-fedramp-authorization-journey
  featured: true
  template: BlogPost
