seo:
  title: Optimize DevOps with enhanced visibility tools in GitLab 14
  description: >-
    How GitLab 14's end-to-end visibility and actionability can help users
    understand and improve delivery and alignment.
  ogTitle: Optimize DevOps with enhanced visibility tools in GitLab 14
  ogDescription: >-
    How GitLab 14's end-to-end visibility and actionability can help users
    understand and improve delivery and alignment.
  noIndex: false
  ogImage: images/blog/hero-images/devops.png
  ogUrl: https://about.gitlab.com/blog/optimizing-devops-visibility-in-gitlab-14
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/optimizing-devops-visibility-in-gitlab-14
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Optimize DevOps with enhanced visibility tools in GitLab 14",
            "author": [{"@type":"Person","name":"Cormac Foster"}],
            "datePublished": "2021-07-21",
          }

content:
  title: Optimize DevOps with enhanced visibility tools in GitLab 14
  description: >-
    How GitLab 14's end-to-end visibility and actionability can help users
    understand and improve delivery and alignment.
  authors:
    - Cormac Foster
  heroImage: images/blog/hero-images/devops.png
  date: '2021-07-21'
  body: "\n[DevOps makes teams and work more efficient](/topics/devops/how-and-why-to-create-devops-platform-team/), more consistent, and more productive – but how much more?\n\nOn its surface, the answer is simple. We need to measure workflow from idea to delivery, identify and remove blockers, and benchmark improvements in a manner that is consistent and replicable. The challenge is the way we've typically built the systems that hold the data we're trying to understand.\n\nEnhanced visibility tools are essential to measuring and optimizing modern DevOps processes, and mapping the work output to ensure the business outcomes that matter are achieved.\n\n## The failure of DIY DevOps\n\nMost businesses operate and maintain a multi-product \"DIY DevOps\" toolchain, but stitched-together applications with bespoke integrations don't lend themselves to visibility. Each component in the toolchain captures a unique set of data, with distinct formatting and metadata, logged to a siloed data store. Extracting, correlating, and displaying that data is a labor intensive chore –\_assuming the various APIs allow proper access at all. Poor visibility can lead to slow and imprecise decision-making and misalignment between teams, but building and maintaining visibility in DIY toolchain saps resources from your business, adding work instead of removing it.\n\n## A platform for visibility\n\nAt GitLab, we believe that stumbling in the dark and maintaining complex toolchains are not viable business strategies. We all deserve better, and [GitLab 14](/gitlab-14/) is the [DevOps platform](/topics/devops-platform/) that provides enhanced visibility without added work. As a complete DevOps platform, GitLab 14 is uniquely capable of delivering visibility into DevOps processes, surfacing out-of-the-box insights from across the product delivery lifecycle and helps users understand what works, what doesn't, and how to make improvements.\n\n## Metrics that matter\n\n![Lead Time for Changes helps you understand your team's velocity, agility, and efficiency, from the first code commit to production.](https://about.gitlab.com/images/blogimages/lead_time.png){: .shadow}\nLead Time for Changes helps you understand your team's velocity, agility, and efficiency.\n{: .note.text-center}\n\nGitLab 14 delivers operational metrics to help users understand DevOps maturity and benchmark progress. The DevOps Research and Assessment (DORA) firm demonstrated how DevOps maturity leads to positive business outcomes like happier customers, greater market share, and increased revenue. They've outlined [four key metrics](https://cloud.google.com/blog/products/devops-sre/using-the-four-keys-to-measure-your-devops-performance) that are highly correlated with business performance, and GitLab 14 surfaces two of the four. [Deployment Frequency](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html#deployment-frequency-charts) charts help monitor the efficiency of deployments over time, find bottlenecks, and understand when and how to improve deployment process. [Lead Time for Changes](https://docs.gitlab.com/ee/user/analytics/ci_cd_analytics.html#lead-time-charts) helps users understand their team's velocity, agility, and efficiency – from the first code commit to all the way through production.\n\n## Actionable insights\n\n![Value Stream Analytics lets you zero in on value blockers and immediately remediate them.](https://about.gitlab.com/images/blogimages/value_stream_analytics.png){: .shadow}\nValue Stream Analytics lets you zero in on value blockers and immediately remediate them.\n{: .note.text-center}\n\nAfter identifying opportunities for change, you should be able to take action right away with GitLab 14. Our [customizable Value Stream Analytics](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html) tools allow teams to monitor specific workflows tailored to their particular needs and identify high-priority blockers to delivering value to customers.\n\nUnlike products that focus exclusively on visibility and discovery, GitLab 14 makes these insights actionable. With one click, users can move from identifying a merge request stuck in code review or an issue waiting for approval to solving the problem. Actionable insights removes wasteful loops of questions and clarifications, and allows all users to focus on productive work.\n\n## See for yourself\n\nWant to learn more? Learn how GitLab customers like [Crédit Agricole](/customers/credit-agricole/), [Hotjar](/customers/hotjar/),and [others](/customers/) are turning visiblity and and insights into business value, or take the next step and [try GitLab Ultimate for free](/free-trial/)!\n\nThis blog is part two in a three-part series on some of the top features of GitLab 14. Learn more about how GitLab 14 includes some of the [top Security features in part one](/blog/2021/07/20/are-you-ready-for-the-newest-era-of-devsecops/). \n"
  category: Insights
  tags:
    - DevOps
    - agile
config:
  slug: optimizing-devops-visibility-in-gitlab-14
  featured: false
  template: BlogPost
