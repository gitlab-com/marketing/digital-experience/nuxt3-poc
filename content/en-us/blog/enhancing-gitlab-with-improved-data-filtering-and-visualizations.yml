seo:
  title: Enhancing GitLab with improved data filtering and visualizations
  description: >-
    Discover how GitLab's new data views will streamline your workflow and power
    decision-making.
  ogTitle: Enhancing GitLab with improved data filtering and visualizations
  ogDescription: >-
    Discover how GitLab's new data views will streamline your workflow and power
    decision-making.
  noIndex: false
  ogImage: images/blog/hero-images/agile.png
  ogUrl: >-
    https://about.gitlab.com/blog/enhancing-gitlab-with-improved-data-filtering-and-visualizations
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/enhancing-gitlab-with-improved-data-filtering-and-visualizations
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Enhancing GitLab with improved data filtering and visualizations",
            "author": [{"@type":"Person","name":"Amanda Rueda"}],
            "datePublished": "2024-03-05",
          }

content:
  title: Enhancing GitLab with improved data filtering and visualizations
  description: >-
    Discover how GitLab's new data views will streamline your workflow and power
    decision-making.
  authors:
    - Amanda Rueda
  heroImage: images/blog/hero-images/agile.png
  date: '2024-03-05'
  body: >
    In the hectic world of product management, quick access, refinement, and
    visualization of data are essential to drive efficiency and productivity
    forward. Our recent exploration into the needs of product managers uncovered
    a vital link between their success and their ability to make data-driven
    decisions. Conversely, the drain on time and development resources emerged
    as a significant deterrent to high performance.


    Recognizing the critical role of data in strategic decision-making and
    prioritization, we're excited to announce an upcoming initiative, [Improved
    Data Filtering and
    Visualization](https://gitlab.com/groups/gitlab-org/-/epics/5516). This
    initiative focuses on the usability, flexibility, and efficiency of GitLab's
    planning views, simplifying how users interact with, recall, and share
    project data.


    ## Streamlining data interaction and retrieval


    We're focusing on consolidating multiple views into a unified platform,
    eliminating confusing navigation, and enabling users to create complex
    queries in a user-friendly manner. This approach not only makes data more
    accessible but also empowers users to visualize it in formats that best suit
    their needs. By providing a single hub for data access and presentation,
    we're making it easier for users to obtain critical information they need to
    make more informed, data-driven decisions quickly, leading to streamlined
    workflows and elevated project outcomes.


    ### The evolution from multiple views to a single, configurable experience


    Here is what it looks like when you evolve from multiple views to a single,
    configurable experience:


    ![Image of moving from multiple views to a single
    view](//images.ctfassets.net/r9o86ar0p03f/5orqBp18axge2xZXGdurJ4/fccd1db8f82ad7c41aa4287ef1e1a75b/image1.png)


    ## Transforming workflows with real-world applications


    Imagine a scenario where you must review the progress of multiple
    interconnected projects. Traditionally, this would involve navigating
    through various parts of GitLab, each with its own set of filters and views.
    With our Improved Data Filtering and Visualization initiative, you will now
    access a consolidated view that allows for creating intuitive queries. This
    new view can display issues and epics in a nested format, providing the
    hierarchical context you need to understand your project structure fully.
    What's more, you will have the ability to easily switch to a roadmap or
    board view as your use case demands.


    Another example involves a development team planning their upcoming sprint.
    Instead of juggling between different pages for issues, epics, and boards,
    the team can utilize a single, customized view that allows them to view the
    context of related work items, update the status of work without opening
    multiple tabs, and understand work item dependencies. This saves precious
    synchronous time for the team and creates an efficient workflow by bringing
    visibility teams need to the forefront.


    ## Engage with us!


    As we venture into this transformative initiative, your insights and
    feedback will become the backbone of our progress. We're not merely
    enhancing features; we're on a mission to revolutionize how product planning
    can be successful within GitLab. Your insights will help turn this ambitious
    vision into reality.


    Delve into our proposed feature iterations [detailed within our
    initiative](https://gitlab.com/groups/gitlab-org/-/epics/5516#feature-iterations)
    and leave your comments on the epic. Your perspective on these enhancements
    is invaluable, helping us refine our approach and ensure it aligns with your
    needs and expectations.


    This is more than a call to action — it's an invitation to shape the future
    of GitLab together. Share your feedback, suggestions, and visions with us!


    ## Leverage Agile planning with GitLab


    Building on our commitment to streamlining workflows with the Improved Data
    Filtering and Visualization initiative, it's worth highlighting that GitLab
    also deeply integrates Agile delivery principles to enhance software
    development lifecycles. Discover how GitLab bridges strategy with execution,
    enabling teams to adopt Agile methodologies like Scrum and Kanban, and scale
    with frameworks such as [SAFe](https://scaledagileframework.com/) and
    [LeSS](https://less.works/less/framework). 


    > Explore more about [enhancing Agile Delivery with
    GitLab](https://about.gitlab.com/solutions/agile-delivery/) and drive faster
    value creation.
  category: Agile Planning
  tags:
    - agile
    - DevSecOps platform
    - collaboration
    - product
config:
  slug: enhancing-gitlab-with-improved-data-filtering-and-visualizations
  featured: false
  template: BlogPost
