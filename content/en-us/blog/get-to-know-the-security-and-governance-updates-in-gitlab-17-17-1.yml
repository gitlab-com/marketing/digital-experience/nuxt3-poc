seo:
  title: Get to know the security and governance updates in GitLab 17, 17.1
  description: >-
    Dive deep into the new enhancements that can strengthen your organization's
    security posture, including how-to videos for SAST, DAST, API security,
    container registry, and more.
  ogTitle: Get to know the security and governance updates in GitLab 17, 17.1
  ogDescription: >-
    Dive deep into the new enhancements that can strengthen your organization's
    security posture, including how-to videos for SAST, DAST, API security,
    container registry, and more.
  noIndex: false
  ogImage: images/blog/hero-images/AdobeStock_282096522.jpeg
  ogUrl: >-
    https://about.gitlab.com/blog/get-to-know-the-security-and-governance-updates-in-gitlab-17-17-1
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/get-to-know-the-security-and-governance-updates-in-gitlab-17-17-1
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Get to know the security and governance updates in GitLab 17, 17.1",
            "author": [{"@type":"Person","name":"Fernando Diaz"}],
            "datePublished": "2024-07-17",
          }

content:
  title: Get to know the security and governance updates in GitLab 17, 17.1
  description: >-
    Dive deep into the new enhancements that can strengthen your organization's
    security posture, including how-to videos for SAST, DAST, API security,
    container registry, and more.
  authors:
    - Fernando Diaz
  heroImage: images/blog/hero-images/AdobeStock_282096522.jpeg
  date: '2024-07-17'
  body: >
    With every GitLab release we enhance and optimize security and governance
    solutions to ensure customers have the tools they need to produce secure and
    compliant software. Our values of
    [iteration](https://handbook.gitlab.com/handbook/values/#iteration) and
    [results for
    customers](https://handbook.gitlab.com/handbook/values/#results) drive our
    release cycles, and GitLab 17 is no exception. We have been releasing every
    month for the past 153 months straight!


    In this article, you'll learn my favorite security and governance
    enhancements released in GitLab 17 and 17.1 and how they can benefit your
    organization’s security requirements. 


    - [SAST analyzer streamlining](#sast-analyzer-streamlining)

    - [Android dependency scanning](#android-dependency-scanning)

    - [Custom roles and granular security permissions
    updates](#custom-roles-and-granular-security-permissions-updates)

    - [Secret detection updates](#secret-detection-updates)

    - [Container registry updates](#container-registry-updates)

    - [API security scanning updates](#api-security-scanning-updates)


    ## SAST analyzer streamlining


    GitLab provides static application security testing
    ([SAST](https://docs.gitlab.com/ee/user/application_security/sast/)) to
    examine your source code for known vulnerabilities, detecting
    vulnerabilities such as SQL injections and cross-site scripting. When SAST
    kicks off, the programming language used is auto-detected and the
    appropriate scanner is loaded.


    In GitLab 17, SAST scans the same languages, but now with fewer analyzers,
    [offering a simpler and more customizable
    experience](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#streamlined-sast-analyzer-coverage-for-more-languages).
    Language-specific analyzers have been replaced with GitLab-managed rules in
    the Semgrep-based analyzer for the following languages:


    - C/C++

    - Swift (iOS)

    - Java/Kotlin (Android)

    - Node.js

    - PHP

    - Ruby


    Having one analyzer for many different languages makes configurations and
    writing rules easier than ever. See the [supported languages and frameworks
    documentation](https://docs.gitlab.com/ee/user/application_security/sast/#supported-languages-and-frameworks)
    for more information.


    Watch this video to learn more:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/_80z6mZmzek?si=i9yPQttxuwVcb7Ye" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Android dependency scanning


    In modern software development, many applications are built from multiple
    dependencies that are best at performing their intended function. For
    example, rather than writing a YAML parser, a developer will use a library
    that parses YAML. This allows developers to focus on the main goal of their
    application, rather than spending time on utility functions.


    While the use of dependencies speeds up efficiency, they can be difficult to
    manage and could introduce vulnerabilities to your application. For this,
    GitLab provides [dependency
    scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/),
    which analyzes dependencies for known vulnerabilities. 


    Many organizations are using dependencies even when creating native mobile
    applications. In GitLab 17, we introduced [Android dependency
    scanning](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#dependency-scanning-support-for-android)
    to bridge the gap. Android dependency scanning can be easily added as a
    [CI/CD catalog
    component](https://gitlab.com/explore/catalog/components/android-dependency-scanning)
    – just include the following code in your `.gitlab-ci.yml`:


    ```

    include:
      - component: gitlab.com/components/android-dependency-scanning/component@1.0.0
        inputs:
          stage: test
    ```


    This job will also generate a CycloneDX software bill of materials
    ([SBOM](https://about.gitlab.com/blog/2022/10/25/the-ultimate-guide-to-sboms/))
    report, which may be necessary for compliance. Make sure to scan your
    Android dependencies as soon as possible, as there are many CVEs out there.


    Watch this video to learn more:

    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/_80z6mZmzek?si=DdB7j4NAenl-UcrJ" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    > Learn more SBOMs and dependencies with [our ultimate guide to
    SBOMs](https://about.gitlab.com/blog/2022/10/25/the-ultimate-guide-to-sboms/).


    ## Custom roles and granular security permissions updates


    GitLab provides [custom
    roles](https://docs.gitlab.com/ee/user/custom_roles.html) to allow
    organizations to create user roles with the precise privileges and
    permissions to meet their needs. This enables organizations to [implement
    the principle of least
    privilege](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/)
    to adhere to various compliance standards.


    ![custom roles
    screenshot](//images.ctfassets.net/r9o86ar0p03f/5EamreipcXz7wvbFQ0uKnV/aa431dcaeabe05e1b3992dd8115b045a/1.png)


    In GitLab 17, managing custom roles has become easier than ever. You can now
    [edit a custom role and its permissions directly from the
    UI](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#edit-a-custom-role-and-its-permissions),
    whereas, in the past, the role needed to be recreated. Also, for those using
    GitLab self-managed, [custom roles are now managed at the instance
    level](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#manage-custom-roles-at-self-managed-instance-level),
    allowing administrators to create the roles, and group owners to assign
    them.


    Watch this video to learn more:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/glvvCoc2hkc?si=dl_SwQ7tyVdzirH5" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    There have also been [several UX
    improvements](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#ux-improvements-to-custom-roles)
    added to this feature along with the introduction of the following
    permissions:


    - assign security policy links

    - manage and assign compliance frameworks

    - manage webhooks

    - manage push rules

    - manage merge request settings (17.1)

    - manage integrations (17.1)

    - manage deploy tokens (17.1)

    - read CRM contacts (17.1)


    GitLab releases usually include new permissions to further enable the
    implementation of the principle of least privilege. To learn more about the
    available granular security permissions, [visit the available custom
    permission
    documentation](https://docs.gitlab.com/ee/user/custom_roles/abilities.html).


    ## Secret detection updates


    Developers may accidentally commit secrets like keys or API tokens to Git
    repositories from time to time. After a sensitive value is pushed to a
    remote repository, anyone with access to the repository can impersonate the
    authorized user of the secret and cause mayhem. When this occurs the exposed
    secrets must be revoked and replaced to address this risk, which can cause
    system downtime.


    GitLab provides [secret
    detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
    to address this risk, and in GitLab 17 it’s gotten even better with the
    following enhancements:


    - [Support for remote rulesets when overriding or disabling
    rules](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#secret-detection-now-supports-remote-rulesets-when-overriding-or-disabling-rules):
    - Allows you to override or disable rules via a remote configuration.
    Therefore, you can scale rule configurations across multiple projects using
    only one [TOML](https://toml.io/en/) file.

    - [Advanced vulnerability
    tracking](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#introducing-advanced-vulnerability-tracking-for-secret-detection):
    Detects when the same secret has moved within a file due to refactoring or
    unrelated changes. This leads to reduced duplicate findings, simplifying
    vulnerability management.


    In GitLab 17.1, [secret push
    protection](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#secret-push-protection-available-in-beta)
    is now in Beta. Secret push protection checks the content of each commit
    pushed to GitLab. If any secrets are detected, the push is blocked and
    displays information about the commit. Therefore, a developer does not need
    to do the extra work of removing and rotating secrets, since they are never
    committed upstream.


    ![Push block eue to detected
    secret](//images.ctfassets.net/r9o86ar0p03f/1zuAiR7uFvMkDSI8wdbfR4/68991aee9614438a8ed095af9727a122/2.png)


    When [push protection
    occurs](https://about.gitlab.com/blog/2024/06/24/prevent-secret-leaks-in-source-code-with-gitlab-secret-push-protection/),
    you can see it displays additional information on the commit, including:


    - the commit ID that contains the secret

    - the filename and line number that contains the secret

    - the type of secret


    **Note:**  [Enabling secret push
    protection](https://docs.gitlab.com/ee/user/application_security/secret_detection/secret_push_protection/#enable-secret-push-protection)
    is as easy as flipping a switch in GitLab Security Configuration.


    Watch this video to learn more:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/ZNtwXVj3tA8?si=4xJ1rWdThpVjvebv" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Container registry updates


    GitLab provides a [built-in container
    registry](https://docs.gitlab.com/ee/user/packages/container_registry/),
    making it easy for developers to store and manage container images for each
    GitLab project without context switching. GitLab 17.1 includes several
    features to enhance the security and efficiency of using the registry:

    - [Container images linked to
    signatures](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#container-images-linked-to-signatures):
    Container images in the registry can now be signed and associated with the
    signature. This can reduce image tampering by allowing developers to quickly
    find and validate the signatures that are associated with a container image

    - [Display the last published date for container
    images](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#display-the-last-published-date-for-container-images):
    The container registry UI has been updated to include accurate
    `last_published_at timestamps`, putting critical data at the top of view.

    - [Sort container registry tags by publish
    date](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#sort-container-registry-tags-by-publish-date):
    Allows developers to quickly find and validate the most recently published
    container image.


    ![Signed container
    details](//images.ctfassets.net/r9o86ar0p03f/1IcEOgesZpg7qs5CL1mQK4/d9114373a33680a754ec8a9c9daf40ee/3.png)


    Additionally we’ve introduced [container scanning for the
    registry](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#container-scanning-for-registry).
    The container images being used in your application may themselves be based
    on other container images that contain known vulnerabilities. Since
    developers heavily make use of the built-in container registry, it is a
    no-brainer to introduce [container
    scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/)
    for the registry.


    [Container scanning for the
    registry](https://docs.gitlab.com/ee/user/application_security/container_scanning/#container-scanning-for-registry)
    can be easily enabled by flipping a switch in GitLab Security Configuration.
    Once it’s enabled, whenever a container image is pushed to the container
    registry in your project, GitLab checks its tag. If the tag is `latest`,
    then GitLab creates a new pipeline that scans the image and even produces a
    CycloneDX SBOM.


    **Note:** At the moment, a vulnerability scan is only performed when a new
    advisory is published. We are working to detract all vulnerabilities in the
    registry itself in future iterations.


    Watch this video to learn more:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/Zuk7Axs-CRw?si=odlgT5HWv_KOnBtq" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## API security scanning updates


    While SAST does a great job of finding vulnerabilities in static source
    code, there can still be vulnerabilities present in the running application
    that cannot be detected in source code, such as broken authentication and
    security misconfigurations. For these reasons, GitLab provides dynamic
    application security testing
    ([DAST](https://docs.gitlab.com/ee/user/application_security/dast/)) and
    [Web API
    fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/index.html)
    to help discover bugs and potential security issues that other QA processes
    may miss. 


    In GitLab 17, we’ve introduced [several
    enhancements](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#api-security-testing-analyzer-updates)
    to our [dynamic scanners which target Web
    APIs](https://docs.gitlab.com/ee/user/application_security/api_security_testing/index.html),
    including:

    - system environment variables are now passed from the CI runner to the
    custom Python scripts used for certain advanced scenarios (like request
    signing)

    - API Security containers now run as a non-root user, which improves
    flexibility and compliance

    - support for servers that only offer TLSv1.3 ciphers, which enables more
    customers to adopt API security testing.

    - scanner image upgraded to Alpine 3.19, which addresses security
    vulnerabilities


    In GitLab 17.1, additional configuration variables were added to [API
    security
    scanning](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#api-security-testing-analyzer-updates)
    and [API
    fuzzing](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#fuzz-testing-analyzer-updates)
    to allow:

    - creation of a comma-separated list of HTTP success status codes that
    define whether the job has passed

    - disabling of waiting for the target API to become available before
    scanning begins

    - specifying the expected status code for the API target availability check


    Watch this video to learn more:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/CcyOoBgSPUU?si=hAMQfmUTlLRKhPSg" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Learn more about other enhancements


    GitLab 17 and 17.1 also introduced several other security and governance
    features and enhancements, too many to cover in this blog. Some of these
    features include:


    - [Updated filtering on the Vulnerability
    Report](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#updated-filtering-on-the-vulnerability-report):
    You can now use the filtered search component to filter the Vulnerability
    Report by any combination of status, severity, tool, or activity.

    - [Toggle merge request approval policies to fail open or fail
    closed](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#toggle-merge-request-approval-policies-to-fail-open-or-fail-closed):
    A new fail open option for merge request approval policies to offer
    flexibility to teams who want to ease the transition to policy enforcement
    as they roll out controls in their organization.

    - [Optional configuration for policy bot
    comment](https://about.gitlab.com/releases/2024/05/16/gitlab-17-0-released/#optional-configuration-for-policy-bot-comment):
    The security policy bot posts a comment on merge requests when they violate
    a policy to help users understand when policies are enforced on their
    project, when evaluation is completed, and if there are any violations
    blocking an MR, with guidance to resolve them.

    - [Merge request approval policies fail open/closed (policy
    editor)](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#merge-request-approval-policies-fail-openclosed-policy-editor):
    Within the policy editor users can now toggle security policies to fail open
    or fail closed. This enhancement extends the YAML support to allow for
    simpler configuration within the policy editor view.

    - [Project owners receive expiring access token
    notifications](https://about.gitlab.com/releases/2024/06/20/gitlab-17-1-released/#project-owners-receive-expiring-access-token-notifications):
    Both project owners and maintainers with direct membership now receive email
    notifications when their project access tokens are close to expiring. This
    helps keep more people informed about upcoming token expiration.


    These are some of the newest security and compliance enhancements provided
    in GitLab 17 and 17.1 that can be applied to strengthen your organization's
    security posture! To learn more about GitLab and the other ways we can
    strengthen your organization's security throughout all parts of the software
    development lifecycle, check out the following links:


    - [GitLab Security and
    Compliance](https://about.gitlab.com/solutions/security-compliance/)

    - [GitLab Application Security
    documentation](https://docs.gitlab.com/ee/user/application_security/)

    - [GitLab security and governance overview
    video](https://youtu.be/Y4RC-SW8Ric)

    - [GitLab Complete DevSecOps
    demo](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/simply-vulnerable-notes)

    - [GitLab Complete DevSecOps
    tutorial](https://gitlab-da.gitlab.io/tutorials/security-and-governance/devsecops/simply-vulnerable-notes/) 

    - [Ultimate guide to the principle of least
    privilege](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/)
  category: Security
  tags:
    - security
    - product
    - tutorial
    - DevSecOps platform
    - features
config:
  slug: get-to-know-the-security-and-governance-updates-in-gitlab-17-17-1
  featured: true
  template: BlogPost
