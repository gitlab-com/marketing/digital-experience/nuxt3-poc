seo:
  title: Hosted Runners for GitLab Dedicated available in Beta
  description: >-
    GitLab Dedicated customers can now scale their CI/CD workloads with no
    maintenance overhead.
  ogTitle: Hosted Runners for GitLab Dedicated available in Beta
  ogDescription: >-
    GitLab Dedicated customers can now scale their CI/CD workloads with no
    maintenance overhead.
  noIndex: false
  ogImage: images/blog/hero-images/dedicatedcoverimage.png
  ogUrl: >-
    https://about.gitlab.com/blog/hosted-runners-for-gitlab-dedicated-available-in-beta
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/hosted-runners-for-gitlab-dedicated-available-in-beta
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Hosted Runners for GitLab Dedicated available in Beta",
            "author": [{"@type":"Person","name":"Fabian Zimmer"}],
            "datePublished": "2024-01-31",
          }

content:
  title: Hosted Runners for GitLab Dedicated available in Beta
  description: >-
    GitLab Dedicated customers can now scale their CI/CD workloads with no
    maintenance overhead.
  authors:
    - Fabian Zimmer
  heroImage: images/blog/hero-images/dedicatedcoverimage.png
  date: '2024-01-31'
  body: >-
    Managing fleets of runners can be complex and requires significant
    experience to ensure all CI/CD jobs can scale to meet the demands of
    developers. Hosted Runners for GitLab Dedicated, now available in Beta,
    allows customers to use runners that are fully managed by GitLab for CI/CD
    jobs running on GitLab Dedicated.


    Hosted Runners for GitLab Dedicated brings the same flexibility, efficiency,
    and control of GitLab Dedicated to runners. The Beta release includes the
    following features:

    - Linux-based runners at the instance level

    - Complete isolation from other tenants, following the same principles as
    GitLab Dedicated

    - Auto-scaling

    - Fully managed by GitLab


    Additional features will be included based on customer demand leading up to
    limited and general availability.


    As we develop this new feature, we are making Hosted Runners for GitLab
    Dedicated available upon invitation for existing GitLab Dedicated customers.
    Please reach out to your Customer Success Manager or [contact
    sales](https://about.gitlab.com/sales/). You can learn more about Gitlab
    Dedicated [on our website](https://about.gitlab.com/dedicated/).
  category: News
  tags:
    - features
    - product
    - CI/CD
config:
  slug: hosted-runners-for-gitlab-dedicated-available-in-beta
  featured: false
  template: BlogPost
