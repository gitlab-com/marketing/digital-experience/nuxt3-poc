seo:
  title: How I use analogy to design for highly technical spaces
  description: >-
    Just how much does a designer need to know about a technical space or
    product to design for it?
  ogTitle: How I use analogy to design for highly technical spaces
  ogDescription: >-
    Just how much does a designer need to know about a technical space or
    product to design for it?
  noIndex: false
  ogImage: images/blog/hero-images/Understand-Highly-Technical-Spaces.jpg
  ogUrl: https://about.gitlab.com/blog/understand-highly-technical-spaces
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/understand-highly-technical-spaces
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How I use analogy to design for highly technical spaces",
            "author": [{"@type":"Person","name":"Camellia Yang"}],
            "datePublished": "2021-08-13",
          }

content:
  title: How I use analogy to design for highly technical spaces
  description: >-
    Just how much does a designer need to know about a technical space or
    product to design for it?
  authors:
    - Camellia Yang
  heroImage: images/blog/hero-images/Understand-Highly-Technical-Spaces.jpg
  date: '2021-08-13'
  body: "\n\nAs a designer, you're lucky when you get to design an application you're familiar with, such as a supermarket app or a travel website – something we might have already used or even use every day. Most of the time, we are tasked with designing an application or technology that's unfamiliar or highly technical. Sometimes, we may not know what the application we're designing is used for, like creating an interface for an MRI machine controlled by a doctor, or a dashboard used by a professional musician – knowing what all the buttons do is already an achievement.\n\nOne of the trickiest questions for designers is understanding exactly how much you need to know how to use an application to design the best system for the user. This conundrum is typical for designers that work in a highly technical, enterprise space such as GitLab. The challenges can be exacerbated when working on Security products, but in my experience, we don't need to _fully understand_ the technology or space we are designing for, but we do need to have some idea of how it all works.\n\nThe most difficult part is deciding: How much knowledge is enough? How much do you need to know about a product to hold a conversation with users? Or be able to explain it to others in your own words?\n\nAll if those questions are reasonable criteria for designers to focus on, but I've found a more exciting strategy to motivate me to translate complex technical spaces into smart designs: Analogy.\n\n## Create analogies to aid the design process\n\nAs a designer, I like to focus on both my creative and analytical sides but thinking of scenarios that do not exist yet. For instance, I like to do some thought experiments where I'll position myself as different types of users while performing tasks, or pretend I'm a user and critique my own work.\n\nIt may be easier to show how I do this through some examples. Below, I'll give security technologies some new clothes through easy-to-remember stories that match up to what the security technology does.\n\nLet's start by looking at some of the standard security technology that we offer on GitLab:\n\n**Static application security testing (SAST)**: A testing methodology that analyses source code to find security vulnerabilities that make your organization's applications susceptible to attack.\n\n**Dynamic application security testing (DAST)**: A testing methodology that communicates with a web application through the web front-end to identify potential security vulnerabilities in the web application and architecture.\n\n**Fuzz testing**: An automated software testing technique that involves sending invalid, unexpected, or random data as inputs to a computer program in an attempt to get it to fail in some way.\n\nNow that we have an idea of the technologies in question, how might we understand them better through analogy?\n\nImagine a person is going to a hospital to check whether they're sick or not. Think of the SAST, DAST, and Fuzz testing technologies as different doctors with different specialties.\n\nSAST is a modern doctor who loves scanning. SAST can use an X-ray-like machine to see through the application's \"skin\". It can see if any bones are broken – and everything else that makes the application work. This is SAST's key advantage –\_it can see every detail of the scanned app and analyze it. It also has maps with predefined problems so SAST can compare and find the problems. In some ways, the SAST maps is what [Gray's Anatomy](https://www.amazon.com/Grays-Anatomy-Anatomical-Clinical-Practice/dp/0702052302), the seminal medical school textbook, is to a doctor's clinical practice.\n\nDAST, on the other hand, is more like your primary care physician. DAST doesn't need to know all of the details about how everything is doing inside your body (or the application). Instead, DAST talks with the app by asking questions and then observing and analyzing the responses. If the response is strange, wrong, or there is no response, DAST knows there are potential problems\n\nFuzz testing is the doctor that is a master of AI. Sometimes it also has a scanner like SAST, but it doesn't analyze in the same way. Fuzz testing has AI X-ray glasses that can mutate based on what it sees – potentially seeing even more. The analysis is the most personalized because of these mutated glasses: When the glasses see something suspicious at the shoulder area, it can change the lights and analyze it from the weirdest angle possible to match the individual shoulder. In other words, it adapts itself based on what was previously discovered and then digs deeper. Similarly, when Fuzz testing does not have a scanner, it has AI hearing, which allows it to change or mutate its questions based on the app's responses. It can possibly ask better questions as it scans to get more valuable answers to identify problems.\n\nI like to use people in my analogies because technology is so complicated but few things are as complex as humans. Creating stories about things that I can relate to in my daily life makes them more accessible.\n\nI hope you've found these examples to be a fun way to conceptualize challenging and highly technical topics. Next time you're designing in highly technical spaces, try building out relatable analogies and remove any fears of working in this space as a designer. One piece of advice: Always verify your analogies with professionals who have a deep understanding of the domain – no one will laugh at a passionate designer who tries to understand an unfamiliar world.\n"
  category: Engineering
  tags:
    - security
    - UX
config:
  slug: understand-highly-technical-spaces
  featured: false
  template: BlogPost
