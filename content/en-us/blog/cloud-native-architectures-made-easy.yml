seo:
  title: Simplifying and optimizing cloud native architectures
  description: >-
    Learn what cloud native architectures are, how to optimize them using
    GitLab's cohesive approach and what features you can use to help be more
    efficient.
  ogTitle: Simplifying and optimizing cloud native architectures
  ogDescription: >-
    Learn what cloud native architectures are, how to optimize them using
    GitLab's cohesive approach and what features you can use to help be more
    efficient.
  noIndex: false
  ogImage: images/blog/hero-images/cloudarchitecture.jpg
  ogUrl: https://about.gitlab.com/blog/cloud-native-architectures-made-easy
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/cloud-native-architectures-made-easy
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Simplifying and optimizing cloud native architectures",
            "author": [{"@type":"Person","name":"Suri Patel"}],
            "datePublished": "2019-11-13",
          }

content:
  title: Simplifying and optimizing cloud native architectures
  description: >-
    Learn what cloud native architectures are, how to optimize them using
    GitLab's cohesive approach and what features you can use to help be more
    efficient.
  authors:
    - Suri Patel
  heroImage: images/blog/hero-images/cloudarchitecture.jpg
  date: '2019-11-13'
  body: >

    Many teams embark on a journey to strengthen operations and development.
    Whether it’s battling monolithic applications by adopting containers and
    microservices or attempting to elevate a mature architecture by switching
    CI/CD tools, it is important to have a solution with robust cloud native
    support. When containers and cloud native workflows are easy to set and
    maintain, teams increase operational efficiency and can focus on delivering
    better products faster.


    ## What goes into a cloud native architecture?


    [Cloud native applications](/topics/cloud-native/) are built using
    [microservices](/topics/microservices/) rather than a monolithic application
    structure. You can think of microservices as smaller pieces that unite to
    perform a specific action. Microservices can be scaled based on load,
    creating a more resilient environment. Container orchestration tools, like
    [Kubernetes](/solutions/kubernetes/), enable developers to manage the way an
    application’s containers function, including scaling and deployment.


    Embracing cloud native architectures results in an increase in developer
    time, a decrease in the amount of money spent on monitoring and scaling
    application resources (through cloud orchestration and container
    schedulers), and faster shipping.


    ## GitLab is designed for cloud native architectures


    GitLab’s [Kubernetes](/solutions/kubernetes/) integration, [built-in
    container
    registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html),
    and advanced [CI/CD features](/features/continuous-integration/) support
    microservices, such as multi-project pipelines, and monorepo projects.
    Furthermore, teams can keep the same workflow regardless of which cloud apps
    they are deploying to, so there’s no need to rework your entire process.


    ## Why choose GitLab for your cloud native needs


    GitLab has a prominent place in the cloud native ecosystem and according to
    Forrester: [“GitLab’s simple and cohesive approach lands it squarely as a
    leader. GitLab's approach of having a single application to manage each
    phase of software development comes through in its developer
    experience”](/analysts/forrester-cloudci19/).


    GitLab doesn’t require manual and painstaking scripts. Our tool has native
    capabilities for Kubernetes integration and an out-of-the-box solution for
    advanced deployment flows for progressive delivery, like incremental rollout
    and canary deploys. GitLab also comes with [feature flagging as a built-in
    capability](/blog/2019/08/06/feature-flags-continuous-delivery/),
    eliminating the need for a third-party solution.


    GitLab’s [multicloud](/topics/multicloud/) strategy with workflow
    portability increases operational efficiencies and makes it the easiest way
    to build cloud native applications.


    Cover image by [Julian Santa Ana](https://unsplash.com/@jul_xander) on
    [Unsplash](https://unsplash.com/photos/FKqH1QhUqaw)

    {: .note}
  category: Company
  tags:
    - CI/CD
    - cloud native
config:
  slug: cloud-native-architectures-made-easy
  featured: false
  template: BlogPost
