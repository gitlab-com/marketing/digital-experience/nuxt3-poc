seo:
  title: Join the GitLab Community Day at SCaLE 18x!
  description: >-
    If you're attending SCaLE 18x, here's how you can find the GitLab community
    at the event.
  ogTitle: Join the GitLab Community Day at SCaLE 18x!
  ogDescription: >-
    If you're attending SCaLE 18x, here's how you can find the GitLab community
    at the event.
  noIndex: false
  ogImage: images/blog/hero-images/2018-09-13-gitlab-hackathon-cover.jpg
  ogUrl: https://about.gitlab.com/blog/gitlab-at-scale
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-at-scale
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Join the GitLab Community Day at SCaLE 18x!",
            "author": [{"@type":"Person","name":"Ray Paik"}],
            "datePublished": "2020-02-17",
          }

content:
  title: Join the GitLab Community Day at SCaLE 18x!
  description: >-
    If you're attending SCaLE 18x, here's how you can find the GitLab community
    at the event.
  authors:
    - Ray Paik
  heroImage: images/blog/hero-images/2018-09-13-gitlab-hackathon-cover.jpg
  date: '2020-02-17'
  body: >


    {::options parse_block_html="true" /}




    We just returned from [FOSDEM](https://fosdem.org/2020/), where we had a
    great time meeting with and talking to GitLab community members. We will be
    on the road again to Southern California in a few weeks at [SCaLE
    18x](https://www.socallinuxexpo.org/scale/18x), and hope to meet with wider
    GitLab community members at the event! If you are not familiar with SCaLE,
    it is the largest volunteer-organized open-source and free software event in
    North America. This year will be the 18th edition of the conference, and the
    SCaLE team provided us with a 40% discount code for GitLab community. When
    you [register](https://register.socallinuxexpo.org/reg6/), you can use the
    code `GIT` for the discount!


    ### GitLab Community Day


    This year, we are organizing a [GitLab Community
    Day](https://www.socallinuxexpo.org/scale/18x/gitlab-community-day), where
    we will have a discussion on resources for the wider community, a hands-on
    workshop on how you can contribute to GitLab, and a tutorial on how to use
    GitLab.


    Several team members from the [GitLab Developer Relations
    team](https://about.gitlab.com/company/team/?department=community-relations)
    will be at SCaLE 18x this time around. If you're interested in learning more
    about GitLab's programs for supporting code contributors, educational
    institutions, evangelists, open source communities, and more, we'd love to
    meet you in person.


    - **WHEN**: Friday, March 6th, from 1:30 p.m to 5:30 p.m. 

    - **WHERE**: **Ballroom F** at the Pasadena Convention Center


    You can find more information on topics and speakers on the [Gitlab
    Community Day
    issue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/900).
    You are welcome to add feedback or suggestions directly on the issue.  


    ### DevOpsDay LA 


    GitLab will again be a sponsor for [DevOpsDay
    LA](https://www.socallinuxexpo.org/scale/18x/devopsday-la) at SCaLE, and we
    will have a **GitLab booth** in the DevOpsDay LA exhibit area on March 6th.
    If you want to come talk to GitLab team members about anything DevOps,
    please swing by our booth and checkout some GitLab swag. Speaking of swag,
    there will also be a raffle at 3:35pm where the winner will get a Nintendo
    Switch! So, please be sure to enter the raffle while you visit the GitLab
    booth. 


    - **WHEN**: Friday, March 6th, all day 

    - **WHERE**: **Ballroom DE** at the Pasadena Convention Center



    ### Talks given by GitLab team members at SCaLE 18x


    A couple of GitLab team members will be speaking at the conference:


    - On Friday March 6th, [Francis Potter](https://gitlab.com/francispotter)
    will discuss [The Future of DevOps and the Importance of Right-to-Left
    Thinking](https://www.socallinuxexpo.org/scale/18x/presentations/future-devops-and-importance-right-left-thinking)
    in the DevOps track.

    - On Saturday March 7th, I ([Ray Paik](https://gitlab.com/rpaik)) will be
    talking about [Building a thriving community in (for-profit) open source
    projects](https://www.socallinuxexpo.org/scale/18x/presentations/building-thriving-community-profit-open-source-projects)
    in the Mentoring track. 




    Please come say hi 👋 at SCaLE, and we look forward to seeing many of you in
    sunny Southern California!
  category: Unfiltered
  tags:
    - community
    - collaboration
    - open source
config:
  slug: gitlab-at-scale
  featured: false
  template: BlogPost
