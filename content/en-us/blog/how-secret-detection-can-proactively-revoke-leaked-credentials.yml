seo:
  title: How Secret Detection can proactively revoke leaked credentials
  description: GitLab extends Secret Detection capabilities to customers on Google Cloud.
  ogTitle: How Secret Detection can proactively revoke leaked credentials
  ogDescription: GitLab extends Secret Detection capabilities to customers on Google Cloud.
  noIndex: false
  ogImage: images/blog/hero-images/security-checklist.png
  ogUrl: >-
    https://about.gitlab.com/blog/how-secret-detection-can-proactively-revoke-leaked-credentials
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/how-secret-detection-can-proactively-revoke-leaked-credentials
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How Secret Detection can proactively revoke leaked credentials",
            "author": [{"@type":"Person","name":"Connor Gilbert"}],
            "datePublished": "2023-06-13",
          }

content:
  title: How Secret Detection can proactively revoke leaked credentials
  description: GitLab extends Secret Detection capabilities to customers on Google Cloud.
  authors:
    - Connor Gilbert
  heroImage: images/blog/hero-images/security-checklist.png
  date: '2023-06-13'
  body: "\n\nModern applications don’t run on their own: They rely on databases, cloud services, APIs, and other services. To connect to those systems, the applications use credentials like private keys and API tokens. These credentials have to be kept secret – if they’re leaked, adversaries can abuse them to steal data, mine cryptocurrency, or disable important systems. Today, we’re increasing the level of protection we offer GitLab Ultimate users against this serious risk via an expansion of our partnership with Google Cloud.\n\n## How GitLab addresses this risk\n[GitLab Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) addresses the risk of leaked secrets by detecting when keys, tokens, and other sensitive values are exposed in code and helping DevSecOps teams respond. It’s imperative to respond quickly when credentials are leaked, especially for keys to cloud provider accounts, since adversaries can do a lot of damage quickly.\_\n\nWith our expanded partnership, we’ve integrated GitLab Secret Detection with Google Cloud to better protect customers who use GitLab to develop applications on Google Cloud. Now, if an organization leaks a Google Cloud credential to a public project on GitLab.com, GitLab can automatically protect the organization by working with Google Cloud to protect the account. This protection is available in GitLab Ultimate.\n\n## GitLab’s investment in automated response\nGitLab has added support for multiple cloud platforms with [automatic response to leaked secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response.html), including the [automatic revocation of GitLab Personal Access Tokens (PATs)](https://about.gitlab.com/blog/2023/01/04/pat-revocation-coming-soon/). We’re working on more integrations now, and are always looking for more cloud service vendors seeking similar protection to join [our partner program](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response.html#partner-program-for-leaked-credential-notifications).\n\nWe’ve also [recently expanded](https://about.gitlab.com/releases/2023/04/22/gitlab-15-11-released/#automatic-response-to-leaked-secrets-on-any-public-branch) the places automatic responses are triggered. Secret Detection users are now protected from credential leaks as soon as they appear in any public branch on GitLab.com.\n\n## Why we’re investing here\nSecurity is better when it’s integrated throughout the software development lifecycle. GitLab’s [2023 Security Without Sacrifices report](https://about.gitlab.com/press/releases/2023-04-20-gitlab-seventh-devsecops-report-security-without-sacrifices.html) found that security is one of the top benefits of a DevSecOps platform. GitLab’s DevSecOps platform enhances secure software development by helping developers and security professionals collaborate to prevent business-critical vulnerabilities. Now, in collaboration with Google Cloud, we’re adding an additional layer of protection for our mutual customers.\n\n## Better protection for GitLab/Google Cloud customers\nGoogle Cloud users on GitLab.com are now better protected. The new integration protects projects that:\n\n* are public. Private projects are unaffected by this change.\n* are hosted on GitLab.com. Projects on GitLab Dedicated or self-managed instances are unaffected.\n* use Secret Detection. If you haven't enabled Secret Detection for a project, we currently won't search it for secrets to revoke.\n\nSecret Detection searches for three types of secrets issued by Google Cloud:\n\n1. [Service account keys](https://cloud.google.com/iam/docs/best-practices-for-managing-service-account-keys)\n2. [API keys](https://cloud.google.com/docs/authentication/api-keys)\n3. [OAuth client secrets](https://support.google.com/cloud/answer/6158849#rotate-client-secret)\n\nPublicly leaked secrets are sent to Google Cloud after they’re discovered. Google Cloud verifies the leaks, then works to protect customer accounts against abuse.\n\n## How the Google Cloud integration works\nOur Google Cloud integration is on by default for projects that use GitLab Secret Detection on GitLab.com. Secret Detection scanning is available in all GitLab tiers, but an automatic response to leaked secrets is currently [only available in Ultimate projects](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response.html#feature-availability).\n\n* To protect a project, [enable GitLab Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/#enable-secret-detection).\n* To protect your entire organization, consider [enforcing scan execution](https://docs.gitlab.com/ee/user/application_security/index.html#enforce-scan-execution) to run Secret Detection in all of your projects.\n\n## What’s next\n\nWe’re excited to improve Secret Detection with this integration, but we aren’t stopping here. Check our [strategy and plans](https://about.gitlab.com/direction/secure/static-analysis/secret-detection/#strategy-and-themes) to learn more about where we’re headed.\n\n_GitLab can help secure your applications, whether they run on Google Cloud or elsewhere. Learn more about our [security and governance solutions](https://about.gitlab.com/solutions/dev-sec-ops/)._\n"
  category: Security
  tags:
    - security
    - cloud native
    - partners
config:
  slug: how-secret-detection-can-proactively-revoke-leaked-credentials
  featured: false
  template: BlogPost
