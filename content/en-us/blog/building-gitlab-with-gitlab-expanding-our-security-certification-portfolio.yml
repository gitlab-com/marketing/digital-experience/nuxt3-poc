seo:
  title: 'Building GitLab with GitLab: Expanding our security certification portfolio'
  description: >+
    Learn how the Security Compliance team uses the Agile planning and security
    features in the GitLab DevSecOps Platform to manage the certification
    process.

  ogTitle: 'Building GitLab with GitLab: Expanding our security certification portfolio'
  ogDescription: >+
    Learn how the Security Compliance team uses the Agile planning and security
    features in the GitLab DevSecOps Platform to manage the certification
    process.

  noIndex: false
  ogImage: images/blog/hero-images/building-gitlab-with-gitlab-no-type.png
  ogUrl: >-
    https://about.gitlab.com/blog/building-gitlab-with-gitlab-expanding-our-security-certification-portfolio
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/building-gitlab-with-gitlab-expanding-our-security-certification-portfolio
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Building GitLab with GitLab: Expanding our security certification portfolio",
            "author": [{"@type":"Person","name":"Madeline Lake"}],
            "datePublished": "2024-04-04",
          }

content:
  title: 'Building GitLab with GitLab: Expanding our security certification portfolio'
  description: >+
    Learn how the Security Compliance team uses the Agile planning and security
    features in the GitLab DevSecOps Platform to manage the certification
    process.

  authors:
    - Madeline Lake
  heroImage: images/blog/hero-images/building-gitlab-with-gitlab-no-type.png
  date: '2024-04-04'
  body: >-
    We recently expanded [our compliance certification
    portfolio](https://about.gitlab.com/security/) to include the automotive
    industry's
    [TISAX](https://about.gitlab.com/blog/2024/01/30/gitlab-drives-automotive-industry-information-security-with-tisax/)
    and to support the issuance of the first [GitLab
    Dedicated](https://about.gitlab.com/dedicated/) [SOC 2 Type
    2](https://www.aicpa-cima.com/topic/audit-assurance/audit-and-assurance-greater-than-soc-2).
    GitLab's Security Compliance team is a proponent of
    [dogfooding](https://handbook.gitlab.com/handbook/values/#dogfooding) our
    platform, including our integrated project management and security features,
    so we accomplished this expansion using the GitLab DevSecOps Platform.


    In this blog, we'll share the details of how we successfully leveraged
    GitLab's native features to implement security controls, enabling us to
    scale our compliance efforts and deliver results faster. You'll also learn
    how you can put these features to work in your own organization.


    > Start using GitLab for compliance today with [a free
    trial](https://gitlab.com/-/trials/).


    ## Agile planning


    Our security certifications structure is built upon GitLab's [Agile
    planning](https://about.gitlab.com/solutions/agile-delivery/) features,
    allowing us to deliver results faster by managing requirements centrally and
    streamlining our workflows. Using Agile planning features also enables
    end-to-end visibility throughout compliance audits.


    1. **[Epics](https://docs.gitlab.com/ee/user/group/epics/),
    [issues](https://docs.gitlab.com/ee/user/project/issues/), and
    [labels](https://docs.gitlab.com/ee/user/project/labels.html).** We leverage
    a parent epic to outline all the external certifications that are ongoing,
    and child epics for each individual certification. Each child epic contains
    issues for each work stream related to the certification, as well as
    evidence requests from the external auditor. Parent and child epics allow
    for project management and visibility across the organization on the audit
    cycle's current status.


    2. **Recurring issues.** Every audit has standard request items and tasks
    that need to be performed. Therefore, to increase efficiency, we have a
    variety of recurring issues that are automatically created for each audit
    cycle that populate the task and/or request details, assignee, and due date.
    Recurring issues can be configured in a [CI
    pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html).


    3. **[Labels](https://docs.gitlab.com/ee/user/project/labels.html) and
    [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html).**
    Labels and issue boards are used to plan, organize, and visualize workflows.
    The Security Compliance team leverages issue boards to not only track
    status, but also to track and group identified deficiencies relating to our
    compliance requirements. Issue boards allow for visibility of all issues
    related to a given program by their risk classification and current
    remediation status.


    These Agile planning features ensure that compliance teams are able to
    leverage the same platform as their engineers, promoting transparency and
    efficient delivery of results.


    ## Security


    Each of GitLab’s security certifications has security and compliance
    requirements that must be operating effectively to achieve certification.


    GitLab offers native features within the platform that enable security and
    the achievement of industry-standard requirements.


    We leveraged these key security features for our certifications and you can,
    too:


    1. **[Merge request approval
    settings](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/settings.html).**
    These settings can be configured to limit merge request approvals, enforce
    segregation of duties responsibilities, and force password authentication.
    For our certifications, merge request approval settings were inspected for
    relevant projects to support the following requirements: 
        - [AICPA Trust Service Critieria (TSC)](https://www.aicpa-cima.com/resources/download/2017-trust-services-criteria-with-revised-points-of-focus-2022) CC8.1

        - [ISO 27001:2022](https://www.iso.org/standard/27001) 5.3, 8.32

        - [TISAX](https://portal.enx.com/en-us/tisax/) 5.2.1

    2. **[Protected branch
    settings](https://docs.gitlab.com/ee/user/project/protected_branches.html#protected-branches).**
    These configuration settings allow administrators to set branch protections
    and limit what users can do based on their configured permissions. For our
    certifications, protected branches were inspected for relevant projects to
    support the following requirements: 
        - AICPA TSC CC8.1
        - ISO 27001:2022 8.32
        - TISAX 5.2.1, 5.2.2

    3. **[Code owners](https://docs.gitlab.com/ee/user/project/codeowners/).**
    This feature specifies the users or groups responsible for specific files
    and directories in a repository. The CODEOWNERS file can be enabled to
    identify owners of a file or directory and require owners to approve
    changes. Code owners can be implemented in conjunction with your approval
    rules. For our certifications, CODEOWNERS files were inspected for relevant
    projects to support the following requirements:
        - AICPA TSC CC8.1
        - ISO 27001:2022 8.32
        - TISAX 5.2.1

    4. **Static application security testing
    ([SAST](https://docs.gitlab.com/ee/user/application_security/sast/))/dynamic
    application security testing
    ([DAST](https://docs.gitlab.com/ee/user/application_security/dast/)).** A
    part of using GitLab CI/CD, SAST and DAST are available to check your source
    code for known vulnerabilities. For our certifications, We leveraged
    SAST/DAST to support the following requirements:
        - AICPA TSC CC3.2, CC7.1, CC9.2
        - ISO 27001:2022 8.28, 8.29
        - TISAX 5.2.5

    5. [Audit
    events](https://docs.gitlab.com/ee/administration/audit_events.html). This
    feature is used to track important events, including who performed what
    action and when. Audit events can be used to support the following
    requirements:
        - AICPA TSC CC8.1
        - ISO 27001:2022 8.15, 8.16
        - TISAX 5.2.4

    ## Get started today

    GitLab makes compliance easier than ever. Agile planning enables end-to-end
    visibility throughout the audit. and security is integrated into the design
    of the product, leading to faster, more comprehensive achievement of
    compliance requirements.


    Here at GitLab we are always pursuing the expansion of our security
    certification portfolio to give our customers and community additional
    assurance as well as additional transparency into our information security
    practices.


    > Have a certification you’d like to see us work towards? Have questions
    about how your organization can set up your GitLab instance to utilize our
    compliance features? Drop us a line by emailing
    customer-assurance@gitlab.com, we’d love to hear from you!


    ## More Building GitLab with GitLab


    * [Building GitLab with GitLab: How GitLab.com inspired
    Dedicated](https://about.gitlab.com/blog/2023/08/03/building-gitlab-with-gitlabcom-how-gitlab-inspired-dedicated/)

    * [Building GitLab with GitLab: Web API Fuzz
    Testing](https://about.gitlab.com/blog/2023/05/09/building-gitlab-with-gitlab-api-fuzzing-workflow/)

    * [Building GitLab with GitLab: Why there is no MLOps without
    DevSecOps](https://about.gitlab.com/blog/2023/10/05/there-is-no-mlops-without-devsecops/)

    * [Building GitLab with GitLab: Stress-testing Product
    Analytics](https://about.gitlab.com/blog/2023/12/14/building-gitlab-with-gitlab-stress-testing-product-analytics/)
  category: Agile Planning
  tags:
    - agile
    - security
config:
  slug: building-gitlab-with-gitlab-expanding-our-security-certification-portfolio
  featured: false
  template: BlogPost
