seo:
  title: The top software developer challenges in 2022
  description: >-
    From AI to hiring, security breaches and Covid, our 2022 Global DevSecOps
    Survey uncovered the top software developer challenges.
  ogTitle: The top software developer challenges in 2022
  ogDescription: >-
    From AI to hiring, security breaches and Covid, our 2022 Global DevSecOps
    Survey uncovered the top software developer challenges.
  noIndex: false
  ogImage: images/blog/hero-images/global-developer-survey.png
  ogUrl: https://about.gitlab.com/blog/the-top-software-developer-challenges-in-2022
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/the-top-software-developer-challenges-in-2022
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "The top software developer challenges in 2022",
            "author": [{"@type":"Person","name":"Valerie Silverthorne"}],
            "datePublished": "2022-10-05",
          }

content:
  title: The top software developer challenges in 2022
  description: >-
    From AI to hiring, security breaches and Covid, our 2022 Global DevSecOps
    Survey uncovered the top software developer challenges.
  authors:
    - Valerie Silverthorne
  heroImage: images/blog/hero-images/global-developer-survey.png
  date: '2022-10-05'
  body: "\nIn our 2022 Global DevSecOps Survey we asked developers about the most difficult parts of their jobs, a question that’s been answered in previous years with comments about tricky toolchain integrations, complex programming languages and business-side folks who \"just don’t get it.”\n\nBut apparently this year *we* didn’t get it: [More than 5,000 respondents](/developer-survey/) told us they were worried about the inability to hire and retain talent, the economy and the post-Covid world they’re expected to work in. They are also concerned about adoption of complex technologies such as artificial intelligence, 5G and edge computing, and the fear of/responsibility for security breaches and what that would mean to their organizations.\n\n(That sound you hear in the background is the shattering of the “devs are oblivious to business” stereotype.)\n\nObviously a tectonic shift in the developer role is underway.\n\n“Two massive waves are crashing against each other right now,” explains [Brendan O’Leary](/company/team/#brendan), staff developer evangelist at GitLab. “One wave is developers as kingmakers. We were ‘brought into the palace’ because every company needed to have software as its core competency and the pendulum swung toward developers. But the other wave is the massive correction in the market. These two things happening at the same time are putting a huge squeeze on businesses and developers.”\n\nA [longstanding shortage of software developers](https://www.forbes.com/sites/forbestechcouncil/2021/06/08/is-there-a-developer-shortage-yes-but-the-problem-is-more-complicated-than-it-looks/?sh=215d08f33b8e) has been made worse by macroeconomic conditions, but demand for software isn’t decreasing despite the market upheaval, O'Leary adds. The result is devs at the center of nearly all the most difficult challenges today, from [hiring](/blog/2021/11/09/6-tips-to-make-software-developer-hiring-easier/) to [security breaches](/blog/2022/08/23/gitlabs-2022-global-devsecops-survey-security-is-the-top-concern-investment/) and new technologies. \n\nTo put it another way: “We can’t be flippant about any part of the job anymore,” he adds.\n\nHere’s a look at what is keeping developers up at night.\n\n### Security\n\nMore than 1,000 respondents said all of the issues around security make their jobs infinitely more difficult and complicated.\n\n- “(The hardest thing is to) keep it secure and keep it updated.”\n- “My challenge is keeping up with the latest tools and security for optimal performance and privacy.”\t\t\t\n- “I am trying to build applications that are secure and stable.”\n- “Cybersecurity attacks are the biggest challenge facing us today.”\n- “The hardest part of my job? Data security, data security, I repeat, data security.”\n\n### “The Covid effect”\n\nHundreds of survey takers pointed to the changes brought about by Covid, including remote/hybrid work, economic forces, \"The Great Resignation,” and a number of other things. One respondent called it “the Covid effect” and many stressed that this new way of working has made their fast-paced jobs harder.\n\n### Staffing\n\nHard to hire, hard to keep, hard to even find...that’s what survey takers said about the issue of staffing.\n\n- “The biggest challenge is finding sufficient coding staff.”\n- “The biggest challenge is to find people to fill the jobs.”\n- “We have experienced significant difficulty in finding and retaining qualified staff.”\n\n### New technologies\n\nWith all the other pressures on developers, even exciting new technologies can seem daunting. One respondent put it this way:\n\n_“4G, 5G, AI, Metaverse, virtual space - developers have to support all of this.”_\n\nMany, many others simply said: “Technology is rapidly changing.”\n\n## Bold new challenges\n\nThis is all a long way of saying there has perhaps never been more on developers’ plates. Two developer respondents summed it up well:\n\n_“We have a development capacity challenge, a recruiting challenge and a knowledge-sharing challenge.”_\n\n_“For me, these are the eight biggest challenges we are facing as software developers: 1) Keeping pace with innovation. 2) Cultural change. 3) Customer experience. 4) Data privacy. 5) Cybersecurity. 6) AI and automation. 7) Data literacy. 8) Cross-platform functionality.”_\n\nWhat do you see as the biggest challenges facing developers? Let us know in the comments field below.\n"
  category: DevSecOps
  tags:
    - developer survey
    - DevOps
    - community
config:
  slug: the-top-software-developer-challenges-in-2022
  featured: false
  template: BlogPost
