seo:
  title: 5 ways GitLab Duo Chat AI can supercharge product management
  description: >-
    Discover how to transform all aspects of product management, boosting
    efficiency and improving decision-making. Learn practical tips for
    leveraging AI throughout your PM workflow.
  ogTitle: 5 ways GitLab Duo Chat AI can supercharge product management
  ogDescription: >-
    Discover how to transform all aspects of product management, boosting
    efficiency and improving decision-making. Learn practical tips for
    leveraging AI throughout your PM workflow.
  noIndex: false
  ogImage: images/blog/hero-images/GitLab_Duo_Blog_Hero_1800x945_r2_B-(1).png
  ogUrl: >-
    https://about.gitlab.com/blog/5-ways-gitlab-duo-chat-ai-can-supercharge-product-management
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/5-ways-gitlab-duo-chat-ai-can-supercharge-product-management
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "5 ways GitLab Duo Chat AI can supercharge product management",
            "author": [{"@type":"Person","name":"Tim Rizzi"}],
            "datePublished": "2024-09-25",
          }

content:
  title: 5 ways GitLab Duo Chat AI can supercharge product management
  description: >-
    Discover how to transform all aspects of product management, boosting
    efficiency and improving decision-making. Learn practical tips for
    leveraging AI throughout your PM workflow.
  authors:
    - Tim Rizzi
  heroImage: images/blog/hero-images/GitLab_Duo_Blog_Hero_1800x945_r2_B-(1).png
  date: '2024-09-25'
  body: >
    As a product manager at GitLab, I constantly seek ways to enhance my
    productivity and decision-making. Recently, I discovered an unexpected ally
    in [GitLab Duo Chat](https://about.gitlab.com/gitlab-duo/). Let me share how
    this AI-powered assistant has transformed my approach to product management.


    ## The daily PM challenges


    Like many PMs, I juggle many daily tasks — from summarizing issues and merge
    requests to crafting detailed product specs and investment cases. The sheer
    volume of writing and analysis required was overwhelming, and I worried
    about potential cognitive biases influencing my work.


    To address these challenges, I needed to:


    1. Increase my efficiency in handling documentation tasks.

    2. Enhance the quality and objectivity of my product decisions.

    3. Improve my communication with various stakeholders.


    ## Leveraging GitLab Duo Chat


    I decided to experiment with GitLab Duo Chat as a support tool for my daily
    PM tasks. Here's how I incorporated it into my workflow, with real examples:


    ### 1. Issue creation and refinement


    I was tasked with creating an issue for a new feature that would enhance the
    Conan repository by adding [revision
    support](https://gitlab.com/gitlab-org/gitlab/-/issues/479437). To start, I
    prompted Chat: "Can you create an issue to add support for downloading Conan
    revisions to the GitLab package registry? Think about the product's value
    from a C++ developer and a platform engineer perspective."


    GitLab Duo provided a comprehensive draft issue, including:


    * a clear description of the feature

    * value propositions for both C++ developers and platform engineers

    * implementation details

    * acceptance criteria

    * related links and labels


    This gave me a solid starting point with all the necessary sections, which I
    refined and customized. Instead of spending an hour writing that issue, I
    spent more time thinking about how this feature fits within the broader
    GitLab strategy.


    ### 2. Summarizing and reviewing


    I often ask GitLab Duo to summarize lengthy merge requests or complicated
    epics. For instance, when reviewing the epic for [protected container
    images](https://gitlab.com/groups/gitlab-org/-/epics/9825), I asked GitLab
    Duo to summarize the key changes and their value for GitLab customers and
    users from a PM perspective.


    GitLab Duo provided a detailed summary, highlighting the following:


    * enhanced security and compliance features

    * improved governance and control

    * streamlined operations

    * increased confidence in CI/CD pipelines

    * better alignment with DevOps practices

    * customizable security posture

    * improved audibility

    * cost-effective security solution


    This summary helped me quickly grasp and share the key points with my team
    more effectively.


    ### 3. Project status updates


    To get a quick overview of a project's status, I asked GitLab Duo to provide
    an update "like a hyper-focused project manager." The response included:


    * overall progress

    * completed items

    * in-progress tasks

    * next steps

    * timeline

    * risks and issues

    * stakeholder input

    * action items

    * key performance indicators

    * communication plans


    This structured overview allowed me to quickly assess the project's status
    and identify areas needing attention.


    ### 4. Value proposition and metric analysis


    When I needed to articulate the value of measuring monthly active users and
    storage costs for the Package stage, I asked GitLab Duo for help. The
    response provided a comprehensive explanation, covering:


    * user engagement and adoption insights

    * feature prioritization

    * capacity planning

    * business model optimization

    * customer success indicators

    * cost management

    * competitive positioning

    * product health assessment

    * ROI calculation

    * future planning considerations


    This well-structured response gave me more than enough content and helped me
    better articulate the value of these essential metrics.


    ### 5. Challenging cognitive biases


    To reveal blind spots in my thinking, I often ask GitLab Duo to answer in
    specific personas, such as:


    * a hyper-focused project manager

    * a frustrated customer

    * a developer who doesn't have time to read issues

    * a product leader who demands excellence


    For example, when I created an investment case for GitLab Package, I asked
    GitLab Duo to review it as a hypercritical CEO. This perspective helped me
    consider including financial projections and competitive analysis in my
    proposal, which I had initially overlooked.


    ## A more efficient and effective PM


    The impact of integrating GitLab Duo Chat into my workflow has been
    significant:


    1. **Increased productivity:** Tasks that used to take hours now often take
    minutes. Creating the initial draft of the Conan issue took about five
    minutes with GitLab Duo, compared to the usual 30-45 minutes I'd spend
    starting from scratch.

    2. **Enhanced quality:** The initial drafts produced with GitLab Duo's help
    are more comprehensive and structured. For the protected container images
    project, GitLab Duo's input helped me more effectively summarize the value
    of my go-to-market strategy and the project's current status.

    3. **Improved decision-making:** I've created more robust, well-rounded
    proposals using GitLab Duo to challenge my assumptions. The critique of my
    investment case led to a more thorough cost-benefit analysis.

    4. **Continuous improvement:** The feedback loop of writing, getting GitLab
    Duo's input, and refining has helped me improve my writing and analytical
    skills. My first drafts are becoming stronger, even without GitLab Duo's
    assistance.


    ## A new era of AI-assisted product management


    While GitLab Duo Chat hasn't replaced my role as a PM, it has become an
    invaluable tool in my arsenal. It's helped me be more efficient, thorough,
    and objective. As AI assistants like GitLab Duo continue to evolve, I'm
    excited about the potential for further enhancing our product management
    practices.


    However, it's crucial to remember that GitLab Duo is a tool, not a
    replacement for human insight and creativity. The best results come from
    combining GitLab Duo's capabilities with our expertise and understanding of
    our unique business context.


    ## Try GitLab Duo


    I encourage fellow PMs to explore how AI assistants like GitLab Duo Chat can
    augment their work. Here are some steps you can take:


    1. **Start small:** Use GitLab Duo for simple tasks like summarizing issues
    or drafting initial proposals.

    2. **Experiment with personas:** Ask GitLab Duo to review your work from
    different perspectives to uncover blind spots.

    3. **Refine your prompts:** Learn how to craft effective prompts to get the
    most valuable responses from GitLab Duo.

    4. **Share your experiences:** Discuss your use of AI tools with your team
    and contribute to best practices.


    With the right approach, these tools can help us focus more on strategic
    thinking and less on routine tasks, ultimately leading to better products
    and happier customers.


    > [Try GitLab Duo free for 60 days
    today!](https://about.gitlab.com/solutions/gitlab-duo-pro/sales/?type=free-trial&toggle=gitlab-duo-pro)
  category: AI/ML
  tags:
    - AI/ML
    - product
    - workflow
config:
  slug: 5-ways-gitlab-duo-chat-ai-can-supercharge-product-management
  featured: true
  template: BlogPost
