seo:
  title: How to implement secret management best practices with GitLab
  description: >-
    Insecure secret management practices pose a risk for companies tasked with
    storage and protection of customer data. Learn how to reduce this risk and
    increase trust.
  ogTitle: How to implement secret management best practices with GitLab
  ogDescription: >-
    Insecure secret management practices pose a risk for companies tasked with
    storage and protection of customer data. Learn how to reduce this risk and
    increase trust.
  noIndex: false
  ogImage: images/blog/hero-images/securitycheck.png
  ogUrl: >-
    https://about.gitlab.com/blog/how-to-implement-secret-management-best-practices-with-gitlab
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/how-to-implement-secret-management-best-practices-with-gitlab
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How to implement secret management best practices with GitLab",
            "author": [{"@type":"Person","name":"Joseph Longo"}],
            "datePublished": "2024-03-12",
          }

content:
  title: How to implement secret management best practices with GitLab
  description: >-
    Insecure secret management practices pose a risk for companies tasked with
    storage and protection of customer data. Learn how to reduce this risk and
    increase trust.
  authors:
    - Joseph Longo
  heroImage: images/blog/hero-images/securitycheck.png
  date: '2024-03-12'
  body: >
    Insecure secret management practices (SMPs) pose a significant risk to any
    company, especially those tasked with the storage and protection of their
    customers' data. It's a common problem, and it is often at the core of a
    company's risk register. A leaked secret can result in a loss of
    confidentiality and potentially a data breach. These types of incidents can
    lead to significant financial losses and a loss of trust amongst an entity's
    customer base.


    For many companies, insecure SMPs may result from a lack of expertise or
    being unaware of the tools and strategies that exist to solve this issue.
    Appropriate SMPs can help reduce the potential for compromise and increase
    trust in an organization's secret management strategy. In this post, we will
    discuss secret management best practices, GitLab's ability to support those
    practices, and our strategy for improving the DevSecOps platform's native
    secret management capabilities.


    ## Develop a cryptographic strategy


    Every company needs a cryptographic strategy as a foundation to ensure
    developers are operating in a standardized way and all applications and
    their components are being developed in accordance with the company's
    cryptographic requirements.


    Understanding the data your company processes, and your company's risk
    tolerance and threat landscape will help you develop a strong cryptographic
    strategy.


    ### Secret generation


    Secrets, including access tokens and SSH keys, should be generated using
    cryptographic devices such as Hardware Security Modules (HSM). These devices
    help generate cryptographically strong secrets and store them in a tamper-
    and intrusion-resistant manner.


    While relying on physical devices can be costly and operationally
    prohibitive for companies, the leading cloud service providers offer cloud
    HSM services – for example, [AWS CloudHSM](https://aws.amazon.com/cloudhsm/)
    and [GCP Cloud HSM](https://cloud.google.com/kms/docs/hsm).


    ### Secret storage


    Storing secrets is just as critical as generating them. Generated secrets
    must be stored in a manner that supports secure long-term storage and
    enables users to securely retrieve and use them when necessary.


    The leading cloud service providers, as well as security companies such as
    [HashiCorp](https://www.vaultproject.io/), offer cloud-based services for
    securely storing and retrieving secrets. These services enable users to
    seamlessly leverage secrets within their processes and code, thereby
    eliminating the need to hardcode secrets.


    #### How does GitLab support secret storage?


    GitLab provides native support for the following [secret management
    providers](https://docs.gitlab.com/ee/ci/secrets/index.html):


    - Vault by HashiCorp

    - Google Cloud Secret Manager

    - Azure Key Vault


    By configuring GitLab to connect with a secret management provider, secrets
    are explicitly requested only when needed by a CI job. When secrets aren't
    needed by CI jobs, they remain securely stored in the secret management
    service's environment, thereby reducing the potential for compromise. In
    addition to native support for the above secret management providers, GitLab
    also offers [OIDC
    authentication](https://docs.gitlab.com/ee/ci/secrets/id_token_authentication.html)
    to authenticate against other providers (i.e. AWS Secret Manager). This is a
    much more secure and preferred method of storing secrets when compared to
    storing and
    [masking](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable)
    secrets as CI/CD variables.


    ### Secret use


    Secrets should be used for a single purpose. Reusing secrets across
    different applications or services increases the potential for exposure and
    increases the impact if the secrets are compromised.


    To reduce the potential for exposure or malicious activity, access to
    secrets should be controlled with the [principle of least
    privilege](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/)
    in mind. Access should only be granted to the individuals or services that
    require such access in order to support their work and operational
    activities.


    #### How does GitLab support secret use?


    GitLab provides administrators with a strong [role-based access control
    model](https://docs.gitlab.com/ee/user/permissions.html) and also offers the
    ability to create [custom
    roles](https://docs.gitlab.com/ee/user/custom_roles.html), thereby allowing
    administrators to align access profiles with their organizational standards
    and risk tolerance.


    GitLab also allows users to perform [secret
    detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
    to check for unintentionally committed secrets and credentials. GitLab
    Ultimate users can enforce [automatic responses to leaked
    secrets](https://docs.gitlab.com/ee/user/application_security/secret_detection/automatic_response/),
    such as revoking the secret, to mitigate the impact of leaked credentials.


    ### Auditability


    Access and use of secrets should be auditable and attributable. In an ideal
    scenario, individuals would not have access to view secrets in plaintext,
    but the state of a company's operations are not always ideal.


    Auditable and attributable secret management allows security teams to
    monitor for anomalous or malicious activity, and quickly respond to such
    activity through automated and manual intervention.


    #### How does GitLab support auditability?


    GitLab's [audit
    events](https://docs.gitlab.com/ee/administration/audit_events.html) capture
    activities related to tokens and keys that are created within GitLab. Some
    examples include:


    - personal access token events

    - deploy token events

    - cluster agent token events


    These activities are saved to the database and are also available for [audit
    event
    streaming](https://docs.gitlab.com/ee/administration/audit_event_streaming/)
    for GitLab Ultimate customers.


    ## Upcoming: GitLab Secret Manager


    GitLab plans to launch a native secret management experience in late 2024.
    GitLab Secret Manager is a multi-tenant, cloud-based solution and will be
    accessible to both GitLab.com and self-managed customers via our Cloud
    Connector service. This new service offers an easy-to-use interface,
    consistent with the current CI/CD variables interface, making adoption
    easier than a third-party product with a minimal learning curve. The GitLab
    Secret Manager will ensure the security and protection of sensitive
    information in your CI pipelines. 


    > For more information or questions about GitLab Secret Manager, please
    visit our [MVC epic](https://gitlab.com/groups/gitlab-org/-/epics/10723) and
    leave a comment. 


    _Disclaimer: This blog contains information related to upcoming products,
    features, and functionality. It is important to note that the information in
    this blog post is for informational purposes only. Please do not rely on
    this information for purchasing or planning purposes. As with all projects,
    the items mentioned in this blog and linked pages are subject to change or
    delay. The development, release, and timing of any products, features, or
    functionality remain at the sole discretion of GitLab._
  category: Security
  tags:
    - security
    - features
    - DevSecOps platform
config:
  slug: how-to-implement-secret-management-best-practices-with-gitlab
  featured: true
  template: BlogPost
