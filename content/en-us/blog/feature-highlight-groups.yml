seo:
  title: 'GitLab Feature Highlight: Groups'
  description: >-
    GitLab groups allow you to group projects into directories and give users
    access to several projects at once.
  ogTitle: 'GitLab Feature Highlight: Groups'
  ogDescription: >-
    GitLab groups allow you to group projects into directories and give users
    access to several projects at once.
  noIndex: false
  ogImage: images/blog/hero-images/logoforblogpost.jpg
  ogUrl: https://about.gitlab.com/blog/feature-highlight-groups
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/feature-highlight-groups
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Feature Highlight: Groups",
            "author": [{"@type":"Person","name":"Jacob Vosmaer"},{"@type":"Person","name":"Job van der Voort"}],
            "datePublished": "2014-06-30",
          }

content:
  title: 'GitLab Feature Highlight: Groups'
  description: >-
    GitLab groups allow you to group projects into directories and give users
    access to several projects at once.
  authors:
    - Jacob Vosmaer
    - Job van der Voort
  heroImage: images/blog/hero-images/logoforblogpost.jpg
  date: '2014-06-30'
  body: "\n\nGitLab is a massive open source project with over 600 contributors, all working together to create an amazing platform to collaborate on code. Every month on the 22nd, a new version of GitLab is released, and every month new features are added.\nTo make you aware of the power of GitLab, we walk through some of its features in these blog posts.\n\n![Barry effectively has 'Master' access to GitLab CI now](https://about.gitlab.com/images/feature_groups/override_access_level.png)\n\nWe start by looking at GitLab Groups. GitLab groups allow you to group projects into directories and give users access to several projects at once.\n\n\n<!--more-->\n\nWhen you create a new project in GitLab, the default namespace for the project is the personal namespace associated with your GitLab user.\nBelow we will see how to create groups, put projects in groups and manage who can access the projects in a group.\n\n## How to create GitLab groups\n\nYou can create a group by going to the 'Groups' tab of the GitLab dashboard and clicking the 'New group' button.\n\n![Click the 'New group' button in the 'Groups' tab](https://about.gitlab.com/images/feature_groups/new_group_button.png)\n\nNext, enter the name (required) and the optional description and group avatar.\n\n![Fill in the name for your new group](https://about.gitlab.com/images/feature_groups/new_group_form.png)\n\nWhen your group has been created you are presented with the group dashboard feed, which will be empty.\n\n![Group dashboard](https://about.gitlab.com/images/feature_groups/group_dashboard.png)\n\nYou can use the 'New project' button to add a project to the new group.\n\nNote that there's also the option to [create subgroups and top-level groups](https://docs.gitlab.com/ee/user/group/manage.html#transfer-a-group).\n\nAfter you create a group, here are a few other things you can do with it:\n\n- Adjust user visibility level\n- Share projects with groups\n- Control project access\n- Transfer projects\n- Create personal projects\n\n## How to transfer an existing project into a group\n\nYou can transfer an existing project into a group you own from the project settings page.\nFirst scroll down to the 'Dangerous settings' and click 'Show them to me'.\nNow you can pick any of the groups you manage as the new namespace for the group.\n\n![Transfer a project to a new namespace](https://about.gitlab.com/images/feature_groups/transfer_project.png)\n\nGitLab administrators can use the admin interface to move any project to any namespace if needed.\n\nUpdate: For more info, check out tutorials in the docs on [moving a personal project to a group](https://docs.gitlab.com/ee/tutorials/move_personal_project_to_a_group.html) and [converting a personal namespace into a group](https://docs.gitlab.com/ee/tutorials/convert_personal_namespace_into_group.html).\n{: .alert .alert-info .text-center}\n\n## How to add users to a group\n\nOne of the benefits of putting multiple projects in one group is that you can give a user access to all projects in the group at the same time with one action.\n\nSuppose we have a group with two projects.\n\n![Group with two projects](https://about.gitlab.com/images/feature_groups/group_with_two_projects.png)\n\nOn the 'Group Members' page we can now add a new user Barry to the group.\n\n![Add user Barry to the group](https://about.gitlab.com/images/feature_groups/add_member_to_group.png)\n\nNow because Barry is a 'Developer' member of the 'Open Source' group, he automatically gets 'Developer' access to all projects in the 'Open Source' group.\n\n![Barry has 'Developer' access to GitLab CI](https://about.gitlab.com/images/feature_groups/project_members_via_group.png)\n\nIf necessary, you can increase the permissions or access level of an individual user for a specific project, by adding them as a Member to the project.\n\n![Barry effectively has 'Master' access to GitLab CI now](https://about.gitlab.com/images/feature_groups/override_access_level.png)\n\nTo see groups where users have a [direct or indirect membership](https://docs.gitlab.com/ee/user/group/manage.html), select \"Your groups.\"\n\n## How to manage group memberships via LDAP\n\nIn GitLab Enterprise Edition it is possible to manage GitLab group memberships using LDAP groups.\nSee [the documentation](https://docs.gitlab.com/ee/user/group/access_and_permissions.html#manage-group-memberships-via-ldap) for more information.\n\n## How to allow only admins to create groups\n\nBy default, any GitLab user can create new groups.\nThis ability can be disabled for individual users from the admin panel.\nIt is also possible to configure GitLab so that new users default to not being able to create groups:\n\n```\n# For omnibus-gitlab, put the following in /etc/gitlab/gitlab.rb\ngitlab_rails['gitlab_default_can_create_group'] = false\n\n# For installations from source, uncomment the 'default_can_create_group'\n# line in /home/git/gitlab/config/gitlab.yml\n```\n\nUpdate: Check out [the documentation](https://docs.gitlab.com/ee/user/group/) for more info on GitLab's latest features for [managing groups](https://docs.gitlab.com/ee/user/group/manage.html), [access control for groups](https://docs.gitlab.com/ee/user/group/access_and_permissions.html), [migrating groups](https://docs.gitlab.com/ee/user/group/import/), [restricting access by domain or email](https://docs.gitlab.com/ee/user/group/access_and_permissions.html#restrict-group-access-by-domain), [group file templates](https://docs.gitlab.com/ee/user/group/manage.html#group-file-templates), and more.\_\n{: .alert .alert-info .text-center}\n"
  category: Insights
config:
  slug: feature-highlight-groups
  featured: false
  template: BlogPost
