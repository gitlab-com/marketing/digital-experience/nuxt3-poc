seo:
  title: GitLab Duo Code Suggestions is generally available
  description: >
    Learn how our AI-powered workflow helps developers write secure code
    efficiently.
  ogTitle: GitLab Duo Code Suggestions is generally available
  ogDescription: >
    Learn how our AI-powered workflow helps developers write secure code
    efficiently.
  noIndex: false
  ogImage: images/blog/hero-images/gitlabduo.png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-duo-code-suggestions-is-generally-available
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-duo-code-suggestions-is-generally-available
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Duo Code Suggestions is generally available",
            "author": [{"@type":"Person","name":"David DeSanto, Chief Product Officer, GitLab"}],
            "datePublished": "2023-12-22",
          }

content:
  title: GitLab Duo Code Suggestions is generally available
  description: >
    Learn how our AI-powered workflow helps developers write secure code
    efficiently.
  authors:
    - David DeSanto, Chief Product Officer, GitLab
  heroImage: images/blog/hero-images/gitlabduo.png
  date: '2023-12-22'
  body: >
    GitLab Duo Code Suggestions, part of the GitLab Duo suite of AI-powered
    workflows, is now generally available with GitLab 16.7. [Code
    Suggestions](https://about.gitlab.com/solutions/code-suggestions/), our
    generative AI code creation assistant within our DevSecOps platform, helps
    developers to write secure code more efficiently and assists in improving
    cycle times by taking care of repetitive, routine coding tasks.


    According to GitLab's [2023 State of AI in Software
    Development](https://about.gitlab.com/developer-survey/#ai) report, 83% of
    DevSecOps professionals said it is essential to implement AI in their
    software development processes to avoid falling behind, and a majority were
    interested in using AI for code generation and code suggestions.


    As DevSecOps teams incorporate AI in the software development lifecycle,
    tapping into easy-to-adopt features like Code Suggestions provides a good
    entry point to achieve improved efficiency, accuracy, and productivity while
    not compromising on security and governance.


    > [Try Code Suggestions for
    free](http://about.gitlab.com/solutions/code-suggestions/sales) through
    February 14.


    ## Faster development with less context switching


    A developer's workload is more than just writing code; it involves extensive
    context switching to search through documentation, hunt for code examples,
    and work through trial and error. All of this interrupts the software
    development process, decreasing time to value.


    Leveraging generative AI, Code Suggestions helps boost developers'
    efficiency and effectiveness by assisting in reducing the time required for
    coding fundamental functions. It also helps in understanding and extending
    existing, and sometimes unfamiliar, codebases while helping ensure adherence
    to security best practices. Code Suggestions includes the following
    capabilities:


    - **Code generation:** automatically generates lines of code, including full
    functions, from single and multi-line comments as well as comment blocks 

    - **Code completion:** automatically proposes new lines of code from a few
    typed characters


    Code Suggestions is available in 15 languages, including C++, C#, Go, Java,
    JavaScript, Python, PHP, Ruby, Rust, Scala, Kotlin, and TypeScript. GitLab
    editor extensions can be found in popular IDE marketplaces; VS Code, Visual
    Studio, JetBrains’ suite of IDEs, and Neovim are all supported. And, of
    course, Code Suggestions is available within GitLab’s Web IDE, giving
    developers a quick way to get up and running with GitLab Duo. Support for
    Code Suggestions is available for self-managed GitLab instances via a secure
    connection to GitLab cloud infrastructure.


    Watch this introduction to Code Suggestions:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/ds7SG1wgcVM?si=9J9gX0qs5De2NXUC" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Discover more AI capabilities with GitLab Duo


    Organizations can use AI to help streamline the entire DevSecOps lifecycle
    and ship better, more secure, software faster. GitLab Duo has 15 AI-assisted
    features that support everyone involved in software development. From
    planning and coding to testing to delivery, there's a [GitLab
    Duo](https://about.gitlab.com/gitlab-duo/) capability to help. 


    For example, [GitLab Duo Vulnerability
    Resolution](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/index.html#vulnerability-resolution)
    helps teams remediate vulnerabilities proactively with the assistance of
    generative AI. In addition, Discussion Summary assists in getting everyone
    up to speed and aligned on lengthy conversations within [GitLab Enterprise
    Agile
    Planning](https://about.gitlab.com/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/).


    Our approach to AI is resonating with our customers. For example, Amado
    Gramajo, Vice President of Infrastructure & DevOps at Nasdaq, recently
    shared his excitement about how GitLab Duo will help Nasdaq protect their
    intellectual property and stay in line with regulatory mandates.


    GitLab is the only platform that integrates AI throughout the entire
    software development lifecycle. As developers become more effective, GitLab
    helps security and operations team members to keep pace. GitLab has a
    privacy- and transparency-first approach to AI and [does not use customer
    code to train AI
    models](https://docs.gitlab.com/ee/user/project/repository/code_suggestions/index.html#code-suggestions-data-usage). 


    ## Get started with Code Suggestions today


    Code Suggestions, which can be trialed for free from December 21 through
    February 14 (subject to [GitLab’s Testing
    Agreement](https://handbook.gitlab.com/handbook/legal/testing-agreement/)),
    is available as an add-on to GitLab subscriptions for an introductory price
    of $9 USD per user/month. [Contact us
    today](http://about.gitlab.com/solutions/code-suggestions/sales) to get
    started with Code Suggestions.
  category: AI/ML
  tags:
    - AI/ML
    - news
    - DevSecOps
config:
  slug: gitlab-duo-code-suggestions-is-generally-available
  featured: true
  template: BlogPost
