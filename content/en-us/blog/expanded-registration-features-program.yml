seo:
  title: >-
    Free access to security, other features with expanded Registration Features
    Program
  description: >-
    More features are now available for free to free self-managed Enterprise
    Edition users when they register and turn on their Service Ping.
  ogTitle: >-
    Free access to security, other features with expanded Registration Features
    Program
  ogDescription: >-
    More features are now available for free to free self-managed Enterprise
    Edition users when they register and turn on their Service Ping.
  noIndex: false
  ogImage: images/blog/hero-images/tanukicover.jpg
  ogUrl: https://about.gitlab.com/blog/expanded-registration-features-program
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/expanded-registration-features-program
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Free access to security, other features with expanded Registration Features Program",
            "author": [{"@type":"Person","name":"Sarah Waldner"}],
            "datePublished": "2023-05-24",
          }

content:
  title: >-
    Free access to security, other features with expanded Registration Features
    Program
  description: >-
    More features are now available for free to free self-managed Enterprise
    Edition users when they register and turn on their Service Ping.
  authors:
    - Sarah Waldner
  heroImage: images/blog/hero-images/tanukicover.jpg
  date: '2023-05-24'
  body: "\nIn GitLab 14.1, we introduced [Registration Features](https://docs.gitlab.com/ee/administration/settings/usage_statistics.html#registration-features-program), which offers free self-managed users running [GitLab Enterprise Edition](https://about.gitlab.com/enterprise/) free use of paid features by registering with GitLab and sending us activity data via Service Ping. This month, we are expanding the program to include five more features:\n\n1. [Password complexity requirements](https://docs.gitlab.com/ee/administration/settings/sign_up_restrictions.html#password-complexity-requirements): By default, the only requirement for user passwords is minimum password length. To increase security of user accounts, you have the option to add additional complexity requirements. Under Minimum password length, select additional password complexity requirements. You can require numbers, uppercase letters, lowercase letters, and symbols.\n2. [Track description changes in issues](https://docs.gitlab.com/ee/user/discussions/index.html#view-description-change-history): When multiple people are collaborating on an issue, it is common to see the description change with no explanation. This feature makes it easy to review previous versions of the issue description or understand who made which specific changes. Issue description versions can be compared by looking at the changes to the description listed in the history. To compare the changes, select Compare with the previous version.\n3. [Configurable issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html#configurable-issue-boards): An issue board can be associated with a milestone, labels, assignee, weight, and current iteration, which automatically filter the board issues accordingly. This allows you to create unique boards according to your team’s needs.\n4. [Coverage-guided fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/#coverage-guided-fuzz-testing): Coverage-guided fuzz testing sends random inputs to an instrumented version of your application in an effort to cause unexpected behavior. Such behavior indicates a bug that you should address. GitLab allows you to add coverage-guided fuzz testing to your pipelines. This helps you discover bugs and potential security issues that other QA processes may miss.\n5. [Maintenance Mode](https://docs.gitlab.com/ee/administration/maintenance_mode/index.html): Maintenance Mode allows administrators to reduce write operations to a minimum while maintenance tasks are performed. The main goal is to block all external actions that change the internal state, including the PostgreSQL database, but especially files, Git repositories, and Container repositories. When Maintenance Mode is enabled, in-progress actions finish relatively quickly since no new actions are coming in, and internal state changes are minimal.\n\nThe above five features join the list of features already available to the registered tier:\n1. [Email from GitLab](https://docs.gitlab.com/ee/administration/email_from_gitlab.html#email-from-gitlab): Allow admins to send mass notification emails to all users, or subset of users based on project or group memberships.\n2. [Repository size limit](https://docs.gitlab.com/ee/administration/settings/account_and_limit_settings.html#repository-size-limit): Ensure that disk space usage is under control by setting a hard limit for your repositories’ size; limits can be set globally, per group or per project.\n3. [Restrict access by IP address](https://docs.gitlab.com/ee/user/group/access_and_permissions.html#restrict-group-access-by-ip-address): Restrict access at the group level to incoming traffic adhering to an IP address subnet; ensures only people from your organization can access particular resources.\n\n## How to to participate in the Registration Features Program\_\nIf you are interested in participating as a free self-managed user running GitLab Enterprise Edition, you can read about [how to turn on Service Ping here](https://docs.gitlab.com/ee/administration/settings/usage_statistics.html#enable-or-disable-usage-statistics).\n"
  category: News
  tags:
    - features
    - news
    - product
config:
  slug: expanded-registration-features-program
  featured: false
  template: BlogPost
