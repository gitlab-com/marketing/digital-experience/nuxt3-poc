seo:
  title: Where to donate your DevOps skills
  description: >-
    Want to feel great and help fill the DevOps talent pipeline? Here are some
    rewarding opportunities to donate your tech knowledge to others.
  ogTitle: Where to donate your DevOps skills
  ogDescription: >-
    Want to feel great and help fill the DevOps talent pipeline? Here are some
    rewarding opportunities to donate your tech knowledge to others.
  noIndex: false
  ogImage: images/blog/hero-images/clark-tibbs-oqstl2l5oxi-unsplash.jpg
  ogUrl: https://about.gitlab.com/blog/where-to-donate-your-devops-skills
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/where-to-donate-your-devops-skills
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Where to donate your DevOps skills",
            "author": [{"@type":"Person","name":"Sandra Gittlen"}],
            "datePublished": "2021-12-08",
          }

content:
  title: Where to donate your DevOps skills
  description: >-
    Want to feel great and help fill the DevOps talent pipeline? Here are some
    rewarding opportunities to donate your tech knowledge to others.
  authors:
    - Sandra Gittlen
  heroImage: images/blog/hero-images/clark-tibbs-oqstl2l5oxi-unsplash.jpg
  date: '2021-12-08'
  body: "\n\nYour technical knowledge could be a gift to someone else. Nonprofits around the world are seeking talented professionals to mentor, volunteer their technical skills, or teach courses in-person and online. The donations of your time and expertise could change the lives of people in your community or halfway around the world. The more access underrepresented groups, including women and minorities, have to skills development and mentoring that lead to higher-paying jobs, the better their chances of rising up out of difficult socio-economic conditions. And don’t forget, volunteering is incredibly rewarding.\n\nHere are six organizations and tech communities that could benefit from your skills and experience:\_\n\n## [Outreachy](https://www.outreachy.org/)\n\nOutreachy provides internships in open source to people subject to systemic bias and impacted by underrepresentation in the technical industry where they are living. Outreachy interns work with experienced mentors from open source communities. Internship projects may include programming, user experience, documentation, graphical design, data science, marketing, user advocacy, or community event planning.\n\nGitLab has participated in the Outreachy internship program, which intersects with our [Diversity, Inclusion, and Belonging](https://about.gitlab.com/handbook/values/#diversity-inclusion) value. Our team, including Senior Backend Engineer [Christian Couder](https://gitlab.com/chriscool), wrote [about their experience](https://about.gitlab.com/blog/2021/04/15/outreachy-sponsorship-winter-2020/): “One of the benefits of the Outreachy technology internship is that the interns do not need to be students. It's a great opportunity for people who are coming back into the workforce after a hiatus, or who are navigating a career change into tech. This technology internship program is unique because it incorporates skill sets beyond engineering – which creates a broader range of skill sets represented in the open source world. The Outreachy internship is remote, making it more relevant than ever during the pandemic by helping interns gain experience working on an all-remote team.”\n\n## [CodeYourFuture](https://codeyourfuture.io/)\n\nCodeYourFuture is a U.K.-based nonprofit that trains “some of the most deprived members of society” to become web developers and helps them to find work in the tech industry. Students are trained in full-stack web development by volunteers from the tech industry, putting a strong emphasis on collaboration and product development through tech projects.\n\nSenior Frontend Engineer [Coung Ngo](https://gitlab.com/cngo) contributes his time to the nonprofit and says that while DevOps is not in the syllabus, the underlying skill sets are. “They teach a full-stack course of HTML, CSS, JS, React, Node, and SQL/MongoDB,” Ngo says. “It's a nice community, so if someone lives in London, Glasgow, Manchester, or Birmingham, it's enjoyable to join in with the in-person classes.”\n\n## [WeThinkCode](https://www.wethinkcode.co.za/about)\n\nWeThinkCode is a nonprofit aimed at closing the skills gap in the digital sector in South Africa and preparing young people to participate in the region’s economy. The organization believes that South Africa’s youth represent a pool of talent that mostly remains untapped and wants to provide businesses with access to this source of tech talent. Nonprofits like WeThinkCode are important because of the way they mobilize previously underestimated groups by providing an avenue for education where traditional paths are often more closed off.\_\n\nWeThinkCode was a GitLab donation recipient and the organization utilizes GitLab’s free SaaS version in their curriculum. The organization has [four ways for professionals to volunteer](https://www.wethinkcode.co.za/volunteer), including virtual opportunities:\n\n- Mentorship - Experienced software development practitioners provide guidance to a group of four to six students on communication, ways of working, insights on tackling programming challenges in the curriculum, and tips on the conduct expected in the workplace.\_\n\_\n- Interview readiness - Practitioners with experience in hiring and recruiting will help conduct mock interviews and then provide constructive feedback.\n\n- WomenThinkCode Meetups - Women in tech to act as role models and deliver talks about their career journeys covering tech and interpersonal aspects.\n\n- Community-hosted talks - Practitioners deliver talks on the real-world application of various technologies.\n\n## [KodewithKlossy](https://www.kodewithklossy.com/)\n\nKodewithKlossy is a nonprofit with the mission to create learning experiences and opportunities for young women and nonbinary individuals that increase their confidence and inspire them to pursue their passions in a technology-driven world. KodewithKlossy found that prior to camp, only two out of 10 attendees (also called scholars) had computer science experience and after, as a result of their camp experience, nine in 10 say they plan to pursue education and opportunities in computer science. Volunteers can serve as role models in the camp speaker series or participate in other important ways.\n\n## [Google Summer of Code](https://summerofcode.withgoogle.com/archive/)\n\nGoogle Summer of Code (GSoC) is a global program focused on bringing more student developers into open source software development. Students work on a three-month programming project with an open source organization during their break from university.\_\n\nIn 2022, Google will [expand its GSOC enrollment](https://opensource.googleblog.com/2021/11/expanding-google-summer-of-code-in-2022.html) beyond students to include all newcomers to open source who are 18 years and older. Google states, “We realize there are many folks that could benefit from the GSoC program that are at various stages of their career, recent career changers, self-taught, those returning to the workforce, etc. so we wanted to allow these folks the opportunity to participate in GSoC.”\n\nGitLab [participated this year](https://summerofcode.withgoogle.com/archive/2021/organizations/5396515480141824/), helping to mentor students, and Couder has been a mentor since 2008.\n\n## Open source communities\n\nOpen source communities like Cloud Native Computing Foundation [(CNCF)](https://www.cncf.io/), which include students and people who are changing careers, are a fantastic outlet to share your DevOps expertise. You can help other community members improve their features or applications, learn about documentation, learn new languages, and uncover bugs. Senior Developer Evangelist [Michael Friedrich](https://gitlab.com/dnsmichi) says it is rewarding to become a mentor in open source communities. “It is important to be honest, but also to be patient and kind. Don’t say something is easy – it’s not easy for that person. Instead, make sure to share your expertise in a constructive and helpful way,” he adds. Listen to more of Friedrich’s [advice for open source contributions](https://www.youtube.com/watch?v=yT63olXdS-I).\n\n_Cover image by Clark Tibbs via [Unsplash](https://unsplash.com/)._\n"
  category: Careers
  tags:
    - DevOps
    - open source
    - community
config:
  slug: where-to-donate-your-devops-skills
  featured: false
  template: BlogPost
