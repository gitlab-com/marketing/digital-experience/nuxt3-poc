seo:
  title: '#GitChallenge: Compare GitLab to GitHub and earn swag'
  description: Send us a review of GitLab and GitHub and get swag.
  ogTitle: '#GitChallenge: Compare GitLab to GitHub and earn swag'
  ogDescription: Send us a review of GitLab and GitHub and get swag.
  noIndex: false
  ogImage: images/blog/hero-images/hero-blog-gitlab-github.jpg
  ogUrl: https://about.gitlab.com/blog/github-free-for-teams
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/github-free-for-teams
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "#GitChallenge: Compare GitLab to GitHub and earn swag",
            "author": [{"@type":"Person","name":"GitLab"}],
            "datePublished": "2020-04-14",
          }

content:
  title: '#GitChallenge: Compare GitLab to GitHub and earn swag'
  description: Send us a review of GitLab and GitHub and get swag.
  authors:
    - GitLab
  heroImage: images/blog/hero-images/hero-blog-gitlab-github.jpg
  date: '2020-04-14'
  body: >


    Are you up for a challenge? Compare GitLab and GitHub! If you send us a link
    to your review on Twitter by tagging @gitlab and #GitChallenge we’ll send
    you some swag for giving us a try.

    {: .alert .alert-gitlab-purple}


    Today, GitHub announced [free private repositories with unlimited
    collaborators](https://github.blog/2020-04-14-github-is-now-free-for-teams/).
    This is great news for developers worldwide. GitHub also announced that they
    are lowering the price of their paid Team product to the same price as
    [GitLab’s Bronze/Starter](/pricing/premium/) offering: $4 per month per
    user.


    At GitLab, we’ve offered free private repositories as part of our Core/Free
    product from the start. We also recently made 18 additional features open
    source, which will help teams collaborate more effectively in a single
    product, and we’ve been steadily gaining market share in the version control
    space, with users switching from BitBucket and GitHub to GitLab.


    ## What your team loses when you go from GitHub Pro to Free


    When you go from GitHub Pro to GitHub Free, you lose some features that are
    already free and available to all users on GitLab and Gitlab.com:


    *   Protected branches in private repos

    *   Draft PRs in private repos

    *   GitHub Pages in private repos (using one)

    *   Wikis in private repos


    ## What your team gains by using GitLab Bronze/Starter


    With GitLab, you get even more features than GitHub Team. When there are
    multiple users on the same team, use [GitLab
    Bronze](/pricing/#gitlab-com)/[Starter](/pricing/#self-managed):


    *   Code owners in private repos

    *   Multiple issue assignees in private repos

    *   Multiple PR assignees in private repos

    *   Code review automatic assignment in private repos

    *   Standard support


    ## GitLab is more complete


    GitLab is a [complete DevOps platform](/topics/devops/), delivered as a
    single application. Here is a visual comparison:


    ![Comparing_GitLab_GitHub](https://about.gitlab.com/images/blogimages/gitlab-github-comparison.jpg){:
    .shadow}


    ## Take the #GitChallenge


    It has never been a better time to compare DevOps tools and find the best
    ones for you.


    Compare GitLab (get your [free trial here](/free-trial/)) and GitHub! You
    can:

    * Record a video and post it on social media

    * Write a blog or Medium post

    * Post your review on one of the many review sites like
    [G2](https://www.g2.com/products/gitlab/reviews)


    After you finish your review, send us a link on Twitter by tagging @gitlab
    and #GitChallenge, and we’ll send you some swag for giving us the feedback!
  category: News
  tags:
    - DevOps
    - git
config:
  slug: github-free-for-teams
  featured: false
  template: BlogPost
