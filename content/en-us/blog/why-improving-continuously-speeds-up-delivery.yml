seo:
  title: Why improving continuously speeds up delivery
  description: >-
    How do you keep pace with rapid changes in technology? The answer is
    continuous improvement.
  ogTitle: Why improving continuously speeds up delivery
  ogDescription: >-
    How do you keep pace with rapid changes in technology? The answer is
    continuous improvement.
  noIndex: false
  ogImage: images/blog/hero-images/just-commit-blog-cover.png
  ogUrl: https://about.gitlab.com/blog/why-improving-continuously-speeds-up-delivery
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/why-improving-continuously-speeds-up-delivery
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Why improving continuously speeds up delivery",
            "author": [{"@type":"Person","name":"John Jeremiah"}],
            "datePublished": "2019-04-09",
          }

content:
  title: Why improving continuously speeds up delivery
  description: >-
    How do you keep pace with rapid changes in technology? The answer is
    continuous improvement.
  authors:
    - John Jeremiah
  heroImage: images/blog/hero-images/just-commit-blog-cover.png
  date: '2019-04-09'
  body: >


    I just finished Tom Friedman’s latest book “[Thank You for Being Late: An

    Optimist's Guide to Thriving in the Age of
    Accelerations](https://www.amazon.com/dp/B01F1Z0QHA),”

    in which he explores how our world is accelerating and everything is
    happening

    faster and faster.  He explores the impact on business, society, economy,
    and

    environment. It’s a fantastic read – at times sobering and others exciting.
    I

    think a fundamental takeaway from his research is that, from now on,
    business

    leaders must learn how to transform their organizations to operate at faster
    cycle

    times than ever before. While that sounds great, the obvious question is:
    How?


    ## Operational efficiency and speed


    One of the classic business books on operational efficiency and speed is Dr.
    Eli

    Goldratt’s classic, [“The
    Goal”](https://www.amazon.com/gp/product/0884271951).

    In “The Goal,” the main character, Alex is a plant manager responsible for
    turning

    around a failing manufacturing plant. He learns a valuable lesson from his
    son’s

    scouting troop on a camping trip. As the group hikes into the woods, they
    spread

    out, because the slower hikers can’t keep up with the faster ones. No matter
    what

    Alex tries, he can't seem to keep them together. Then, he makes a small
    adjustment

    that changes everything. He puts the slowest hiker in the front so that the
    entire

    troop moves along at the speed of the slowest hiker. It’s the same in your

    development lifecycle: The fastest you can go depends on the most
    time-consuming

    step in the [end-to-end value stream](/solutions/value-stream-management/).


    So, how do you identify the most time-consuming step in your value stream?
    This

    daunting task can be accomplished by adopting DevOps practices. In

    [“The Phoenix
    Project”](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592)

    and subsequent blog posts, Gene Kim describes the

    [“Three
    Ways”](https://itrevolution.com/the-three-ways-principles-underpinning-devops/)

    from which all DevOps patterns arise. These philosophies boil DevOps down to
    a set

    of three principles that can help organizations increase efficiency and
    speed by

    carefully examining the value stream:


    1. **The First Way: Systems Thinking** – This first way is a flow of value
    from the business to the customer – or from Dev to Ops.

    1. **The Second Way: Amplify Feedback Loops** – The second way is to gather
    feedback from the customer, the business – or from Ops back to Dev.

    1. **The Third Way: Culture of Continual Experimentation and Learning** –
    Think of the third way as many smaller feedback loops of learning and
    improvement.


    What Alex learned in “The Goal” is an important lesson to remember: No
    matter

    what you change, you can only go as fast as the slowest. The same is true in
    your

    value stream. The principles of continuous improvement, exemplified by
    Gene’s

    Three Ways and [Kaizen](https://en.wikipedia.org/wiki/Kaizen) can be a
    powerful

    force to help drive incremental and lasting change.


    ## Continuous improvement through small changes


    Why should you adopt a Kaizen approach?  Because it works. Kaizen is a
    strategy

    that refers to continuous improvement through small changes that result in
    major

    improvement. When applied in a business setting, Kaizen has significant
    impact

    on culture, productivity, and quality.


    When teams practice continuous improvement, they;


    - Start with understanding their value stream.

    - Look for bottlenecks and waste.

    - Prioritize what to improve (remember the hikers).

    - Experiment with a minor change and learn.


    In principle, continuous improvement and [DevOps isn’t
    difficult](/topics/devops/), if you approach

    it from a perspective of Kaizen and Gene Kim’s “Three Ways.” However, the

    complexity of fragmented toolchains and processes, siloed incentives, and
    lack

    of collaboration often get in the way of making lasting improvements in
    software

    delivery.


    ## Increase your DevOps success and reduce cycle time


    To set the speed in the competitive race of software innovation, I have
    three suggestions:


    1. **Simplify your scope.** Focusing improvement efforts on one specific
    value 

    stream at a time narrows your efforts to hone in on major problem areas
    rather

    than becoming overwhelmed.

    1. **Empower your team.** Giving your delivery team the authority to
    experiment and

    improve enables innovation to become a focus.   

    1. **Measure your value stream.** Understanding your cycle time and
    identifying 

    bottlenecks enables you to take an objective look at what's slowing you
    down.


    Increasing your DevOps success and reducing cycle time through continuous

    improvement can help your organization continuously improve your value
    stream.

    At GitLab, we’re helping teams reduce cycle time with our approach to
    DevOps,

    which unifies teams to focus on delivering value.


    Are you ready to reduce cycle

    time? [Just commit.](/blog/2018/10/12/strategies-to-reduce-cycle-times/)

    {: .alert .alert-gitlab-purple .text-center}
  category: Insights
  tags:
    - DevOps
    - workflow
config:
  slug: why-improving-continuously-speeds-up-delivery
  featured: false
  template: BlogPost
