seo:
  title: Tips to configure browser-based DAST scans
  description: >-
    Learn how to use the browser-based analyzer with common dynamic application
    security testing settings, based on web application attributes, to ensure
    successful scans.
  ogTitle: Tips to configure browser-based DAST scans
  ogDescription: >-
    Learn how to use the browser-based analyzer with common dynamic application
    security testing settings, based on web application attributes, to ensure
    successful scans.
  noIndex: false
  ogImage: images/blog/hero-images/securitycheck.png
  ogUrl: https://about.gitlab.com/blog/tips-to-configure-browser-based-dast-scans
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/tips-to-configure-browser-based-dast-scans
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Tips to configure browser-based DAST scans",
            "author": [{"@type":"Person","name":"Julie Byrne"},{"@type":"Person","name":"Jerez Solis"}],
            "datePublished": "2023-11-14",
          }

content:
  title: Tips to configure browser-based DAST scans
  description: >-
    Learn how to use the browser-based analyzer with common dynamic application
    security testing settings, based on web application attributes, to ensure
    successful scans.
  authors:
    - Julie Byrne
    - Jerez Solis
  heroImage: images/blog/hero-images/securitycheck.png
  date: '2023-11-14'
  body: >
    The GitLab Dynamic Application Security Testing
    ([DAST](https://docs.gitlab.com/ee/user/application_security/dast/)) scan
    uses an actively running environment to crawl the application and find
    misconfigurations of your application server or incorrect assumptions about
    security controls that may not be visible from the source code. GitLab now
    provides a proprietary [browser-based
    analyzer](https://docs.gitlab.com/ee/user/application_security/dast/browser_based.html)
    for scanning applications that make heavy use of JavaScript, including
    single-page web applications. The DAST scan needs to be configured properly
    to account for various web application attributes, including authentication
    mechanism, authenticated landing page, and page load times. In this
    tutorial, you will learn common configurations that have helped our
    customers use the browser-based analyzer to successfully implement DAST
    scans. 


    ## General considerations


    The browser-based DAST scan takes the URL of the application it's supposed
    to scan from the `DAST_WEBSITE` environment variable. This should point to a
    test environment - you should not run a DAST scan against a production
    environment, even if you are only running a passive scan. For ephemeral
    environments that are deployed as part of the CI/CD pipeline, you can save
    the URL of the environment as an artifact `environment_url.txt`, which will
    then be used by the [DAST scan template
    job](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml)
    to set the `DAST_WEBSITE` variable.  The [GitLab Auto DevOps deploy template
    job](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Jobs/Deploy.gitlab-ci.yml)
    has an example of this. 


    Depending on the size of the web application, a DAST scan may take an hour
    or more to complete. You will want to ensure that whatever runner is used to
    perform the DAST scan has a [job timeout
    value](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#set-maximum-job-timeout-for-a-runner)
    set to be long enough to allow the scan to complete. Similarly, you should
    ensure that the [project level CI/CD
    timeout](https://docs.gitlab.com/ee/ci/pipelines/settings.html#set-a-limit-for-how-long-jobs-can-run)
    is sufficient to allow the job to complete. **Note:** Shared runners on
    gitlab.com have a runner timeout of 180 minutes, regardless of the project
    CI/CD timeout set.


    ## Configuration options for websites requiring authentication


    Many web applications require a user to log in to access the site. Logins
    can be implemented as basic http authentication or, more commonly, as form
    authentication.  For form authentication, the login form might be
    implemented in one of several ways:


    1. username and password fields on the main website landing page

    2. a login button that opens a modal (also called a modal window or
    lightbox) that displays in front of the page and disables all other page
    content until the login is completed or the modal is closed; there is not a
    separate URL associated with a modal window

    3. a login button that opens a new window, with its own URL 


    Additionally, the form may be either a single-step form, where the username
    and password fields are on the same page, or a multi-step form, where the
    username and password fields are on separate pages.  


    When running a DAST scan, the analyzer must know how to
    [authenticate](https://docs.gitlab.com/ee/user/application_security/dast/authentication.html). 
    We need to specify these details via the appropriate variables.  


    The `DAST_USERNAME` and `DAST_PASSWORD` variables specify the login
    credentials to be used. The variable values should be set via masked
    variables at the project level, not included within the `.gitlab-ci.yml`
    file.


    ### URL variable values


    Various URL values must also be specified: 
       - `DAST_AUTH_URL` - the URL of the login page
       - `DAST_WEBSITE` - specifies the URL of the page the user is redirected to after logging in

    **Note:** If your website uses authentication with a login button that opens
    a new window with its own URL, you should specify the URL of that new window
    as the `DAST_AUTH_URL` value.  


    GitLab enhancements are currently being implemented to support cases where
    additional actions must be taken post-login prior to being brought to the
    main site. See [this
    epic](https://gitlab.com/groups/gitlab-org/-/epics/11585)  for details: DAST
    browser-based analyzer multi-step login form does not support 'keep me
    signed in' workflow (AzureAD).


    ### Field variable values


    `FIELD` variables specify the page elements used. These values can typically
    be identified by inspecting the page source. For single-step login pages,
    you will need to specify:
     - `DAST_USERNAME_FIELD`
     - `DAST_PASSWORD_FIELD`
     - `DAST_SUBMIT_FIELD` 

    For multi-step logins, you would instead specify:

     - `DAST_FIRST_SUBMIT_FIELD` - the button clicked after entering the username
     - `DAST_SUBMIT_FIELD` - the button clicked after entering the password

    If your login button opens a modal, you should also specify
    `DAST_BROWSER_PATH_TO_LOGIN_FORM`, which provides the path of elements to
    click to get from the initial login URL to the login fields.


    #### Examples


    ![username email
    field](//images.ctfassets.net/r9o86ar0p03f/2NoNeLMOVEn2bjXEKz2Zik/86a2035676d2d7ab21ae1c2fea4e80bc/username_email_field.png)


    In this example, you can target the input element for the username field in
    different ways. Keep in mind that the selector you chose should be resilient
    to the application changing. We recommend to use the `id` and `name`
    attributes, as these are generally unique on a page and rarely change.


    ```

    DAST_USERNAME_FIELD: "id:user_login"

    DAST_USERNAME_FIELD: "name:user[login]"

    DAST_USERNAME_FIELD: "css:input[type=text]"

    ```


    The same process can be followed for the password field. For example:


    ```

    DAST_PASSWORD_FIELD: "id:user_password"

    DAST_PASSWORD_FIELD: "name:user[password]"

    DAST_PASSWORD_FIELD: "css:input[type=password]"

    ```


    **Submit/Sign in/Login button**


    ![login](//images.ctfassets.net/r9o86ar0p03f/yV86Bpx9qt9BQe3ZfOQZp/34ac682d4be3c785b217a6135ca1a7b7/login.png)


    You can target the submit/sign in/login button using this selector:


    ```

    DAST_SUBMIT_FIELD: "css:button[type=submit]"

    ```


    **Note:** If the submit button is not a `<button>`, but an input element in
    the form of `<input type="submit" name="login">`, you can use one of the
    following selectors:


    ```

    DAST_SUBMIT_FIELD: "css:input[type=submit]"

    DAST_SUBMIT_FIELD: "css:input[name=login]"

    ```


    ### Other variables to set


    If the username and password fields are on separate pages, DAST has to wait
    after submitting the username before looking for the password field.


    The `DAST_BROWSER_ACTION_STABILITY_TIMEOUT` variable, with a default value
    of 800ms, specifies the wait time. This time can be increased if the login
    response time is slow.


    If the website has a large JavaScript file that is required to load the
    target application, it is recommended that you use the variable
    `DAST_BROWSER_MAX_RESPONSE_SIZE_MB` to increase the limit for response
    sizes. The default is 10MB but can be increased for 50MB or more, if
    necessary.


    ## Tools for troubleshooting


    Several tools will help with DAST scan troubleshooting:


    - [authentication
    report](https://docs.gitlab.com/ee/user/application_security/dast/authentication.html#configure-the-authentication-report)
    - This report can be produced during the scan and saved as a CI/CD job
    artifact to assist with understanding the cause of an authentication
    failure. The report contains steps performed during the login process, HTTP
    requests and responses, the Document Object Model (DOM), and screenshots. To
    configure the report, set `DAST_AUTH_REPORT` to `true` and configure an
    artifacts attribute for the DAST job, e.g.:


    ```

    dast:
       variables:
          DAST_WEBSITE: "https://example.com"
          DAST_AUTH_REPORT: "true"
        artifacts:
          paths: [gl-dast-debug-auth-report.html]
          when: always
    ```


    - [analyzer
    logs](https://docs.gitlab.com/ee/user/application_security/dast/browser_based_troubleshooting.html#browser-based-analyzer-logging)
    - Setting `DAST_BROWSER_LOG` to `auth:debug` or `auth:trace` will provide
    additional logging that may help identify an issue with the scan. 


    The browser-based DAST scan configuration depends on the specific attributes
    of the web application you're testing, including how authentication is
    implemented to access the web site, what buttons are used, and how fast your
    browser loads once the user has authenticated. Using the appropriate
    variables to guide the analyzer through the authentication process will
    ensure that you are able to run a successful scan. And robust error logging
    and the authentication report will provide additional pointers to where the
    configuration might be incorrect and need to be adjusted.


    Try DAST scanning with [a free trial of GitLab
    Ultimate](https://about.gitlab.com/free-trial/).


    ## Read more

    - [How to configure DAST full scans for complex web
    applications](https://about.gitlab.com/blog/2020/08/31/how-to-configure-dast-full-scans-for-complex-web-applications/)

    - [How we're using DAST 2 for easier scan
    configuration](https://about.gitlab.com/blog/2021/10/27/how-were-using-dast2-for-easier-scan-configuration/)
  category: Security
  tags:
    - security
    - testing
    - testing
    - tutorial
config:
  slug: tips-to-configure-browser-based-dast-scans
  featured: false
  template: BlogPost
