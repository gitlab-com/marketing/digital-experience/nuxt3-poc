seo:
  title: Get ready for the Q1'2019 GitLab Hackathon
  description: >-
    The first Hackathon in 2019 for the GitLab community will take place on
    February 12-13.
  ogTitle: Get ready for the Q1'2019 GitLab Hackathon
  ogDescription: >-
    The first Hackathon in 2019 for the GitLab community will take place on
    February 12-13.
  noIndex: false
  ogImage: images/blog/hero-images/2018-09-13-gitlab-hackathon-cover.jpg
  ogUrl: https://about.gitlab.com/blog/q1-hackathon-announcement
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/q1-hackathon-announcement
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Get ready for the Q1'2019 GitLab Hackathon",
            "author": [{"@type":"Person","name":"Ray Paik"}],
            "datePublished": "2019-01-14",
          }

content:
  title: Get ready for the Q1'2019 GitLab Hackathon
  description: >-
    The first Hackathon in 2019 for the GitLab community will take place on
    February 12-13.
  authors:
    - Ray Paik
  heroImage: images/blog/hero-images/2018-09-13-gitlab-hackathon-cover.jpg
  date: '2019-01-14'
  body: >


    First of all, I want to wish a Happy New Year to everyone in the GitLab
    community! I'm certainly looking forward to continued collaboration with
    everyone in 2019. Following successful [Hackathons in
    2018](/community/hackathon/past-events/), I'm excited to announce that the
    first Hackathon this year will take place on Feb. 12-13.


    ## What's the deal?


    This is a virtual event where community members get together to work on
    merge requests (MRs) and also to welcome and help new contributors. We will
    be adding more details on [the Hackathon landing
    page](/community/hackathon/), as we get closer to the event, including
    prizes for everyone who has MRs merged within 10 days of the conclusion of
    the Hackathon.


    ## What else is taking place?


    We are again planning tutorial sessions where community experts will lead
    presentations plus Q&A sessions on a variety of topics. As speakers get
    confirmed, you will see tutorial sessions added on [the Hackathon landing
    page](/community/hackathon/). All the tutorial sessions will be recorded and
    added to the [GitLab Hackathon
    playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthapq-CyXBTVnT2yKqg1JrNh).
    If you missed tutorials from past Hackathons, I encourage you to check out
    videos from the playlist.


    ![Hackthon
    playlist](https://about.gitlab.com/images/blogimages/hackathon-playlist.png){:
    .shadow.medium.center}

    *<small>Tutorial videos on the Hackathon playlist</small>*


    For the upcoming Hackathon, we will also be highlighting issues from
    different [GitLab product categories](/handbook/product/categories/) that we
    want to encourage community members to work on. There will be additional
    prizes for community members who work on these issues and have MRs merged.


    ## Where can I find help during the Hackathon?


    For communications during the Hackathon, we will again use the [GitLab
    Community room in Gitter](https://gitter.im/gitlabhq/community). This is a
    channel designed to have community-related discussions and for community
    members to help each other as people have questions when contributing to
    GitLab. This is open to everyone, so please [join the
    room](https://gitter.im/gitlabhq/community) if you are not part of it
    already.


    ## How do I get started with contributing?


    A good place to start is the [Contributing to GitLab
    page](/community/contribute/), where you can learn how you can

    contribute to GitLab code, documentation, translation, and UX design.


    If you have any questions, you are always welcome to reach me at
    rpaik@gitlab.com.


    Cover image: ["GitLab application
    screengrab"](https://unsplash.com/photos/ZV_64LdGoao) by [Pankaj
    Patel](https://unsplash.com/@pankajpatel)

    {: .note}
  category: Open Source
  tags:
    - community
    - collaboration
    - open source
    - events
config:
  slug: q1-hackathon-announcement
  featured: false
  template: BlogPost
