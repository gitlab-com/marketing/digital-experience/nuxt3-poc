seo:
  title: How we built a Stack Overflow Community questions analyzer (and you can too)
  description: >-
    We wanted to better understand what Stack Overflow GitLab Community members
    wanted to know, so we automated a way to keep track of it all. Here's a
    step-by-step look at how we did it.
  ogTitle: How we built a Stack Overflow Community questions analyzer (and you can too)
  ogDescription: >-
    We wanted to better understand what Stack Overflow GitLab Community members
    wanted to know, so we automated a way to keep track of it all. Here's a
    step-by-step look at how we did it.
  noIndex: false
  ogImage: images/blog/hero-images/gitlabonstackoverflow.png
  ogUrl: >-
    https://about.gitlab.com/blog/how-we-built-a-stack-overflow-community-questions-analyzer-and-you-can-too
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/how-we-built-a-stack-overflow-community-questions-analyzer-and-you-can-too
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How we built a Stack Overflow Community questions analyzer (and you can too)",
            "author": [{"@type":"Person","name":"William Arias"}],
            "datePublished": "2022-04-28",
          }

content:
  title: How we built a Stack Overflow Community questions analyzer (and you can too)
  description: >-
    We wanted to better understand what Stack Overflow GitLab Community members
    wanted to know, so we automated a way to keep track of it all. Here's a
    step-by-step look at how we did it.
  authors:
    - William Arias
  heroImage: images/blog/hero-images/gitlabonstackoverflow.png
  date: '2022-04-28'
  body: "\nBeing part of the GitLab collective is an opportunity to learn first hand about the challenges the community using the DevOps Platform is facing. As a [Collective Member](https://stackoverflow.com/collectives/gitlab) logging between 2-3 times a week in StackOverflow  reading the questions and discussion posted about GitLab and manually sorting them by 'Recent Activity', 'Trending' and using Dates, I asked myself:  how can we leverage this  wealth of data and discover feedback, while finding  the most frequent topics where the community has questions? \n\nThis would be an opportunity to get a quick overview of topics where the community regularly needs help; this would also make it easier for us to create relevant content for them.  Manually sorting and extracting the text of each question wouldn’t be sustainable, so creating an automated way would be the most efficient way to proceed.\n\n## Experimenting with data-oriented content creation\n\nFinding out what the community is working on, and what they need help with while using GitLab, can help us to create better educational content that could expand their understanding of GitLab. To achieve this goal, the solution I created  after a few iterations is depicted below:\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/fontes.png)\n\nWhere the Bill Of Materials consists mainly of:\n\n- GitLab DevOps Platform\n- Stackoverflow API\n- Kubernetes Cluster\n- Open Source Python libraries:\n- scikit-learn (TF-IDF)\n- Streamlit (front-end)\n- Spacy                 \n\nI leveraged the GitLab DevOps Platform to organize the projects using groups:\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/organize.png)\n\nThe Loader project pulls questions about GitLab from the StackOverflow API, pre-processes the text and makes it usable for a second project: a Visualizer to create customized dashboards. \n\nThe automated process executed using the DevOps Platform is outlined below: \n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/automatedprocess.png)\n\n- Pull data from [StackOverflow API](https://api.stackexchange.com/docs)\n- Preprocess the response extracting relevant fields from returned JSON\n- Build a corpus and calculate TF-IDF\n- Scan for security vulnerabilities\n- Review Application and display its resulting dashboards using [Streamlit](https://streamlit.io/)\n- Deploy the built application to a Kubernetes cluster\n\nLoader and Visualizer projects have their own codebase and pipelines, which is helpful if different teams need to work separately on them. However, one project can require the other, which raises the need for  cross-project  automation. \n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/pipeline.png)\n\nThis scenario means a [multi-project pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html) is useful to automate the whole process. The multi-project pipeline enables use cases such as:\n\n- As an NLP Developer I want to work on the NLP Pipeline in the Loader Project and automatically trigger the creation of a new visualization \n- As a Streamlit Developer I want to work independently in the buttons and data visualization without touching any NLP Pipeline backend  \n\nThe outlined process above is automatically run defining the steps in a [multi-project pipeline](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html) sharing artifact:\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/demo1-gif.gif)\n\n## Finding the most frequently occurring words\n\nThe Feature Engineering step will help me to analyze the text in the whole dataset of GitLab questions. Using a simple yet powerful technique – TF-IDF – we aim to find the most relevant terms utilized by the community. By using this technique in the pipeline execution,  I represent words in numerical values and later rank them in order of importance.  This approach serves as a baseline for further improvements. More detail about this algorithm can be found [here](https://en.wikipedia.org/wiki/Tf%E2%80%93idf).\n\n## Did we achieve any success?\n\nOne run of the multi-pipeline in our solution results in dashboards such as this one:\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/demo2-gif.gif)\n\nAs an end-user of these dashboards I can immediately conclude that the main source of questions are around GitLab CI, pipelines and usage of Docker images. Not bad for a first run!  Having the data processed enables us to ask more questions and use data to answer it, such as, what are the questions from the highest [StackOverflow reputation](https://stackoverflow.com/help/whats-reputation) users ? \n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/questions.png)\n\nCould these questions be inspiration for tutorials for the most advanced users, or the implementation of a new feature? \n\nBecause everyone can contribute, let's take a look at the users who just started gaining their StackOverflow reputation:\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/contributors.png)\n\nThe question about access and reading/writing permissions in Portuguese is interesting. It makes me wonder about content localization and GitLab meetups in Portuguese-speaking countries. Not surprisingly, there were also \tquestions about GitLab CI too as the text processing and ranking found most relevant in the corpus. \n\nDid we achieve any success? Yes, using a baseline technique such as TF-IDF sped up by  DevOps practices allowed us  to find out relevant terms and help us to understand where the majority of the community needs help in their DevOps journey. I have automated many steps that will allow me to focus on data exploration and possible implementation of more complex NLP Techniques rather than infrastructure allocation or manual input of commands and tests.\n\n![Alt text for your image](https://about.gitlab.com/images/blogimages/demo-reduced.gif)\n\nAs a Technical Marketing Manager, I want to create content that is relevant to enable or inspire the  community to succeed. \n\nA personal take away: Educating about the latest GitLab DevOps platform capabilities and the problems they solve  is important and so is keeping an eye on the content that might not be related to a new feature but is needed right now.\n\nAre we done? No, quoting Da Vinci's altered quote about [Art](https://www.artshub.com.au/news/features/art-is-never-finished-only-abandoned-262096-2370305/#:~:text=Lottie%20Consalvo%20in%20her%20studio,writers%2C%20and%20creatives%20would%20recognise) but with software: \"Software is never finished, only abandoned.\"\n\nThere is room for improvement and adding capabilities to this project. We continue iterating, listening to the community, and we encourage you to clone these projects, try it yourself, and adjust it with the topics that make sense to you. Create a merge request to improve the codebase and suggest new dashboards ideas!\n\nExplore the [group of projects](https://gitlab.com/tech-marketing/ad-fontes) and take a look at the [dashboard](https://bit.ly/3jeTFQp).\n"
  category: Engineering
  tags:
    - DevOps
    - community
    - contributors
config:
  slug: how-we-built-a-stack-overflow-community-questions-analyzer-and-you-can-too
  featured: false
  template: BlogPost
