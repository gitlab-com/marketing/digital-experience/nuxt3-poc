seo:
  title: Why design systems benefit everyone
  description: >-
    Learn how the GitLab digital experience team built the Slippers design
    system for our marketing website.
  ogTitle: Why design systems benefit everyone
  ogDescription: >-
    Learn how the GitLab digital experience team built the Slippers design
    system for our marketing website.
  noIndex: false
  ogImage: images/blog/hero-images/slippers-sys.jpg
  ogUrl: https://about.gitlab.com/blog/starting-from-the-start-slippers-design-system
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/starting-from-the-start-slippers-design-system
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Why design systems benefit everyone",
            "author": [{"@type":"Person","name":"Stephen McGuinness"}],
            "datePublished": "2021-03-05",
          }

content:
  title: Why design systems benefit everyone
  description: >-
    Learn how the GitLab digital experience team built the Slippers design
    system for our marketing website.
  authors:
    - Stephen McGuinness
  heroImage: images/blog/hero-images/slippers-sys.jpg
  date: '2021-03-05'
  body: "\n\nThe [Digital Experience team](/handbook/marketing/digital-experience/) is new at GitLab, but we spent the past few months [creating Slippers, a new design system, which is a centralized location for design assets and code](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui). This blog post explains how we managed to build a design system in record time and accounts for how we overcame some of the challenges we encountered along the way.\n\nWe built Slippers because we needed a design system that we could rapidly iterate on and that would scale. We needed to use technologies that offered a single source of truth so our growing team could build on the repo. This process is not without its frustrations –\_what can work for one team might not work for the entire marketing department. In the past, discrepancies in design would happen because we didn't have a style guide.\n\nFortunately, creating a system that can respond to quick iterations can provide a solution to this complex problem. But \"simple\" in this case is misleading. We needed a new way of thinking and working. It is not enough to create a UI kit of consistent design assets for your designers to work with, doing this alone will fall at the first hurdle if it is not reflected in a coded repo. Designs will produce variations over time. Technical and design debt builds up due to small changes made over time and you end up where you started – with fragmented design and code.\n\nTime and effort as well as a vision are necessary to create a design system solution. This is the place our new team was at near the end of 2020. An already bizarre year for many, this was a great time to create a team to tackle this technical challenge head-on.\n\n## Why design systems are for everyone\n\nA common misconception of a design system is that it is for designers. You create a UI kit, hand it to developers, and you are off to the races. While a UI kit is important to the success of a system, it is just one part of what is a technical and efficient product.\n\nOur goal was to create a reusable library of assets, which included design assets (typestack, colors, spacing, grid, buttons, etc.) along with documentation on usage criteria. This is a big project that requires a lot of effort. First, we aligned around a common vision and product architecture. I want to emphasize \"product\" because this system acts as a product serving multiple teams across GitLab. Next, we rallied our team around a common goal and got to work. Our team established a set of guiding principles that would always act as our anchor for the project. [You can read more about them here](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui).\n\n*\"The more decisions you put off, and the longer you delay them, the more expensive they become.\"*\n\n**―[Craig Villamor](https://www.linkedin.com/in/craigvillamor), senior design director of Google Maps**\n\nWe found this quote from Craig in a [Medium post about the benefits of design systems](https://medium.com/agileactors/7-quotes-about-design-systems-that-will-inspire-you-9a89557fb26f). His remarks describe the dangers of putting off building a design system for too long. The fact is, the longer you design without a clear system and rubric, the more tech and design debt accumulates.\n\n## How we built the design system\n\nProducts exist to solve problems, so we articulated our vision with working sessions. The sessions were a platform for aligning our vision based on what we considered maintainable design and technology.\n\nOnce we aligned on our guiding principles we set about creating a roadmap. Our team decided how we wanted our product to be built, and agreed on tooling, tech stacks, and a cadence of delivery during our working sessions.\n\nWe decided on Figma for design since this was already being used within GitLab. Next, we created our core elements along with some [baseline components such as type, color, and spacing for the design system](https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=1292%3A573). We used existing pages as templates to refactor and give us a broader idea of what was and was not working. This process gave our developers time to investigate the best way to code our product and determine what shape it would take.\n\n## The value of a shared language\n\nOur engineering team started working on our tech stack and our designers started to work on what we called our \"foundations\". This can also be referred to as \"elements\". We did this in a way so we could stress-test our foundations package by refactoring existing pages with new styles that gave us an idea of the direction of our design system.\n\nNext, we applied these core elements to a select sample of pages to act as a proof of concept. We chose to edit the [homepage](https://about.gitlab.com/), [enterprise page](/enterprise/), [pricing page](/pricing/), and [entire GitLab Blog section](/blog/). We identified pain points and apply stop-gaps along the way. Since we are [results-driven](/handbook/values/#results), we used local CSS (Cascading Style Sheets) tightly coupled to the site itself. The perk of this approach is that you can deliver results quickly. After doing some UX and UI refinements on these pages, introducing new technology was easier because each of the pages are actively maintained. We used this time to learn and apply this practice to improve the system.\n\n## What's next\n\nThough the Digital Experience team has only been established for four months we've made huge inroads. We are starting to see how the Slippers design system will look once it is implemented across the entire organization.\n\nBuilding the Slippers design system is an example of a research and development (R&D) project. By laying out these foundations, we are set up for large-scale learning and success. The team is continuously gathering data for this R&D project and using it to better inform and refine our design system.\n\nAlso, since GitLab is open source, we are factoring open source values into our Slippers roadmap. We do this through posting our video updates to our partners and [public YouTube videos](https://www.youtube.com/c/GitLabUnfiltered/featured).\n\nThe reality is, this work takes time and investment. There is a herculean effort still left for us to bring the system fully to life. But already we have demonstrated the value of a design system to our leadership by delivering more than 2000 new CMS pages.\n\nEven at this very early stage the Slippers project has been rewarding and provides us with a continuous source of valuable insights. We're encouraged to push the boundaries and take calculated risks in what we learn and what we do.\n\nStay up-to-speed on our progress by checking out our [Slippers project](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui) and [watching our team videos on GitLab Unfiltered](https://www.youtube.com/c/GitLabUnfiltered/featured).\n\nCover photo by [Nihal Demirci](https://unsplash.com/@nihaldemirci?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/photos/0ME-BIUBmUs)\n{: .note}\n"
  category: Engineering
  tags:
    - collaboration
    - inside GitLab
    - UI
    - UX
config:
  slug: starting-from-the-start-slippers-design-system
  featured: false
  template: BlogPost
