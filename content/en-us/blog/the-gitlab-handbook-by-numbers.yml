seo:
  title: The GitLab handbook by numbers
  description: >-
    Two GitLab team-members take a fresh look at GitLab's open source team
    handbook, charting its evolution over the years to the weighty tome it is
    today.
  ogTitle: The GitLab handbook by numbers
  ogDescription: >-
    Two GitLab team-members take a fresh look at GitLab's open source team
    handbook, charting its evolution over the years to the weighty tome it is
    today.
  noIndex: false
  ogImage: images/blog/hero-images/handbook-cover.jpg
  ogUrl: https://about.gitlab.com/blog/the-gitlab-handbook-by-numbers
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/the-gitlab-handbook-by-numbers
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "The GitLab handbook by numbers",
            "author": [{"@type":"Person","name":"Lukas Eipert"},{"@type":"Person","name":"Lee Matos"}],
            "datePublished": "2019-04-24",
          }

content:
  title: The GitLab handbook by numbers
  description: >-
    Two GitLab team-members take a fresh look at GitLab's open source team
    handbook, charting its evolution over the years to the weighty tome it is
    today.
  authors:
    - Lukas Eipert
    - Lee Matos
  heroImage: images/blog/hero-images/handbook-cover.jpg
  date: '2019-04-24'
  body: >

    Sharing and retrieving information is a crucial part of everyday work life.

    Where do you get information from, be it about hiring processes, social
    media guidelines, or reporting expenses?

    At GitLab, all of that can be found in [the
    handbook](https://handbook.gitlab.com/) – have a look, it's public!

    [Sid](/company/team/#sytses), our CEO, [wrote about the importance and the
    open sourcing of our handbook][sid-blog-post] about two and a half years
    ago.

    Back then we were just shy of 100 employees.

    In this post we will look at how the handbook has developed over time, how
    we interact with it,

    and how it still works for over 550 employees.


    [sid-blog-post]: /blog/2016/07/12/our-handbook-is-open-source-heres-why/


    ## One book to guide them all


    At the time of writing, the handbook contains about 605,000 words.

    While probably a bit less captivating than the tales of Frodo and Middle
    Earth,

    we have composed more pages than "The Lord of the Rings" and "The Hobbit"
    combined, since the [first commit][first-commit] in 2015.

    It would take around 50 hours of continuous reading to cover the whole
    handbook, front to back.


    ### Is it overwhelming to read through it all?


    It would be, but as the handbook covers a wide range of topics, you probably
    don't need to read every single word.

    As the handbook changes over time it is not necessary to memorize it all,
    but it is more important to remember how to retrieve information.

    So as long as you know where to find something, you are on the safe side.


    > It would take around 50 hours of continuous reading to cover the whole
    handbook, front to back


    [first-commit]:
    https://gitlab.com/gitlab-com/www-gitlab-com/blob/2d2ced8f79da96fe981a3a6f6cf5918fa2dd992a/source/team-handbook/index.html


    ## One book to be written by them all


    ![Graph showing the growth of the handbook over time (May 2015 - April
    2019)](https://about.gitlab.com/images/blogimages/evolution_handbook/handbook-history.png){:
    .shadow.center}

    *<small>Graph showing the growth of the handbook, broken down by
    subcategory, over time (May 2015 – April 2019)</small>*


    Currently all knowledge in the handbook is spread across 550 unique web
    pages, with the average page containing around 1,100 words.

    The most words have been written in the subcategory engineering (138,000
    words), with marketing a close second (115,000 words).

    Typically, as teams grow, more of their processes get documented in the
    handbook, which leads to a natural growth of the respective category.


    > The most words have been written in the subcategory engineering (138,000
    words)


    ### Who contributes to the handbook?


    You might think that there is someone special who writes all those pages,
    but it's important

    to remember that [everyone can
    contribute](https://handbook.gitlab.com/handbook/company/mission/) to the
    handbook. It is actually part of our [onboarding process]

    to improve something about the handbook – whether that's clarifying wording
    or making it easier to find something.

    Nothing is exempt from change; even [our core values are adjusted over the
    course of time][values-history].


    ### How do you make changes to the handbook?


    If someone at GitLab or from the wider community wants to change something,
    they follow a simple workflow that is familiar to every GitLab user:


    1. Create a merge request which introduces the change.

    2. Discuss the merge request with the stakeholders.

    3. Iterate on the change and come to an agreement.

    4. Let the merge request be merged.


    More important changes (not every typo of course!) are then announced via
    Slack or our [company call].

    The handbook also has its own [changelog] which you can check regularly to
    see what has been changed over time.


    [onboarding process]:
    https://handbook.gitlab.com/handbook/people-group/general-onboarding/

    [values-history]:
    https://gitlab.com/gitlab-com/www-gitlab-com/commits/master/source/handbook/values/index.html.md

    [company call]: https://handbook.gitlab.com/handbook/communication/

    [changelog]: https://handbook.gitlab.com/handbook/about/changelog/


    ## One book to be read by them all


    In 2018 we had several hundred thousand page views on pages in the handbook.
    It is hard to tell which views come from GitLab team-members and which from
    the wider community.

    Among the most-read pages are our [Markdown Guide], the pages about [global
    compensation], our [values], the [hiring process], our [product],
    [benefits], and how to [communicate].

    These pages are topics of general interest to people within and outside the
    company.

    What could be a better resource to potential candidates than those pages
    that show the inner workings of GitLab?


    ### How do you find anything in the handbook?


    The handbook has a search function; you can use the [index
    page](https://handbook.gitlab.com/) as an entry point, or just use your
    favorite search engine to find information.

    Whenever someone asks a question in our Slack, there is a high probability
    that someone will answer with a link to the handbook.

    If someone asks a question that has no answer in the handbook, we highly
    encourage people to add that information to document it and make it easier
    for future GitLab team-members to find answers.


    > Whenever someone asks a question in our Slack, there is a high probability
    that someone will answer with a link to the handbook


    [Markdown Guide]: https://handbook.gitlab.com/handbook/markdown-guide/

    [global compensation]:
    https://handbook.gitlab.com/handbook/total-rewards/compensation/

    [product]: https://handbook.gitlab.com/handbook/product/

    [communicate]: https://handbook.gitlab.com/handbook/communication/

    [values]: https://handbook.gitlab.com/handbook/values/

    [benefits]: https://handbook.gitlab.com/handbook/total-rewards/benefits/

    [hiring process]: https://handbook.gitlab.com/handbook/hiring/


    ## One book to be the future


    We hope that this glimpse into the handbook is as interesting for you as it
    was for us.

    In an all-remote company it is especially important to write everything
    down, so that no matter

    where you are in the world or what time zone you choose to work in, the
    information you need is accessible.

    At the moment we are happy to say that we think that the handbook works as
    well for us now as it did with 100 employees.

    It aligns with our [values] more than ever.


    For us it is the most transparent way to collaborate on documentation of
    company internals.

    We are able to efficiently iterate on topics, resulting in more in-depth
    coverage over time.

    Personally the authors cannot see many reasons why the handbook should not
    be able to scale even further.

    Eventually it will evolve further, from the three tomes we have today, to a
    digital encyclopedia.

    We are definitely excited to see what the future holds!


    Have you taken inspiration from our handbook? Let us know by tweeting
    [@gitlab](https://twitter.com/gitlab).


    Photo by [Beatriz Pérez
    Moya](https://unsplash.com/photos/XN4T2PVUUgk?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)
    on
    [Unsplash](https://unsplash.com/search/photos/books?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)

    {: .note}
  category: Company
  tags:
    - collaboration
    - features
    - inside GitLab
    - open source
config:
  slug: the-gitlab-handbook-by-numbers
  featured: false
  template: BlogPost
