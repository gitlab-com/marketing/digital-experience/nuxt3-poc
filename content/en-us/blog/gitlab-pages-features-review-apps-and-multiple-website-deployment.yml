seo:
  title: GitLab Pages features review apps and multiple website deployment
  description: >-
    GitLab Pages helps organizations reap the rewards of knowledge management,
    including better collaboration and accessibility. Learn how to use a new
    feature, Parallel Deployments.
  ogTitle: GitLab Pages features review apps and multiple website deployment
  ogDescription: >-
    GitLab Pages helps organizations reap the rewards of knowledge management,
    including better collaboration and accessibility. Learn how to use a new
    feature, Parallel Deployments.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(1).png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-pages-features-review-apps-and-multiple-website-deployment
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-pages-features-review-apps-and-multiple-website-deployment
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Pages features review apps and multiple website deployment",
            "author": [{"@type":"Person","name":"Matthew Macfarlane"},{"@type":"Person","name":"Janis Altherr"}],
            "datePublished": "2024-09-23",
          }

content:
  title: GitLab Pages features review apps and multiple website deployment
  description: >-
    GitLab Pages helps organizations reap the rewards of knowledge management,
    including better collaboration and accessibility. Learn how to use a new
    feature, Parallel Deployments.
  authors:
    - Matthew Macfarlane
    - Janis Altherr
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(1).png
  date: '2024-09-23'
  body: >
    [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) has long been
    a popular choice for hosting static websites, allowing users to showcase
    their projects, blogs, and documentation directly from their repositories.


    Before GitLab 17.4, you could only have a single version of your GitLab
    Pages website. So you couldn’t preview your changes or have multiple
    versions of your website deployed simultaneously. Now, with a Premium or
    Ultimate license, you can do both!


    ### Introducing Parallel Deployments


    With Parallel Deployments, users can now easily preview changes and manage
    multiple environments for their GitLab Pages sites. This enhancement allows
    seamless experimentation with new ideas, enabling users to confidently test
    and refine their sites. By catching any issues early, users can ensure the
    live site remains stable and polished, building on the already great
    foundation of GitLab Pages.


    ### Why Parallel Deployments is a game-changer


    1. **Version control made easy**\
       If your project involves software development or documentation that covers multiple versions (such as user guides for different software releases), Parallel Deployments makes it easy to manage. Or you can use the feature to localize your website for different languages.
    2. **Flexibility to experiment**\
       Want to try out a new design or feature? Parallel Deployments lets you experiment freely. You can create a separate version of your site to test new ideas without impacting the current site. This flexibility encourages creativity and continuous improvement.

    ### How to add review apps to your GitLab Pages project


    To add a review app to your GitLab Pages project, first enable the Parallel
    Deployments feature in your project:


    - On the left sidebar, select "Search" or "Go to" and find your project.

    - Select Deploy > Pages.

    - Under Settings, check the box near “Enable Parallel deployments.” 


    **Note:** If you see the “Get started with Pages” screen instead of your
    Pages settings, you’ll need to create a (primary) deployment first. Either
    follow the steps on this page to set up your `.gitlab-ci.yml` from scratch.
    Or, if you already have one in your repository, run the pipeline manually by
    visiting **Build > Pipelines** and clicking **Run Pipeline**.


    The next step is to edit your `.gitlab-ci.yml` file to create a deployment
    for each merge request (MR). Let’s assume you start with a `.gitlab-ci.yml`
    file somewhat like this:


    ```

    pages:
      stage: deploy
      script:
        - npm run build
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH # only run this job when there's a commit to the default branch
    ```


    To also run the `pages` pipeline when there’s an MR being opened or updated,
    we can add another rule to `pages.rules`:


    ```

    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

    ```


    If we only add this rule, however, each Pages job will always replace the
    main deployment – each time an MR is opened! You likely don’t want that to
    happen.


    To provide each individual deployment with its own URL, we’ve introduced the
    new `pages.pages.path_prefix` property.


    A Pages deployment with this configuration...


    ```

    pages:
      script:
        - ...
      artifacts:
        paths:
        - public
      pages:
        path_prefix: my-review-app
    ```


    ...will be available at
    `https://my-pages-app-7fe824.gitlab.io/my-review-app`, or, with unique
    domains disabled,  `https://my-group.gitlab.io/my-project/my-review-app`.


    But there’s no need to hardcode the `path_prefix`. You can dynamically
    generate it using CI variables. That’s particularly useful for review apps –
    to create a path for each MR, use the `CI_MERGE_REQUEST_IID` variable:


    ```

    pages:
      script:
        - ...
      artifacts:
        paths:
        - public
      pages:
        path_prefix: mr-$CI_MERGE_REQUEST_IID
    ```


    An MR with the ID 114 would then automatically create a deployment at
    `https://my-pages-app-7fe824.gitlab.io/mr-114`.


    With those concepts at hand, we’d like our Pages job from above to
    dynamically create either a main deployment for the default branch, or a
    `path_prefix`ed-review app for MR events. Let’s leverage job variables for
    this.


    First, let’s add a `PAGES_PREFIX` variable to the job:


    ```

    pages:
      stage: deploy
      script:
        - npm run build
      variables:
        PAGES_PREFIX: "" # No prefix by default
      pages:
        path_prefix: "$PAGES_PREFIX" # use whatever value is set in the variable
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    ```


    By default, it’s an empty string. This is what we want for our default
    branch.


    Next, we’ll add said rule to the deployment. But this time, we can update
    the job variable if (and only if) said rule applies:


    ```

    pages:
      stage: deploy
      script:
        - npm run build
      variables:
        PAGES_PREFIX: "" # No prefix by default
      pages:
        path_prefix: "$PAGES_PREFIX" # use whatever value is set in the variable
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
          variables:
            PAGES_PREFIX: 'mr-$CI_MERGE_REQUEST_IID' # Prefix with the mr-<iid>, like `mr-123`
    ```


    Now you’re creating a deployment both when pushing to the default branch,
    and prefixed parallel deployments when creating or updating MRs!


    For the best experience, add the URL to the `environment` job property. This
    will add a link to the review app to the MR page:


    ```

    pages:
      stage: deploy
      script:
        - npm run build
      variables:
        PAGES_PREFIX: "" # No prefix by default
      pages:
        path_prefix: "$PAGES_PREFIX" # use whatever value is set in the variable
      environment:
        name: "Pages ${PAGES_PREFIX}"
        url: "${CI_PAGES_URL}/${PAGES_PREFIX}"
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
          variables:
            PAGES_PREFIX: 'mr-$CI_MERGE_REQUEST_IID' # Prefix with the mr-<iid>, like `mr-123`
    ```


    Congratulations, you’ve now set up MR review apps for your Pages site.


    ### How to deploy documentation for different versions of your product


    The Parallel Deployments feature is also a useful tool if you maintain the
    documentation of multiple versions of your software simultaneously.


    The below CI config will not only create a pages deployment when there is a
    commit to the default branch, but also for any commit to branches named
    “v1”, “v2,” or “v3”.


    ```

    pages:
      stage: deploy
      script:
        - ...
      variables:
        PAGES_PREFIX: "$CI_COMMIT_BRANCH" # Use the branch name by default
      pages:
        path_prefix: "$PAGES_PREFIX" # use whatever value is set in the variable
      environment:
        name: "Pages ${PAGES_PREFIX}"
        url: "${CI_PAGES_URL}/${PAGES_PREFIX}"
      artifacts:
        paths:
        - public
      rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
          variables:
            PAGES_PREFIX: '' # No prefix
        - if: $CI_COMMIT_BRANCH == 'v1'
        - if: $CI_COMMIT_BRANCH == 'v2'
        - if: $CI_COMMIT_BRANCH == 'v3'

    ```


    By using the `$CI_COMMIT_BRANCH` variable as the path_prefix value, each of
    these branches will deploy their documentation to their own sub-path of your
    website:

    - The branch named `v1` has its docs published to `<my-domain>/v1`.

    - The branch named `v2` has its docs published to `<my-domain>/v2`.

    - The branch named `v3` has its docs published to `<my-domain>/v3`.


    A new commit to one of these branches will then trigger a new deployment to
    its respective path, keeping the documentation of multiple versions up to
    date.


    The Parallel Deployments feature is a significant upgrade to GitLab Pages,
    offering a more flexible and efficient way to manage your knowledge. Whether
    you're working on a small project or a large-scale site with multiple
    versions, this new capability will make your workflow smoother and more
    efficient.


    > Visit our [Parallel Deployments
    documentation](https://docs.gitlab.com/ee/user/project/pages/#create-multiple-deployments)
    to get started today!


    ### Feedback


    Share your ideas and other comments in our [feedback
    issue](https://gitlab.com/gitlab-org/gitlab/-/issues/482040)!
  category: Product
  tags:
    - agile
    - CI/CD
    - DevSecOps
    - features
    - product
    - tutorial
config:
  slug: gitlab-pages-features-review-apps-and-multiple-website-deployment
  featured: false
  template: BlogPost
