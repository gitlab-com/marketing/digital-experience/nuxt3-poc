seo:
  title: 'Tutorial: How to set up your first GitLab CI/CD component'
  description: >-
    Use Python scripts in your GitLab CI/CD pipelines to improve usability. In
    this step-by-step guide, you'll learn how to get started building your own
    CI/CD component.
  ogTitle: 'Tutorial: How to set up your first GitLab CI/CD component'
  ogDescription: >-
    Use Python scripts in your GitLab CI/CD pipelines to improve usability. In
    this step-by-step guide, you'll learn how to get started building your own
    CI/CD component.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(26).png
  ogUrl: >-
    https://about.gitlab.com/blog/tutorial-how-to-set-up-your-first-gitlab-ci-cd-component
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/tutorial-how-to-set-up-your-first-gitlab-ci-cd-component
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Tutorial: How to set up your first GitLab CI/CD component",
            "author": [{"@type":"Person","name":"Sophia Manicor"},{"@type":"Person","name":"Noah Ing"}],
            "datePublished": "2024-11-12",
          }

content:
  title: 'Tutorial: How to set up your first GitLab CI/CD component'
  description: >-
    Use Python scripts in your GitLab CI/CD pipelines to improve usability. In
    this step-by-step guide, you'll learn how to get started building your own
    CI/CD component.
  authors:
    - Sophia Manicor
    - Noah Ing
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(26).png
  date: '2024-11-12'
  body: >
    Do you use Python scripts in your GitLab CI pipelines? Do you want to create
    pipelines at scale? This tutorial shows how to set up your first [GitLab
    CI/CD component](https://docs.gitlab.com/ee/ci/components/) to deploy Python
    scripts. 


    A [CI/CD component is a reusable single pipeline configuration
    unit](https://about.gitlab.com/blog/2023/07/10/introducing-ci-components/).
    Use components to create a small part of a larger pipeline, or even to
    compose a complete pipeline configuration.


    # Prerequisites

    - Basic Python knowledge

    - Working knowledge of GitLab CI

    - 8 minutes


    ## Python script 


    * **[The demo Python
    script](https://gitlab.com/demos/templates/gitlab-python-cicd-component/-/blob/main/src/script.py?ref_type=heads)**


    This Python script utilizes a library called
    [ArgParse](https://docs.python.org/3/library/argparse.html) . ArgParse
    allows you to pass variables to script through the command line. This script
    takes in three arguments:

    [Python_container_image](https://docs.gitlab.com/ee/ci/yaml/#image): This is
    the Python container image you wish to use.

    [Stage](https://docs.gitlab.com/ee/ci/yaml/#stage): This is the GitLab CI
    stage in which you job will run in. 

    Name: This is your name.


    ```python

    import argparse


    parser = argparse.ArgumentParser(description='Python CICD Component
    Boilerplate')

    parser.add_argument('python_container_image', type=str,
    help='python:3.10-slim')

    parser.add_argument('stage', type=str, help='Build')

    parser.add_argument('persons_name', type=str, help='Noah')

    args = parser.parse_args()


    python_container_image = args.python_container_image

    stage = args.stage

    persons_name = args.persons_name

    ```


    This will take in these three variables and print out simple statements:


    ```python

    print("You have chosen " + python_container_image + " as the container
    image")

    print("You have chosen " + stage + " as the stage to run this job")

    print("Thank you " + persons_name + "! you are succesfully using GitLab CI
    with a Python script.")

    ```


    To test this script locally, you can call on the script by utilizing the
    following command:


    ```bash

    python3 src/script.py python_container_image stage name

    ```


    Modify this script accordingly if you’d like to add in your own arguments!


    ## Template 


    * **[Demo of
    template](https://gitlab.com/demos/templates/gitlab-python-cicd-component/-/blob/main/templates/template.yml?ref_type=heads)**


    **Note:** As long as the `gitlab-ci.yml` is placed in the
    templates/directory, the CI/CD component will know to pick it up. We named
    our template `templates.yml`, but any name would work for this YAML file.


    Now, getting into the fun part of CI/CD components, inputs! 
    [Inputs](https://docs.gitlab.com/ee/ci/yaml/inputs.html) allow you to pass
    through variables into your pipeline. 


    ```yml

    spec:
      inputs:
        python_container_image:
          default: python:3.10-slim
          description: "Define any python container image"
        stage:
          default: build
          description: "Define the stage this job will run in"
        persons_name:
          default: Noah
          description: "Put your name here"
    ```

    Here we have defined the three inputs that are our arguments in our Python
    script. You can see for each input we have added in a default value – this
    will be what the input is set to if not overridden. If we took out this
    default keyword the input would become mandatory when we use our component.
    As it is written now, adding in these inputs when we use our component is
    optional due to our default values.


    We can also set descriptions to ensure that other developers can understand
    what to input when they use our component. Descriptions are optional but
    they provide self documentation within the code itself, which is always
    nice.


    After we set up our inputs, let’s write the rest of our component:


    ```yml

    component:
      image: $[[ inputs.python_container_image ]]
      stage: $[[ inputs.stage ]]
      before_script:
        - pip3 install -r src/requirements.txt
      script: python3 src/script.py $[[ inputs.python_container_image ]] $[[ inputs.stage ]] $[[ inputs.persons_name ]]
    ```


    To use inputs in our component, we need to use the syntax `$[[
    inputs.$VARIABLE ]]`. In the above code, you can see that we use inputs to
    define our image and stage with  `$[[ inputs.python_container_image ]]`
    and   `$[[ inputs.stage ]] `.


    ```

    script: python3 src/script.py $[[ inputs.python_container_image ]] $[[
    inputs.stage ]] $[[ inputs.persons_name ]]

    ```

    Diving into the script section, you can see we call upon our Python script..
    We are able to pass our inputs in with the help of the ArgParse.


    Now that you have reviewed how the Python script works and the template has
    been set up, it is time to use the component!


    ## Using the component 


    * **[A demo of including the
    component](https://gitlab.com/demos/templates/gitlab-python-cicd-component/-/blob/main/.gitlab-ci.yml?ref_type=heads)


    In order to utilize the CI/CD component we just created, we need to include
    it in the `.gitlab-ci.yml` file that is in the root of our directory. 


    ```

    include:
      # include the component located in the current project from the current SHA
      - component: $CI_SERVER_FQDN/$CI_PROJECT_PATH/template@$CI_COMMIT_SHA
        inputs:
          python_container_image: python:3.11-slim
          stage: test
          persons_name: Tanuki
    ```


    One way to include it is to call upon it locally in the current project from
    the current `Commit SHA`. You can find other ways to [reference a component
    in our
    documentation](https://docs.gitlab.com/ee/ci/components/#use-a-component).


    To override the defaults, we have passed in other inputs so we get the
    correct image, stage, and name for our job. 


    Try and change the `persons_names` to your own and watch the pipeline run!


    ![ci/cd component tutorial - pipeline
    running](//images.ctfassets.net/r9o86ar0p03f/1DFkV07STdqkREFL3rJlai/376cb83733afc7a23e38331d70fed7f1/image1.png)


    Voila! You have learned how to set up a basic C/ICD component utilizing a
    Python ArgParse script!


    ## What's next?

    In the Python script, there is a commented out GitLab Python library and OS
    library. If you would like to interact with the GitLab API, you can
    uncomment these and add in a [GitLab personal access
    token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
    to the [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/) named
    `GLPAT`.


    ```

    import gitlab

    import os

    ```

    Afterwards you can then interact with the GitLab API.


    ```

    glpat = os.environ['GLPAT']


    gl = gitlab.Gitlab(private_token=glpat)

    # SELF_HOSTED gl = gitlab.Gitlab(url='https://gitlab.example.com',
    private_token='xxxxxxxxxxxxxx')

    try:
       projects = gl.projects.list(get_all=True)
       print(projects)
    except Exception as error:
       print("Error:", error)
    ```


    > Learn more about CI/CD components and how to avoid building pipelines from
    scratch with the [GitLab CI/CD
    Catalog](https://about.gitlab.com/blog/2024/05/08/ci-cd-catalog-goes-ga-no-more-building-pipelines-from-scratch/). 


    ## Read more


    - [FAQ: GitLab CI/CD
    Catalog](https://about.gitlab.com/blog/2024/08/01/faq-gitlab-ci-cd-catalog/)

    - [Introducing CI/CD Steps, a programming language for DevSecOps
    automation](https://about.gitlab.com/blog/2024/08/06/introducing-ci-cd-steps-a-programming-language-for-devsecops-automation/)

    - [A CI/CD component builder's
    journey](https://about.gitlab.com/blog/2024/06/04/a-ci-component-builders-journey/)
  category: Engineering
  tags:
    - CI/CD
    - tutorial
    - DevSecOps
    - solutions architecture
config:
  slug: tutorial-how-to-set-up-your-first-gitlab-ci-cd-component
  featured: true
  template: BlogPost
