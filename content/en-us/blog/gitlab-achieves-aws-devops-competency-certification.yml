seo:
  title: GitLab achieves AWS DevOps Competency certification
  description: >-
    GitLab has been certified with AWS DevOps Competency, affirming our further
    commitment as a technology partner with Amazon Web Services.
  ogTitle: GitLab achieves AWS DevOps Competency certification
  ogDescription: >-
    GitLab has been certified with AWS DevOps Competency, affirming our further
    commitment as a technology partner with Amazon Web Services.
  noIndex: false
  ogImage: images/blog/hero-images/gitlab-aws-cover.png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-achieves-aws-devops-competency-certification
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-achieves-aws-devops-competency-certification
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab achieves AWS DevOps Competency certification",
            "author": [{"@type":"Person","name":"Tina Sturgis"},{"@type":"Person","name":"Eliran Mesika"}],
            "datePublished": "2018-11-28",
          }

content:
  title: GitLab achieves AWS DevOps Competency certification
  description: >-
    GitLab has been certified with AWS DevOps Competency, affirming our further
    commitment as a technology partner with Amazon Web Services.
  authors:
    - Tina Sturgis
    - Eliran Mesika
  heroImage: images/blog/hero-images/gitlab-aws-cover.png
  date: '2018-11-28'
  body: "\n\nToday, we are proud to announce GitLab has been certified with [AWS DevOps Competency](https://aws.amazon.com/devops/partner-solutions/), affirming our further commitment as a technology partner with Amazon Web Services (AWS).\n\nBuilding on the foundation of our AWS partnership over the last three years, with this DevOps certification we’ve now received the highest level of accreditation available from AWS. We bring proven customer success with measurable return on investment for customers running GitLab on AWS and [using GitLab to deploy their software to AWS](/partners/technology-partners/aws/).\n\n![AWS DevOps Competency badge](https://about.gitlab.com/images/blogimages/DevOps_competency_badge.png){: .small.right.wrap-text}\n\n### Why the AWS DevOps Competency matters\n\nAchieving this certification sets GitLab apart as an AWS Partner Network (APN) member that provides demonstrated DevOps technical proficiency and proven customer success, with specific focus in the [Continuous Integration](/features/continuous-integration/) and [Continuous Delivery](/features/continuous-integration/) category.\n\nThis is important for our own customers who are either looking to move to AWS or are already using it, as well as for current AWS customers. Potential users of GitLab with AWS can be assured that the GitLab solution has been reviewed and approved by an AWS Architect Review Board and that it meets [AWS Security Best Practices](https://d0.awsstatic.com/whitepapers/Security/AWS_Security_Best_Practices.pdf).\n\nThrough this process we were able to demonstrate our product is production ready on AWS for DevOps, specifically for improving application delivery, application build/test, or infrastructure/configuration management.\n\n### GitLab and AWS customer success\n\nTo learn more about the GitLab customer case studies considered for this competency, please review both the Axway and [Trek10](/customers/trek10/) case studies. You can also access information about other customers on the [GitLab customers page](/customers/).\n\n### More about the AWS Competency Program\n\nAWS established the program to help customers identify, through the AWS Partner Network, partners with deep industry experience and expertise in specialized solution areas. Attaining an AWS Competency allows partners to differentiate themselves to customers by showcasing expertise in a specific solution area.\n\nWe are honored to obtain this AWS DevOps Competency status, and believe this helps advance [our mission to allow everyone to contribute](/company/mission/#mission). Our definition of everyone now extends further, to those who are small and large users of AWS and AWS Services on their DevOps journey. \_\_\n\nFor more information on GitLab’s partnership with AWS, check out [about.gitlab.com/solutions/aws](/partners/technology-partners/aws/).\n\nTo learn more about GitLab’s Technology Partners, visit [about.gitlab.com/partners](/partners/technology-partners/).\n"
  category: Company
  tags:
    - CI
    - DevOps
    - news
config:
  slug: gitlab-achieves-aws-devops-competency-certification
  featured: false
  template: BlogPost
