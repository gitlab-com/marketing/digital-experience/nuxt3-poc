seo:
  title: GitLab's 2024 bug bounty year in review
  description: >-
    Who were the 2024 top 5 bug reporters? Find out in this look back at 12
    months of bug hunting. Also learn how to participate in 2025's bug bounty
    program.
  ogTitle: GitLab's 2024 bug bounty year in review
  ogDescription: >-
    Who were the 2024 top 5 bug reporters? Find out in this look back at 12
    months of bug hunting. Also learn how to participate in 2025's bug bounty
    program.
  noIndex: false
  ogImage: images/blog/hero-images/AdobeStock_941867776.jpeg
  ogUrl: https://about.gitlab.com/blog/gitlabs-2024-bug-bounty-year-in-review
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlabs-2024-bug-bounty-year-in-review
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab's 2024 bug bounty year in review",
            "author": [{"@type":"Person","name":"Ottilia Westerlund"}],
            "datePublished": "2025-01-06",
          }

content:
  title: GitLab's 2024 bug bounty year in review
  description: >-
    Who were the 2024 top 5 bug reporters? Find out in this look back at 12
    months of bug hunting. Also learn how to participate in 2025's bug bounty
    program.
  authors:
    - Ottilia Westerlund
  heroImage: images/blog/hero-images/AdobeStock_941867776.jpeg
  date: '2025-01-06'
  body: >
    It’s that time again when everyone reflects on the year that just passed,
    and the [Application
    Security](https://handbook.gitlab.com/handbook/security/security-engineering/application-security/)
    team at GitLab is no different. We run the bug bounty program at GitLab, and
    every year we summarize our stats for those who are curious. We wouldn't be
    where we are without the collaboration of our bug bounty community, and we
    consider these awards hugely beneficial and money well spent. 


    ## GitLab Bug Bounty Program by the numbers 


    * Awarded over US$1 million in bounties across 275 valid reports.  

    * Received a total of 1,440 reports from 457 researchers in 2024.  

    * Our busiest month was July, when we paid out over US$193,000!


    *Note: Data is accurate as of 31st of December, 2024.* 


    You can see program statistics updated daily on our [HackerOne program
    page](https://hackerone.com/gitlab).


    ## GitLab Bug Bounty Researchers of the Year


    It's time to shine a spotlight on the brilliant minds who have contributed
    to making GitLab more secure. Our bug bounty program continues to be a
    crucial part of our security strategy, and we're thrilled to recognize the
    outstanding efforts of our top researchers.


    ### Most Valid Reports: joaxcar


    Leading the pack with an impressive 55 valid reports,
    [joaxcar](https://hackerone.com/joaxcar?type=user) has demonstrated
    exceptional dedication and skill in identifying potential vulnerabilities.
    joaxcar’s consistent contributions have played a significant role in
    enhancing GitLab's security posture, and has risen to our No. 1 contributing
    researcher.


    ### Newcomer of the Year: a92847865


    We're always excited to welcome fresh talent to our bug bounty program. This
    year, [a92847865](https://hackerone.com/a92847865?type=user) caught our
    attention by submitting 16 valid reports since their first submission on May
    10. Their quick impact showcases the importance of new perspectives in
    security research.


    ### Most Innovative Report: yvvdwf


    Innovation is key to staying ahead of potential threats. A report made by
    [yvvdwf](https://hackerone.com/yvvdwf?type=user) stood out for its creative
    approach to identifying a complex vulnerability. This kind of out-of-the-box
    thinking is invaluable in our ongoing security efforts.


    ### Most Impactful Finding: ahacker1


    Sometimes, a single discovery can have far-reaching implications. One of
    [ahacker1's](https://hackerone.com/ahacker1?type=user) reports was
    particularly impactful this year. This finding led to significant
    improvements in our pipeline security and API access controls. 


    ### Best Written Report: matanber


    Clearly written communication is crucial in bug bounty reports. This year,
    [matanber](https://hackerone.com/matanber) provided an exceptionally
    detailed explanation of a complex Web IDE vulnerability. The report included
    comprehensive technical diagrams, relevant code snippets, and step-by-step
    explanations that showcased the issue perfectly. The clarity and
    thoroughness of the report made it easier for our team to understand,
    validate, and promptly fix the issue.


    ### Special swag


    As a token of our gratitude (in addition to the monetary reward, of course),
    we are sending our top bug bounty researchers some limited edition swag!
    Psst, winners, make sure to check your HackerOne emails!


    ## Other highlights


    We continued running our 90-day challenges where researchers focused on
    different areas of GitLab in return for an extra bug bounty bonus payout. We
    saw a great turnout for these, and it’s something we will look into
    continuing in 2025. 


    We also hosted another "Ask a hacker AMA" – this time with @ahacker1. [Read
    the recap blog](https://hackerone.com/ahacker1?type=user) or watch the
    interview:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/EPV0eNOOfv4?si=byNqXWKZzZLXfLfW" title="GitLab Ask a Hacker AMA with Alexander Siyou Tan (@ahacker1)" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Looking ahead


    As we move into 2025, we're excited to see the new discoveries of our bug
    bounty community. Your efforts continue to be a cornerstone of our security
    strategy, helping us build a more secure platform for developers around the
    world.


    To all our researchers: Thank you for your hard work, creativity, and
    commitment to security. Here's to another year of smashing bugs!


    > #### Learn how to participate in the [GitLab 2025 Bug Bounty
    program](https://hackerone.com/gitlab?type=team).
  category: Security
  tags:
    - bug bounty
    - security
    - community
config:
  slug: gitlabs-2024-bug-bounty-year-in-review
  featured: false
  template: BlogPost
