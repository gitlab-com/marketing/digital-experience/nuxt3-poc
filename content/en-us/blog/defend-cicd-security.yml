seo:
  title: Defending the CI/CD pipeline
  description: >-
    Speed to launch often comes at the cost of security – but it doesn’t have
    to. Here are four ways to achieve both by using a CI/CD pipeline
  ogTitle: Defending the CI/CD pipeline
  ogDescription: >-
    Speed to launch often comes at the cost of security – but it doesn’t have
    to. Here are four ways to achieve both by using a CI/CD pipeline
  noIndex: false
  ogImage: images/blog/hero-images/defend-cicd-security.jpg
  ogUrl: https://about.gitlab.com/blog/defend-cicd-security
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/defend-cicd-security
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Defending the CI/CD pipeline",
            "author": [{"@type":"Person","name":"Vanessa Wegner"}],
            "datePublished": "2019-11-19",
          }

content:
  title: Defending the CI/CD pipeline
  description: >-
    Speed to launch often comes at the cost of security – but it doesn’t have
    to. Here are four ways to achieve both by using a CI/CD pipeline
  authors:
    - Vanessa Wegner
  heroImage: images/blog/hero-images/defend-cicd-security.jpg
  date: '2019-11-19'
  body: >

    [CI/CD](/topics/ci-cd/) is a way to release software as quickly as possible,
    which, unfortunately, often comes at the expense of security. [Synopsys and 

    451 Research
    found](https://www.synopsys.com/blogs/software-security/security-challenges-cicd-workflows/) 

    the most significant [application security](/topics/devsecops/) challenges
    in CI/CD workflows 

    include a lack of automated, integrated security testing tools,
    inconsistent 

    methods, slowed workflows, and too many false positives.


    There’s also the challenge of securing the pipeline itself. Traditional and 

    manual security practices can’t scale to the level of CI/CD – the resulting
    delivery pipelines expand a company’s attack surface by a significant
    measure. The pipeline represents an end-to-end lifecycle for your software
    which makes it a 

    prime target for hackers. It's clear [CI/CD
    security](/solutions/dev-sec-ops/) can’t be an afterthought. DevOps teams 

    must bring security issues to the forefront of their considerations
    throughout the SDLC. 


    ## Security risks in enterprise CI/CD


    CI/CD significantly broadens your attack surface with a lengthy list of 

    components – repositories, servers, containers, and for those who don’t use 

    GitLab, a wide array of tools. A large number of moving pieces presents a 

    tempting ROI for hackers – one compromised segment of the ecosystem could
    open 

    up the entire infrastructure for exploitation. [As tech journalist Twain
    Taylor 

    explains](https://thenewstack.io/the-biggest-security-risks-lurking-in-your-ci-cd-pipeline/), 

    securing the CI/CD pipeline is not a straightforward process. Teams need to
    study the 

    pipeline, understand what information the pipeline ingests, uncover any
    major 

    vulnerabilities and find ways to eliminate those risks.


    Also, tools that lack transparency, require frequent switching 

    between platforms, and inhibit the overall workflow are less likely to be 

    adopted – and more likely to be worked around. Workarounds can create
    friction in the pipeline which can mean inconsistent 

    testing and remediation, all of which can allow more vulnerabilities to make
    their way 

    through to production and launch.


    ## Defending against CI/CD pipeline risks


    Secure CI/CD can be achieved through [DevSecOps](/topics/devsecops/) but
    you’ll need a mature CI/CD solution to get you there. In addition to the 

    stability of the solution, your lifecycle ecosystem must be well-maintained
    and 

    easily monitored for suspicious activity. Four of the most important aspects
    of 

    a secure CI/CD pipeline are automation, access management, positive user 

    experience, and transparency.


    ### Automation


    Automation, at the very least, should allow you to bring your security 

    practices (especially
    [testing](/stages-devops-lifecycle/application-security-testing/)) 

    up to the speed and scale of CI/CD. The value of automation magnifies when 

    processes are standardized across teams and organizations. By introducing 

    repeatability to your projects, you’re also creating expected functionality
    and operations within your pipeline. When there are behaviors 

    or activities that don’t align to the expected, a red flag will be triggered
    alerting developers to potential threats.


    ### Access management


    Access rights should be considered for both human-to-tool and tool-to-tool 

    interactions. [Tripwire
    recommends](https://www.tripwire.com/state-of-security/devops/security-ci-cd-pipeline-flowing/) 

    requiring authentication for anyone to push changes to the pipeline, 

    implementing login tracking, and confirming that builds reside on secure 

    servers. 


    Communication between tools and components should be carefully managed 

    to ensure that access is only granted on an as-needed basis. The New Stack's
    Twain also notes it’s important to consider what secrets are contained in
    pipeline scripts. He recommends removing any keys, credentials, and secrets
    from scripts and 

    protecting them with trusted secrets managers. He also suggests
    implementing 

    access control across your entire toolchain to revoke anything anonymous or
    shared, and to regularly audit the controls across the 

    ecosystem. 


    ### User experience


    Seamless integration between tools will make a night-and-day difference in 

    securing your CI/CD pipeline (alternatively, you could also use [a single
    tool 

    for the entire lifecycle]/handbook/product/single-application/)). 

    Even though security is gaining traction in the minds of non-security 

    professionals, it still remains a challenge for many development teams.
    Provide 

    developers with tools and practices that are standard across the
    organization, 

    and reduce friction between tools as much as possible. 


    With lower barriers to 

    adoption, your team will be less likely to create workarounds that could 

    jeopardize your business or customers. Providing users with immediate 

    feedback on the security of their code will enable them to remediate on the 

    spot and serve an educational purpose, showing developers what to watch out 

    for when writing code. 


    ### Transparency


    It's vital to have a view into what happens throughout the CI/CD pipeline.
    Maintain a single source of truth that logs every change – 

    as well as its origin – and include functionality that allows sign-off for
    any 

    high-stakes updates. Transparency also builds accountability among team
    members, 

    reenforcing the idea that everyone is responsible for security. Lastly, 

    transparency is crucial to your team communication strategy. Methodologies
    and 

    knowledge should be communicated openly and thoroughly, so that everyone on
    the 

    team understands how to apply best practices and what the intended outcomes
    are.


    ## Speed and security: No longer a paradox


    Each of the above steps will help your security efforts shift left in the 

    SDLC. Moving it all earlier in the process will enable you to release
    secure, quality software at the 

    speed of the business. This can only happen if there is true collaboration
    between development, operations, 

    and security. Set policies and standard practices, understand respective 

    goals, and foster a culture of responsibility for the software as a 

    whole – and not just one facet of its creation or performance.


    ## The security benefits of a single CI/CD tool for the entire lifecycle


    It’s extremely important to use established tools that have been thoroughly 

    vetted by both your internal teams and the market at large. That being
    said, 

    finding the best-in-class tools for every phase of the lifecycle and then 

    successfully (and securely) stringing them together can be a nightmare and
    result in untold technical debt. A single CI/CD tool relieves much of 

    that burden, by eliminating unnecessary platform switching and enabling
    high 

    transparency throughout the pipeline. With GitLab in particular, security 

    checks are embedded within the development workflow, which both reduces 

    friction for developers and provides a single source of truth for the
    entire 

    pipeline.


    Regardless of your tool (or tools) of choice, it’s critical that you and
    your 

    team prioritize security in all aspects of work.


    Cover image by [Boban Simonovski](https://unsplash.com/@3031n) on
    [Unsplash](https://unsplash.com/photos/akQ06aB6MfM)

    {: .note}
  category: Insights
  tags:
    - CI/CD
    - DevOps
    - security
config:
  slug: defend-cicd-security
  featured: false
  template: BlogPost
