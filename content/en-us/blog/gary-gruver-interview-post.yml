seo:
  title: IT executives! Take the lead in DevOps transformations
  description: >-
    Gary Gruver, author of "Starting and Scaling DevOps in the Enterprise,"
    shares his thoughts on the role of executives in a DevOps transformation.
  ogTitle: IT executives! Take the lead in DevOps transformations
  ogDescription: >-
    Gary Gruver, author of "Starting and Scaling DevOps in the Enterprise,"
    shares his thoughts on the role of executives in a DevOps transformation.
  noIndex: false
  ogImage: images/blog/hero-images/gary-gruver-cover.jpg
  ogUrl: https://about.gitlab.com/blog/gary-gruver-interview-post
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gary-gruver-interview-post
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "IT executives! Take the lead in DevOps transformations",
            "author": [{"@type":"Person","name":"Suri Patel"}],
            "datePublished": "2018-06-14",
          }

content:
  title: IT executives! Take the lead in DevOps transformations
  description: >-
    Gary Gruver, author of "Starting and Scaling DevOps in the Enterprise,"
    shares his thoughts on the role of executives in a DevOps transformation.
  authors:
    - Suri Patel
  heroImage: images/blog/hero-images/gary-gruver-cover.jpg
  date: '2018-06-14'
  body: >


    The changes in both workflow and culture during a DevOps transformation
    highlight the need for IT executives to guide development and operations
    teams. [Gary Gruver](http://www.garygruver.com), the renowned DevOps
    consultant, discovered that executives are essential in setting up teams to
    adopt a DevOps model successfully. Ahead of his June 19 webcast, Gary Gruver
    sat down with GitLab to share his thoughts on the greatest challenges that
    executives encounter and to provide tactical steps to help executives
    support their teams.


    ## The role of an executive in the DevOps transformation is leading it.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://drive.google.com/file/d/1a9sGyFz9fW9zeXOa7MJGib8fUN4ZrIsA/preview" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->



    ## One of the biggest challenges is getting everybody on the same page.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://drive.google.com/file/d/1d5w4GRRbHA2poHJocUT-4cBfW2KBEsw2/preview" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## One of the biggest things executives need to do to prepare their teams is
    giving a common view of the inefficiencies for the entire deployment
    pipeline.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://drive.google.com/file/d/1iseA7ifwF9qIkXDNQun9j0ps-b4_QfuK/preview" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## In terms of culture, executives can help teams adjust to the changes that
    are coming. Their role is to be that of an investigative reporter.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://drive.google.com/file/d/1788pyYj0QGx8z29YaP6PoseS-OJI_u_d/preview" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## When I work with executives, I always start with, "What are the
    objectives about your software development processes?"


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://drive.google.com/file/d/1ainmogqtovRD0QKq_gJW9XT3_Lii2Hbm/preview" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    Join Gary Gruver for a webcast on June 19 to discover ways to analyze your
    current deployment pipeline to target your first improvements on the largest
    inefficiencies in software development and deployment, and [download your
    free copy](/resources/scaling-enterprise-devops/) of "Starting and Scaling
    DevOps in the Enterprise."
  category: Insights
  tags:
    - DevOps
config:
  slug: gary-gruver-interview-post
  featured: false
  template: BlogPost
