seo:
  title: 'GitLab Duo Chat 101: Get more done on GitLab with our AI assistant'
  description: >-
    In this first article in our series learn how Chat can improve developer
    productivity – for example, by summarizing issues – and how to improve
    prompts to get better answers faster.
  ogTitle: 'GitLab Duo Chat 101: Get more done on GitLab with our AI assistant'
  ogDescription: >-
    In this first article in our series learn how Chat can improve developer
    productivity – for example, by summarizing issues – and how to improve
    prompts to get better answers faster.
  noIndex: false
  ogImage: images/blog/hero-images/GitLab_Duo_Blog_Hero_1800x945_r2_B-(1).png
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-duo-chat-101-get-more-done-on-gitlab-with-our-ai-assistant
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-duo-chat-101-get-more-done-on-gitlab-with-our-ai-assistant
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab Duo Chat 101: Get more done on GitLab with our AI assistant",
            "author": [{"@type":"Person","name":"Abubakar Siddiq Ango"}],
            "datePublished": "2024-05-29",
          }

content:
  title: 'GitLab Duo Chat 101: Get more done on GitLab with our AI assistant'
  description: >-
    In this first article in our series learn how Chat can improve developer
    productivity – for example, by summarizing issues – and how to improve
    prompts to get better answers faster.
  authors:
    - Abubakar Siddiq Ango
  heroImage: images/blog/hero-images/GitLab_Duo_Blog_Hero_1800x945_r2_B-(1).png
  date: '2024-05-29'
  body: >-
    GitLab Duo Chat became [generally
    available](https://about.gitlab.com/blog/2024/04/18/gitlab-duo-chat-now-generally-available/)
    in [GitLab
    16.11](https://about.gitlab.com/releases/2024/04/18/gitlab-16-11-released/)
    and its power as a personal assistant can not be overstated. On a DevSecOps
    platform, more has to happen than just generating code; planning,
    discussions, security, compliance, and technical reviews are all critical to
    developing secure software faster. Issues, epics, merge requests, and other
    sections of GitLab are where this work happens, with knowledge often buried
    deep in comment threads. It can take a lot of time to get up to speed on
    these threads, especially when they've grown to hundreds of comments and
    interactions and when you've been away from them for a while. This is where
    GitLab Duo Chat can help.


    In this first part of our GitLab Duo Chat 101 series, we'll introduce you to
    Chat's capabilities and then dig into how to use Chat to summarize comment
    threads.


    > Live demo! Discover the future of AI-driven software development with our
    GitLab 17 virtual launch event. [Register
    today](https://about.gitlab.com/seventeen/)!


    ## GitLab Duo Chat's capabilities


    With Chat, you can refactor [existing
    code](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#refactor-code-in-the-ide),
    learn how a [block of code
    works](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#explain-code-in-the-ide),
    and write
    [tests](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#write-tests-in-the-ide)
    for your code, learn about your issues and epics, and much more. Depending
    on your prompts, you can make Chat do impressive things that boost developer
    productivity. In the video below, I showcased how you can use GitLab Duo
    Chat to interact with GitLab and learn about your issues and epics.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/RJezT5_V6dI?si=XlXGs2DHAYa8Awzs" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## Use cases   


    GitLab Duo Chat’s capabilities allow for productivity gains across multiple
    parts of the software development lifecycle:


    - Product and project managers can use Chat’s issues and epics capabilities
    to gain insights into discussions and plan faster.

    - Developers can create solutions faster with code suggestions and
    refactoring capabilities. When it comes to working with legacy code or code
    from other team members, less time is spent on research with the ` /explain`
    capability providing the necessary insights to understand the code.

    - Quality assurance and test engineers can generate tests and check for
    vulnerabilities

    - New employees can get a better understanding of their code base and get
    started solving problems.

    - Beginner programmers can understand and pick up a language or framework
    quickly and create solutions with Chat providing next steps and insights.


    > Check out "[10 best practices for using GitLab Duo
    Chat](https://about.gitlab.com/blog/2024/04/02/10-best-practices-for-using-ai-powered-gitlab-duo-chat/)"
    for tips and tricks to craft AI prompts. 


    ## Summarizing issues


    When you encounter an issue, especially one with a lot of comments, you skim
    through the issue description, along with a couple of comments, but can't
    always get the complete picture of the conversations. GitLab Duo Chat can
    get you up to speed fast. In the image below, I asked Chat to summarize an
    issue along with a follow-up question. In two prompts, I got what I needed
    to understand what is going on in the issue without spending hours reading
    through the comments.


    ![Chat summarizing an
    issue](//images.ctfassets.net/r9o86ar0p03f/1Z4aqUpHeFgRgofRm3EKBO/e407069f21c6196dc1e3cdc35792dcf2/image1.png)


    You can use GitLab Duo Chat on the GitLab interface, as well as [the WebIDE,
    Visual Studio Code, and JetBrains
    interfaces](https://docs.gitlab.com/ee/user/gitlab_duo_chat.html#use-gitlab-duo-chat-in-the-web-ide).


    ## Prompts and context


    Getting the best responses from any AI tool requires carefully crafting the
    questions asked. Sometimes, you need to give examples of the responses you
    expect to prime the large language models (LLMs) toward a desired response.
    Here are some areas to focus on to get desired responses.


    ### Context


    Here are three prompts with similar objectives but worded differently:


    | Prompt 1     | Prompt 2     | Prompt 3    |

    | ---------- | ---------- | ---------- |

    | ![Prompt: Can you summarize this issue's
    description?](//images.ctfassets.net/r9o86ar0p03f/4zYOXjNpe7LciMUmTZ7DKP/a5081c4673e996b60c272cbe8607f784/image5.png)      
    | ![Prompt: Can you provide a high-level summary of this
    issue?](//images.ctfassets.net/r9o86ar0p03f/wgi8xH6syjG1mcrnkzhxv/0005dd2c8456f6f1b183a31f5d2c9be8/image4.png)     
    | ![Prompt: Why is this issue
    popular?](//images.ctfassets.net/r9o86ar0p03f/4dQN7kNT1HrxQSeIPhZ28y/d7c56f9447aaa57360a24adb67f1912e/image2.png)     
    |


    The context, “this issue,” is common among the three prompts; this tells
    Chat what resource to use in looking for answers. Prompt 1 gives additional
    context on what to focus on: the description of the issue. Prompt 2 is not
    limited in its scope, which means the LLMs will spend more time going
    through the description and all the comments to provide a more detailed
    summary of the whole issue. (Note: As of the publication of this blog, there
    were more than 90 comments in that issue.) Prompt 3 got a poorer response
    because not much expectation was set for the type of response expected. 


    [Low-context
    communication](https://handbook.gitlab.com/handbook/company/culture/all-remote/effective-communication/#understanding-low-context-communication)
    is critical in crafting your prompt for the best responses, as all
    information needed for the LLMs to provide an informed response is provided.


    ### Simplicity


    The wordiness of prompts can sometimes lead to incorrect or no responses. In
    the image below, you can see that rephrasing a prompt from “Customers have
    mentioned why this issue is important to them. Can you list the top 3
    reasons they mentioned?” to “Why is this issue important to customers?” led
    to the expected response. When you don’t get the response you desire,
    simplifying or changing the words used in your prompt can improve the
    quality of responses.


    ![Wordy Chat
    prompts](//images.ctfassets.net/r9o86ar0p03f/4Mw5fbPQ4mgjgt7zi3ec4k/9fea83809d9f9e77593bfd90fa81b36f/image6.png)


    ### Follow-up questions


    GitLab Duo Chat can have follow-up conversations – an essential capability.
    In the image below, I continued asking how the issue in question can be
    solved in GitLab's code along with a follow-up question asking for code
    samples.


    ![Streamlined Chat prompt
    shown](//images.ctfassets.net/r9o86ar0p03f/3KIgGu3N5sCPLbPKrSczXu/c1eb0047aeddea480dcfe2566a071cd3/image3.png)


    Follow-up questions allow the application to maintain context and provide
    faster responses. A recommendation is to provide parts of Chat’s previous
    responses in the next prompt. In the example above, I mentioned “Rails App,”
    as previously suggested. 


    ## Get started with GitLab Duo Chat 


    GitLab Duo Chat does more than help you write better code, it helps you
    navigate through problems and quickly find solutions. With the right prompts
    and context, you can build secure software faster.


    > Want to try GitLab Duo Chat? [Start your free
    trial](https://about.gitlab.com/gitlab-duo/#free-trial) today.
  category: AI/ML
  tags:
    - AI/ML
    - DevSecOps platform
    - DevSecOps
    - tutorial
config:
  slug: gitlab-duo-chat-101-get-more-done-on-gitlab-with-our-ai-assistant
  featured: true
  template: BlogPost
