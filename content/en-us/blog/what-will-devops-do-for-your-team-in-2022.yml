seo:
  title: What will DevOps do for your team in 2022?
  description: >-
    DevOps brings the technical wins but business is winning too, thanks to this
    modern software development strategy. Here's what our latest DevOps
    assessment found.
  ogTitle: What will DevOps do for your team in 2022?
  ogDescription: >-
    DevOps brings the technical wins but business is winning too, thanks to this
    modern software development strategy. Here's what our latest DevOps
    assessment found.
  noIndex: false
  ogImage: images/blog/hero-images/data.jpg
  ogUrl: https://about.gitlab.com/blog/what-will-devops-do-for-your-team-in-2022
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/what-will-devops-do-for-your-team-in-2022
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "What will DevOps do for your team in 2022?",
            "author": [{"@type":"Person","name":"Valerie Silverthorne"}],
            "datePublished": "2022-01-19",
          }

content:
  title: What will DevOps do for your team in 2022?
  description: >-
    DevOps brings the technical wins but business is winning too, thanks to this
    modern software development strategy. Here's what our latest DevOps
    assessment found.
  authors:
    - Valerie Silverthorne
  heroImage: images/blog/hero-images/data.jpg
  date: '2022-01-19'
  body: "\n\nOver the last six months, we’ve asked teams and individual contributors to assess their DevOps platform practices by answering a 20-question quiz. To date, more than 600 people have shared their experiences, providing a clear, and somewhat surprising, snapshot of DevOps as it’s done _today_. There are obvious technical wins, of course, but there are also glimpses of how DevOps and modern software development are driving business change. \n\nHere are some of the key takeaways:\n\n### DevOps is a stand up (and out) choice\t\n\nAlmost 35% of respondents say they’ve been doing DevOps for between one and three years, while 22% report they’ve been at DevOps less than a year. And 16% are in that DevOps sweet spot of between three and five years, while 15% are seasoned DevOps pros with more than five years of experience. \n\nDevOps, of course, enables faster and safer software development and it’s clearly taking teams and entire organizations along for the ride, with much greater levels of collaboration/planning and a commitment to cross-functional processes. Nearly one-quarter of respondents say everyone in their organization considers themselves to be part of the DevOps team. And 17% say security, test, and design have joined dev and ops to create their DevOps teams. \n\nBig changes are happening within those teams as well. Just shy of 30% say the traditional roles of “dev” and “ops” are definitely blurring and 16% report everyone on their team is “cross-functional”. Nearly 15% say dev, sec, ops, and test are all seeing roles change and blend together.\n\nWhen asked how teams handle planning and collaboration, 50% say their processes were either “long-established and effective” or “completely seamless and baked into everything.” Meanwhile, 43% are either just starting a planning/collaboration process or are well underway. \n\nTo put it another way, it appears DevOps drives faster releases *and* better planning and collaboration almost in equal measure. \n\n### A DevOps platform in 2022\n\nJust shy of 36% of quiz takers use an “out of the box” [DevOps platform](/solutions/devops-platform/), while only 7% are considering one. Nearly one-third of respondents say their DevOps platform is a “hybrid” affair of homegrown and purchased solutions, or what GitLab refers to as [DIY DevOps](/blog/2021/08/03/welcome-to-the-devops-platform-era/#phase-3"
  category: DevSecOps
  tags:
    - DevOps
    - collaboration
    - community
config:
  slug: what-will-devops-do-for-your-team-in-2022
  featured: false
  template: BlogPost
