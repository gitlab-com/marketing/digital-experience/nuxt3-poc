seo:
  title: >-
    You’re hired! Two GitLab contributors turn their success into full-time
    engineering roles
  description: >-
    As we continue to celebrate the 10th anniversary of the first commit to
    GitLab, here’s a look at how two highly active community members became
    enthusiastic team members.
  ogTitle: >-
    You’re hired! Two GitLab contributors turn their success into full-time
    engineering roles
  ogDescription: >-
    As we continue to celebrate the 10th anniversary of the first commit to
    GitLab, here’s a look at how two highly active community members became
    enthusiastic team members.
  noIndex: false
  ogImage: images/blog/hero-images/tanuki-bg-full.png
  ogUrl: >-
    https://about.gitlab.com/blog/you-are-hired-two-gitlab-contributors-turn-their-success-into-full-time-engineering-roles
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/you-are-hired-two-gitlab-contributors-turn-their-success-into-full-time-engineering-roles
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "You’re hired! Two GitLab contributors turn their success into full-time engineering roles",
            "author": [{"@type":"Person","name":"GitLab"}],
            "datePublished": "2021-11-12",
          }

content:
  title: >-
    You’re hired! Two GitLab contributors turn their success into full-time
    engineering roles
  description: >-
    As we continue to celebrate the 10th anniversary of the first commit to
    GitLab, here’s a look at how two highly active community members became
    enthusiastic team members.
  authors:
    - GitLab
  heroImage: images/blog/hero-images/tanuki-bg-full.png
  date: '2021-11-12'
  body: "[Greg Myers](https://gitlab.com/greg) and [Rajendra Kadam](https://gitlab.com/rkadam3) have something beyond their engineering roles at GitLab in common – both started out as GitLab contributors. We wanted to share their stories as part of our celebration around the 10th anniversary of the first commit to GitLab.\n\nMyers, a GitLab Senior Support Engineer, says his contributions started in 2018, when he first found his passion for helping other community forum members.\_\n\n“Most of my early contributions involved helping people set up, configure, and troubleshoot self-hosted GitLab installations,” Myers says.\n\nHe enjoyed this helper role so much he applied for an engineering position, but failed the technical interview and didn’t receive an offer. “I kept contributing to GitLab and helping others in the forum while I leveled up in my weak areas,” he says.\n\nKadam, a GitLab Back-end Engineer and [GitLab hero](/community/heroes/members/), started contributing to GitLab in Jan 2020 to learn more about Ruby on Rails and apply it to his then-workplace.\_\n\n“I did not stop after that since it is more than the code. I loved working with people at GitLab and the culture, even though I was not a full-time team member,” Kadam says.\n\nLike Kadam, Myers enjoyed being a part of the GitLab community. “The majority of my ‘code’ contributions back then were quite simple – fixing typos and markdown formatting issues in documentation,” he says. “I'd never contributed to an open source project of this size and caliber, and I was impressed by how easy and smooth it was to get involved and contribute.”\n\nHe remembers feeling “star-struck” when GitLab co-founder Dmitriy Zaporozhets personally responded in the comments to one of his first MRs.\n\nUsing what he learned as a contributor, Kadam earned a promotion from his employer. He went on to participate in [GitLab hackathons](/community/hackathon/), winning three in a series. His prominence in the GitLab community led him to be offered and to accept an internal engineering role in February 2021. Kadam blogged about the journey from being a contributor to a team member [on Medium](https://rajendraak.medium.com/how-i-got-a-job-at-gitlab-a3515214b74b).\n\nMyers, meanwhile, feeling more confident about his skills, took another shot at a team member role. “After four months, I reapplied for the support engineer position, and this time I got the job. Now it is my job to help others with GitLab and contribute to GitLab, and I love what I do,” Myers says.\n\nAs a Developer Relations Support counterpart, he helps others in the GitLab community forum and advocates for the GitLab wider community. And, as a GitLab Open Source Support Liaison, “I give back to open source communities I know and love,” he says.\n\nHe encourages others to not only contribute to the GitLab community but to help other forum members as he did. After all, you never know where those contributions can lead. “Being a GitLab community member and contributor led me to my dream job,” he says."
  category: DevSecOps
  tags:
    - DevOps
    - contributors
    - inside GitLab
config:
  slug: >-
    you-are-hired-two-gitlab-contributors-turn-their-success-into-full-time-engineering-roles
  featured: false
  template: BlogPost
