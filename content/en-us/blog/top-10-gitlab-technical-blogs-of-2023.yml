seo:
  title: Top 10 GitLab technical blogs of 2023
  description: >-
    2023 was a big year! Catch up on expert insights into DevSecOps, AI, CI/CD,
    and more.
  ogTitle: Top 10 GitLab technical blogs of 2023
  ogDescription: >-
    2023 was a big year! Catch up on expert insights into DevSecOps, AI, CI/CD,
    and more.
  noIndex: false
  ogImage: images/blog/hero-images/tanukilifecycle.png
  ogUrl: https://about.gitlab.com/blog/top-10-gitlab-technical-blogs-of-2023
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/top-10-gitlab-technical-blogs-of-2023
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Top 10 GitLab technical blogs of 2023",
            "author": [{"@type":"Person","name":"Sandra Gittlen"}],
            "datePublished": "2024-01-09",
          }

content:
  title: Top 10 GitLab technical blogs of 2023
  description: >-
    2023 was a big year! Catch up on expert insights into DevSecOps, AI, CI/CD,
    and more.
  authors:
    - Sandra Gittlen
  heroImage: images/blog/hero-images/tanukilifecycle.png
  date: '2024-01-09'
  body: >
    2023 brought fresh insights from experts across GitLab and beyond —  all of
    them focused on the challenges and opportunities facing DevSecOps teams.
    From Lockheed Martin to CARFAX, organizations are trying to understand and
    unlock the power of technologies such as artificial intelligence (AI),
    CI/CD, security automation, and more. Our experts provided tips, best
    practices, and tutorials to use throughout the software development
    lifecycle.


    Here are the top 10 technical blogs from what was an incredible year in
    DevSecOps innovation.


    **1. [Jenkins to GitLab: The ultimate guide to modernizing your CI/CD
    environment](https://about.gitlab.com/blog/2023/11/01/jenkins-gitlab-ultimate-guide-to-modernizing-cicd-environment/)**

    Looking for a smooth transition from Jenkins to GitLab? Follow this
    step-by-step tutorial to learn how GitLab's integrated CI/CD capabilities
    help deliver high-quality software faster.


    **2. [U.S. Navy Black Pearl: Lessons in championing
    DevSecOps](https://about.gitlab.com/blog/2023/12/12/u-s-navy-black-pearl-lessons-in-championing-devsecops/)**

    Sigma Defense's director of engineering details what it's like to manage the
    U.S. Navy's Black Pearl, which uses GitLab as its DevSecOps platform. The
    DevSecOps champion relays his experience implementing DevSecOps and the
    benefits of that decision.


    **3. [Quickstart guide for GitLab Remote Development
    workspaces](https://about.gitlab.com/blog/2023/06/26/quick-start-guide-for-gitlab-workspaces/)**

    Enabling developers to work in their preferred environments empowers
    DevSecOps teams to build and deliver software more efficiently. With these
    quickstart instructions, developers can create a workspace, use the Web IDE
    Terminal to install dependencies or start their server, and view their
    running application.


    **4. [Introducing the GitLab CI/CD Catalog
    Beta](https://about.gitlab.com/blog/2023/12/21/introducing-the-gitlab-ci-cd-catalog-beta/)**

    CI/CD catalogs are a game-changer, allowing developers to discover,
    integrate, and share pre-existing CI/CD components with ease. This tutorial
    shows how to get the most from this new DevSecOps platform feature.


    **5. [Combine GitLab Flow and GitLab Duo for a workflow
    powerhouse](https://about.gitlab.com/blog/2023/07/27/gitlab-flow-duo/)**

    GitLab Flow and GitLab Duo can help organizations achieve significant
    improvements in end-to-end workflow efficiency that can lead to higher
    levels of productivity, deployment frequency, code quality and overall
    security, and production resiliency and availability. Find out how with this
    step-by-step guide.


    **6. [Efficient DevSecOps workflows: Hands-on python-gitlab API
    automation](https://about.gitlab.com/blog/2023/02/01/efficient-devsecops-workflows-hands-on-python-gitlab-api-automation/)**

    The python-gitlab library is a useful abstraction layer for the GitLab API.
    Dive into hands-on examples and best practices in this tutorial.


    **7. [Building GitLab with GitLab: Why there is no MLOps without
    DevSecOps](https://about.gitlab.com/blog/2023/10/05/there-is-no-mlops-without-devsecops/)**

    At GitLab, we believe in the power of MLOps, especially when combined with
    DevSecOps. So follow along as our data scientists adopt DevSecOps practices
    and enjoy the benefits of automation, repeatable workflows, standardization,
    and automatic provisioning of infrastructure.


    **8. [Explore the Dragon Realm: Build a C++ adventure game with a little
    help from
    AI](https://about.gitlab.com/blog/2023/08/24/building-a-text-adventure-using-cplusplus-and-code-suggestions/)**

    Readers are invited to create a mystical world while learning how to
    integrate AI into their coding environment. This tutorial demonstrates how
    to use GitLab Duo Code Suggestions to create a text-based adventure game,
    including magical locations to visit and items to procure, using C++. 


    **9. [How GitLab's Red Team automates C2
    testing](https://about.gitlab.com/blog/2023/11/28/how-gitlabs-red-team-automates-c2-testing/)**

    The GitLab Red Team conducts security exercises that simulate real-world
    threats. They apply professional development practices to using the same
    open source C2 tools as threat actors. In this tutorial, the GitLab Red Team
    shares how they implement continuous testing for the Mythic framework, their
    design philosophy, and a public project that can be forked for use by other
    Red Teams.


    **10. [Building GitLab with GitLab: How GitLab.com inspired
    Dedicated](https://about.gitlab.com/blog/2023/08/03/building-gitlab-with-gitlabcom-how-gitlab-inspired-dedicated/)**

    The design of GitLab Dedicated, our single-tenancy SaaS version of the
    DevSecOps platform, came from the lessons learned while building GitLab.com.
    In this peek behind the curtains, learn the considerations that sparked
    different decisions regarding automation, databases, monitoring,
    availability, and more – and what the outcome was.


    Sign up for the GitLab newsletter using the form to the right to receive the
    latest blogs right in your inbox.
  category: DevSecOps
  tags:
    - AI/ML
    - CI/CD
    - DevSecOps platform
    - DevSecOps
    - security
    - tutorial
config:
  slug: top-10-gitlab-technical-blogs-of-2023
  featured: true
  template: BlogPost
