seo:
  title: New names for GitLab self-managed pricing tiers
  description: >-
    Understand GitLab's pricing tiers and know which features your subscription
    gives you access to.
  ogTitle: New names for GitLab self-managed pricing tiers
  ogDescription: >-
    Understand GitLab's pricing tiers and know which features your subscription
    gives you access to.
  noIndex: false
  ogImage: images/blog/hero-images/gitlab-tiers-cover.png
  ogUrl: https://about.gitlab.com/blog/gitlab-tiers
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/gitlab-tiers
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "New names for GitLab self-managed pricing tiers",
            "author": [{"@type":"Person","name":"William Chia"}],
            "datePublished": "2018-04-20",
          }

content:
  title: New names for GitLab self-managed pricing tiers
  description: >-
    Understand GitLab's pricing tiers and know which features your subscription
    gives you access to.
  authors:
    - William Chia
  heroImage: images/blog/hero-images/gitlab-tiers-cover.png
  date: '2018-04-20'
  body: "\n\n_Note: We've continued to iterate on our platform and pricing model since this blog post was published in 2018. To see what's new (including everything from security and container-focused capabilities to guest users), check out our [platform](https://about.gitlab.com/platform/),\_[pricing](https://about.gitlab.com/pricing/), and [why GitLab](https://about.gitlab.com/why-gitlab/) pages._\n\nAt GitLab, [iteration is one of our ore values](/handbook/values/#iteration). We’ve recently iterated on the names of our self-managed pricing tiers, so [Marcia](/company/team/#XMDRamos) and I got together and wrote this post\nto catch you up on the current options. We’ll explain each tier, and share how to figure out\nwhich features your subscription gives you access to.\n\n- [GitLab deployment options](#gitlab-deployment-options)\n- [GitLab self-hosted](#gitlab-self-managed)\n- [GitLab.com](#gitlabcom)\n- [Repository architecture](#repository-architecture)\n- [Subscription model](#subscription-model)\n- [Examples of use cases](#examples)\n\n## GitLab deployment options\n\nTo use GitLab, you have two options:\n\n- **GitLab self-managed**: Install, administer, and maintain your own GitLab self-managed instance.\n- **GitLab.com**: GitLab's SaaS offering. You don't need to install anything to use GitLab.com,\nyou only need to [sign up](https://gitlab.com/users/sign_in) and start using GitLab\nstraight away.\n\n### GitLab self-managed\n\nWith GitLab self-managed, you deploy your own GitLab instance on-premises or in the cloud. From\nbare metal to Kubernetes, you can [install GitLab almost\nanywhere](/install/). GitLab self-managed has both [free\nand paid options](/pricing/):\n**Core**, **Starter**, **Premium**, and **Ultimate**.\n\nYou can see a full list of features in each self-managed tier on the [self-managed feature\ncomparison](/pricing/feature-comparison/) page. For more details on storage amounts and CI/CD minutes per month, see our [pricing page](https://about.gitlab.com/pricing/).\n\n### GitLab.com\n\nGitLab.com is hosted, managed, and administered by GitLab, Inc., with\n[free and paid options](/pricing/) for individuals\nand teams: **Free**, **Bronze**, **Silver**, and **Gold**.\n\nTo support the open source community and encourage the development of\nopen source projects, GitLab grants access to **Gold** features\nfor all GitLab.com **public** projects, regardless of the subscription.\n\nYou can see a full list of features in each GitLab.com tier on the [GitLab.com feature\ncomparison](/pricing/feature-comparison/) page.\n\n### Repository architecture\n\nWe develop GitLab from two repositories, one for GitLab Community Edition (CE)\nand another for GitLab Enterprise Edition (EE):\n\n- [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/): open source code, [MIT-based\nlicense](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/LICENSE), from which we deliver\nGitLab CE packages.\n- [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/): open core code, [proprietary\nlicense](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/LICENSE), from which we deliver\nGitLab EE packages.\n\nGitLab EE grants you access to features by installing a license key. You\ncan also install GitLab EE and run it for free without a license key which will give you\naccess to the same features as CE. This makes it easier to upgrade later on.\n\nVisit the CE vs EE page to see [which GitLab installation method to\nchoose](/install/ce-or-ee/).\n\n### Subscription model\n\nGitLab Core contains all of the open source features of GitLab. Whether you are running GitLab\nCE or GitLab EE without a license key, you'll get access to the same Core features. The\nproprietary features of EE are unlocked by purchasing a license key.\n\nTiers are additive:\n- Starter contains all the features of Core\n- Premium contains all the features of Starter and Core\n- Ultimate contains all of the features of Premium, Starter, and Core\n\n![GitLab Core, Starter, Premium, Ultimate](https://about.gitlab.com/images/blogimages/gitlab-tiers-repos-and-tiers.jpg)\n\n### Examples\n\n- Consider a user of [GitLab Premium](/pricing/premium/) who wants to contribute to a given feature present in GitLab Core, e.g. Issue Boards. The code is submitted to the CE repo, therefore, it's open source code. The master branch of GitLab CE is then merged into GitLab EE. The CE code will be available to this Premium user in the next release.\n- Consider a user of GitLab Premium who wants to contribute to a given feature present only in Premium, e.g., Geo. The code is submitted directly to the EE repo, therefore, it's proprietary. The same is valid for Starter and Ultimate features.\n\n### Use cases\n\n#### GitLab self-managed use cases\n\n- I installed GitLab CE: I’m a Core user. I have access to Core features. The software I’m using is 100 percent open source.\n- I installed GitLab EE: the software I’m using is open core- it includes both open source and proprietary code.\n  - I don't have a subscription: I have access to Core features.\n  - I have a Starter subscription: I have access to Starter features.\n  - I have a GitLab Premium subscription: I have access to Premium features.\n  - I have a GitLab Ultimate subscription: I have access to Ultimate features.\n- I have a trial installation: I installed GitLab EE, and I’m an Ultimate user during the valid period of the trial. If the trial period expires and I don’t get a paid subscription (Starter, Premium, or Ultimate), I’ll become a Core user, with access to Core features.\n\n#### GitLab.com use cases\n\n- I use GitLab.com, a huge installation of GitLab EE. I’m using proprietary software.\n- I don’t have access to administration features as GitLab.com is administered by GitLab, Inc.\n- _Subscriptions_:\n  - I have a Bronze subscription: my private projects get access to Bronze features. My public projects get access to Gold features.\n  - I have a Silver subscription: my private projects get access to Silver features. My public projects get access to Gold features.\n  - I have a Gold subscription: my private projects get access to Gold features, as well as my public projects.\n  - I don’t have any paid subscriptions: I’m a Free GitLab.com user:\n      - I have access to Free features for private projects.\n      - I have access to Gold features for public projects.\n\n_Questions, comments? Let us know what you think below._\n"
  category: Company
  tags:
    - inside GitLab
    - features
config:
  slug: gitlab-tiers
  featured: false
  template: BlogPost
