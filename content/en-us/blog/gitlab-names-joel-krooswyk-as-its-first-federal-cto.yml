seo:
  title: GitLab names Joel Krooswyk as its first Federal CTO
  description: New role reaffirms company’s commitment to the public sector.
  ogTitle: GitLab names Joel Krooswyk as its first Federal CTO
  ogDescription: New role reaffirms company’s commitment to the public sector.
  noIndex: false
  ogImage: images/blog/hero-images/bab_cover_image.jpg
  ogUrl: >-
    https://about.gitlab.com/blog/gitlab-names-joel-krooswyk-as-its-first-federal-cto
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/gitlab-names-joel-krooswyk-as-its-first-federal-cto
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "GitLab names Joel Krooswyk as its first Federal CTO",
            "author": [{"@type":"Person","name":"GitLab"}],
            "datePublished": "2022-11-14",
          }

content:
  title: GitLab names Joel Krooswyk as its first Federal CTO
  description: New role reaffirms company’s commitment to the public sector.
  authors:
    - GitLab
  heroImage: images/blog/hero-images/bab_cover_image.jpg
  date: '2022-11-14'
  body: >-
    [Gitlab Federal](/solutions/public-sector/), LLC, provider of The One DevOps
    Platform for the public sector, announced that [Joel
    Krooswyk](https://gitlab.com/jkrooswyk), former Senior Manager of Solutions
    Architecture, has been named Federal CTO.


    ![Photo of Joel
    Krooswyk](https://about.gitlab.com/images/blogimages/krooswyk.jpg){:
    .shadow.small.left.wrap-text}


    “The creation of the Federal CTO position recognizes the importance of the
    public sector in the world of DevSecOps. Joel’s experience allows him to
    provide expert insight to government agencies as they seek guidance on
    DevOps practices, building software factories, meeting compliance
    requirements and more,” says [Bob Stevens](https://gitlab.com/bstevens1),
    Vice President of Public Sector at GitLab. “We are excited to reaffirm our
    commitment to the public sector through this new role and Joel’s
    appointment.”


    As Federal CTO, Krooswyk will ensure that GitLab has a voice in developing
    key [DevSecOps](/topics/devsecops/) practices coming from standards bodies,
    Congressional committees, industry working groups, and other influential
    organizations. He also will assist GitLab in continuing to build and
    strengthen relationships with federal DevSecOps professionals to help them
    streamline and secure their software development environments with a
    DevSecOps platform.


    “This is an exciting time in DevSecOps, and the federal government is on the
    leading edge, helping navigate such challenging issues as software supply
    chain security and regulatory compliance. I am thrilled to step into this
    new role and to be GitLab’s voice at the table, ensuring that our software
    development and security technology and practices are reflected in efforts
    across the public sector,” Krooswyk says.


    Krooswyk has actively been involved in GitLab’s growth since 2017. He has 25
    years of experience in the software industry. His experience spans
    development, QA, product management, portfolio planning, and technical
    sales, and he has written a half million lines of unique code throughout his
    career. Joel holds a B.S. in Electrical Engineering from Purdue University
    as well as multiple industry certifications.
  category: News
  tags:
    - news
    - DevOps
    - inside GitLab
    - public sector
config:
  slug: gitlab-names-joel-krooswyk-as-its-first-federal-cto
  featured: false
  template: BlogPost
