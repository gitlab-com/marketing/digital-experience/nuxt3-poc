seo:
  title: What we're reading
  description: >-
    GitLab team-members are a passionate group of learners who enjoy reading to
    strengthen their skills, develop new techniques, and enhance their
    knowledge.
  ogTitle: What we're reading
  ogDescription: >-
    GitLab team-members are a passionate group of learners who enjoy reading to
    strengthen their skills, develop new techniques, and enhance their
    knowledge.
  noIndex: false
  ogImage: images/blog/hero-images/gitlabreading.jpg
  ogUrl: https://about.gitlab.com/blog/what-we-re-reading
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/what-we-re-reading
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "What we're reading",
            "author": [{"@type":"Person","name":"Suri Patel"}],
            "datePublished": "2018-08-27",
          }

content:
  title: What we're reading
  description: >-
    GitLab team-members are a passionate group of learners who enjoy reading to
    strengthen their skills, develop new techniques, and enhance their
    knowledge.
  authors:
    - Suri Patel
  heroImage: images/blog/hero-images/gitlabreading.jpg
  date: '2018-08-27'
  body: >+

    At GitLab, we ❤️ reading. Here are a few of our recent pageturners.


    ### How Rust’s standard library was vulnerable for years and nobody noticed

    [Nick Thomas](/company/team/#nick.thomas), Staff Developer, enjoyed

    [this
    article](https://medium.com/@shnatsel/how-rusts-standard-library-was-vulnerable-for-years-and-nobody-noticed-aebf0503c3d6)

    on Rust, a new systems programming language. Nick commented, "It was very

    interesting from a general-security point of view, especially the

    'everything is broken' and 'here is how security advisories actually work'
    bits."


    ### Designing Delivery: Rethinking IT in the Digital Service Economy

    [Kristie McGoldrick](/company/team/#Krist_McG), Solutions Architect, has
    been devouring

    [this
    book](https://www.amazon.com/Designing-Delivery-Rethinking-Digital-Service/dp/1491949880/ref=mt_paperback?_encoding=UTF8&me=&qid=),

    finding it "very applicable to GitLab."


    ### It's the Future

    [Lin Jen-Shin](/company/team/#godfat-gitlab), Developer, fondly reflects on

    [this CircleCI blog post](https://circleci.com/blog/its-the-future/), saying

    "I think I'll remember this post forever. One person is trying to persuade

    a teammate, who just wants to get things done right now, to use future
    technology.

    The future tech is not mature enough to get things done easily, so it

    overcomplicates a lot of things, and the teammate just wants something
    simple. It's

    pretty technical, but it's funny! That future tech is getting closer to
    becoming

    mature now, and GitLab is  trying to use Kubernetes and collaborating with
    Google

    to make this tech better."


    ### The Phoenix Project: A Novel about IT, DevOps, and Helping Your Business
    Win


    [Rebecca Dodd](/company/team/#rebecca), Content Editor, recently read

    ["The Phoenix
    Project"](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509)

    and confidently declared that, "I think we’ve

    all read 'The Phoenix Project.'"


    ### The DevOps Handbook: How to Create World-Class Agility, Reliability, and
    Security in Technology Organizations


    [Mike Miranda](/company/team/#zmikemiranda), Sales Development
    Representative, just

    picked up ["The DevOps
    Handbook"](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations-ebook/dp/B01M9ASFQ3/)

    to understand the needs of IT leaders.

  category: Culture
  tags:
    - inside GitLab
config:
  slug: what-we-re-reading
  featured: false
  template: BlogPost
