seo:
  title: The road to smarter code reviewer recommendations
  description: >-
    Machine learning is coming to GitLab's code review process. Here's what you
    need to know, and how you can help!
  ogTitle: The road to smarter code reviewer recommendations
  ogDescription: >-
    Machine learning is coming to GitLab's code review process. Here's what you
    need to know, and how you can help!
  noIndex: false
  ogImage: images/blog/hero-images/retrospectivesgitlabpost.jpg
  ogUrl: >-
    https://about.gitlab.com/blog/the-road-to-smarter-code-reviewer-recommendations
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/the-road-to-smarter-code-reviewer-recommendations
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "The road to smarter code reviewer recommendations",
            "author": [{"@type":"Person","name":"Taylor McCaslin"}],
            "datePublished": "2022-01-04",
          }

content:
  title: The road to smarter code reviewer recommendations
  description: >-
    Machine learning is coming to GitLab's code review process. Here's what you
    need to know, and how you can help!
  authors:
    - Taylor McCaslin
  heroImage: images/blog/hero-images/retrospectivesgitlabpost.jpg
  date: '2022-01-04'
  body: >

    You may recall back in June 2021, we [announced the acquisition of
    UnReview](/press/releases/2021-06-02-gitlab-acquires-unreview-machine-learning-capabilities/),
    a machine learning (ML) based solution for automatically identifying
    appropriate expert [code reviewers](/stages-devops-lifecycle/create/) and
    controlling review workloads and distribution of knowledge.


    At the start of the new year we wanted to provide an update on our
    integration progress and our wider vision of leveraging machine learning to
    make GitLab's [DevOps Platform](/solutions/devops-platform/) smarter. You
    see, the acquisition of UnReview also was the initial staffing of [our new
    ModelOps stage](/direction/modelops/).


    ### Our Newest DevOps Stage


    This new stage, which we’ve named ModelOps, is focused on enabling and
    empowering data science workloads on GitLab. GitLab ModelOps aims to bring
    data science into GitLab both within existing features to make them smarter
    and more intelligent, but also empowering GitLab customers to build and
    integrate data science workloads within GitLab.


    So what is ModelOps you may wonder? We view ModelOps as an all encompassing
    term to cover the entire end to end lifecycle of artificial intelligence
    models. We wanted to set our vision wide to fully cover everything needed to
    power data science workloads. DataOps is the processing of data workloads
    (think traditional ELT: extract, load, transform) and MLOps is the building,
    training, and deployment of machine learning models. If you’re confused
    don’t worry, it’s a lot to wrap your head around.


    ![a look at the stages of
    MLOps](https://about.gitlab.com/images/blogimages/MLops.png){:
    .shadow.small.center}


    Today our DevOps Platform helps plan, build, test, secure, deploy, and
    monitor traditional software. Now we want to extend our DevOps Platform to
    include AI and ML workloads. If this is interesting to you, be sure to check
    out our recent Contribute talk where we dive deeper into plans for our
    ModelOps stage.


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/C08QVI99JLo" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ### UnReview as our first feature


    So what does this have to do with UnReview? Our acquisition of UnReview is
    going to be our first [AI Assisted](/direction/ai-powered/) group’s feature:
    suggested reviewers within [GitLab’s existing reviewers
    experience](/blog/2020/10/13/merge-request-reviewers/). Today, a developer
    in a merge request has to manually choose a reviewer to look at their code.
    With UnReview we can leverage the contribution history for a project and
    recommend someone well-suited for code review of your specific changes.


    Here’s an early mockup (and it may differ from our final UI) of how we’re
    thinking about this integration:


    ![an early mockup of our
    UI](https://about.gitlab.com/images/blogimages/codereviewmockup.png){:
    .shadow.small.left}


    The UnReview algorithm looks at a variety of data points from your project’s
    contribution history to suggest an appropriate reviewer. We’re still in the
    early days of this integration but our initial internal testing shows great
    suggestions.


    ### Customer beta coming soon!


    This leads me to a final question, might you want to be one of our first
    customers to try this new code review experience? In early 2022, we’ll begin
    a private customer beta of this new functionality. If interested, [fill out
    this form to express
    interest](https://docs.google.com/forms/d/e/1FAIpQLScpmCwpwyBr0GrXxBQ6vE02eokclFAs9lFk_g5dcyuGaHqFuQ/viewform).
    Do note that we can’t accept everyone and we’ll focus initially on customer
    profiles that are well suited for the initial version of the suggestion
    algorithm. Our only ask is we’d like to find customers with active projects
    that have a healthy number of contributors. The model currently works best
    on larger repositories with lots of contributors where it may not
    immediately be clear who is an ideal code reviewer.


    We can’t wait for customers to begin using this new reviewer suggestion
    experience and will be providing more updates in early 2022.
  category: DevSecOps
  tags:
    - DevOps
    - integrations
    - workflow
    - AI/ML
config:
  slug: the-road-to-smarter-code-reviewer-recommendations
  featured: false
  template: BlogPost
