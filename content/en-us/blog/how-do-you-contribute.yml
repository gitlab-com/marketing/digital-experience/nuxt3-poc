seo:
  title: How do you contribute?
  description: >-
    Your contribution graph captures a moment in time like few things can, and
    we want to celebrate it.
  ogTitle: How do you contribute?
  ogDescription: >-
    Your contribution graph captures a moment in time like few things can, and
    we want to celebrate it.
  noIndex: false
  ogImage: images/blog/hero-images/contribute-social-cover.png
  ogUrl: https://about.gitlab.com/blog/how-do-you-contribute
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/how-do-you-contribute
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "How do you contribute?",
            "author": [{"@type":"Person","name":"Emily von Hoffmann"}],
            "datePublished": "2019-05-07",
          }

content:
  title: How do you contribute?
  description: >-
    Your contribution graph captures a moment in time like few things can, and
    we want to celebrate it.
  authors:
    - Emily von Hoffmann
  heroImage: images/blog/hero-images/contribute-social-cover.png
  date: '2019-05-07'
  body: >


    Contribution graphs tell the story of your year.


    Especially when you’re a daily user, they have a funny way of capturing not
    only professional milestones, but personal ones too. Vacations, sabbaticals,
    and family leave are all captured by characteristic little gray squares
    where no work is done. Equally telling are the times when work overflows
    into the weekends, or when it forms those satisfyingly deep navy streaks for
    consecutive days with over 30 commits each.


    ![Inner narration when I look at my contribution
    graph](https://about.gitlab.com/images/blogimages/contribute-social-launch/evh_contribution_graph.png){:
    .shadow.center.medium}

    *<small><center>Some inner narration when I look at my contribution
    graph</center></small>*


    Using GitLab, you don’t just have to commit code in order to get the visual
    feedback for all of your hard work. Actions like commenting, opening, and
    closing issues and merge requests are also included. Your contribution graph
    is as unique as your fingerprint. It captures a moment in time like few
    things can, and we want to celebrate it. Check out some examples from the
    GitLab team:


    #### Tackling weekend side projects 🙌


    ![Weekend side
    projects](https://about.gitlab.com/images/blogimages/contribute-social-launch/brendan-activity.png){:
    .shadow.center.medium}



    #### Managing a team 🗣


    ![Managing a
    team](https://about.gitlab.com/images/blogimages/contribute-social-launch/lee-activity.png){:
    .shadow.center.medium}



    #### Going on parental leave 🤗


    ![Parental
    leave](https://about.gitlab.com/images/blogimages/contribute-social-launch/brittany-activity.png){:
    .shadow.center.medium}



    #### Producing GitLab videos 🎥


    ![Producing GitLab
    videos](https://about.gitlab.com/images/blogimages/contribute-social-launch/aricka-activity.png){:
    .shadow.center.medium}



    #### Cultivating perfect weekends 🙏


    ![Cultivating perfect
    weekends](https://about.gitlab.com/images/blogimages/contribute-social-launch/sean-activity.png){:
    .shadow.center.medium}



    #### Managing this blog! 📝


    ![Managing this
    blog](https://about.gitlab.com/images/blogimages/contribute-social-launch/rebecca-activity.png){:
    .shadow.center.medium}



    #### Starting a new job 💜


    ![Starting a new
    job](https://about.gitlab.com/images/blogimages/contribute-social-launch/jeff-activity.png){:
    .shadow.center.medium}



    #### Advancing in your role 💪


    ![Adjusting to a new
    role](https://about.gitlab.com/images/blogimages/contribute-social-launch/emilie-activity.png){:
    .shadow.center.medium}



    #### Flexing your skills 💻


    ![Mastering your
    craft](https://about.gitlab.com/images/blogimages/contribute-social-launch/taurie-activity.png){:
    .shadow.center.medium}



    ## Everyone can #contribute


    At GitLab, we believe that everyone can contribute, and that means you!
    Whether you build software yourself or enable others who do; whether you
    write code, contracts, or novels; whether you manage a team at work or in
    your own household, your tasks and timelines can find a home in GitLab.


    So tell us about your contributions! While we’re at GitLab
    [Contribute](/events/gitlab-contribute/) in New Orleans, we want to hear
    about all the ways that you contribute. Tweet a screenshot of your activity
    using #contribute, and tell us about your year as represented by your GitLab
    profile. Tweeting with the hashtag will enter you to win a swag bag to deck
    you out in GitLab gear from head to toe 😍


    We can't wait to hear from you! Tweet us
    [@gitlab](https://twitter.com/gitlab).
  category: Open Source
  tags:
    - community
config:
  slug: how-do-you-contribute
  featured: false
  template: BlogPost
