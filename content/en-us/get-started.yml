seo:
  title: 'Get started with GitLab'
  description: 'How would you like to get started?'
content:
  - componentName: CommonHero
    componentContent:
      title: 'Get Started with GitLab'
      description: New to GitLab and not sure where to start? We'll walk you through the basics so you know what to expect along the way.
      primaryButton:
        text: 'Start your free trial'
        config:
          href: 'https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fplatform/'
          dataGaName: 'free trial'
          dataGaLocation: 'hero'
      secondaryButton:
        text: 'Watch a demo'
        config:
          href: '/demo/'
          dataGaName: 'demo'
          dataGaLocation: 'hero'
      image:
        altText: 'The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).'
        config:
          src: '/images/working-at-desk.jpeg'
          theme: 'rounded'
      config:
        background: purple
  - componentName: GetStartedLinks
    componentContent:
      title: 'Quick setup checklists'
      blocks:
        - name: 'Put your business first'
          links:
            - text: 'For Small Business'
              config:
                url: '/get-started/small-business/'
                dataGaName: 'small business'
                dataGaLocation: 'links'
                icon: 'ClipboardChecks'
            - text: 'For Enterprise'
              config:
                url: '/get-started/enterprise/'
                dataGaName: 'enterprise'
                dataGaLocation: 'links'
                icon: 'ClipboardChecks'
            - text: 'For building your business case'
              config:
                url: '/get-started/build-business-case/'
                dataGaName: 'business case'
                dataGaLocation: 'links'
                icon: 'ClipboardChecks'
            - text: 'Why GitLab'
              config:
                url: '/why-gitlab/'
                dataGaName: 'why gitlab'
                dataGaLocation: 'links'
                icon: 'DocsAlt'
        - name: 'Get the most from GitLab'
          links:
            - text: 'For scaling your GitLab usage'
              config:
                url: 'https://docs.gitlab.com/ee/development/scalability'
                dataGaName: 'for scaling your gitlab usage'
                dataGaLocation: 'links'
                icon: 'Increase'
            - text: 'Get Started with Continuous Integration'
              config:
                url: '/get-started/continuous-integration/'
                dataGaName: 'get started with continuous integration'
                dataGaLocation: 'links'
                icon: 'Cog'
            - text: 'For setting up security and compliance'
              config:
                url: '/solutions/continuous-software-security-assurance/'
                dataGaName: 'for setting up security and compliance'
                dataGaLocation: 'links'
                icon: 'CogCode'
            - text: 'For installing newer versions'
              config:
                url: 'https://docs.gitlab.com/ee/update/'
                dataGaName: 'for installing newer versions'
                dataGaLocation: 'links'
                icon: 'CogCheck'
            - text: 'For up-tiering'
              config:
                url: '/pricing/'
                dataGaName: 'for up tiering'
                dataGaLocation: 'links'
                icon: 'Increase'
            - text: 'For using more capabilities'
              config:
                url: '/features/'
                dataGaName: 'for using more capabilities'
                dataGaLocation: 'links'
                icon: 'Cogs'
        - name: 'Make a seamless transition'
          links:
            - text: 'For transitioning from GitHub'
              config:
                url: 'https://docs.gitlab.com/ee/user/project/import/github.html'
                dataGaName: 'for transitioning from github'
                dataGaLocation: 'links'
                icon: 'DigitalTransformation'
            - text: 'For importing your project issues from Jira'
              config:
                url: 'https://docs.gitlab.com/ee/user/project/import/jira.html'
                dataGaName: 'for importing your project issues from jira'
                dataGaLocation: 'links'
                icon: 'DigitalTransformation'
            - text: 'For migrating other projects to GitLab'
              config:
                url: 'https://docs.gitlab.com/ee/user/project/import/'
                dataGaName: 'for migrating other projects to gitLab'
                dataGaLocation: 'links'
                icon: 'DigitalTransformation'
      config:
        image: true
  - componentName: GetStartedResources
    componentContent:
      title: 'More Resources'
      cards:
        - title: 'GitLab Docs'
          description: 'Documentation for GitLab Community Edition, GitLab Enterprise Edition, Omnibus GitLab, and GitLab Runner.'
          button:
            text: 'Visit Docs'
            config:
              url: 'https://docs.gitlab.com/'
              dataGaName: 'visit docs'
              dataGaLocation: 'resource cards'
          config:
            icon: 'DocsAlt'
        - title: 'Developer Portal'
          description: 'Documentation for contributors to the GitLab Project -- information about our codebase, APIs, webhooks, design system, UI framework, and more!'
          button:
            text: 'See Developer Portal'
            config:
              url: 'https://developer.gitlab.com/'
              dataGaName: 'developer portal'
              dataGaLocation: 'resource cards'
          config:
            icon: 'Code'
        - title: 'Blog'
          description: 'Visit the GitLab blog to learn about releases, applications, contributions, news, events, and more.'
          button:
            text: 'Read the Blog'
            config:
              url: '/blog/'
              dataGaName: 'blog'
              dataGaLocation: 'resource cards'
          config:
            icon: 'DocPencilAlt'
  - componentName: CommonNextSteps
