seo:
  title: 'Platform'
  description: 'Learn more about how the GitLab platform can help teams collaborate and build software faster.'
config:
  enableAnimations: true
content:
  - componentName: CommonHero
    componentContent:
      tagline: 'The most comprehensive'
      titleHighlight: 'AI-powered'
      title: 'DevSecOps Platform'
      description: 'Deliver better software faster with one platform for your entire software delivery lifecycle.'
      secondaryButton:
        text: 'Get Free Trial'
        config:
          href: 'https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com%2Fplatform/'
          dataGaName: 'free trial'
          dataGaLocation: 'hero'
      tertiaryButton:
        text: 'Learn about pricing'
        config:
          href: '/pricing/'
          dataGaName: 'pricing'
          dataGaLocation: 'hero'
      image:
        altText: 'The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).'
        config:
          src: '/images/loop-shield-duo.svg'
      config:
        theme: dark
  - componentName: CommonTable
    componentContent:
      columns:
        - name: 'Planning'
          icon:
            altText: 'Calendar'
            config:
              name: 'PlanAlt2'
          config:
            href: '/solutions/visibility-measurement/'
          features:
            - name: 'DevOps Reports'
              config:
                href: '/solutions/visibility-measurement/#devops-reports'
            - name: 'DORA Metrics'
              config:
                href: '/solutions/value-stream-management/dora/'
            - name: 'Value Stream Management'
              config:
                href: '/solutions/value-stream-management/'
            - name: 'Value Stream Forecasting'
              config:
                href: '/solutions/visibility-measurement/#value-stream-forecasting'
            - name: 'Service Desk'
              config:
                href: '/solutions/visibility-measurement/#service-desk'
            - name: 'Wiki'
              config:
                href: '/solutions/visibility-measurement/#wiki'
            - name: 'Portfolio Management'
              config:
                href: '/solutions/visibility-measurement/#portfolio-management'
            - name: 'Team Planning'
              config:
                href: '/solutions/visibility-measurement/#team-planning'
            - name: 'Generate issue description'
              config:
                href: '/solutions/visibility-measurement/#generate-issue-description'
            - name: 'Discussion Summary'
              config:
                href: '/solutions/visibility-measurement/#discussion-summary'
            - name: 'Design Management'
              config:
                href: '/solutions/visibility-measurement/#team-planning'
          replacement:
            text: Replacement for
            product: Jira
        - name: 'Source Code Management'
          icon:
            altText: 'Cog'
            config:
              name: 'CogCode'
          config:
            href: '/solutions/delivery-automation/'
          features:
            - name: 'Remote Development'
              config:
                href: '/solutions/delivery-automation/#remote-development'
            - name: 'Source Code Management'
              config:
                href: '/solutions/source-code-management/'
            - name: 'Web IDE'
              config:
                href: '/solutions/delivery-automation/#web-ide'
            - name: 'GitLab CLI'
              config:
                href: '/solutions/delivery-automation/#gitlab-cli'
            - name: 'Code Review Workflow'
              config:
                href: '/solutions/delivery-automation/#code-review-workflow'
            - name: 'Code Suggestions'
              config:
                href: '/solutions/delivery-automation/#code-suggestions'
            - name: 'Code Explanation'
              config:
                href: '/solutions/delivery-automation/#code-explanation'
            - name: 'Code Review Summary'
              config:
                href: '/solutions/delivery-automation/#code-review-summary'
            - name: 'Test Generation'
              config:
                href: '/solutions/delivery-automation/#test-generation'
            - name: 'Code Refactorization'
              config:
                href: '/solutions/source-code-management/#capabilities'
            - name: 'GitLab Duo for the CLI'
              config:
                href: '/solutions/source-code-management/#capabilities'
          replacement:
            text: Replacement for
            product: GitHub
        - name: 'Continuous Integration'
          icon:
            altText: 'Automation'
            config:
              name: 'AutomatedCodeAlt'
          config:
            href: '/solutions/delivery-automation/'
          features:
            - name: 'Secrets Management'
              config:
                href: '/solutions/delivery-automation/#secrets-management'
            - name: 'Review Apps'
              config:
                href: '/solutions/delivery-automation/#review-apps'
            - name: 'Code Testing and Coverage'
              config:
                href: '/solutions/delivery-automation/#code-testing-and-coverage'
            - name: 'Merge Trains'
              config:
                href: '/solutions/delivery-automation/#merge-trains'
            - name: 'Suggested Reviewers'
              config:
                href: '/solutions/delivery-automation/#suggested-reviewers'
            - name: 'Merge Request Summary'
              config:
                href: '/solutions/delivery-automation/#merge-requests'
            - name: 'Root Cause Analysis'
              config:
                href: '/solutions/delivery-automation/#root-cause-analysis'
            - name: 'Discussion Summary'
              config:
                href: '/solutions/delivery-automation/#discussion-summary'
            - name: 'Merge Commit Message Generation'
              config:
                href: '/solutions/delivery-automation/#merge-requests'
            - name: 'Pipeline Composition and Component Catalog'
              config:
                href: '/solutions/delivery-automation/#cicd-components'
          replacement:
            text: Replacement for
            product: Jenkins
        - name: 'Security'
          icon:
            altText: 'Lock'
            config:
              name: 'SecureAlt2'
          config:
            href: '/solutions/security-compliance/'
          features:
            - name: 'Container Scanning'
              config:
                href: '/solutions/security-compliance/#container-scanning'
            - name: 'Software Composition Analysis'
              config:
                href: '/solutions/security-compliance/#software-composition-analysis'
            - name: 'API Security'
              config:
                href: '/solutions/security-compliance/#api-security'
            - name: 'Coverage-guided Fuzz Testing'
              config:
                href: '/solutions/security-compliance/#fuzz-testing'
            - name: 'DAST'
              config:
                href: '/solutions/security-compliance/#dast'
            - name: 'Code Quality'
              config:
                href: '/solutions/security-compliance/#code-quality'
            - name: 'Secret Detection'
              config:
                href: '/solutions/security-compliance/#secret-detection'
            - name: 'SAST'
              config:
                href: '/solutions/security-compliance/#sast'
            - name: 'Vulnerability Explanation'
              config:
                href: '/solutions/security-compliance/#vulnerability-explanation'
            - name: 'Vulnerability Resolution'
              config:
                href: '/solutions/security-compliance/#vulnerability-resolution'
            - name: 'GitLab Advisory Database'
              config:
                href: '/solutions/security-compliance/#gitlab-advisory-database'
          replacement:
            text: Replacement for
            product: Snyk
        - name: 'Compliance'
          icon:
            altText: 'Shield'
            config:
              name: 'ProtectAlt2'
          config:
            href: '/solutions/security-compliance/'
          features:
            - name: 'Release Evidence'
              config:
                href: '/solutions/security-compliance/#release-evidence'
            - name: 'Compliance Management'
              config:
                href: '/solutions/security-compliance/#compliance-management'
            - name: 'Audit Events'
              config:
                href: '/solutions/security-compliance/#audit-events'
            - name: 'Software Bill of Materials'
              config:
                href: '/solutions/security-compliance/#software-bill-of-materials'
            - name: 'Dependency Management'
              config:
                href: '/solutions/security-compliance/#dependency-management'
            - name: 'Vulnerability Management'
              config:
                href: '/solutions/security-compliance/#vulnerability-management'
            - name: 'Security Policy Management'
              config:
                href: '/solutions/security-compliance/#security-policy-management'
        - name: 'Artifact Registry'
          icon:
            altText: 'Code'
            config:
              name: 'PackageAlt2'
          config:
            href: '/solutions/delivery-automation/'
          features:
            - name: 'Virtual Registry'
              config:
                href: '/solutions/delivery-automation/#virtual-registry'
            - name: 'Container Registry'
              config:
                href: '/solutions/delivery-automation/#container-registry'
            - name: 'Helm Chart Registry'
              config:
                href: '/solutions/delivery-automation/#helm-chart-registry'
            - name: 'Package Registry'
              config:
                href: '/solutions/delivery-automation/#package-registry'
            - name: 'Model Registry (Beta)'
              config:
                href: '/solutions/delivery-automation/#model-registry'
            - name: 'Dependency Proxy'
              config:
                href: '/solutions/delivery-automation/#package-registry'
          replacement:
            text: Replacement for
            product: JFrog
        - name: 'Continuous Delivery'
          icon:
            altText: 'Continuous'
            config:
              name: 'ContinuousDeliveryAlt'
          config:
            href: '/solutions/delivery-automation/'
          features:
            - name: 'Release Orchestration'
              config:
                href: '/solutions/delivery-automation/#release-orchestration'
            - name: 'Infrastructure as Code'
              config:
                href: '/solutions/delivery-automation/#infrastructure-as-code'
            - name: 'Pages'
              config:
                href: '/solutions/visibility-measurement/#pages'
            - name: 'Feature Flags'
              config:
                href: '/solutions/delivery-automation/#feature-flags'
            - name: 'Environment Management'
              config:
                href: '/solutions/delivery-automation/#environment-management'
            - name: 'Deployment Management'
              config:
                href: '/solutions/delivery-automation/#deployment-management'
            - name: 'Auto DevOps'
              config:
                href: '/solutions/delivery-automation/'
          replacement:
            text: Replacement for
            product: Harness
        - name: 'Observability'
          icon:
            altText: 'Monitor'
            config:
              name: 'MonitorAlt2'
          config:
            href: '/solutions/visibility-measurement/'
          features:
            - name: 'On-call Schedule Management'
              config:
                href: '/solutions/visibility-measurement/#oncall-schedule-management'
            - name: 'Incident Management'
              config:
                href: '/solutions/visibility-measurement/#incident-management'
            - name: 'Error Tracking'
              config:
                href: '/solutions/visibility-measurement/#error-tracking'
            - name: 'Product Analytics Visualization'
              config:
                href: '/solutions/visibility-measurement/#product-analytics-visualization'
            - name: 'AI Product Analytics'
              config:
                href: '/solutions/visibility-measurement/#ai-product-analytics'
            - name: 'AI Impact Dashboard'
              config:
                href: '/solutions/visibility-measurement/#ai-product-analytics'
            - name: 'Metrics'
              config:
                href: '/solutions/visibility-measurement/#metrics'
            - name: 'Distributed Tracing'
              config:
                href: '/solutions/visibility-measurement/#distributed-tracing'
            - name: 'Logs'
              config:
                href: '/solutions/visibility-measurement/#logs'
          replacement:
            text: Replacement for
            product: Sentry
  - componentName: PlatformDevSecOpsTabs
    componentContent:
      header:
        highlighted: One platform
        text: to empower Dev, Sec, and Ops teams
      image:
        altText: code source image
        config:
          src: '/images/one-platform.svg'
      tabs:
        - tabButtonText: Development
          config:
            dataGaName: development
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: AI-powered workflow
                content: Boost efficiency and reduce cycle times of every user with the help of AI in every phase of the software development lifecycle - from planning and code creation to testing, security, and monitoring.
                config:
                  darkMode: true
                primaryCtas:
                  - text: GitLab Duo
                    config:
                      href: /gitlab-duo/
                      dataGaName: GitLab Duo
                      dataGaLocation: body
                secondaryCtaHeader: 'See it in action:'
                secondaryCtas:
                  - text: GitLab Duo
                    config:
                      href: 'https://player.vimeo.com/video/855805049/'
                      dataGaName: GitLab Duo
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: Code Suggestions
                    config:
                      href: 'https://player.vimeo.com/video/894621401/'
                      dataGaName: Code Suggestions
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: Chat
                    config:
                      href: 'https://player.vimeo.com/video/927753737/'
                      dataGaName: Chat
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Single application
                content: GitLab brings all DevSecOps capabilities into one application with a unified data store so everything is all in one place.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLab's use of DORA metrics video
                    config:
                      href: 'https://player.vimeo.com/video/892023781/'
                      dataGaName: DORA metrics - User Analytics
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLab's Value Streams Dashboard video
                    config:
                      href: 'https://player.vimeo.com/video/819308062?h=752d064728&badge=0&autopause=0&player_id=0&app_id=58479/'
                      dataGaName: GitLab's Value Streams Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Enhanced developer productivity
                content: GitLab's single application delivers a superior user experience, which improves cycle time and helps prevent context switching.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLab's Portfolio Management video
                    config:
                      href: 'https://player.vimeo.com/video/925629920/'
                      dataGaName: GitLab's Portfolio Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLab's OKR Management video
                    config:
                      href: 'https://player.vimeo.com/video/925632272/'
                      dataGaName: GitLab's OKR Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: Design Uploads to GitLab issues video
                    config:
                      href: 'https://player.vimeo.com/video/925633691/'
                      dataGaName: Design Uploads to GitLab issues
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Better automation
                content: GitLab's automation tools are more reliable and feature rich, helping remove cognitive load and unnecessary grunt work.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLab's CD Overview video
                    config:
                      href: 'https://player.vimeo.com/video/892023715/'
                      dataGaName: GitLab's CD Overview
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: Error tracking documentation
                    config:
                      href: 'https://docs.gitlab.com/ee/operations/error_tracking.html'
                      dataGaName: Error tracking documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: Incident management documentation
                    config:
                      href: https://docs.gitlab.com/ee/operations/incident_management/
                      dataGaName: Incident management documentation
                      dataGaLocation: body
                      iconName: Docs
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: The vision that GitLab has in terms of tying strategy to scope and to code is very powerful. I appreciate the level of investment they are continuing to make in the platform.
                metrics:
                  - number: $150k
                    text: approximate cost savings per year
                  - number: 20 hours
                    text: saved in onboarding time per project
                author:
                  name: Jason Monoharan
                  title: VP of Technology
                  company: Iron Mountain
                cta:
                  text: Read the study
                  config:
                    href: '/customers/iron-mountain/'
                    dataGaName: iron mountain case study
                    dataGaLocation: body
                config:
                  logo: /images/customer_logos/iron-mountain-logo-white.svg
                  headshot: /images/headshots/jason-monoharan-headshot.png
          featuredCta:
            config:
              dataGaName: gitlab duo
              dataGaLocation: body
              href: /gitlab-duo/
            boxText:
              title: Harness the power of AI with
              highlight: GitLab Duo
              text: Learn more
        - tabButtonText: Security
          config:
            dataGaName: security
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: Security is built in, not bolted on
                content: GitLab's security capabilities - such as DAST, fuzz testing, container scanning, and API screening - are integrated end-to-end.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: Dynamic Application Security Testing (DAST) video
                    config:
                      href: 'https://player.vimeo.com/video/925635707/'
                      dataGaName: Dynamic Application Security Testing (DAST) video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: Container scanning video
                    config:
                      href: 'https://player.vimeo.com/video/925676815/'
                      dataGaName: Container scanning video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: API security and web API Fuzzing video
                    config:
                      href: 'https://player.vimeo.com/video/925677603/'
                      dataGaName: API security and web API Fuzzing video
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Compliance and precise policy management
                content: GitLab offers a comprehensive governance solution allowing for separation of duties between teams. GitLab's policy editor allows customized approval rules tailored to each organization's compliance requirements, reducing risk.
                secondaryCtas:
                  - text: Compliance Management documentation
                    config:
                      href: '/solutions/security-compliance/#compliance-management'
                      dataGaName: Compliance Management documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: GitLab's Compliance Frameworks video
                    config:
                      href: 'https://player.vimeo.com/video/925679314/'
                      dataGaName: GitLab's Compliance Frameworks
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLab's Requirements Management video
                    config:
                      href: 'https://player.vimeo.com/video/925679982/'
                      dataGaName: GitLab's Requirements Management
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Security automation
                content: GitLab's advanced automation tools enable velocity with guardrails, ensuring code is automatically scanned for vulnerabilities.
                secondaryCtas:
                  - text: GitLab's Security Dashboard video
                    config:
                      href: 'https://player.vimeo.com/video/925680640/'
                      dataGaName: GitLab's Security Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: GitLab is helping us catch security flaws early and it's integrated it into the developer's flow. An engineer can push code to GitLab CI, get that immediate feedback from one of many cascading audit steps and see if there's a security vulnerability built in there, and even build their own new step that might test a very specific security issue.
                metrics:
                  - number: 7.5x
                    text: faster pipeline time
                  - number: 4 hours
                    text: development time per engineer saved/weekly
                author:
                  name: Mitch Trale
                  title: Head of Infrastructure
                  company: Hackerone
                cta:
                  text: Read the study
                  config:
                    href: '/customers/hackerone/'
                    dataGaName: hackerone case study
                    dataGaLocation: body
                config:
                  logo: /images/customer_logos/hackerone-logo-white.svg
          featuredCta:
            config:
              dataGaName: ci pipeline
              dataGaLocation: body
              iconName: LaptopVideo
              modal: true
            boxText:
              title: See how to add security scans to your
              highlight: CI pipeline
              text: Launch demo
            demo:
              config:
                demoHref: https://capture.navattic.com/clq78b76l001b0gjnbxbd5k1f
                videoFallbackHref: https://player.vimeo.com/video/892023806
              subtitle: Add security scans to your CI/CD Pipeline
              scheduleButton:
                text: Schedule a custom demo
                config:
                  href: /sales/
                  dataGaName: demo
                  dataGaLocation: body
        - tabButtonText: Operations
          config:
            dataGaName: operations
            dataGaLocation: body
          tabPanelContent:
            accordion:
              - header: Scale Enterprise workloads
                content: GitLab easily supports the Enterprise at any scale with the ability to manage and upgrade with nearly zero downtime.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: Infrastructure as code (IaC) documentation
                    config:
                      href: https://docs.gitlab.com/ee/user/infrastructure/iac/
                      dataGaName: Infrastructure as code (IaC) documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: Incident management documentation
                    config:
                      href: https://docs.gitlab.com/ee/operations/incident_management/
                      dataGaName: Incident management documentation
                      dataGaLocation: body
                      iconName: Docs
              - header: Unparalleled metrics visibility
                text: GitLab's unified data store provides analytics for the entire software development lifecycle in one place, eliminating the need for additional product integrations.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: GitLab's use of DORA metrics video
                    config:
                      href: 'https://player.vimeo.com/video/892023781/'
                      dataGaName: DORA metrics - User Analytics
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
                  - text: GitLab's Value Streams Dashboard video
                    config:
                      href: 'https://player.vimeo.com/video/819308062/'
                      dataGaName: GitLab's Value Streams Dashboard
                      dataGaLocation: body
                      iconName: PlayCircle
                      modal: true
              - header: Cloud-native, multi-cloud and legacy support
                content: GitLab provides a complete DevSecOps platform that allows teams to have the same productivity metrics and governance, regardless of your infrastructure mix.
                config:
                  darkMode: true
                secondaryCtas:
                  - text: Multicloud documentation
                    config:
                      href: /topics/multicloud/
                      dataGaName: Multicloud documentation
                      dataGaLocation: body
                      iconName: Docs
                  - text: GitOps documentation
                    config:
                      href: /solutions/gitops/
                      dataGaName: GitOps documentation
                      dataGaLocation: body
                      iconName: Docs
              - header: Lower Total Cost of Ownership
                content: ''
                secondaryCtas:
                  - text: Lockheed Martin case study
                    description: "Learn how the world's largest defence contractor uses GitLab to shrink toolchains, speed production, and improve security:"
                    config:
                      href: /customers/lockheed-martin/
                      dataGaName: Lockheed Martin case study
                      dataGaLocation: body
                      iconName: Docs
                  - text: CARFAX case study
                    description: 'Learn how CARFAX trimmed their DevSecOps toolchain and improved security with GitLab:'
                    config:
                      href: /customers/carfax/
                      dataGaName: CARFAX case study
                      dataGaLocation: body
                      iconName: Docs
          caseStudy:
            config:
              darkMode: true
            cards:
              - quote: Improved development and delivery efficiency by over 87%,resulting in over $23 million in savings. GitLab enabled the organizations to drastically reduce time spent across each phase of the entire DevOps lifecycle.
                metrics:
                  - number: $200.5M
                    text: total benefits over three years
                  - number: 427%
                    text: total return on investment (ROI)
                cta:
                  text: Read the study
                  config:
                    href: 'https://page.gitlab.com/resources-study-forrester-tei-gitlab-ultimate.html'
                    dataGaName: resources study forrester
                    dataGaLocation: body
                config:
                  logo: /images/logos/forrester-logo.svg
          featuredCta:
            config:
              dataGaName: try our roi calculator
              dataGaLocation: body
              href: /calculator/
            boxText:
              title: How much is your toolchain
              highlight: costing you?
              text: Try our ROI Calculator
  - componentName: CommonVideoSpotlight
    componentContent:
      title: |
        Want to increase velocity?
        Consolidate your toolchain today.
      benefits:
        - text: 'Improve collaboration'
        - text: 'Reduce admin burden'
        - text: 'Increase security'
        - text: 'Lower total cost of ownership'
        - text: 'Scale seamlessly'
      cta: |
        **Don't know where to start?**
        Our sales team can help guide you.
      button:
        text: 'Talk to sales'
        config:
          url: '/sales/'
          dataGaName: 'sales'
          dataGaLocation: 'body'
      video:
        buttonText: 'Learn more'
        config:
          dataGaName: 'Learn More'
          dataGaLocation: 'body'
          src: 'https://player.vimeo.com/video/799236905?h=4eee39a447'
          thumbnail: images/platform/platform-video-thumbnail.jpg
  - componentName: CommonRecognition
    componentContent:
      title: 'Industry leaders trust GitLab'
      subtitle: 'GitLab ranks as a G2 Leader across DevOps categories.'
      badges:
        - altText: 'g2 winter 2024 leader'
          config:
            src: '/images/gartner/easiest-to-use.svg'
        - altText: 'g2 winter 2024 easiest to use'
          config:
            src: '/images/gartner/users-love-us.png'
        - altText: 'g2 winter 2024 users love us'
          config:
            src: '/images/gartner/best-usability.svg'
        - altText: 'g2 winter 2024 best usability'
          config:
            src: '/images/gartner/devops-leader-emea.png'
        - altText: 'g2 winter 2024 leader emea'
          config:
            src: '/images/gartner/devops-leader-enterprise.png'
        - altText: 'g2 winter 2024 leader enterprise'
          config:
            src: '/images/gartner/momentum-leader.png'
        - altText: 'g2 winter 2024 momentum leader'
          config:
            src: '/images/gartner/devops-leader-asia.png'
        - altText: 'g2 winter 2024 leader asia'
          config:
            src: 'https://images.ctfassets.net/xz1dnu24egyd/712lPKncEMDkCErGOD91KQ/95aefe77ccfd4ec9e4f6288c072314e4/DevOps_Leader_Leader.png'
      cards:
        - description: 'GitLab is a Leader in the 2024 Gartner® Magic Quadrant™ for DevOps Platforms'
          image:
            altText: gartner logo
            config:
              src: /images/logos/gartner.svg
          button:
            text: Read the report
            config:
              href: /gartner-magic-quadrant/
              dataGaName: gartner
              dataGaLocation: analyst
        - description: 'GitLab is the only Leader in The Forrester Wave™: Integrated Software Delivery Platforms, Q2 2023'
          image:
            altText: forrester logo
            config:
              src: /images/logos/forrester-logo.svg
          button:
            text: Read the report
            config:
              href: 'https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023/'
              dataGaName: forrester
              dataGaLocation: analyst
  - componentName: PlatformPricingBlurb
    componentContent:
      icon:
        altText: 'Calendar'
        config:
          name: 'PlanAlt2'
      title: 'Find out which pricing plan works best for your growing team'
      button:
        text: 'Why GitLab Premium?'
        config:
          href: '/pricing/premium/'
          dataGaName: 'why gitlab premium'
          dataGaLocation: 'body'
      shimmerButton:
        text: 'Why GitLab Ultimate?'
        config:
          href: '/pricing/ultimate/'
          dataGaName: 'why gitlab ultimate'
          dataGaLocation: 'body'
  - componentName: CommonNextSteps
