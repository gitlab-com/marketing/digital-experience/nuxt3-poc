config:
  enableAnimations: true
seo:
  title: GitLab's Board of Directors
  description: Meet GitLab's Board of Directors
content:
  - componentName: CompanyTeamHeadshotBlock
    componentContent:
      header: GitLab's Board of Directors
      buttons:
        - text: Board Meetings
          config:
            href: /handbook/board-meetings/
            dataGaName: board meetings
            dataGaLocation: body
        - text: Meet our Executive Group
          config:
            href: '/company/team/e-group/'
            dataGaName: egroup
            dataGaLocation: body
      cards:
        - title: Sid Sijbrandij
          subtitle: Executive Chair
          description: |
            Sid Sijbrandij (pronounced see-brandy) is the Co-founder and Executive Chair of the Board of GitLab Inc., where he served as CEO until December 2024. He is also the founder and General Partner at Open Core Ventures and serves on the board of the GitLab Foundation.

            Sid spent four years building recreational submarines for U-Boat Worx. While at Ministerie van Justitie en Veiligheid, he worked on the Legis project, which developed several innovative web applications to aid lawmaking. He first saw Ruby code in 2007 and loved it so much that he taught himself how to program. In 2012, as a Ruby programmer, he encountered GitLab and discovered his passion for open source. Soon after, Sid commercialized GitLab, and by 2015, he led the company through Y Combinator's Winter 2015 batch.

            Sid studied at the University of Twente in the Netherlands, where he received an M.S. in Management Science. Forbes named him one of the greatest minds of the pandemic and recognized him as a thought leader in remote work and the open source community.
          image:
            config:
              src: /images/headshots/sid-sijbrandij-headshot.webp

        - title: Bill Staples
          subtitle: Chief Executive Officer
          description: |
            Bill Staples is the CEO of GitLab, the most comprehensive AI-powered DevSecOps platform for software innovation. He is passionate about developers and has spent nearly 30 years building developer platforms and tools for them as customers. He is an execution-focused leader who loves to build and scale businesses. Staples believes we're still in the early stages of a software transformation, and AI will accelerate how software changes the human experience in the coming decade. He believes there has never been a better time in history to be in the software business and serve developers, helping improve their work and lives and ultimately reaching billions of people around the world in profound ways.

            Before joining GitLab, Staples served as CEO of New Relic, where he significantly increased the company's value by accelerating revenue and driving increased profitability while making New Relic one of the most broadly adopted platforms in its category. During his time at Microsoft, he served as corporate vice president, Azure application platform, where he incubated and launched dozens of Azure services for developers and scaled them globally. As Adobe's Vice President, Experience Cloud, Staples led the global technical organization and drove the roadmap strategy and execution for multiple product lines with a combined $3 billion in annual revenue while transforming the engineering culture to be cloud-first, agile, and DevOps. He successfully led transformative product, culture, and technical innovation evolutions at Microsoft and Adobe, helping both companies expand multi-billion dollar cloud portfolios with developers and IT as the customers.
          image:
            config:
              src: /images/headshots/bill-staples-headshot.webp

        - title: Godfrey Sullivan
          subtitle: Board Member
          description: |
            Godfrey Sullivan has served on GitLab's board since 2020. He also serves as a member of the board for CrowdStrike and Marqeta, Inc.. Godfrey previously served as President and Chief Executive Officer at Splunk from 2008 to 2015 and was Chair of the board of directors from 2011 to 2019. Prior to joining Splunk, he served as President and Chief Executive Officer at Hyperion Solutions Corporation (acquired by Oracle Corporation). Godfrey has also served as a member of the board of directors of RingCentral, Informatica Corporation from 2008 to 2013, and Citrix Systems from 2005 to 2018. Godfrey holds a B.B.A. from Baylor University.
          image:
            config:
              src: /images/headshots/godfrey-sullivan-headshot.webp

        - title: Karen Blasing
          subtitle: Board Member
          description: |
            Karen Blasing has served on GitLab's board since 2019. She also currently serves on the boards of Autodesk and Zscaler. She previously served on the board of Ellie Mae, LogRhythm (acquired by Thoma Bravo), and MetricStream. Karen served as Chief Financial Officer for Guidewire Software from 2009 to 2015, where she led the company's successful initial public offering in 2012 and two subsequent offerings. Karen also served as Chief Financial Officer for Force10 Networks (acquired by Dell), Nuance Communications, and Counterpane Internet Security (acquired by BT). She has held senior finance roles at Salesforce, Informix Corporation (acquired by IBM), Oracle Corporation, and Syntex Pharmaceuticals (now Roche). She holds B.A.'s in Economics and Business Administration from the University of Montana and an MBA from the University of Washington.
          image:
            config:
              src: /images/headshots/karen-blasing-headshot.webp

        - title: Matthew Jacobson
          subtitle: Board Member
          description: |
            Matthew Jacobson has served on GitLab's board since 2018. He currently sits on the boards of Datadog, BambooHR, Miro, Monte Carlo Data, Orca Security, Collibra, Gem, Pigment and Relativity. He previously served on the boards of Sprinklr from 2014 to 2022 and Braze from 2017 to 2023. He has been a Partner at ICONIQ Capital since 2013 and a member of the firm's executive, management and investment committees. Prior to ICONIQ Capital, Matthew held operating roles at Groupon and investing roles at Battery Ventures and Technology Crossover Ventures. He holds a B.S. in Economics from The Wharton School at the University of Pennsylvania.
          image:
            config:
              src: /images/headshots/matthew-jacobson-headshot.webp

        - title: Merline Saintil
          subtitle: Board Member
          description: |
            Merline Saintil has served on GitLab's board since 2020. She currently serves on the boards of TD Synnex, Symbotic, Evolv Technology, and Rocket Lab, where she is the Lead Independent Director of this space exploration company. Merline was the former Chief Operating Officer at Change Healthcare and previously held leadership roles at Intuit, Yahoo!, PayPal, Joyent, Adobe, and Sun Microsystems. Merline began her career as a software engineer and was honored as one of the Most Influential Corporate Directors in 2019 and one of the 22 Most Powerful Women Engineers in the World by Business Insider. She also received a Lifetime Achievement Award from Girls in Tech, and in 2022 was awarded the NACD Directorship 100™ and Worth Magazine's Worthy 100. Merline is certified in Cyber-Risk Oversight by the National Association of Corporate Directors (NACD) and the Carnegie Mellon Software Engineering Institute. She has an M.S. in Software Engineering Management from Carnegie Mellon University.
          image:
            config:
              src: /images/headshots/merline-saintil-headshot.webp

        - title: Sue Bostrom
          subtitle: Board Member
          description: |
            Sue Bostrom has served on GitLab's board since 2019. In addition to GitLab, Sue currently serves on the boards of Samsara, ServiceNow, Outreach, and SingleStore. Sue previously served on the boards of Anaplan, Cadence Design Systems, Marketo, Nutanix, Rocket Fuel, and Varian Medical Systems. She was formerly on the Stanford Hospital Board, Stanford Children's Hospital Board,  Stanford Medicine Advisory Council, the Stanford Institute for Economic Policy Research (SIEPR) advisory board, and Georgetown University's board of directors. Sue is the former EVP and CMO at Cisco Systems and previously held leadership positions at FTP Software and National Semiconductor. She also served for seven years at McKinsey & Company as a leader in the electronics practice. Sue holds a B.S. in business from the University of Illinois and an MBA from Stanford Graduate School of Business, where she served on its management and advisory boards.
          image:
            config:
              src: /images/headshots/sue-bostrom-headshot.webp

        - title: Sunny Bedi
          subtitle: Board Member
          description: |
            Sunny Bedi has served on GitLab's board since 2021. Sunny serves as the Chief Information & Data Officer for Snowflake and has been with the company since 2020. Sunny oversees global IT, data, and security functions, helping the company scale in both headcount and customer base, culminating in the company's IPO. He also launched the "Snowflake on Snowflake" program, making Snowflake customer zero for all new products and features. This program focuses on enhancing the customer experience by refining the architecture, creating customer use cases and providing valuable feedback to engineering and product management teams. Prior to joining Snowflake, Sunny held Corporate IT and Operations leadership roles at NVIDIA from 2008-2020 as they scaled from less than 2,000 employees to 15,000. Previously, Sunny served in leadership roles at VMware, JDSU, Deloitte Consulting and Andersen Consulting. He holds a BS and MBA from University of San Francisco, and completed an Executive Education in Leadership and Technology program from Stanford University.
          image:
            config:
              src: /images/headshots/sunny-bedi-headshot.webp

        - title: David Henshall
          subtitle: Board Member
          description: |
            David Henshall has served on GitLab's board since 2025. He previously served as President and Chief Executive Officer and as a member of the board of directors of Citrix Systems, Inc., or Citrix, from 2017 to 2021. Prior to this role, he served in various other roles at Citrix, including Chief Financial Officer and Chief Operating Officer, among others, beginning in 2003. Before joining Citrix, he served as Chief Financial Officer of Rational Software Corporation, a software company acquired by IBM Corporation and held various finance positions at Cypress Semiconductor and Samsung. David currently serves on the board of directors at HashiCorp, Blackline Systems, Inc., Aspen Technology, Inc., and Feedzai. David holds a B.S. in Business Administration from the University of Arizona and a Masters in Business Administration from Santa Clara University.
          image:
            config:
              src: /images/headshots/david-henshall-headshot.jpeg
