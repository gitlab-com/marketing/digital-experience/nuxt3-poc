config:
  enableAnimations: true
seo:
  title: GitLab's Executive Group
  description: Meet GitLab's Executive Group
content:
  - componentName: CompanyTeamHeadshotBlock
    componentContent:
      header: GitLab's Executive Group
      buttons:
        - text: Meet our Board of Directors
          config:
            href: /company/team/board-of-directors/
            dataGaName: board of directors
            dataGaLocation: body
      cards:
        - title: Bill Staples
          subtitle: Chief Executive Officer
          description: |
            Bill Staples is the CEO of GitLab, the most comprehensive AI-powered DevSecOps platform for software innovation. He is passionate about developers and has spent nearly 30 years building developer platforms and tools for them as customers. He is an execution-focused leader who loves to build and scale businesses. Staples believes we're still in the early stages of a software transformation, and AI will accelerate how software changes the human experience in the coming decade. He believes there has never been a better time in history to be in the software business and serve developers, helping improve their work and lives and ultimately reaching billions of people around the world in profound ways.

            Before joining GitLab, Staples served as CEO of New Relic, where he significantly increased the company's value by accelerating revenue and driving increased profitability while making New Relic one of the most broadly adopted platforms in its category. During his time at Microsoft, he served as corporate vice president, Azure application platform, where he incubated and launched dozens of Azure services for developers and scaled them globally. As Adobe's Vice President, Experience Cloud, Staples led the global technical organization and drove the roadmap strategy and execution for multiple product lines with a combined $3 billion in annual revenue while transforming the engineering culture to be cloud-first, agile, and DevOps. He successfully led transformative product, culture, and technical innovation evolutions at Microsoft and Adobe, helping both companies expand multi-billion dollar cloud portfolios with developers and IT as the customers.
          image:
            config:
              src: /images/headshots/bill-staples-headshot.webp

        - title: Robin Schulman
          subtitle: Chief Legal Officer, Head of Corporate Affairs, and Corporate Secretary
          description: |
            ##### San Francisco, CA

            Robin Schulman is the Chief Legal Officer, Head of Corporate Affairs, and Corporate Secretary of GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.

            GitLab's CLO and Corporate Secretary since 2019, Robin leverages her leadership experience scaling high growth technology companies to create a culture of compliance and set and manage the company's overall global legal, compliance, policy, privacy, corporate development, ESG philosophy and strategy. She also served as GitLab's Acting Chief Information Security Officer from October 2022 to May 2023. Supported by a talented team of attorneys and legal professionals, Robin ensures GitLab maintains balance across the company's business activities and practices with its values and culture. Additionally, Robin provides counsel to the GitLab Board of Directors across the entire spectrum of legal, compliance and corporate governance matters that pertain to the company. Robin is the executive sponsor of GitLab's women's and pride team member resource groups and the unofficial sponsor of our #dog Slack channel.

            Prior to joining GitLab, Robin oversaw global legal affairs, public policy and compliance at Couchbase, Inc. (NASDAQ: BASE) as their SVP, Chief Legal Officer and Corporate Secretary. She also established, scaled, and led New Relic Inc.'s (NYSE: NEWR) global legal and compliance organization as their General Counsel and Chief Compliance Officer from pre-IPO to profitable public company. Prior to that, Robin was Legal Counsel to Adobe Inc. (NASDAQ: ADBE) where she led the legal function for several of Adobe's Marketing and Creative Cloud products and specialized in advising high growth companies while an associate at Fenwick & West LLP, a law firm providing legal services to technology and life science companies.

            Robin earned a B.F.A. in Dramatic Writing and Film from New York University and a J.D. from Rutgers University School of Law - Newark. In 2017, Robin was honored by the Silicon Valley Business Journal and the San Francisco Business Times with an award for Best Bay Area Corporate Counsel for a Public Company General Counsel. She has been a member of the Board of Directors of the GitLab Foundation since December 2021.She has taught intellectual property law at Santa Clara Law School and is a board observer for a private biotech company.
          image:
            config:
              src: /images/headshots/robin-schulman-headshot.webp

        - title: Brian Robins
          subtitle: Chief Financial Officer
          description: |
            ##### Washington, D.C.

            Brian Robins is the Chief Financial Officer at GitLab Inc., the DevSecOps platform. GitLab's single application helps organizations deliver software faster and more efficiently while strengthening their security and compliance.

            As CFO, Brian is responsible for GitLab's financial, data and business systems functions, including accounting, tax, treasury, corporate finance, IT and investor relations. He helps to ensure GitLab's finance and accounting systems and processes scale and grow with the company. His team oversees GitLab's financial reporting, provides data driven decision support and offers strategic guidance to the business.

            Prior to GitLab, Brian served as CFO at Sisense, Cylance, AlienVault, and Verisign. As a 20-plus year veteran leading both private and public high-growth software companies, and with extensive experience with IPOs and M&As, Brian has a long, documented track record of improving financial performance, increasing productivity, and creating shareholder value. He lends this wisdom as a special advisor at Brighton Park Capital, L.P. and on the Advisory Council at ForgePoint Capital Cybersecurity. Brian serves on the GitLab Foundation Board of Directors, as well as on the Board of the Directors for ID.me, where he is Audit Committee Chair.

            Brian holds a B.S. in Finance from Lipscomb University and an M.B.A from Vanderbilt University's Owen Graduate School of Management.
          image:
            config:
              src: /images/headshots/brian-robins-headshot.webp

        - title: Ashley Kramer
          subtitle: Interim Chief Revenue Officer, and Chief Marketing & Strategy Officer
          description: |
            ##### San Francisco, CA

            Ashley Kramer is the Interim Chief Revenue Officer, and Chief Marketing & Strategy Officer of GitLab, the most comprehensive AI-powered DevSecOps platform for software innovation. A former GitLab customer, Ashley works closely with our customers and is the executive sponsor of many enterprise accounts. She leverages experience from her marketing, product, and technology roles to message and position GitLab as the leading DevSecOps platform through the next stage of growth. She is responsible for setting GitLab's long-term strategy - including leading the company's enterprise data team, open source strategy, and AI vision - and driving core marketing and pipeline generation.

            Before joining GitLab, Ashley was CPO and CMO of Sisense and has held several post-IPO leadership roles, including SVP of Product at Alteryx (NYSE: AYX) where she led the messaging, positioning and roadmap for the Alteryx Analytics Platform and Head of Cloud at Tableau where she led the effort to transform Tableau (now a Salesforce company) to a cloud-first company and ran Tableau Online, its fastest-growing product. She also has held various marketing, product, and engineering leadership roles at Amazon (NASDAQ: AMZN), Oracle (NYSE: ORCL), and NASA. As a former engineer, Ashley understands and can capitalize on the value of GitLab's unique ability to solve a deep developer pain point - streamlining the development process and bringing innovative ideas to customers more quickly and efficiently. She approaches everything with a customer-first mindset and is passionate about solving the challenge of positioning and messaging software platforms to technical audiences.

            Ashley has an MSBA with a Concentration in Computer Information Science from Colorado State University and a BS in Computer Science from Old Dominion University, where she also played Division I soccer. She is on the boards of Seeq Corporation and dbt Labs and is an advisor for Snorkel AI and several other startups.
          image:
            config:
              src: /images/headshots/ashley-kramer-headshot.webp

        - title: Sabrina Farmer
          subtitle: Chief Technology Officer
          description: |
            ##### San Francisco, CA

            Sabrina Farmer is the Chief Technology Officer at GitLab, where she leads software engineering, operations, and customer support teams to execute the company's technical vision and strategy and oversee the development and delivery of GitLab's products and services.

            Prior to GitLab, Sabrina spent nearly two decades at Google, where she most recently served as vice president of engineering, core infrastructure. During her tenure with Google, she was directly responsible for the reliability, performance, and efficiency of all of Google's billion-user products and infrastructure.

            A long-time advocate for women in technology, Farmer earned a B.S. in Computer Science at the University of New Orleans, where she established two scholarships to help level the playing field for inclusion and empowerment in technology.
          image:
            config:
              src: /images/headshots/sabrina-farmer-headshot.webp

        - title: David DeSanto
          subtitle: Chief Product Officer
          description: |
            ##### Philadelphia, PA

            David DeSanto is the Chief Product Officer at GitLab Inc., where he leads GitLab's product division to define and execute GitLab's product vision and roadmap. David is responsible for ensuring the company builds, ships, and supports the platform that reinforces GitLab's leadership in the DevSecOps platform market.

            David joined GitLab in 2019 to expand GitLab's Ultimate tier and build security into the GitLab DevOps platform. He was promoted to chief product officer in 2022. Prior to GitLab, David has held product and engineering leadership roles at Spirent Communications, NSS Labs and ICSA Labs.

            David holds an M.S. in Cybersecurity from New York University and a B.S. in Computer Science from Millersville University of Pennsylvania. He is a frequent speaker at major international conferences on topics including AI, DevSecOps, platform engineering, threat intelligence, and cloud security, in addition to being the co-author of Threat Forecasting.
          image:
            config:
              src: /images/headshots/david-desanto-headshot.webp

        - title: Josh Lemos
          subtitle: Chief Information Security Officer
          description: |
            ##### Seattle, WA

            Josh Lemos is the Chief Information Security Officer at GitLab Inc., where he brings 20 years of experience leading information security teams to his role. He is responsible for establishing and maintaining the enterprise vision, strategy, and program to ensure information assets and technologies are adequately protected, fortifying the Gitlab DevSecOps platform and ensuring the highest level of security for customers.

            A talented security practitioner and technology leader, Josh is widely recognized for his strategic vision, his ability to drive growth and innovation, and his passion for building and empowering teams. He believes in technology's potential to transform the world and the need to secure it against emerging threats. Josh has led security teams at numerous high-growth technology companies including ServiceNow, Cylance, and most recently Block (formerly known as Square).

            Josh's commitment to securing technologies to make a positive impact in the world has been a common thread throughout his career. He serves as a mentor to aspiring information security professionals, and is active in supporting organizations that promote diversity and inclusion in the technology industry. Josh holds a B.S. in Computer and Information Systems Security from the University of San Francisco.
          image:
            config:
              src: /images/headshots/josh-lemos-headshot.webp

        - title: Rob Allen
          subtitle: Chief People Officer
          description: |
            ##### San Francisco, CA

            Rob Allen is the Chief People Officer at GitLab Inc., where he is responsible for the company's overall team member experience. As CPO, Rob works closely with leaders to champion GitLab's customer-centric, values-driven culture to ensure they scale as the company grows.

            Rob was appointed Chief People Officer after four years in senior HR leadership roles at the company. Before joining GitLab, Rob served as VP, Global Experience and Talent Operations at Atlassian and led the global recruiting teams at Red Hat

            Rob spent the early years of his life in Brussels, Belgium, before moving to the United Kingdom, where he attended the University of Reading.
          image:
            config:
              src: /images/headshots/robert-allen-headshot.webp
