config:
  layout: 'default'
  template: 'DevSurveyLanding'
  enableAnimations: true
seo:
  title: GitLab 2024 Global DevSecOps Report
  ogTitle: GitLab 2024 Global DevSecOps Report
  description: >-
    GitLab surveyed over 5,000 development, security, and operations professionals
    about what's next in DevSecOps. Check out the report to learn more.
  twitterDescription: >-
    GitLab surveyed over 5,000 development, security, and operations professionals
    about what's next in DevSecOps. Check out the report to learn more.
  ogDescription: >-
    GitLab surveyed over 5,000 development, security, and operations professionals
    about what's next in DevSecOps. Check out the report to learn more.
  ogImage: /images/developer-survey/2024/2024-devsecops-survey-landing-page-opengraph.jpg
content:
  hero:
    year: 2024
    title: Global DevSecOps Report
  intro:
    surveyHeader:
      text: >-
        This year's survey highlights evolving attitudes towards security, AI, and
        developer experience.
      config:
        imgVariant: 1
    summary:
      title: Executive summary
      text: >
        Our survey of more than 5,000 DevSecOps professionals worldwide showed that organizations are prioritizing investments in security, AI, and automation — and all of these are having positive effects on the experiences of developers and software engineering teams. However, this year's survey also highlighted specific areas, such as software supply chain security, that warrant particular attention as organizations build out their DevSecOps strategies.
    highlights:
      title: Highlights
      config:
        type: text
        gaLocation: summary
      buttons:
        previous: Previous
        next: Next
      blocks:
        - header: AI is a core part of software development
          text: >-
            78% of respondents said they are currently using AI in software
            development or plan to in the next 2 years, up from 64% in 2023
        - header: Organizations are serious about automation
          text: >-
            67% of respondents said their software development lifecycle is mostly
            or completely automated
        - header: The toolchain struggle is real
          text: >-
            64% of respondents said they want to consolidate their toolchain
        - header: Software supply chain security is key
          text: >-
            67% of developers said a quarter or more of the code they work on is
            from open source libraries — but only 21% of organizations are
            currently using a software bill of materials (SBOM) to document the
            ingredients that make up their software components
    list:
      title: Top IT investment priorities in 2024
      items:
        - text: Security
        - text: AI
        - text: DevSecOps platform
        - text: Automation
        - text: Cloud computing
  form:
    subtext: Free access
    header: Register to read the full report
    text: |
      Get free access to the full report with insights from over 5,000 DevSecOps professionals in 39 countries worldwide.

      **What's in the report**

      - Part 1: Top investment areas in 2024
      - Part 2: AI
      - Part 3: Developer experience
      - Part 4: Security
    config:
      formId: 1002
      formName: resources
  sections:
    - title: 'Security, AI, and automation: Top investment areas in 2024'
      mobileSubtitle: 'Part 1: IT Investment Trends'
      text: >-
        This year, we noted a reshuffling of priorities as organizations continue
        to balance long-term, traditional priorities such as security and the
        cloud with other technologies such as AI and automation.
      config:
        imgSrc: '/images/developer-survey/2024/investment-trends-hero.png'
      subsections:
        - subtext: Top IT investment priorities in 2024
          config:
            imgVariant: 2
            resultsFile: 2024_it_investment
          textBlocks:
            - title: Analysis
              text: >
                Security remains a top priority, taking the number one spot this
                year despite a slight decrease in the percentage of respondents
                who identified security as a priority. AI showed a slight but
                statistically significant increase from 2023 — suggesting that AI
                is increasingly becoming a primary focus for many organizations. 


                The number of respondents who identified a DevSecOps platform as
                an investment priority and the number of respondents who said
                their organizations are currently using a DevSecOps platform this
                year were similar to 2023, suggesting that interest and investment
                in DevSecOps platforms remains stable. Meanwhile, interest in
                automation saw a significant increase in 2024, with automation
                jumping from priority number six in 2023 to number four this year.


                One of the biggest signals of the reshuffling of priorities is the
                fate of cloud computing: The cloud was the top priority in 2023,
                but this year it dropped to number five. However, it's clear that
                the cloud continues to be important — we saw a significant
                decrease year over year in the number of respondents who said they
                are running less than half of their apps in the cloud (68% in 2023
                to 43% in 2024), and a corresponding increase in the number of
                respondents who said they are running 50% or more of their apps in
                the cloud (32% in 2023 to 55% in 2024). This suggests that while
                the cloud is still mission-critical for many businesses, it's now
                “table stakes” — and at the same time, the list of priorities for
                technical teams and IT leaders continues to grow.


                Finally, while metrics and analytics didn't make the top five
                investment priorities this year, we did observe a significant
                uptick compared to last year (9% in 2023 to 12% in 2024),
                suggesting that this is an up-and-coming priority for
                organizations.
            - title: Global trends & industry insights
              text: >
                ### Highly regulated industries are doubling down on security

                Not surprisingly, security remained the number one investment
                priority in highly regulated industries such as the public sector
                (defined in our survey as government, aerospace, and defense
                organizations), financial services, and telecommunications.


                ### Tech leads the way on AI

                Software, SaaS, and computer hardware organizations were the most
                enthusiastic about AI, with a quarter of respondents identifying
                AI as a top priority.
    - title: The time for AI is now — but adoption should be intentional
      mobileSubtitle: 'Part 2: AI'
      config:
        imgSrc: '/images/developer-survey/2024/ai-hero.png'
      text: >-
        As we noted last year, DevSecOps professionals are optimistic about AI,
        and adoption of AI in the software development lifecycle is picking up
        speed. However, this year's data highlights several areas that
        organizations should watch to ensure that the introduction of AI is as
        productive as possible.
      subsections:
        - subtext: AI is now a "must have"
          config:
            imgVariant: 3
            inverse: true
            resultsFile: 2024_ai
          textBlocks:
            - title: Analysis
              text: >
                This year, 78% of respondents said they are currently using AI in
                software development or plan to in the next 2 years, up from 64%
                last year; 39% of respondents said they are already using AI, up
                from 23% last year. There was also a corresponding decrease in the
                number of respondents who said their organizations have no plans
                to use AI.


                These findings suggest that the use of AI for software development
                is now status quo — and not adopting AI is becoming a less viable
                option for many organizations, even those that were initially
                hesitant.
            - title: Global trends & industry insights
              text: >
                ### Are the U.S. and U.K. lagging behind?

                Nearly two-thirds of respondents in the U.S. and the U.K. agreed
                that incorporating AI into software development is essential.
                However, both the U.S. (34%) and the U.K. (31%) had significantly
                lower rates of AI adoption than Australia (62%), Germany (53%),
                France (48%), and Japan (48%).


                ### Automotive and the public sector accelerate on AI 

                Of the industries included in our survey, the automotive industry
                was the furthest along in AI adoption, with 66% of respondents
                saying that they are already using AI for software development.
                The public sector also showed high adoption, with nearly half
                (47%) of public sector respondents indicating that they are
                currently using AI.
        - subtext: Toolchains in the age of AI
          config:
            resultsFile: 2024_toolchains
            imgVariant: 4
          textBlocks:
            - title: Analysis
              text: >
                How is the rapid introduction of AI tools impacting DevSecOps
                teams' day-to-day? As one metric for how teams feel about their
                existing tools, we asked them whether they want to consolidate
                their toolchain.


                Nearly three-quarters (74%) of respondents whose organizations are
                currently using AI for software development said they wanted to
                consolidate their toolchain, compared to 57% of those who aren't
                using AI. However, there wasn't a significant difference between
                the two groups in the number of tools respondents actually
                reported using.


                Why would AI accelerate the desire to consolidate? One explanation
                could be that the rapid adoption of AI is simply shedding new
                light on organizations' already cumbersome and counterproductive
                toolchains. In many cases, AI is only as valuable as the data
                available to it — so more tools in the toolchain means more tools
                an AI solution will need to integrate with. Teams get more value
                out of AI when toolchains are smaller, making it easier to
                integrate AI across the entire software development lifecycle.
        - subtext: Teams want AI integrated into all aspects of software development
          config:
            inverse: true
            imgVariant: 5
            resultsFile: 2024_ai_integration
          textBlocks:
            - title: Analysis
              text: >
                As we observed last year, software engineering teams are eager to
                adopt generative AI to help them accelerate code creation. This
                year, code generation and code suggestions (47%) again topped the
                list of software development use cases where respondents were
                interested in applying AI, followed by explanations of how code
                works (40%) and summaries of code changes (38%).


                When asked how they are planning to use or interested in using AI,
                this year's respondents identified a different set of use cases,
                including forecasting of productivity metrics and identification
                of anomalies (38%), explanations of how a vulnerability can be
                exploited and how to remediate it (37%), and chatbots that allow
                users to ask questions in natural language (36%).


                Chatbots appeared in both the top five current use cases and the
                top five use cases that respondents were interested in, suggesting
                that natural-language chat interfaces are an appealing way for
                DevSecOps teams to engage with AI tools.
        - subtext: Unlocking AI's full potential
          config:
            resultsFile: 2024_full_potential
            imgVariant: 1
          textBlocks:
            - title: Analysis
              text: >
                Although the code generation use case had the highest adoption
                rate in our survey — likely because this was the first to be
                introduced — our survey findings suggest that the real opportunity
                of AI lies not in single tasks but rather across the entire
                developer workflow.


                As we observed in 2023, this year developers reported spending
                less than a quarter of their time writing new code, with the rest
                spent on meetings and administrative tasks (15%), improving
                existing code (15%), understanding code (13%), testing (12%),
                maintaining code (11%), and identifying and mitigating security
                vulnerabilities (10%). That represents over 75% of developers'
                day-to-day where AI — in the form of vulnerability explanations,
                code change summaries, automated tests, and more — can have an
                impact by introducing efficiencies and boosting productivity and
                collaboration.
        - subtext: >-
            Executives are optimistic about AI, but need more visibility into
            teams' experiences
          config:
            inverse: true
            imgVariant: 2
            resultsFile: 2024_ai_training
          textBlocks:
            - title: Analysis
              text: >
                C-level executives expressed cautious optimism about AI this year,
                but were also cognizant of the risks of AI adoption: 62% of
                C-level respondents agreed that it is essential to implement AI in
                software development to avoid falling behind, while 56% agreed
                that introducing AI into the software development lifecycle is
                risky.


                However, individual contributors and managers were significantly
                more likely than C-level executives to say that their
                organizations are unprepared to adopt AI. In addition, individual
                contributors were significantly more likely than C-level
                executives to say that their organizations do not provide adequate
                training and resources for using AI. This could suggest that
                organizational leaders lack visibility into the reality of AI
                adoption on the ground — or that organizations are making a
                top-down attempt to make AI resources available to employees, but
                those resources may not be adequate, or some employees may not be
                aware of them.


                How can leaders get more visibility into how their teams are
                adapting to using AI? One answer is to incorporate AI tools into a
                single data store that touches all stages of software development
                — for example, through a DevSecOps platform — so leaders can
                leverage capabilities such as AI impact reporting to understand
                how teams are using AI and where it's introducing efficiencies, as
                well as where teams may be struggling. That data can help
                executives monitor AI adoption and assess the benefits and
                business value of AI features.
    - title: Toolchains remain a barrier to developer experience
      mobileSubtitle: 'Part 3: Developer experience'
      config:
        imgSrc: '/images/developer-survey/2024/developer-experience-hero.png'
      text: >-
        Great developer experience means removing obstacles so developers can
        onboard quickly and start creating value right away. Developer experience
        has been shown to help organizations accelerate innovation, enhance
        efficiency, and attract top talent. But the road to better developer
        experience isn't always a straight line — new technologies and trends can
        introduce efficiencies in some areas and complications in others. While
        organizations are making efforts to improve developer experience across
        the board, this year's survey revealed a number of potential stumbling
        blocks.
      subsections:
        - subtext: 'What developers want: Automation, collaboration, and AI'
          config:
            resultsFile: 2024_dev_satisfaction
            imgVariant: 3
          callOutBox:
            text: 67% of respondents said their software development lifecycle is mostly or completely automated.
            stat:
              number: 67
              config:
                color: '#FCA326'
          textBlocks:
            - title: Analysis
              text: >
                When asked how their organizations can improve developer
                experience, developers were clear: automation, collaboration, and
                use of AI tools topped the list, in addition to work
                considerations such as better pay and more flexible work
                arrangements.


                As we've seen in the list of investment priorities, organizations
                seem to be listening, as they're increasing investments in
                automation, collaboration (in the form of DevSecOps platforms),
                and AI. In fact, more than half (67%) of respondents this year
                said their software development lifecycle is mostly or completely
                automated. In Europe, where our survey showed particularly rapid
                AI adoption, this was notably higher, at 72%.
        - subtext: Teams are approaching tool overload
          config:
            inverse: true
            imgVariant: 4
            resultsFile: 2024_number_of_tools
          textBlocks:
            - title: Analysis
              text: >
                Despite increased investment in many of the drivers of positive
                developer experience, one key indicator showed significant warning
                signs in our survey: the number of tools teams are using on a
                daily basis. Overall, 64% of respondents this year told us they
                want to consolidate their toolchain. Of those who told us they
                have begun to or want to consolidate, more than a quarter (27%)
                said that having too many tools negatively impacts developer
                experience due to too much context-switching from tool to tool.


                Consistent with the large proportion of respondents who want to
                consolidate their toolchain, this year we observed a significant
                increase in the number of respondents who said they use 6-10,
                11-14, and more than 15 tools compared to last year, and a
                corresponding decrease in the number of respondents who said they
                use between 5 or fewer tools.
            - title: Global trends & industry insights
              text: |
                ### Tech is feeling the toolchain strain

                Respondents at software, SaaS, and computer hardware organizations
                seemed to be feeling the strain the most, with a full quarter
                (25%) of respondents saying that they use 11 or more tools. The
                automotive industry, by comparison, had just 9% using 11 or more
                tools.



                ### France has lighter toolchains — but still wants to consolidate

                Although less than half (47%) of respondents in France said they
                use 6 or more tools — compared to 70% in Germany and 63% in both
                the U.S. and Canada — the majority of respondents in France (66%)
                still want to consolidate their toolchain.
        - subtext: Onboarding is slowing developers down
          config:
            imgVariant: 5
            resultsFile: 2024_onboarding_time
          textBlocks:
            - title: Analysis
              text: >
                Developer onboarding is another key indicator of developer
                experience that showed signs of decline in 2024 — perhaps in part
                because of the continued acceleration of tool sprawl.


                This year, the number of respondents who said it takes between 1
                and 2 months for developers to get onboarded and become productive
                increased significantly compared to last year, and fewer
                respondents said it takes a month or less.
            - title: Global trends & industry insights
              text: >
                ### Tech and automotive organizations lead the pack

                Software, SaaS, and computer hardware organizations and automotive
                organizations had comparatively shorter developer onboarding
                times, with just 16% and 9% of respondents, respectively,
                reporting developer onboarding times of more than 3 months. More
                than a third (39%) of automotive respondents reported developer
                onboarding times of less than 2 weeks.

                ### Are technology investments paying off in Germany?

                Germany had higher rates of AI adoption (53%) and automation (74%)
                than other countries in our survey, and our findings suggest that
                these investments might be paying off in the form of developer
                onboarding. Of the countries included in the survey, Germany had
                the most respondents (18%) who said it takes less than 2 weeks to
                get developers onboarded.
        - subtext: AI and DevSecOps platform usage speed up developer onboarding
          config:
            inverse: true
            imgVariant: 1
            resultsFile: 2024_ai_onboarding
          textBlocks:
            - title: Analysis
              text: >
                This year, we observed both AI usage and a DevSecOps platform to
                have a significant positive effect on developer onboarding.
                Respondents who said they are currently using AI for software
                development (43%) were much more likely than those not using AI
                (20%) to say that developer onboarding typically takes less than a
                month. The same effect was observed for DevSecOps platform usage,
                with 44% of those currently using a platform saying that developer
                onboarding takes less than a month, compared to 20% of those not
                using a platform.
        - subtext: >-
            Executives acknowledge the importance of developer productivity, but
            measurement is difficult
          config:
            imgVariant: 2
            resultsFile: 2024_developer_productivity
          textBlocks:
            - title: Analysis
              text: >
                When organizations remove obstacles so developers can onboard
                quickly and start creating value right away, one of the primary —
                and most measurable — outcomes is developer productivity.


                In our survey, over half (55%) of C-level executives agreed that
                developer productivity is important to the success of their
                organizations, and 57% of C-level executives agreed that measuring
                developer productivity is key to business growth.


                However, only 42% of C-level executives currently measure
                developer productivity within their organization and are happy
                with their approach. Over a third (36%) feel their methods for
                measuring developer productivity are flawed, while 15% want to
                measure developer productivity but aren't sure how.


                Leveraging a DevSecOps platform provides a foundation for
                measuring developer productivity by bringing metrics from all
                stages of the development process into a single data store. In our
                survey, C-level respondents whose organizations are using a
                platform (56%) were much more likely than those not using a
                platform (33%) to be happy with their current approach to
                measuring developer productivity.
    - title: 'Security responsibilities are shifting, but challenges remain'
      mobileSubtitle: 'Part 4: Security'
      config:
        imgSrc: '/images/developer-survey/2024/security-hero.png'
      text: >-
        Security continues to be a top priority for organizations, and this year's
        survey highlighted a number of ways investments in security are paying
        off. However, the survey respondents also flagged several areas where
        shifts in mindset haven't quite translated into shifts in practice, as
        well as gaps that organizations should prioritize as they seek to more
        deeply embed security into software development workflows.
      subsections:
        - subtext: The reality of shared security responsibility sets in
          config:
            inverse: true
            imgVariant: 3
            resultsFile: 2024_responsibility_role
          textBlocks:
            - title: Analysis
              text: >
                This year, developers were significantly more likely than security
                respondents to say that developers are responsible for security.
                This is different from the trend observed in 2023, where
                developers were more likely than security respondents to say
                security is primarily responsible.


                In addition, a quarter (25%) of all respondents strongly agreed
                that they are primarily responsible for application security, and
                another 33% agreed, indicating that over half (58%) of all
                respondents feel some degree of responsibility for application
                security. In other words, security is not just the responsibility
                of the security team.


                Despite the move to a more collaborative attitude towards
                security, however, security teams themselves still voiced several
                concerns in our survey. Over half (58%) of security respondents
                said they have a difficult time getting development to prioritize
                remediation of vulnerabilities, up from 42% last year, and 55% of
                security respondents said security vulnerabilities are mostly
                discovered by the security team after code is merged into a test
                environment, up from 46% last year.


                Security and operations teams were also more likely to experience
                frustration with toolchains: 86% of operations respondents and 84%
                of security respondents told us they spend a quarter or more of
                their time on toolchain maintenance and integration, compared to
                68% of developers.
        - subtext: Software supply chain security is a potential weak spot
          config:
            resultsFile: 2024_security
            imgVariant: 4
          textBlocks:
            - title: Analysis
              text: >
                Overall, 67% of developers told us that a quarter or more of the
                code they work on is from open source libraries. This was even
                higher in software, SaaS, and computer hardware organizations, at
                85% of developers, as well as in the Asia-Pacific region (76%).


                Capabilities like a software bill of materials (SBOM) — a list of
                all the components, libraries, and modules that make up an
                application — are essential for maintaining the security of the
                software supply chain, especially as the amount of code pulled
                from open source libraries increases. However, in our survey, only
                21% of respondents said their organizations are currently using
                SBOMs to enable security in the software development lifecycle.
                This was particularly low in the Asia-Pacific region, at just 15%.


                Organizations face a growing imperative to be aware of the makeup of the
                software that they are producing. Organizations should consider
                using tools like SBOMs in tandem with other capabilities of a
                DevSecOps platform, such as software composition analysis, to
                prevent software supply chain security breaches, and to ensure
                insecure open source components aren't creating additional work
                for security teams.
        - subtext: Bridging the security perception gap
          config:
            inverse: true
            imgVariant: 5
            resultsFile: 2024_security_perception
          textBlocks:
            - title: Analysis
              text: >
                Our survey findings highlighted that C-level executives appear to
                be optimistic about DevSecOps and the security benefits it's
                bringing to their organizations — but, as we observed with AI,
                leaders should ensure that they keep a pulse on how tools and
                methodologies are being implemented by their teams in practice.


                Security is clearly top of mind for the C-suite, and leaders are
                seeing positive results from their investments in DevSecOps.
                C-level executives identified more secure applications as the top
                benefit of DevOps and DevSecOps, followed by faster iteration and
                better code quality. C-level respondents at organizations that are
                currently using a DevSecOps platform also pointed to security as
                the number two benefit of adopting a platform, after improved
                operational efficiency.


                However, discrepancies in attitudes between leaders and individual
                contributors could suggest that leaders are unaware of some of the
                security challenges teams are facing. Over 60% of C-level
                executives told us they feel DevSecOps practices are
                well-ingrained in their organization, but significantly fewer
                individual contributors (52%) felt the same way. Similarly,
                C-level executives (64%) were significantly more likely than
                managers (59%) and individual contributors (58%) to say they are
                confident in their organization's approach to application
                security.


                What's causing the disconnect? Security teams themselves pointed
                to frustrations with organizational processes that could be
                negatively impacting security practices. Over half (52%) of
                security respondents said their efforts to quickly fix
                vulnerabilities are often slowed by red tape at their
                organization, up from 43% in 2023.


                The perception gap between leaders and individual contributors
                could potentially prevent organizations from fully realizing the
                benefits of DevSecOps. Our survey suggests that investing in tools
                that foster collaboration and cut through red tape, like a
                DevSecOps platform, is one way to close the gap: Respondents who
                are using a platform (68%) were more likely than those not using a
                platform (56%) to be confident in their organization's approach to
                application security.
    - title: Who took the survey?
      config:
        demographicsSection: true
        imgSrc: '/images/developer-survey/2024/2024-survey-hero.png'
      mobileSubtitle: Demographics
      text: >-
        We collected a total of 5,315 survey responses in April 2024 from
        individual contributors and leaders in development, IT operations,
        and security across a mix of industries and business sizes
        worldwide.


        **We used two sampling methods for the data collection:**

        - We distributed the survey via GitLab's social media channels and
        email lists.

        - A third-party research partner conducted panel sampling, which
        reduces bias in the sample. Our research partner used its
        proprietary access to lists, panels, and databases to gather
        quality responses and cleaned the data throughout fielding to
        ensure data quality.
      subsections:
        - subtitle: null
          config:
            resultsFile: 2024_demographics
          textBlocks:
            - title: Methodology
              config:
                mobileOnly: true
              text: >
                We collected a total of 5,315 survey responses in April 2024 from
                individual contributors and leaders in development, IT operations,
                and security across a mix of industries and business sizes
                worldwide.


                **We used two sampling methods for the data collection:**

                - We distributed the survey via GitLab's social media channels and
                email lists.

                - A third-party research partner conducted panel sampling, which
                reduces bias in the sample. Our research partner used its
                proprietary access to lists, panels, and databases to gather
                quality responses and cleaned the data throughout fielding to
                ensure data quality.
  allReports:
    header: Explore previous Global DevSecOps Reports
    reports:
      - label: 2023
        config:
          href: '/developer-survey/previous/2023/'
      - label: 2022
        config:
          href: '/developer-survey/previous/2022/'
      - label: 2021
        config:
          href: '/developer-survey/previous/2021/'
      - label: 2020
        config:
          href: '/developer-survey/previous/2020/'
      - label: 2019
        config:
          href: '/developer-survey/previous/2019/'
      - label: 2018
        config:
          href: '/developer-survey/previous/2018/'
