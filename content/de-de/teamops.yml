config:
  enableAnimations: true
seo:
  title: 'TeamOps: Optimierung der Teameffizienz'
  description: TeamOps ist eine zielorientierte Teammanagement-Disziplin. Sie baut Entscheidungsblockaden ab und ermöglicht eine rasche und effiziente Realisierung. Mehr erfahren!
  ogImage: /images/open-graph/teamops-opengraph.png
  ogImageAlt: teamops opengraph
content:
  - componentName: TeamOpsHero
    componentContent:
      title: Bleib in Verbindung. Bleib produktiv.
      subTitle: Teamarbeit für das moderne Zeitalter neu definieren
      description: Teamarbeit für das moderne Zeitalter neu definieren
      primaryButton:
        text: Erfahre mehr
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: enroll
          dataGaLocation: hero
      image:
        altText: 'Image: TeamOps'
        config:
          src: /images/teamops/hero-illustration.png
  - componentName: TeamOpsSpotlight
    componentContent:
      title: 'TeamOps: Ein Rahmen für die Zukunft der Arbeit'
      subTitle: Fragst du dich, wie du die Herausforderungen unserer neuen Arbeitsdynamik meistern kannst? TeamOps ebnet den Weg für optimierte Entscheidungen sowie eine nahtlose Ausführung und fördert erstklassige Teams, die bemerkenswerte Fortschritte erzielen.
      description: |
        TeamOps ist ein leistungsorientierter operativer Ansatz mit dem Ziel, die Teamdynamik zu optimieren, die Entscheidungsfindung zu vereinfachen und die Produktivität in Unternehmen zu steigern.

        TeamOps wurde von GitLab entwickelt, realisiert und verfeinert – es basiert auf umsetzbaren Prinzipien, die die Arbeitsweise und Beziehung von Teams verändern.
      link:
        text: TeamOps im Tarif Free freischalten
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: Unlock TeamOps for Free
          dataGaLocation: body
      list:
        title: 'TeamOps löst die häufigsten Herausforderungen, mit denen Teams konfrontiert sind:'
        items:
          - Verzögerungen bei der Entscheidungsfindung
          - Meetingüberdruss
          - Interne Fehlkommunikation
          - Langsame Übergaben und Workflow-Verzögerungen
  - componentName: TeamOpsFeatures
    componentContent:
      title: Optimiert für alle Teams. Remote, Hybrid oder im Büro.
      image:
        altText: 'Bild: Gründe, um an TeamOps zu glauben'
        config:
          src: /images/teamops/reasons-to-believe.png
      items:
        - title: TeamOps ist ein organisatorisches Betriebsmodell, das Teams dabei hilft, die Produktivität, Flexibilität und Autonomie zu maximieren, indem es Informationen effizienter verwaltet.
          description: Mit TeamOps können Unternehmen größere Fortschritte erzielen, indem es das Wissensmanagement, die Entscheidungsfindung und die Autonomie der Mitarbeitenden optimiert.
          config:
            icon: group
        - title: In der Praxis bewährt
          description: Der historische Erfolg von GitLab mit TeamOps steigert die Produktivität und die Arbeitsmoral des Teams. Wir haben es intern entwickelt und sind zuversichtlich, dass es Unternehmen weltweit verändern kann.
          config:
            icon: clipboard-check-alt
        - title: Geschäftliche Ergebnisse
          description: TeamOps basiert auf vier Leitprinzipien, die Unternehmen dabei helfen können, die dynamische, sich verändernde Natur der Arbeit effizient zu steuern.
          config:
            icon: principles
        - title: Strategien in Aktion
          description: Jedes Prinzip wird durch eine Reihe von Handlungsgrundsätzen unterstützt – praktische Arbeitsmethoden, die sofort umgesetzt werden können.
          config:
            icon: cog-user-alt
        - title: Beispiele aus der Praxis
          description: Anhand einer wachsenden Bibliothek von Beispielen, die auf echten Ergebnissen von GitLab und anderen TeamOps-Entwicklungsfachkräften basieren, erwecken wir die Handlungsgrundsätze zum Leben.
          config:
            icon: case-study-alt
        - title: Lernservices
          description: Erhalte Unterstützung bei der Einführung des TeamOps-Modells mittels Schulungen, Workshops, Zertifizierungen, Bewertungen, Beratung und mehr.
          config:
            icon: verification
  - componentName: TeamOpsVideoSpotlight
    componentContent:
      title: Tauche ein in TeamOps
      subTitle: Die Welt hat viele Meinungen über die Zukunft der Arbeit.
      description: |
        Die TeamOps-Schulung hilft Unternehmen dabei, größere Fortschritte zu erzielen, indem die Beziehungen zwischen Teammitgliedern als ein lösbares Problem betrachtet wird. In nur wenigen Stunden führt dich unsere kostenlose Schulung durch die einzelnen Leitprinzipien sowie Handlungsgrundsätze des Modells und prüft die Kompatibilität mit den aktuellen Arbeitsabläufen in deinem Team.
      video:
        config:
          url: https://player.vimeo.com/video/754916142
      link:
        text: Jetzt registrieren
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: enroll your team
          dataGaLocation: body
  - componentName: TeamOpsCardSection
    componentContent:
      title: Leitprinzipien von TeamOps
      description: TeamOps erleichtert die effektive Zusammenarbeit anhand von vier Phasen der Gruppenausrichtung.
      cards:
        - title: Geteilte Realität
          description: Während andere Managementphilosophien die Geschwindigkeit der Wissensübertragung in den Vordergrund stellen, ist TeamOps auf die Geschwindigkeit des Wissensabrufs optimiert.
          link:
            text: Mehr erfahren (nur in englischer Sprache)
            config:
              href: https://handbook.gitlab.com/teamops/shared-reality/
          config:
            icon: d-and-i
            color: '#FCA326'
        - title: Gleiche Beiträge
          description: Unternehmen müssen ein System schaffen, in dem jede(r) Informationen nutzen und Beiträge leisten kann, unabhängig von Ebene, Funktion oder Standort.
          link:
            text: Mehr erfahren (nur in englischer Sprache)
            config:
              href: https://handbook.gitlab.com/teamops/equal-contributions/
          config:
            icon: user-collaboration
            color: '#966DD9'
        - title: Entscheidungsgeschwindigkeit
          description: 'Der Erfolg steht und fällt mit der Entscheidungsgeschwindigkeit: der Anzahl der Entscheidungen, die in einer bestimmten Zeitspanne getroffen werden, und den Ergebnissen, die sich aus dem schnelleren Fortschritt ergeben.'
          link:
            text: Mehr erfahren (nur in englischer Sprache)
            config:
              href: https://handbook.gitlab.com/teamops/decision-velocity/
          config:
            icon: speed-alt-2
            color: '#FD8249'
        - title: Transparente Maßstäbe
          description: Es geht darum, die richtigen Dinge zu messen. Die Entscheidungsfindungsprinzipien von TeamOps sind nur dann nützlich, wenn du die Ergebnisse ausführst und bewertest.
          link:
            text: Mehr erfahren (nur in englischer Sprache)
            config:
              href: https://handbook.gitlab.com/teamops/measurement-clarity/
          config:
            icon: target
            color: '#256AD1'
  - componentName: TeamOpsJoinUsSection
    componentContent:
      title: Schließe dich der Bewegung an
      description: |
        TeamOps ist ein organisatorisches Betriebsmodell, das Teams dabei hilft, die Produktivität, Flexibilität und Autonomie zu maximieren, indem es Entscheidungen, Informationen und Aufgaben effizienter verwaltet.

        Schließe dich einer wachsenden Anzahl von Unternehmen an, die TeamOps verwenden.
      link:
        text: Jetzt registrieren
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: enroll your team
          dataGaLocation: body
  - componentName: CommonNextSteps
