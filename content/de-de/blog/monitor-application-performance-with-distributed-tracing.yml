seo:
  title: Application Performance Monitoring (APM) mit Distributed Tracing
  description: >-
    Erfahre, wie Distributed Tracing die Fehlerbehebung bei Problemen mit der
    Application Performance unterstützt, indem es eine durchgängige Transparenz
    und eine nahtlose Zusammenarbeit innerhalb deines Unternehmens ermöglicht.
  ogTitle: Application Performance Monitoring (APM) mit Distributed Tracing
  ogDescription: >-
    Erfahre, wie Distributed Tracing die Fehlerbehebung bei Problemen mit der
    Application Performance unterstützt, indem es eine durchgängige Transparenz
    und eine nahtlose Zusammenarbeit innerhalb deines Unternehmens ermöglicht.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(8).png
  ogUrl: >-
    https://about.gitlab.com/blog/monitor-application-performance-with-distributed-tracing
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/monitor-application-performance-with-distributed-tracing
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Application Performance Monitoring (APM) mit Distributed Tracing",
            "author": [{"@type":"Person","name":"Sacha Guyon"}],
            "datePublished": "2024-06-13",
          }

content:
  title: Application Performance Monitoring (APM) mit Distributed Tracing
  description: >-
    Erfahre, wie Distributed Tracing die Fehlerbehebung bei Problemen mit der
    Application Performance unterstützt, indem es eine durchgängige Transparenz
    und eine nahtlose Zusammenarbeit innerhalb deines Unternehmens ermöglicht.
  authors:
    - Sacha Guyon
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(8).png
  date: '2024-06-13'
  body: >
    Ausfallzeiten aufgrund von Anwendungsfehlern oder Performance-Problemen
    können für Unternehmen verheerende finanzielle Folgen haben. Laut
    Information einer [Umfrage der ITIC von
    2022](https://itic-corp.com/server-and-application-by-the-numbers-understanding-the-nines/
    "Umfrage der ITIC von 2022") kostet eine Stunde Ausfallzeit Unternehmen
    schätzungsweise 301.000 $ oder mehr. Diese Probleme sind häufig auf von
    Menschen vorgenommene Änderungen zurückzuführen, z. B. Code- oder
    Konfigurationsänderungen.


    Um Probleme schnell zu beheben, müssen Entwicklungs- und Betriebsteams Hand
    in Hand arbeiten, um die Ursache zu finden und das System zügig
    wiederherzustellen. Häufig nutzen sie jedoch unterschiedliche Tools, um ihre
    Anwendungen und Infrastruktur zu verwalten und zu überwachen. Das führt zu
    isolierten Daten, schlechter Kommunikation und einem unvollständigen
    Überblick – was die Lösung von Vorfällen verzögert.


    GitLab setzt genau hier an und vereint Softwarebereitstellung und Monitoring
    auf einer Plattform. Letztes Jahr haben wir [Error
    Tracking](https://docs.gitlab.com/ee/operations/error_tracking.html "Error
    Tracking") in [GitLab
    16.0](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#error-tracking-is-now-generally-available
    "GitLab 16.0") als feste Funktion integriert. Jetzt freuen wir uns, die
    [Beta-Version von Distributed
    Tracing](https://docs.gitlab.com/ee/operations/tracing "Beta-Version von
    Distributed Tracing") vorzustellen – der nächste große Schritt zu einem
    umfassenden Monitoring, das vollständig in die GitLab-DevSecOps-Plattform
    eingebettet ist.


    ## Eine neue Ära der Effizienz: GitLab Observability


    GitLab Observability ermöglicht es Entwicklungs- und Operationsteams,
    Fehler, Traces, Protokolle und Metriken aus ihren Anwendungen und ihrer
    Infrastruktur zu visualisieren und zu analysieren. Durch die Integration des
    Application Performance Monitoring in bestehende
    Softwarebereitstellungs-Workflows werden Kontextwechsel minimiert und die
    Produktivität erhöht, sodass die Teams auf einer einheitlichen Plattform
    fokussiert zusammenarbeiten können.


    Darüber hinaus überbrückt GitLab Observability die Kluft zwischen
    Entwicklung und Betrieb, indem es Einblicke in die Application Performance
    in der Produktion gewährt. Dies verbessert die Transparenz, den
    Informationsaustausch und die Kommunikation zwischen den Teams. Folglich
    können sie Fehler und Leistungsprobleme, die durch neuen Code oder
    Konfigurationsänderungen entstehen, schneller und effektiver erkennen und
    beheben und so verhindern, dass diese Probleme zu größeren Vorfällen
    eskalieren, die sich negativ auf das Geschäft auswirken könnten.


    ## Was ist Distributed Tracing?


    Mit Distributed Tracing können Entwickler(innen) die Ursache von
    Leistungsproblemen bei Anwendungen und in der Architektur verteilter Systeme
    ermitteln. Ein Trace stellt eine einzelne Benutzeranfrage dar, die
    verschiedene Dienste und Systeme durchläuft. Die Entwickler(innen) sind in
    der Lage, den zeitlichen Ablauf der einzelnen Vorgänge und alle auftretenden
    Fehler zu analysieren.


    Jede Protokollierung besteht aus einem oder mehreren Abschnitten, die
    einzelne Vorgänge oder Arbeitseinheiten darstellen. Spans enthalten
    Metadaten wie den Namen, Zeitstempel, Status und relevante Tags oder
    Protokolle. Durch die Untersuchung der Abhängigkeiten zwischen Spans können
    Entwickler(innen) den Request Flow verstehen, Performance-Engpässe erkennen
    und Probleme lokalisieren.


    ### Distributed Tracing in Microservices


    Distributed Tracing ist besonders wertvoll für
    [Microservices-Strukturen](https://about.gitlab.com/de-de/topics/microservices/
    "Microservices-Strukturen"), bei denen eine einzige Anfrage zahlreiche
    Serviceaufrufe in einem komplexen System nach sich ziehen kann. Tracing
    bietet Einblick in diese Interaktion und ermöglicht es den Teams, Probleme
    schnell zu diagnostizieren und zu beheben.


    ![Nutzeranfrage beim Distributed Tracing, um Produktempfehlungen
    abzurufen](//images.ctfassets.net/r9o86ar0p03f/1qNRNgWU97JxByxJpEto1p/1757a61deae05c67e28e702a19e3dbe6/image4.png)


    Dieses Beispiel veranschaulicht, wie eine Nutzeranfrage verschiedene Dienste
    durchläuft, um Produktempfehlungen auf einer E-Commerce-Website abzurufen:

    - `User Action`: Dies zeigt die erste Aktion der Nutzer(innen) an, z. B. das
    Anklicken einer Schaltfläche zur Anforderung von Produktempfehlungen auf
    einer Produktseite.

    - `Web front-end`: Das Web-Frontend sendet eine Anfrage an den
    Empfehlungsdienst, um Produktempfehlungen abzurufen.

    - `Recommendation service`: Die Anfrage vom Web-Frontend wird vom
    Recommendation Service (dt. Empfehlungsservice) bearbeitet, der die Anfrage
    verarbeitet und eine Liste empfohlener Produkte erstellt.

    - `Catalog service`: Der Recommendation Service ruft den Servicekatalog auf,
    um Details zu den empfohlenen Produkten abzurufen. Ein Warnsymbol weist auf
    ein Problem oder eine Verzögerung in dieser Phase hin, z. B. eine zu
    langsame Rückmeldung oder einen Fehler beim Abrufen von Produktdetails.

    - `Database`: Der Catalog Service fragt die Datenbank ab, um die aktuellen
    Produktdaten abzurufen. Dieser Bereich zeigt die SQL-Abfrage in der
    Datenbank.


    Durch die Visualisierung dieses End-to-End-Trace können Entwickler(innen)
    Leistungsprobleme – hier ein Fehler im Servicekatalog – erkennen und
    Probleme im gesamten Distributionssystem schnell diagnostizieren und
    beheben.


    ![Visualisierung des End-to-End-Trace in
    GitLab](//images.ctfassets.net/r9o86ar0p03f/7eBuBrUWPXSt2ckw96P0HM/1d0a356e2bfec01a67a9d16d0874f7a9/image1.png)


    ## Wie funktioniert Distributed Tracing?


    Im Folgenden wird die Funktionsweise von Distributed Tracing Tools
    erläutert.


    ### Sammeln von Daten aus beliebigen Anwendungen mit OpenTelemetry


    Traces und Spans können mit
    [OpenTelemetry](https://opentelemetry.io/docs/what-is-opentelemetry/
    "OpenTelemetry") gesammelt werden, einem Open-Source-Framework für
    Beobachtungen, das eine breite Palette von SDKs und Bibliotheken für [die
    wichtigsten Programmiersprachen und
    Frameworks](https://opentelemetry.io/docs/languages/ "die wichtigsten
    Programmiersprachen und Frameworks") unterstützt. Dieses Framework bietet
    einen herstellerneutralen Ansatz für die Erfassung und den Export von
    Telemetriedaten, sodass Entwickler(innen) nicht an einen bestimmten Anbieter
    gebunden sind und die Tools auswählen können, die ihren Anforderungen am
    besten entsprechen.


    Das bedeutet, dass du, wenn du bereits OpenTelemetry mit einem anderen
    Anbieter verwendest, Daten an uns senden kannst, indem du einfach unseren
    Endpunkt zu deiner Konfigurationsdatei hinzufügst –so kannst du unsere
    Funktionen ohne Weiteres ausprobieren!


    ![Daten aus verschiedenen Anwendungen werden über OpenTelemetry and GitLab
    Observability
    geschickt.](//images.ctfassets.net/r9o86ar0p03f/kAyd25WzKkdYClze4iz7L/3501a45c9765ae22542a204faf839328/image5.png)


    ### Datenerfassung und -speicherung in großem Umfang mit schnellen
    Echtzeitabfragen


    Beobachtbarkeiterfordert die Speicherung und Abfrage riesiger Datenmengen
    bei gleichzeitiger Beibehaltung niedriger Latenzzeiten für
    Echtzeit-Analysen. Um diese Anforderungen zu erfüllen, haben wir eine
    horizontal skalierbare Langzeitspeicherlösung mit ClickHouse und
    [Kubernetes](https://about.gitlab.com/de-de/solutions/kubernetes/
    "Kubernetes") entwickelt, die auf unserer Übernahme von Opstrace basiert.
    Diese
    [Open-Source-Plattform](https://gitlab.com/gitlab-org/opstrace/opstrace
    "Open-Source-Plattform") gewährleistet eine schnelle Abfrageleistung und
    Skalierbarkeit auf Unternehmensebene, während gleichzeitig die Kosten
    minimiert werden.


    ### Traces mühelos untersuchen und analysieren


    Eine fortschrittliche Benutzeroberfläche auf nativer Ebene ist entscheidend
    für eine effektive Datenanalyse. Wir haben eine solche Schnittstelle von
    Grund auf entwickelt. Der Trace Explorer ermöglicht es den Nutzer(innen),
    Traces zu untersuchen und die Leistung ihrer Anwendung zu verstehen:


    - __Erweiterte Filterung:__ Filtere nach Diensten, Vorgangsnamen, Status und
    Zeitspanne. Die automatische Vervollständigung vereinfacht die Abfrage.

    - __Fehlerhervorhebung:__ Fehlerbereiche in den Suchergebnissen werden
    einfach erkannt.

    - __RED-Metriken:__ Visualisiere die Anforderungsrate, die Fehlerrate und
    die durchschnittliche Dauer in einem Zeitseriendiagramm für jede Suche in
    Echtzeit.

    - __Timeline-Ansicht:__ Einzelne Traces werden als Wasserfalldiagramm
    dargestellt. So erhältst du einen vollständigen Überblick über eine Anfrage,
    die über verschiedene Dienste und Vorgänge verteilt ist.

    - __Historische Daten:__ Nutzer(innen) können bis zu 30 Tage in die
    Vergangenheit zurückreichende Daten abfragen.


    ![Tracing Benutzeroberfläche für eine effektive
    Datenanalyse.](//images.ctfassets.net/r9o86ar0p03f/01HtmDfoJjsDxQV1f6Of6L/9cb26cffe466f40af020f3ab6f3484a0/image3.png)


    ## Wie wir bei GitLab das Distributed Tracing einsetzen


    [Dogfooding](https://handbook.gitlab.com/handbook/values/#dogfooding
    "Dogfooding") ist ein zentraler Wert und eine wichtige Praxis bei GitLab.
    Wir haben bereits frühere Versionen von Distributed Tracing für unsere
    technischen und betrieblichen Anforderungen verwendet. Hier sind ein paar
    Anwendungsbeispiele aus unseren Teams:


    ### 1. Beheben von Fehlern und Leistungsproblemen in GitLab Agent für
    Kubernetes


    Die
    [Environments-Gruppe](https://handbook.gitlab.com/handbook/engineering/development/ops/deploy/environments/
    "Environments-Gruppe") hat Distributed Tracing verwendet, um Probleme mit
    dem [GitLab Agent für
    Kubernetes](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent
    "GitLab Agent für Kubernetes") zu beheben, z. B. Zeitüberschreitungen oder
    hohe Latenzzeiten. Die Ansichten „Trace List“ und „Trace Timeline“ bieten
    dem Team wertvolle Einsichten, mit denen sie derartige Probleme effizient
    lösen können. Diese Traces werden in den [entsprechenden GitLab
    Issues](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/386#note_1576431796
    "entsprechenden GitLab Issues") geteilt und diskutiert, wo das Team dann
    gemeinsam an der Lösung arbeitet.


    *„Die Funktion Distributed Tracing ist von unschätzbarem Wert, wenn es darum
    geht, festzustellen, wo Latenzprobleme auftreten, sodass wir uns auf die
    eigentliche Ursache konzentrieren und diese schneller beheben können.“ -
    Mikhail, GitLab-Ingenieur*


    ### 2. Optimierung der Dauer der GitLab-Build-Pipeline durch Ermittlung von
    Leistungsengpässen 


    Langsame Deployments von GitLab-Quellcode können sich erheblich auf die
    Produktivität des gesamten Unternehmens sowie auf unsere Rechenleistung
    auswirken. In unserem Haupt-Repository werden jeden Monat [über 100.000
    Pipelines](https://gitlab.com/gitlab-org/gitlab/-/pipelines/charts "über
    100.000 Pipelines") ausgeführt. Wenn sich die Zeit für die Ausführung dieser
    Pipelines nur um eine Minute ändert, können dadurch mehr als 2.000 Stunden
    Arbeitszeit hinzukommen oder wegfallen. Das sind 87 zusätzliche Tage!


    Um die Ausführungszeit von Pipelines zu optimieren, verwenden die
    [Entwicklerteams der
    GitLab-Plattform](https://handbook.gitlab.com/handbook/engineering/infrastructure/
    "Entwicklerteams der GitLab-Plattform") ein [speziell entwickeltes
    Tool](https://gitlab.com/gitlab-com/gl-infra/gitlab-pipeline-trace "speziell
    entwickeltes Tool"), das GitLab-Deployment-Pipelines in Traces umwandelt.


    Die Ansicht „Trace Timeline“ ermöglicht es den Teams, die detaillierte
    Ausführung komplexer Pipelines zu visualisieren und festzustellen, welche
    Aufträge Teil des kritischen Pfads sind und den gesamten Prozess
    verlangsamen. Durch die Identifizierung dieser Engpässe können sie die
    Auftragsausführung optimieren, indem sie z. B. dafür sorgen, dass der
    Auftrag schneller fehlschlägt oder mehr Aufträge parallel ausgeführt werden,
    um die Gesamteffizienz der Pipeline zu verbessern.


    ![Trace Timeline und
    Details](//images.ctfassets.net/r9o86ar0p03f/18cvI5mSqS1ntoI3Yj33fY/56a524d1b13a0a5b52b224560ae1a11a/image2.gif)


    Das Skript ist frei verfügbar, sodass du es für deine eigenen Pipelines
    anpassen kannst.


    *„Der Einsatz von Distributed Tracing für unsere Deployment-Pipelines war
    ein echter Durchbruch. Es hat uns geholfen, Engpässe schnell zu erkennen und
    zu beseitigen, was unsere Bereitstellungszeiten erheblich verkürzt hat“ -
    Reuben, GitLab Engineer*


    ## Was kommt als Nächstes?

    In den nächsten Monaten werden wir unsere Beobachtbarkeits- und
    Monitoring-Funktionen mit den kommenden Versionen Metrics und Logging weiter
    ausbauen. Weitere Informationen findest du auf unserer Seite zu
    GitLabObservability.


    ## Nimm an der privaten Beta teil

    Möchtest du Teil dieser aufregenden Entwicklung sein? Melde dich für die
    private Beta-Version an und probiere unsere Funktionen aus. Mit deinem
    Beitrag kannst du die Zukunft der Beobachtbarkeit in GitLab mitgestalten und
    sicherstellen, dass unsere Tools perfekt auf deine Bedürfnisse und
    Herausforderungen abgestimmt sind.
  category: Produkt
  tags:
    - performance
    - features
    - news
    - DevSecOps platform
    - collaboration
config:
  slug: monitor-application-performance-with-distributed-tracing
  featured: true
  template: BlogPost
