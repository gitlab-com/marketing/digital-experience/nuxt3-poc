seo:
  title: "Automatisierung mit GitLab\_Duo, Teil\_1: Generierung von Tests"
  description: >-
    Hier erfährst du, wie wir die KI-gestützte DevSecOps-Plattform genutzt
    haben, um automatisierte Tests zu erstellen und unsere
    Entwicklungsgeschwindigkeit und -qualität zu verbessern.
  ogTitle: "Automatisierung mit GitLab\_Duo, Teil\_1: Generierung von Tests"
  ogDescription: >-
    Hier erfährst du, wie wir die KI-gestützte DevSecOps-Plattform genutzt
    haben, um automatisierte Tests zu erstellen und unsere
    Entwicklungsgeschwindigkeit und -qualität zu verbessern.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(4).png
  ogUrl: >-
    https://about.gitlab.com/blog/automating-with-gitlab-duo-part-1-generating-tests
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/automating-with-gitlab-duo-part-1-generating-tests
  schema: "\n                        {\n        \"@context\": \"https://schema.org\",\n        \"@type\": \"Article\",\n        \"headline\": \"Automatisierung mit GitLab\_Duo, Teil\_1: Generierung von Tests\",\n        \"author\": [{\"@type\":\"Person\",\"name\":\"Byron Boots\"}],\n        \"datePublished\": \"2024-12-02\",\n      }\n                  "
content:
  title: "Automatisierung mit GitLab\_Duo, Teil\_1: Generierung von Tests"
  description: >-
    Hier erfährst du, wie wir die KI-gestützte DevSecOps-Plattform genutzt
    haben, um automatisierte Tests zu erstellen und unsere
    Entwicklungsgeschwindigkeit und -qualität zu verbessern.
  authors:
    - Byron Boots
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(4).png
  date: '2024-12-02'
  body: "Automatisierte Tests sind zeitaufwändig und können das Gefühl vermitteln, ein Projekt nicht voranzubringen. Doch wie viele Entwickler(innen) wahrscheinlich schon erlebt haben, machen sich automatisierte Tests insgesamt positiv bemerkbar. Für die Entwicklung eines benutzerdefinierten Moduls (in diesem Artikel nennen wir es gitlab-helper) traf dies besonders zu.\n\nZu Beginn der Entwicklung konzentrierten wir uns darauf, bewährte Funktionen aus bestehenden Skripts in ein neues Modul zu migrieren, dessen einziger Zweck es war, als Grundlage für zukünftige Funktionen zu dienen. Obwohl die bestehenden Skripts nicht automatisiert getestet wurden, war ihre konsequente Nutzung ein deutlicher Beweis dafür, dass die Funktionen wie erwartet funktionierten.\n\nUnser Ziel war es, eine ausgereiftere Lösung für dieses Problem zu entwickeln, sodass automatisierte Tests unumgänglich wurden. Das stellte uns vor die Herausforderung, die Entwicklung effizient zu gestalten und gleichzeitig die Zeit für das Testen und die Sicherstellung eines robusten Produkts unter einen Hut zu bringen; mit insgesamt nur drei Teammitgliedern war das gar nicht mal so einfach. Deshalb entschied sich das Team, für die Testerstellung die Vorteile unserer KI-Suite [GitLab\_Duo](https://about.gitlab.com/de-de/gitlab-duo/) zu nutzen und so die Entwicklungsgeschwindigkeit und Qualität des fertigen Produkts zu verbessern.\n\nIn dieser dreiteiligen Serie zur Automatisierung mit GitLab\_Duo behandeln wir die folgenden Punkte:\n\n1. Wie wir GitLab\_Duo verwendet haben, um Tests für unseren Code zu erstellen  \n2. Wie wir bei komplexeren Situationen interaktiv mit GitLab\_Duo gearbeitet haben  \n3. Die Ergebnisse, die wir erzielen konnten (Spoiler: 1\_Person\_+ GitLab\_Duo\_= 84\_% Testabdeckung in 2\_Tagen)\n\n## GitLab\_Duo zur Erstellung von Tests für Code verwenden\n\nDieser Artikel behandelt die Verwendung von GitLab\_Duo in VS\_Code mit der [GitLab Workflow Extension for VS Code](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow) (nur in englischer Sprache verfügbar), um Tests zu generieren. Diese Funktionalität ist in mehreren Tools verfügbar. Links zu anderen GitLab-Duo-Optionen findest du in den [Referenzen](#Referenzen) weiter unten.\n\n### GitLab\_Duo installieren und aktivieren\n\nUm GitLab\_Duo nutzen zu können, benötigen wir ein Konto, auf dem GitLab\_Duo aktiviert ist. Wenn du GitLab\_Duo noch nicht aktiviert hast, kannst du dich für eine [kostenlose 60-tägige Testversion anmelden](https://about.gitlab.com/de-de/solutions/gitlab-duo-pro/sales/?type=free-trial).Um GitLab\_Duo\_Chat in VS\_Code zu verwenden, folgten wir den [Anweisungen zur Installation](https://docs.gitlab.com/ee/user/gitlab_duo_chat/#use-gitlab-duo-chat-in-vs-code) (nur in englischer Sprache verfügbar). Dann war die GitLab-Duo-Chat-Erweiterung in der Seitenleiste verfügbar und wir konnten das Chat-Fenster öffnen.\n\n![Stelle-eine-Frage-Fenster](//images.ctfassets.net/r9o86ar0p03f/2btPkY0hnOVQWZSHcuBBEx/6d51e42b076d74d5fd30c89dbc3c51ca/image4.png)\n\n### Tests mit Chat generieren\n\ngitlab-helper ist ein benutzerdefiniertes Modul, das für die Standardisierung der Interaktion mit der GitLab-API innerhalb des Teams entwickelt wurde und andere Bibliotheksfunktionen erweitert, um die Entwicklungs- und Skriptarbeit zu vereinfachen. Sobald eine Methode oder Funktion in gitlab-helper migriert wurde und angemessen implementiert zu sein schien, konnten ganz einfach Tests dafür erstellt werden:\n- Wir wählten die Methode, die Klasse oder die gesamte Datei in der IDE aus.\n- Wir öffneten das Menü für den ausgewählten Code mit der rechten Maustaste.\n- Unter **GitLab\_Duo\_Chat** wählten wir **Tests generieren** aus.\n\n![Reihenfolge für die Erstellung von Tests, einschließlich der Auswahl-Liste für die Erstellung von Tests](//images.ctfassets.net/r9o86ar0p03f/3T9nbwY8UhXAKWWxQQRzJn/7ea80821fe0d1e19f34c0bb0eb3b8239/image1.png)\n\nInnerhalb weniger Sekunden waren die Tests erstellt und im GitLab-Duo-Chat-Fenster angezeigt. Diese Tests können überprüft und/oder durch Kopieren/Einfügen in bestehende oder neue Testdateien zur Codebase hinzugefügt werden. Wie bei den meisten Ergebnissen von linguistischer Datenverarbeitung, vor allem wenn es um den Kontext geht, schlugen einige der ersten von GitLab\_Duo erstellten Tests fehl, so dass eine Feinabstimmung erforderlich war (z.\_B. beim Umgang mit verschachtelten Abhängigkeiten).\n\n> **Profi-Tipp:** GitLab\_Duo erstellt nicht automatisch Dateien, in die generierte Tests eingefügt werden können. Wir haben festgestellt, dass es hilfreich ist, neue Testdateien zu erstellen und sie mit dem Kommentar `# Tests Generated by Duo` zu versehen und ihnen den Zusatz `_duo.py` hinzuzufügen, um anzugeben, woher die Tests stammen.\n\nGitLab\_Duo war ein großartiger Ausgangspunkt für die Entwicklung der automatisierten Tests von gitlab-helper und hat die Effizienz beim Schreiben von Tests und die Testabdeckung erheblich verbessert, was den Entwicklungsprozess erheblich beschleunigt hat. Neben GitLab\_Duo wurden zahlreiche sinnvolle Tests unter menschlicher Aufsicht in das gitlab-helper-Modul integriert.\n\nIm nächsten Teil dieser Serie erfährst du, [was wir bei der Verwendung von GitLab\_Duo für die Erstellung automatisierter Tests und die interaktive Arbeit mit KI für komplexere Situationen gelernt haben](https://about.gitlab.com/de-de/blog/2024/12/10/automating-with-gitlab-duo-part-2-complex-testing/).\n\n## Referenzen\n\nEs gibt mehr als eine Möglichkeit, GitLab\_Duo zu verwenden, um Tests zu generieren. Sieh dir die anderen Optionen unten an:\n\n* Die GitLab-Benutzeroberfläche  \n* [Die GitLab-Web-IDE (VS\_Code in der Cloud)](https://docs.gitlab.com/ee/user/project/web_ide/index.html) (nur in englischer Sprache verfügbar) \n* VS\_Code, mit der [GitLab Workflow Extension for VS\_Code](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow) (nur in englischer Sprache verfügbar)\n* JetBrains IDEs, mit dem [GitLab-Duo-Plugin für JetBrains](https://plugins.jetbrains.com/plugin/22325-gitlab-duo)  (nur in englischer Sprache verfügbar) \n* Visual Studio für Windows mit der [GitLab-Erweiterung für Visual Studio](https://marketplace.visualstudio.com/items?itemName=GitLab.GitLabExtensionForVisualStudio) (nur in englischer Sprache verfügbar)\n"
  category: KI/ML
  tags:
    - AI/ML
    - tutorial
    - testing
    - DevSecOps platform
    - features
config:
  slug: automating-with-gitlab-duo-part-1-generating-tests
  featured: false
  template: BlogPost
