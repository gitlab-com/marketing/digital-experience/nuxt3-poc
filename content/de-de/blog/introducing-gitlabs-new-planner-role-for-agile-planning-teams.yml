seo:
  title: >-
    Jetzt neu in GitLab: die neue Rolle „Planer(in)“ für Teams im Bereich der
    Agile-Planung
  description: >-
    Erfahre, wie die neue Rolle „Planer(in)“ in GitLab Agile-Teams dabei hilft,
    Workflows mit maßgeschneidertem Zugriff über SaaS-, Dedicated- und
    Self-Managed-Lösungen zu planen.
  ogTitle: >-
    Jetzt neu in GitLab: die neue Rolle „Planer(in)“ für Teams im Bereich der
    Agile-Planung
  ogDescription: >-
    Erfahre, wie die neue Rolle „Planer(in)“ in GitLab Agile-Teams dabei hilft,
    Workflows mit maßgeschneidertem Zugriff über SaaS-, Dedicated- und
    Self-Managed-Lösungen zu planen.
  noIndex: false
  ogImage: images/blog/hero-images/blog-image-template-1800x945-(3).png
  ogUrl: >-
    https://about.gitlab.com/blog/introducing-gitlabs-new-planner-role-for-agile-planning-teams
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/introducing-gitlabs-new-planner-role-for-agile-planning-teams
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Jetzt neu in GitLab: die neue Rolle „Planer(in)“ für Teams im Bereich der Agile-Planung",
            "author": [{"@type":"Person","name":"Amanda Rueda"}],
            "datePublished": "2024-11-25",
          }

content:
  title: >-
    Jetzt neu in GitLab: die neue Rolle „Planer(in)“ für Teams im Bereich der
    Agile-Planung
  description: >-
    Erfahre, wie die neue Rolle „Planer(in)“ in GitLab Agile-Teams dabei hilft,
    Workflows mit maßgeschneidertem Zugriff über SaaS-, Dedicated- und
    Self-Managed-Lösungen zu planen.
  authors:
    - Amanda Rueda
  heroImage: images/blog/hero-images/blog-image-template-1800x945-(3).png
  date: '2024-11-25'
  body: "GitLab hat eine neue Rolle auf der DevSecOps-Plattform eingeführt\_– Planer(in). Die Rolle als Planer(in) wurde im Rahmen der Strategie von GitLab, eine flexible, rollenbasierte Zugriffskontrolle zu bieten, entwickelt, was sich auch in der Veröffentlichung [benutzerdefinierter Rollen](https://docs.gitlab.com/ee/user/custom_roles.html) zeigt. Als Planer(in) können Entwicklungsteams und für die Planung zuständige Benutzer(innen) Zugang zu den Tools erhalten, die sie brauchen, um Agile-Workflows zu verwalten, ohne dabei zu viele Berechtigungen zu bekommen, die ein unnötiges Risiko darstellen. Indem der Zugriff auf die spezifischen Bedürfnisse der Benutzer(innen) zugeschnitten wird, stellt die Rolle als Planer(in) sicher, dass Teams produktiv bleiben und gleichzeitig die Sicherheit und Compliance wahren\_– ganz im Sinne des [Prinzips der geringsten Privilegien](https://about.gitlab.com/blog/2024/03/06/the-ultimate-guide-to-least-privilege-access-with-gitlab/).\n\n## Warum wir die Rolle „Planer(in)“ entwickelt haben\n\nUnser Weg zu dieser neuen Rolle begann mit dem Feedback unserer Kund(inn)en und internen Teams. Wir haben immer wieder gehört, dass GitLab zwar umfassende Tools für die Planung und die Verwaltung von agilen Entwicklungszyklen hat, aber es eine spezifischere, rollenbasierte Zugriffskontrolle bräuchte. Produktmanager(innen), Projektleitung und andere Planungsrollen brauchen oft Zugriff auf Planungsfunktionen, jedoch nicht die kompletten Entwicklungsberechtigungen. Im Gegenteil: Es ist nicht gewünscht, dass sie breiten Zugriff haben, da dies ein Sicherheitsrisiko darstellt und das Fehlerpotenzial vergrößert, indem beispielsweise unabsichtliche Änderungen am Code oder an sensiblen Konfigurationen vorgenommen werden. Diese Wünsche haben wir uns zu Herzen genommen.\n\nDurch Befragungen von Benutzer(inne)n, Wettbewerbsanalyse und umfassende Forschungen haben wir den Wunsch nach einer Rolle validiert, die vollständigen Zugriff auf Planungstools bietet, aber gleichzeitig die Sicherheit wahrt, indem kein Zugriff auf Funktionen für Entwickler(innen) möglich ist.\n\n## Was hat die Rolle „Planer(in)“ zu bieten?\n\nPlaner(in) ist eine Rolle, die sich zwischen den bestehenden Rollen [„Gast“ und „Reporter(in)“](https://docs.gitlab.com/ee/user/permissions.html#roles) einordnet, aber speziell für jene entwickelt wurde, die Zugriff auf Planungs-Workflows brauchen.\n\nDas erwartet dich:\n\n* Zugriff auf wichtige Planungstools wie Epics, Roadmaps, Issue-Übersichten und [OKRs](https://docs.gitlab.com/ee/user/okrs.html) (*Für einige Funktionen ist eine GitLab-Premium- oder -Ultimate-Lizenz erforderlich*)\n* Verbesserte Sicherheit, indem der unnötige Zugriff auf sensible Entwicklungsfunktionen eingeschränkt wird\n* Die Rolle als Planer(in) kann zusammen mit dem Add-on GitLab\_Enterprise Agile Planning verwendet werden, wodurch Teams maßgeschneiderten Zugriff auf Planungstools erhalten, während gleichzeitig Sicherheit und Kontrolle gewahrt werden. (*Die Rolle als Planer(in) ist aber in allen Lizenzstufen verfügbar*).\n\nDie Rolle „Planer(in)“ ist in allen GitLab-Lösungen verfügbar, darunter SaaS, GitLab\_Dedicated und GitLab\_Self-Managed. So können alle Kund(inn)en von diesem maßgeschneiderten Zugriff profitieren.\nMit dieser Rolle können Teams Berechtigungen flexibel an Jobfunktionen zuweisen, was ein ausgewogenes Maß an Barrierefreiheit und Sicherheit ermöglicht.\n\n## So unterstützt die Rolle als Planer(in) Agile-Praktiken\n\nIn der [Agile-Softwareentwicklung](https://about.gitlab.com/de-de/blog/categories/agile-planning/) ist es entscheidend für die Effizienz von Workflows, dass jedes Teammitglied die richtigen Tools und Berechtigungen für seine Rolle zur Verfügung hat. Die Rolle als Planer(in) unterstützt dies, indem sie es den Mitgliedern des Planungsteams ermöglicht, an allen Planungsphasen des Software-Entwicklungsprozesses mitzuwirken, ohne dass die Gefahr besteht, unabsichtlich in Bereiche wie Entwicklung oder Bereitstellung zu gelangen.\n\nVon der Erstellung und Verwaltung von Epics bis hin zum Festlegen von Roadmaps: Mit der Rolle als Planer(in) haben Agile-Teams nun die Tools zur Verfügung, die sie brauchen, um abgestimmt und produktiv zu bleiben.\n\n## Kundenorientiertes Design\n\nWir haben diese Rolle nicht isoliert entwickelt. Wir haben unsere Community bei jedem Schritt in den Prozess eingebunden. Durch Umfragen, Interviews und Tests haben wir die Berechtigungen äußerst fein abgestimmt und so sichergestellt, dass sie den praktischen Anforderungen von Produkt- und Projektmanager(inne)n entsprechen.\n\nDiese Rolle greift auch die langjährige Mission von GitLab auf, eine Plattform für Agile-Teams auf Unternehmensebene zu sein\_– sie bietet Unternehmen nämlich die Flexibilität und Kontrolle, um Agile-Methoden in großem Umfang umzusetzen.\n\n## Feedback und Engagement der Community\n\nWir freuen uns über deine Beiträge und bitten dich, deine Erfahrungen mit der neuen Rolle „Planer(in)“ zu teilen. Dein Feedback ist unerlässlich, um dein GitLab-Erlebnis weiter zu verfeinern und zu verbessern. In unserem [Feedback-Ticket](https://gitlab.com/gitlab-org/gitlab/-/issues/503817) kannst du uns deine Gedanken und Vorschläge mitteilen.\n\n## Beginne jetzt mit der Planung – mit GitLab!\n\nDie Rolle als Planer(in) ist nur eine von vielen Möglichkeiten, wie GitLab es Softwareentwicklungsteams ermöglicht, effizient zu planen, zusammenzuarbeiten und zu liefern. Egal, ob du deine Produktmanagement-Workflows optimieren, die Zusammenarbeit deines Teams verbessern oder deine Agile-Praktiken abstimmen möchtest\_– GitLab hat das passende Tool, das dir zum Erfolg verhilft.\n\n> Möchtest du das volle Potenzial von GitLab erleben? [Melde dich für eine kostenlose 60-tägige Testversion von GitLab Ultimate an](https://about.gitlab.com/de-de/free-trial/) und plane dein nächstes Projekt mit der Rolle „Planer(in)“, die auf die individuellen Bedürfnisse deines Teams zugeschnitten ist.\n\n## Weiterlesen\n- [Beyond Devs: GitLab Enterprise Agile Planning add-on for all roles (nur in englischer Sprache verfügbar)](https://about.gitlab.com/blog/2023/11/16/gitlab-enterprise-agile-planning-add-on-for-all-roles/)\n- [How to use GitLab for Agile software development (nur in englischer Sprache verfügbar)](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)\n- [First look: The new Agile planning experience in GitLab (nur in englischer Sprache verfügbar)](https://about.gitlab.com/blog/2024/06/18/first-look-the-new-agile-planning-experience-in-gitlab/)"
  category: Agile Planning
  tags:
    - agile
    - DevSecOps platform
    - features
    - product
config:
  slug: introducing-gitlabs-new-planner-role-for-agile-planning-teams
  featured: true
  template: BlogPost
