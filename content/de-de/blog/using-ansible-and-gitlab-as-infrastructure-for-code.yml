seo:
  title: Konfigurationsmanagement mit GitLab und Ansible
  description: >-
    Entdecke die beeindruckende Leistungsfähigkeit von GitLab CI, wenn es darum
    geht, Ansible-Playbooks im Rahmen von Infrastructure as Code umzusetzen.
  ogTitle: Konfigurationsmanagement mit GitLab und Ansible
  ogDescription: >-
    Entdecke die beeindruckende Leistungsfähigkeit von GitLab CI, wenn es darum
    geht, Ansible-Playbooks im Rahmen von Infrastructure as Code umzusetzen.
  noIndex: false
  ogImage: images/blog/hero-images/gitlab-ansible-cover.png
  ogUrl: >-
    https://about.gitlab.com/blog/using-ansible-and-gitlab-as-infrastructure-for-code
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: >-
    https://about.gitlab.com/blog/using-ansible-and-gitlab-as-infrastructure-for-code
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Konfigurationsmanagement mit GitLab und Ansible",
            "author": [{"@type":"Person","name":"Brad Downey"},{"@type":"Person","name":"Sara Kassabian"}],
            "datePublished": "2019-07-01",
          }

content:
  title: Konfigurationsmanagement mit GitLab und Ansible
  description: >-
    Entdecke die beeindruckende Leistungsfähigkeit von GitLab CI, wenn es darum
    geht, Ansible-Playbooks im Rahmen von Infrastructure as Code umzusetzen.
  authors:
    - Brad Downey
    - Sara Kassabian
  heroImage: images/blog/hero-images/gitlab-ansible-cover.png
  date: '2019-07-01'
  body: >
    [GitLab CI](https://about.gitlab.com/de-de/solutions/continuous-integration/
    "GitLab CI") ist ein vielseitiges Tool, das für eine Vielzahl von
    Aufgabenbereichen wie [Infrastructure as
    Code](https://about.gitlab.com/topics/gitops/infrastructure-as-code/
    "Infrastructure as Code") und
    [GitOps](https://about.gitlab.com/de-de/topics/gitops/ "GitOps") eingesetzt
    werden kann. Eine der Eigenschaften von GitLab ist seine Unabhängigkeit von
    spezifischen Tools. In dieser Demonstration liegt der Fokus auf Ansible, da
    viele Entwickler(innen) dieses Tool für Konfigurationsmanagement bevorzugen.


    ## Was ist Ansible?


    Ansible ist ein Open-Source-Automatisierungstool, das für die
    Bereitstellung, Konfiguration und Verwaltung von Computersystemen genutzt
    wird. Es gehört zur Kategorie der Konfigurationsmanagement-Tools.
    Entwickler(innen) und Systemadministrator(inn)en können damit komplexe,
    wiederholbare Prozesse wie Serverkonfiguration, Anwendungsbereitstellung und
    Netzwerkgerätemanagement automatisieren. Ansible verwendet YAML, um
    deklarative Aufgabenbeschreibungen zu erstellen, bekannt als Playbooks.
    Diese beschreiben den gewünschten Systemzustand, den Ansible durch
    SSH-Verbindungen zu den Zielhosts erreicht, um Befehle auszuführen.


    ## Demo: GitLab CI und Ansible


    Eine Funktion von GitLab CI ist die Möglichkeit, den Code aus dem
    Ansible-Playbook zu bearbeiten und bereitzustellen, ohne lokale
    Abhängigkeiten zu installieren. So kann das Demo-Projekt, das eine
    monatliche Aktualisierung der SNMP-Strings auf allen Geräten gemäß unseren
    Sicherheitsrichtlinien erfordert, problemlos auf
    [GitLab.com](https://about.gitlab.com/de-de/pricing/ "GitLab Pricing")
    durchgeführt werden.


    Beginnen Sie, indem Sie das Ansible-Playbook öffnen, das vier Aufgaben
    beinhaltet:


    - Erfassen der Router-Fakten

    - Anzeigen der Version

    - Anzeigen der Seriennummer

    - Konfigurieren von SNMP


    Der Schwerpunkt dieser Demo liegt auf der Konfiguration der SNMP-Strings,
    die in einer einfachen Reihe von Schritten durchgeführt werden kann.


    ## Zu Beginn: das Issue Board


    Jeder Plan bei GitLab beginnt an derselben Stelle: mit einem Problem. Der
    erste Schritt im GitLab-Workflows besteht darin, das Issue Board des
    [„ansible-demo“-Projekts (auf
    Englisch)](https://gitlab.com/bdowney/ansible-demo/-/boards "ansible-demo")
    zu überprüfen. Dort sehen wir bereits ein Issue zur Änderung der
    SNMP-Strings auf allen Routern. Das Issue verweist auf das
    GitLab-Sicherheitsrichtlinien-Wiki, das besagt, dass SNMP-Strings monatlich
    geändert werden müssen und unterschiedliche Strings für „read-only“ und
    „read-write“ erforderlich sind.


    ![Security
    policies](https://about.gitlab.com/images/blogimages/ansible_screenshots/security_policies_1A.png){:
    .shadow.medium.center}


    Gemäß unserer GitLab-Sicherheitsrichtlinien müssen die SNMP-Strings
    monatlich aktualisiert werden. Als Nächstes prüfst du, ob die Befehle zur
    Konfiguration der SNMP-Strings in der [Zwei-Router-Demo (auf
    Englisch)](https://gitlab.com/bdowney/ansible-demo/blob/master/ci-cd-demo/ci.yml
    "Zwei-Router-Demo") den in diesem Issue beschriebenen
    GitLab-Sicherheitsrichtlinien entsprechen.


    ![Ansible SNMP
    change](https://about.gitlab.com/images/blogimages/ansible_screenshots/ansible_snmp_change_2.png){:
    .shadow.medium.center}


    Die Befehle zur Konfiguration der SNMP-Strings findest du im
    Ansible-Playbook.


    Geh danach zurück zum Issue und weise es dir selbst zu. Ändere in der
    rechten Seitenleiste des Issues oder indem du es zwischen den Spalten des
    Issue Boards von „to-do“ auf „doing“ ziehst.


    ## Merge Request erstellen


    Als Nächstes erstellst du einen Merge Request (MR) für das Issue. Stelle
    sicher, dass du das Kennzeichen „Draft“ deinem MR hinzufügst, damit er nicht
    vorzeitig mit dem Master zusammengeführt wird. Statt eine lokale Verbindung
    herzustellen, nutze die Web IDE von GitLab, da die Änderungen an den
    SNMP-Strings minimal sind. 


    - Öffne den CI/CD-Demo-Bereich

    - Rufe das Ansible-Playbook auf

    - Bearbeite den SNMP-Abschnitt wie folgt:

    ```

    -snmp-server community New-SNMP-DEMO1 RO


    -snmp-server community Fun-SNMP-RW-STR RW

    ```

    - Achte darauf, dass read-only (RO) und read-write (RW) auf unterschiedliche
    Strings gemäß den GitLab-Sicherheitsrichtlinien gesetzt sind, wie sie im
    Issue beschrieben werden.


    ## Änderungen übertragen


    Nachdem du den SNMP-String gemäß den Richtlinien aktualisiert hast, committe
    die Änderungen. Öffne den Seite-an-Seite-Vergleich der Änderungen, um zu
    sehen, dass der MR mit deinem letzten Commit aktualisiert wurde.


    ![Commit
    changes](https://about.gitlab.com/images/blogimages/ansible_screenshots/side-by-side_3.png){:
    .shadow.medium.center}


    Der Seite-an-Seite-Vergleich ist eine einfache Möglichkeit, deine Änderungen
    zu visualisieren.


    ## Ausgabe des Merge-Request


    Durch den Commit der Änderungen wird automatisch eine GitLab CI-Pipeline
    gestartet. Diese führt beispielsweise folgende Aufgaben durch:


    - Syntaxprüfung

    - Dry Run

    - Testen der Änderungen in einer Labor-/Simulationsumgebung


    Du kannst den Fortschritt und die Ausgabe jedes Auftrags in der
    GitLab-CI-Pipeline anzeigen, um die SNMP-Aktualisierungen durchzuführen.


    ![Job
    running](https://about.gitlab.com/images/blogimages/ansible_screenshots/job_running_4.png){:
    .shadow.medium.center}


    Schau dir die Ergebnisse deines Auftrags an, die zeigen, dass die
    SNMP-Aktualisierungen in der simulierten Umgebung erfolgreich waren. Alle
    diese Aufgaben werden im Merge Request durchgeführt und dokumentiert.


    ![Pipeline](https://about.gitlab.com/images/blogimages/ansible_screenshots/pipeline_5A.png){:
    .shadow.medium.center}


    Die grünen Häkchen zeigen an, dass jeder Job in der GitLab CI-Pipeline
    erfolgreich abgeschlossen wurde. Anschließend kannst du dich auf den
    Lab-Routern einloggen und die durchgeführten Änderungen sehen.


    ![routers
    snmp](https://about.gitlab.com/images/blogimages/ansible_screenshots/routersnmp_6.png){:
    .shadow.medium.center}


    Die Änderungen an den RO- und RW-SNMP-Strings werden in den Routern
    berücksichtigt.


    ## MR-Review


    Eine weitere Möglichkeit besteht darin, MR-Approvals zu aktivieren. Dadurch
    haben mehr Benutzer(innen) die Chance, Änderungen zu überprüfen, bevor sie
    in die Produktionsumgebung übernommen werden.


    ![approvers](https://about.gitlab.com/images/blogimages/ansible_screenshots/approvers_7.png){:
    .shadow.medium.center}


    Du kannst den MR so einstellen, dass eine andere Person deine Arbeit
    überprüfen muss, bevor sie in den Master-Branch übernommen wird.


    ## Merge in den Master-Branch


    Sobald die Tests abgeschlossen sind, können die Änderungen in den
    Master-Branch übernommen werden, welcher den Code der Produktionsumgebung
    enthält.

    Wenn du bereit bist, klickst du auf die Schaltfläche Mark as ready (Als
    bereit markieren). Anschließend klickst du auf `Merge`.


    Durch das Auflösen des WIP-Status kann der MR zusammengeführt und das Issue
    abgeschlossen werden.


    Nun startet eine neue Pipeline, die alle durchgeführten Tests erneut
    ausführt, mit dem zusätzlichen Schritt, das Playbook in der
    Produktionsumgebung auszuführen.


    Du kannst den Fortschritt und die Protokolle auf dem „Pipelines“-Seite
    anzeigen. Sobald dieser Prozess abgeschlossen ist, kannst du dich bei deinen
    Produktionsroutern anmelden und sehen, dass die SNMP-Sicherheitsstrings
    aktualisiert wurden.


    ## Die Magie von GitLab CI


    Die Magie, die all dies möglich gemacht hat, ist GitLab CI. GitLab
    CI-Pipelines sind eine Reihe von aufeinanderfolgenden Aufgaben, die alles
    ausführen, was du zum Testen und Implementieren deines Ansible-Codes
    benötigst.


    GitLab CI wird mit einer einzigen einfachen YAML-Datei konfiguriert, die im
    Repository unter dem Namen `.gitlab-ci.yml` zu finden ist.

    In dieser Demo siehst du, dass die `.gitlab-ci.yml` Datei drei Schritte
    beinhaltet:


    1. Deploy: Damit wird das Zwei-Router-Simulationsnetzwerk in AWS mithilfe
    von Ansible erstellt.

    2. Demo: Damit wird das Playbook ausgeführt, das die SNMP-Strings ändert.

    3. Destroy: Hiermit wird das Zwei-Router-Simulationsnetzwerk zerstört.


    GitLab CI startet mit einem Basis-Image. In diesem Fall verwenden wir ein
    Docker-Image, das alle erforderlichen Ansible-Binärdateien und
    Abhängigkeiten enthält. Wir geben die Befehle an, die in jeder Phase
    ausgeführt werden sollen, und die Abhängigkeiten, soweit erforderlich.


    ![More
    code](https://about.gitlab.com/images/blogimages/ansible_screenshots/more_code_9A.png){:
    .shadow.medium.center}


    Eine einfache YAML-Datei enthält die drei Phasen des GitLab-CI.


    ![More
    Code](https://about.gitlab.com/images/blogimages/ansible_screenshots/more_code_10A.png){:
    .shadow.medium.center}


    Ein Blick in die Demostufe von GitLab CI, die das Ansible-Playbook ausführt.


    Wenn du dir die Pipelines ansiehst, kannst du sehen, wie GitLab CI verwendet
    wird, um Konfigurationsmanagement umzusetzen, ohne Ansible auf deinem
    Computer installieren zu müssen. Dies ist nur ein Beispiel dafür, wie GitLab
    CI genutzt werden kann, um Infrastruktur als Code auszuführen. Schau dir das
    Video unten an, um das vollständige Tutorial zu sehen:


    <!-- blank line -->

    <figure class="video_container">
      <iframe src="https://www.youtube.com/embed/M-SgRTKSeOg" frameborder="0" allowfullscreen="true"> </iframe>
    </figure>

    <!-- blank line -->


    ## FAQ – Häufig gestellte Fragen


    ### Wie nutze ich den Ansible Tower?


    Ansible Tower ist eine webbasierte Benutzeroberfläche und eine
    Enterprise-Ergänzung zu Ansible, die zusätzliche Funktionen zur
    Automatisierung, Verwaltung und Überwachung von Ansible-Umgebungen bietet.
    Teams können mit Ansible Tower ihre Automatisierungsprozesse zentralisieren,
    Zugriffskontrollen verwalten, Zeitpläne für Aufgaben erstellen, Berichte
    generieren und mehr. Es erleichtert die Skalierung und den Einsatz von
    Ansible in großen Umgebungen und bietet eine benutzerfreundliche Oberfläche
    für die Automatisierung von IT-Abläufen.


    ### Was ist der Ansible GitLab Runner?


    Ein Ansible GitLab Runner ist ein integraler Bestandteil des GitLab
    CI/CD-Ökosystems, der speziell für die Ausführung von Ansible-Playbooks
    entwickelt wurde. Er ist ein Prozess, der von GitLab genutzt wird, um Jobs
    in CI/CD-Pipelines auszuführen. Entwickler(innen) können einen GitLab Runner
    so konfigurieren, dass er Ansible-Playbooks auf den Zielhosts ausführt, was
    es ermöglicht, die Infrastruktur automatisiert und ohne zusätzliche
    Ressourcen zu verwalten.
  category: Engineering
  tags:
    - demo
    - CI/CD
config:
  slug: using-ansible-and-gitlab-as-infrastructure-for-code
  featured: false
  template: BlogPost
