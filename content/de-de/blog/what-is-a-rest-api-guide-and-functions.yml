seo:
  title: Was ist eine REST-API? Guide & Funktionen
  description: >-
    REST-APIs sind der de-facto-Standard für die Kommunikation zwischen Server
    und Client. Erfahren Sie hier alles Wissenswerte zum Thema!
  ogTitle: Was ist eine REST-API? Guide & Funktionen
  ogDescription: >-
    REST-APIs sind der de-facto-Standard für die Kommunikation zwischen Server
    und Client. Erfahren Sie hier alles Wissenswerte zum Thema!
  noIndex: false
  ogImage: images/blog/hero-images/API-REST.jpeg
  ogUrl: https://about.gitlab.com/blog/what-is-a-rest-api-guide-and-functions
  ogSiteName: https://about.gitlab.com
  ogType: article
  canonicalUrls: https://about.gitlab.com/blog/what-is-a-rest-api-guide-and-functions
  schema: |2-

                            {
            "@context": "https://schema.org",
            "@type": "Article",
            "headline": "Was ist eine REST-API? Guide & Funktionen",
            "author": [{"@type":"Person","name":"GitLab Germany Team"}],
            "datePublished": "2024-10-16",
          }

content:
  title: Was ist eine REST-API? Guide & Funktionen
  description: >-
    REST-APIs sind der de-facto-Standard für die Kommunikation zwischen Server
    und Client. Erfahren Sie hier alles Wissenswerte zum Thema!
  authors:
    - GitLab Germany Team
  heroImage: images/blog/hero-images/API-REST.jpeg
  date: '2024-10-16'
  body: >-
    REST-APIs sind seit über zwei Jahrzehnten ein zentraler Baustein des
    Internets. Bei ihnen handelt es sich um Programmierschnittstellen (APIs),
    die den Austausch von Daten zwischen Client und Server regeln. REST-APIs
    unterliegen einem Satz von Bedingungen, welche der Wissenschaftler Roy
    Fielding im Jahr 2000 entwickelt und unter der Abkürzung REST
    (representational state transfer) [festgelegt
    hat](https://ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm#sec_5_1_7
    "festgelegt hat").


    REST legt die genaue technische Umsetzung dieser Schnittstellen nicht fest.
    Fielding hat REST vielmehr als einen „Architektur-Stil“
    [bezeichnet](https://www.youtube.com/watch?v=6oFAmQUM8ws "bezeichnet"), der
    eine Vielzahl praktischer Lösungen erlaubt. Jede API, die sich innerhalb der
    Grenzen dieser Architektur bewegt, entspricht dem REST-Standard.


    In diesem Artikel zeigen wir Ihnen, warum sich REST seit seiner Einführung
    zum dominanten Modell entwickelt hat und welche Vorteile sich für
    Web-Development-Teams daraus ergeben. 


    ## Constraints: Die Grundbausteine der REST-Architektur


    REST baut auf insgesamt sechs sogenannten „Constraints“ (Einschränkungen)
    auf. 


    Aus ihnen ergibt sich eine Architektur, die einfach und anpassungsfähig ist
    und auch in einem rasch wandelnden Geschäftsumfeld langfristig Bestand haben
    kann.


    ## Definition: Was ist eine Rest-API?


    REST-APIs sind praktische Softwarelösungen, die auf der REST-Dokumentation
    aufbauen und gemäß der folgenden sechs REST-Prinzipien für ein
    Client-Server-Modell erstellt werden: 


    - Unabhängigkeit zwischen Client und Server,

    - Zustandslosigkeit,

    - ein mehrschichtiges Systemmodell, 

    - eine einheitliche Schnittstelle,

    - Cache-Fähigkeit und

    - ein optionales Constraint: Code on Demand.


    In den folgenden Abschnitten betrachten wir diese Constraints genauer. 


    ### Unabhängigkeit zwischen Client und Server


    REST findet wie erwähnt für APIs Anwendung, die den Austausch von Ressourcen
    zwischen einem Client und einem Server ermöglichen. Entscheidend ist, dass
    Client und Server vollkommen unabhängig voneinander bleiben. 


    So kann beispielsweise der Code des Servers verändert werden, ohne dass der
    Client ebenfalls Änderungen vornehmen muss, um weiterhin Informationen
    anfragen und erhalten zu können. 


    ### Zustandslosigkeit


    Die Einschränkung der Zustandslosigkeit hat der REST-Architektur ihren Namen
    verliehen. Für APIs soll gemäß dieser Vorgabe gelten, dass für die korrekte
    Beantwortung einer Anfrage die übermittelten Informationen der aktuellen
    Sitzung ausreichen.


    Das bedeutet: Es ist keine dauerhafte Verbindung zwischen Client und Server
    erforderlich und Client-Anfragen müssen auf der Server-Seite auch nicht
    zwischengespeichert werden. 


    Zustandslosigkeit führt zu höherer Daten- und Ausfallsicherheit.
    Gleichzeitig gilt aber auch: Benötigen Nutzer(innen) dieselben Informationen
    ein zweites Mal, müssen sämtliche Informationen der vorigen Sitzung erneut
    eingeben werden. 


    ### Mehrschichtiges Systemmodell


    Für jedes Unternehmen ist eine andere Server-Struktur optimal. Werden
    Informationen beispielsweise in verschiedenen Schichten gespeichert,
    gestaltet sich die Abfrage mehr oder weniger komplex. Das aber sollte für
    eine Anfrage unerheblich bleiben, solange die Daten korrekt übermittelt
    werden.  


    Die Einschränkung des mehrschichtigen Systemmodells ist somit das Gegenstück
    zur Zustandslosigkeit. Während letztere besagt, dass der Server vom Client
    nichts weiter benötigt als die Anfrageinformationen, verlangt das
    mehrschichtige Modell, dass dem Client nicht bekannt zu sein braucht, wie
    der Server die angeforderten Daten bereitstellt. 


    Der Server kann also mit einer Vielzahl verschiedener Architekturen
    arbeiten, ohne dass die REST-API-Schnittstelle beeinflusst wird. 


    ### Einheitliche Schnittstelle


    Diese Einschränkung ist etwas komplexer und wiederum aus vier Unterpunkten
    aufgebaut. Für Roy Fielding war sie die womöglich wichtigste der gesamten
    REST-API-Architektur. 


    Die einheitliche Schnittstelle fordert:


    - dass jeder Datensatz über eine einzige URI eindeutig gekennzeichnet ist,

    - dass Veränderungen oder auch Löschungen der Daten nur mittels der
    grundlegenden Netzwerkprotokollbefehle, wie GET, POST, PUT/PATCH und DELETE,
    vorgenommen werden können,

    - dass jede Nachricht, die versandt wird, mittels Metadaten sämtliche
    Informationen bereithält, die für die Bearbeitung der Daten erforderlich ist
    und

    - dass, sofern erforderlich, Hyperlinks (HATEOAS) bereitgestellt werden, um
    weitere benötigte Informationen einzuholen.


    Die einheitliche Schnittstelle sorgt für maximale Klarheit und eine
    einfache, standardisierte Client-Server-Kommunikation. 


    ### Cache-Fähigkeit


    Zwar findet bei REST-APIs der Ressourcenaustausch zustandslos statt,
    zugleich aber sollten einmal angeforderte Daten aus einer Sitzung auf
    demselben Endgerät weiterverwendet werden können.


    Indem REST das Cachen dieser Informationen ermöglicht, sorgt es für eine
    höhere Effizienz und beschleunigt viele Prozesse bedeutend.


    ### Code on Demand


    Für die meisten Anwendungen reicht die Bereitstellung der Daten in der Form
    von XML or JSON vollkommen aus. In bestimmten Fällen aber kann es von
    Vorteil sein, dem Client darüber hinaus –      oder stattdessen – eine
    Anwendung bereitzustellen. Denken Sie dabei an Java Applets oder JavaScript.


    Code on Demand stellt eine sinnvolle Erweiterung der REST-Architektur dar,
    aber sie ist als einzige der sechs Einschränkungen optional. 


    ## Wofür eignen sich REST-APIs?


    Flexibilität ist eines der Hauptmerkmale von REST-APIs. So sind sehr viele
    praktische Anwendungen denkbar:


    - Ganz grundsätzlich das Abrufen und Bereitstellen von Daten.
    Social-Media-Seiten wie Instagram oder Facebook nutzen REST-APIs
    beispielsweise um Updates zu posten.

    - Gerade weil sie zustandslos sind, eignen sich REST-APIs für
    Cloud-Services: Auch wenn die Verbindung abbricht, können die Daten weiter
    genutzt werden. 

    - [Microservices](https://microservices.io/ "Microservices") gewinnen rasch
    an Beliebtheit und Bedeutung. Mit ihnen geht ein umfassender
    Perspektivenwechsel einher: Applikationen werden nicht mehr als riesige, in
    sich geschlossene Systeme gedacht, sondern als modular und aus kleineren,
    schlanken Elementen zusammengesetzt. REST-APIs bilden die ideale
    Schnittstelle zur nahtlosen Integration dieser verschiedenen Bausteine.


    Im Laufe der letzten 20 Jahre sind Alternativen zu REST-APIs verfügbar
    geworden. Dennoch hat sich keine davon auf breiter Basis durchsetzen können.
    Ganz offensichtlich ist REST auch aktuell der beste Ansatz zum
    Datenaustausch im Netz. 


    ## Was sind die Vorteile einer Rest-     API?


    In den frühen Tagen des Internets war SOAP (Simple object access protocol)
    die dominante Form des Datenaustauschs. 


    Dass sich REST seitdem durchgesetzt hat und fast ein Vierteljahrhundert lang
    relevant geblieben ist, lässt sich recht einfach aus seinen inhärenten
    Vorteilen erklären:


    - SOAP vs REST: SOAP ist ein Protokoll, das Regeln und auch technische
    Realisierungen im Gegensatz zu REST sehr präzise vorschreibt. Daraus ergibt
    sich unmittelbar, dass APIs, die auf diesen Anforderungen aufbauen, weitaus
    komplexer als REST-APIs sind.

    - Die APIs, die gemäß der REST-Richtlinien programmiert wurden, basieren auf
    dem HTTP-Standard. Sie sind schnell und effizient und in nahezu jedem
    Kontext einsetzbar. Aus genau diesen Gründen eignen sich REST-APIs auch
    besonders gut für mobile Anwendungen.

    - Der Begriff der Skalierbarkeit ist bereits gefallen und er ist auch einer
    der maßgeblichen Argumente für die Verwendung von REST-APIs. Darunter ist zu
    verstehen, dass bei Erweiterungen der Server-Architektur nicht die darunter
    liegende Datenaustausch-Technologie verändert werden muss. 

    - Einige Unternehmen setzen aus Sicherheitserwägungen immer noch auf SOAP.
    Allerdings sind REST-APIs keineswegs grundsätzlich unsicher. Entscheidend
    ist, einige grundlegende Best Practices anzuwenden – darunter die Verwendung
    von HTTPS sowie Autorisierung und Authentifizierung. 


    ## Gibt es eine REST-API von GitLab?


    Auch bei GitLab sind wir von den Vorzügen der REST-Architektur überzeugt.
    Aus diesem Grund stellen wir unseren Nutzer(inne)n eine
    [GitLab-REST-API](https://docs.gitlab.com/ee/api/rest/ "GitLab-REST-API")
    zur Verfügung. 


    Bei GitLab handelt es sich um den führenden Anbieter von DevSecOps-Lösungen.
    Anwender(innen) nutzen die Plattform, um sicher, fehlerfrei und kollaborativ
    an Entwicklungsprojekten zu arbeiten. 


    Die GitLab-API kann sowohl genutzt werden, um öffentlich sichtbare, als auch
    nicht öffentliche Daten (nach erfolgter Authentifizierung und Autorisierung)
    abzurufen. Weil die API unmittelbar auf GitLab abgestimmt ist, erfolgt der
    Austausch sicher, schnell und effizient. 


    ## REST-API FAQs


    ### Ist REST ein Standard?


    REST ist ein Satz aus sechs Einschränkungen, die den Datenaustausch in einer
    Client-Server-Beziehung regeln. Alle API-Schnittstellen, die diesen Vorgaben
    entsprechen, sind „RESTful“. 

    Es ist somit nicht falsch, REST als einen Standard zu bezeichnen. Allerdings
    gilt dies weniger im Sinne eines Protokolls oder konkreter Anweisungen. REST
    gibt vielmehr Leitlinien und Anforderungen vor, deren Umsetzung zu
    gewünschten Ergebnissen führen, unabhängig davon, wie diese technisch
    realisiert werden.


    Aus diesem Grund wird REST zumeist als ein „Architektur-Stil“ definiert. 


    ### Muss ich alle sechs REST-Einschränkungen befolgen?


    Roy Fielding hat hierzu persönlich im Laufe der Jahre mehrfach [Stellung
    bezogen](https://www.infoq.com/articles/roy-fielding-on-versioning/
    "Stellung bezogen"). Seine Ansichten zu dieser Frage sind eindeutig: REST
    ist nicht in allen Fällen zwangsläufig die beste Option. Wenn man aber eine
    REST-konforme Architektur wünscht, müssen sämtliche Constraints ausnahmslos
    umgesetzt werden. 


    Eine API beispielsweise, die alle Einschränkungen umsetzt, bei der aber
    keine Daten gecached werden können, ist nicht RESTful. 


    Die einzige Ausnahme ist Code on Demand. Diese Einschränkung ist optional
    und muss somit nicht umgesetzt werden, damit eine REST-API die Kriterien
    erfüllt. 


    ### Wie passen Cache-Fähigkeit und Zustandslosigkeit zusammen?


    Zwischen den Einschränkungen der Cache-Fähigkeit und Zustandslosigkeit
    scheint ein Spannungsverhältnis zu bestehen. Wenn bei jeder Anfrage die
    Daten neu übermittelt werden müssen, widerspricht das Zwischenspeichern von
    Daten im Cache dann nicht dieser Forderung? 


    In Wahrheit fordert Zustandslosigkeit lediglich, dass der Server jede
    Anfrage so behandelt, als wäre sie die erste. Es besteht keine Zuordnung der
    Daten im Cache zu einer aktuellen Anfrage. 


    Das Cachen der Daten dient lediglich einer höheren Effizienz und sorgt für
    Stabilität. 


    ### Was bedeutet der Begriff Idempotenz im Zusammenhang mit REST-APIs?


    Wenn ein Client dieselbe Anfrage mehrfach nacheinander stellt, spricht man
    von Idempotenz. Das kann entweder passieren, weil die Verbindung instabil
    oder der Code fehlerhaft ist. 


    Entscheidend ist, dass eine solche idempotente Anfrage nicht zu einem Fehler
    bei der Beantwortung der Anfrage führt. 


    Die Einschränkung der Cache-Fähigkeit sorgt dafür, dass idempotente Anfragen
    als solche erkannt und fehlerfrei bearbeitet werden können. 


    ### Wie lassen sich Rest-APIs sichern?


    REST-APIs können sehr effektiv gesichert werden. 


    Übliche und sehr effiziente Methoden sind die Verwendung von HTTPS und
    API-Schlüsseln, die Durchführung von Authentifizierungen und Autorisierungen
    sowie die Durchführung einer Input-Validation und eines Audit-Loggings. 


    Auch die Begrenzung der Anfragen in einem bestimmten Zeitfenster im Sinne
    eines Rate-Limiting empfiehlt sich.
  category: DevSecOps
  tags:
    - DevOps
    - DevSecOps
config:
  slug: what-is-a-rest-api-guide-and-functions
  featured: false
  template: BlogPost
