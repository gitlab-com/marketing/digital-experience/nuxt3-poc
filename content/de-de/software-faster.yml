seo:
  title: 'Schnellere Software'
  description: 'GitLab vereinfacht dein DevSecOps, damit du dich auf das Wesentliche konzentrieren kannst. Mehr erfahren!'
config:
  enableAnimations: true
content:
  - componentName: SoftwareFasterHero
    componentContent:
      hero:
        titleHighlight: 'Entwicklung mit 7-facher Geschwindigkeit'
        secondaryButton:
          text: 'Was ist GitLab?'
          config:
            dataGaName: 'what is gitlab'
            dataGaLocation: 'hero'
      video:
        text: 'Was ist GitLab?'
        config:
          videoUrl: 'https://player.vimeo.com/video/799236905?h=4eee39a447&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
          videoThumbnail: 'images/software-faster-thumbnail.jpg'
          dataGaName: 'what is gitlab'
          dataGaLocation: 'hero'
      backgroundImage:
        altText: 'Hero image for software faster page.'
        config:
          src: '/images/backgrounds/software-faster-hero.svg'
      config:
        highlightText: true
  - componentName: CommonFeatureCards
    componentContent:
      header: 'Gemeinsam besser: Kund(inn)en stellen Software mit GitLab schneller bereit'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '800'
      caseStudies:
        - header: 'Nasdaq: Schneller, nahtloser Übergang zur Cloud'
          description: 'Nasdaq hat die Vision, zu 100 % in die Cloud zu wechseln. Um dieses Ziel zu erreichen, arbeiten sie mit GitLab zusammen.'
          showcaseImg:
            altText: 'Realistic cloud with up and down arrows'
            config:
              url: '/images/nasdaq-showcase.png'
          logoImg:
            altText: 'Nasdaq logo'
            config:
              url: '/images/logos/nasdaq.svg'
          link:
            text: 'Video ansehen'
            config:
              dataGaName: 'nasdaq'
              dataGaLocation: 'body'
              videoUrl: 'https://player.vimeo.com/video/767082285?h=e76c380db4'
              videoThumbnail: 'images/software-faster-thumbnail.jpg'
        - header: 'Erzielen einer 5 x schnelleren Bereitstellungszeit'
          description: 'HackerOne hat mit GitLab Ultimate die Pipeline-Zeit, die Bereitstellungs­geschwindigkeit und die Effi­zienz der Entwickler(innen) verbessert.'
          showcaseImg:
            altText: 'Person with multiple screens of code'
            config:
              url: '/images/hackerone-showcase.png'
          logoImg:
            altText: 'Hackerone logo'
            config:
              url: '/images/logos/hackerone-bw.png'
          link:
            text: 'Mehr erfahren'
            config:
              href: '/de-de/customers/hackerone/'
              dataGaName: 'hackerone'
              dataGaLocation: 'body'
        - header: 'Funktionen wurden 144 x schneller veröffentlicht'
          description: 'Airbus Intelligence verbesserte seine Workflows und Codequalität mit Single Application CI.'
          showcaseImg:
            altText: 'Plane poking out of hangar'
            config:
              url: '/images/airbus-showcase.png'
          logoImg:
            altText: 'Airbus logo'
            config:
              url: '/images/customer_logos/airbus.png'
          link:
            text: 'Zum Bericht'
            config:
              href: '/de-de/customers/airbus/'
              dataGaName: 'airbus'
              dataGaLocation: 'body'
  - componentName: SoftwareFasterCustomerLogos
    componentContent:
      title:
        text: 'Diese Unternehmen vertrauen uns:'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '1600'
          offset: '200'
      logos:
        - altText: 'Tmobile'
          config:
            src: '/images/customer_logos/deutsche-telekom-color-logo.jpg'
            href: '/de-de/customers/deutsche-telekom/'
        - altText: 'Goldman Sachs logo'
          config:
            src: '/images/customer_logos/goldman-sachs.svg'
            href: '/de-de/customers/goldman-sachs/'
        - altText: 'Siemens Color logo'
          config:
            src: '/images/customer_logos/siemens-color-logo.png'
            href: '/de-de/customers/siemens/'
        - altText: 'Nvidia logo'
          config:
            src: '/images/customer_logos/nvidia-color-logo.svg'
            href: '/de-de/customers/nvidia/'
        - altText: 'UBS logo'
          config:
            src: '/images/customer_logos/ubs-logo-black.svg'
            href: '/customers/ubs/'
  - componentName: SoftwareFasterFeatureCards
    componentContent:
      hero:
        title: 'GitLab vereint den gesamten Lebenszyklus der Softwareentwicklung innerhalb einer einzigen KI-gestützten Anwendung'
        secondaryButton:
          text: 'Mehr erfahren'
          config:
            href: '/de-de/platform/'
            dataGaName: 'platform'
            dataGaLocation: 'hero'
        image:
          altText: 'The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).'
          config:
            src: '/images/devsecops-infinity-shield.svg'
      cards:
        - title: 'Bessere Einblicke'
          description: 'End-to-End-Transparenz über den gesamten Lebenszyklus der Softwareentwicklung.'
          config:
            icon: 'CaseStudyAlt'
        - title: 'Höhere Effizienz'
          description: 'Integrierte Unterstützung für Automatisierung und Integration mit Drittanbieterdiensten.'
          config:
            icon: 'Principles'
        - title: 'Verbesserte Zusammenarbeit'
          description: 'Ein Workflow, der Entwickler-, Sicherheits- und Betriebsteams zusammenführt.'
          config:
            icon: 'Roles'
        - title: 'Schnellere Amortisation'
          description: 'Kontinuierliche Verbesserung durch beschleunigte Feedbackschleifen.'
          config:
            icon: 'Verification'
  - componentName: CommonCaseStudies
    componentContent:
      charcoalBg: true
      title: 'Schneller auf dem gleichen Stand: Erkunde die DevSecOps-Ressourcen'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '500'
      rows:
        - title: 'DevSecOps-Umfragebericht 2024'
          subtitle: 'Wir haben mehr als 5.000 DevSecOps-Expert(inn)en nach ihrer Meinung zum Stand der Sicherheit, der KI und mehr gefragt.'
          button:
            text: 'Zum Bericht'
            config:
              href: '/de-de/developer-survey/'
              dataGaName: 'developer survey'
              dataGaLocation: 'body'
          image:
            alt: '2024 developer survey image'
            config:
              url: '/images/2024-devsecops-survey.png'
          config:
            icon:
              name: 'DocPencilAlt'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
        - title: 'Rhythmus ist alles: 10 x technische Unternehmen für 10 x Ingenieure und Ingenieurinnen.'
          subtitle: 'Der CEO und Mitbegründer von GitLab, Sid Sijbrandij, über die Bedeutung des Rhythmus in technischen Unternehmen.'
          button:
            text: 'Blogbeitrag lesen (nur in englischer Sprache verfügbar)'
            config:
              href: '/blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/'
              dataGaName: 'cadence '
              dataGaLocation: 'body'
          image:
            alt: 'People running race in city street'
            config:
              url: '/images/athlinks_running.jpg'
          config:
            icon:
              name: 'Blog'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
  - componentName: CommonNextSteps
