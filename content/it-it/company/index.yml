config:
  enableAnimations: true
seo:
  title: Informazioni su GitLab
  description: Scopri di più su GitLab e cosa ci motiva.
content:
  - componentName: CommonHero
    componentContent:
      title: Informazioni su GitLab
      description: Dietro le quinte della piattaforma DevSecOps
      image:
        altText: ''
        config:
          src: '/images/company/group-of-gitlab-employees.jpg'
          theme: 'rounded'
      config:
        background: purple
        typographyVariants:
          description: heading5
  - componentName: CompanyCopyAbout
    componentContent:
      title: Cosa facciamo
      description: |
        **Siamo l'azienda che ha sviluppato GitLab, la piattaforma DevSecOps più completa sul mercato.**

        Nata nel 2011 come progetto open-source per migliorare la collaborazione fra i membri di un team, la nostra piattaforma viene oggi utilizzata da milioni di persone per distribuire software in modo più rapido ed efficiente, rafforzando al contempo la sicurezza e la conformità.

        Fin dall'inizio abbiamo creduto fermamente nel lavoro da remoto, nell'open-source, nella metodologia DevSecOps e in un approccio iterativo allo sviluppo delle applicazioni. Il primo pensiero quando attiviamo la nostra postazione è lavorare insieme alla community di GitLab per offrire ogni mese nuove soluzioni innovative che permettano ai team di distribuire un software di qualità eccezionale in meno tempo senza doversi preoccupare della toolchain.
      cta:
        text: Learn more about GitLab
        config:
          href: '/it-it/why-gitlab/'
          dataGaName: about gitlab
          dataGaLocation: body
  - componentName: CompanyByTheNumbers
    componentContent:
      title: I numeri di GitLab
      asOfDate: Dicembre 2024
      rows:
        - item:
            - title: |
                Collaboratori

                al codice
              number: 3,300+
              config:
                col: 4
            - title: |
                Uffici

                completamente da remoto sin dall'inizio
              number: '0'
              config:
                col: 4
        - item:
            - title: |
                Release consecutive

                ogni mese
              number: '133'
              config:
                col: 4
                href: /releases/
                dataGaName: releases
                dataGaLocation: body
            - title: |
                Team distribuito

                in più di 60 paesi
              number: 1,800+
              config:
                col: 4
                href: /company/team/
                dataGaName: team
                dataGaLocation: body
        - item:
            - title: |
                Stima degli utenti registrati
              number: Over 30 million
              config:
                col: 8
      ctaBox:
        title: |
          Dalla pianificazione alla produzione,
          riunisci i team in un'unica applicazione
        cta:
          text: Get free trial
          config:
            href: 'https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com/'
            dataGaName: free trial
            dataGaLocation: body
      customerLogos:
        logos:
          - company: Deutsche Telekom
            altText: Logo T-Mobile
            config:
              src: /images/customer_logos/deutsche-telekom.svg
              url: /customers/deutsche-telekom/
          - company: Goldman Sachs
            altText: Logo Goldman Sachs
            config:
              src: /images/customer_logos/goldman-sachs.svg
              url: /customers/goldman-sachs/
          - company: Siemens
            altText: Logo Siemens
            config:
              src: /images/customer_logos/siemens-color-logo.png
              url: /customers/siemens/
          - company: Nvidia
            altText: Logo NVIDIA
            config:
              src: /images/customer_logos/nvidia.svg
              url: /customers/nvidia/
          - company: UBS
            altText: Logo UBS
            config:
              src: /images/customer_logos/ubs.svg
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
  - componentName: CompanyCopyMission
    componentContent:
      title: La mission di GitLab
      description: Il nostro obiettivo principale è permettere a tutti di contribuire alla creazione del software che muove il mondo.
      cta:
        text: Scopri di più
        config:
          href: /company/mission/
          dataGaName: mission
          dataGaLocation: body
  - componentName: CompanyCardGrid
    componentContent:
      title: I valori di GitLab
      config:
        cardType: common
        backgroundColor: $color-surface-800
      cards:
        - title: Collaborazione
          text: Diamo priorità a quegli atteggiamenti che ci consentono di lavorare insieme in modo efficace - l'assumere un atteggiamento positivo, ringraziare o fare ammenda e fornire feedback in tempi rapidi.
          icon:
            config:
              name: CollaborationAlt4
          button:
            config:
              href: /handbook/values/#collaboration
              dataGaName: Collaboration
              dataGaLocation: body
        - title: Risultati per i clienti
          text: Spinti da un senso di urgenza e da uno spirito votato all'azione, aiutiamo sempre i nostri clienti a raggiungere risultati sempre più soddisfacenti.
          icon:
            config:
              name: Increase
          button:
            config:
              href: /handbook/values/#results
              dataGaName: results
              dataGaLocation: body
        - title: Efficienza
          text: Dalla scelta di soluzioni affidabili ancorché noiose alla redazione della documentazione, passando per l'autogestione delle rispettive mansioni, cerchiamo sempre di agire nel modo corretto per ottenere progressi in tempi rapidi.
          icon:
            config:
              name: DigitalTransformation
          button:
            config:
              href: /handbook/values/#efficiency
              dataGaName: efficiency
              dataGaLocation: body
        - title: Diversità, inclusione e appartenenza
          text: Lavoriamo costantemente affinché GitLab sia una luogo inclusivo e familiare per tutti, indipendentemente dall'estrazione e dalle circostanze.
          icon:
            config:
              name: Community
          button:
            config:
              href: /handbook/values/#diversity-inclusion
              dataGaName: diversity inclusion
              dataGaLocation: body
        - title: Iterazione
          text: Il nostro modus operandi? Partire dalle piccole cose, generare valore immediato, incamerare il feedback e alzare l'asticella.
          icon:
            config:
              name: ContinuousDelivery
          button:
            config:
              href: /handbook/values/#iteration
              dataGaName: iteration
              dataGaLocation: body
        - title: Trasparenza
          text: Seguiamo una politica di trasparenza totale - dal manuale aziendale agli strumenti di monitoraggio dei ticket, tutto ciò che facciamo è di dominio pubblico.
          icon:
            config:
              name: OpenBook
          button:
            config:
              href: /handbook/values/#transparency
              dataGaName: transparency
              dataGaLocation: body
  - componentName: CompanyScrollingTimeline
    componentContent:
      title: GitLab negli anni
      items:
        - text: Il progetto GitLab prende il via con un commit
          year: 2011
        - text: Una nuova versione di GitLab viene rilasciata il 22 di ogni mese
        - text: Nasce la prima versione della CI di GitLab
          year: 2012
        - text: GitLab diventa una società a tutti gli effetti
          year: 2014
        - text: L'azienda partecipa al programma Y Combinator e pubblica il manuale GitLab nel repository del proprio sito web
          year: 2015
        - text: L'azienda annuncia il uso piano strategico e raccoglie 20 milioni di dollari attraverso un secondo ciclo di finanziamenti
          year: 2016
        - text: 'GitLab Inc. diventa una società quotata sul Nasdaq Global Market (NASDAQ: GTLB)'
          year: 2021
  - componentName: CompanyCopyAbout
    componentContent:
      title: Lavorare in GitLab
      description: |
        Il nostro obiettivo è creare un ambiente completamente da remoto e fruibile in qualsiasi parte del mondo, in cui tutti i membri del team possano dare il meglio di sé, fornire un contributo prezioso e sentirsi ascoltati e appoggiati nelle loro scelte, in modo da raggiungere un sano equilibro fra lavoro e vita privata.

        Se ti interessa far parte del team, ti invitiamo ad [approfondire cosa voglia dire lavorare in GitLab](/jobs/) e a candidarti per qualsiasi posizione aperta che risponda alle tue aspettative.
      cta:
        text: View all open roles
        config:
          href: /jobs/all-jobs/
          dataGaName: all jobs
          dataGaLocation: body
  - componentName: CompanyTeamOpsCard
    componentContent:
      title: Team migliori. Progressi più rapidi. Un mondo migliore.
      description: TeamOps è una pratica specifica implementata da GitLab per supportare i dipendenti e rendere il lavoro di squadra una disciplina oggettiva. Grazie a essa, GitLab è passata dall'essere una startup al diventare una società quotata in borsa in soli dieci anni. Con l'offerta di una certificazione gratuita e accessibile, anche altre organizzazioni possono sfruttare TeamOps per affinare il processo decisionale, produrre risultati migliori e dare un contributo prezioso al mondo dello sviluppo software.
      images:
        mainImage: /images/company/company-teamops.svg
        titleIcon: /images/logos/teamops.svg
      primaryButton:
        text: Ulteriori informazioni su TeamOps
        config:
          href: '/it-it/teamops/'
          dataGaName: learn more about teamops
          dataGaLocation: body
      secondaryButton:
        text: Ottieni la certificazione
        config:
          href: 'https://levelup.gitlab.com/learn/course/teamops/'
          dataGaName: get certified
          dataGaLocation: body
  - componentName: CompanyCardGrid
    componentContent:
      title: Scopri di più
      config:
        cardType: resource
        backgroundColor: $color-surface-50
      cards:
        - header: Scopri le ultime novità su GitLab, DevOps, sicurezza e non solo.
          type: Blog
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Visita il blog
            config:
              href: /blog/
              dataGaName: blog
              dataGaLocation: body
              icon: Blog
        - header: Notizie, comunicati stampa, loghi e asset del marchio GitLab
          type: Rassegna stampa
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Scopri di più
            config:
              href: /press/press-kit/
              dataGaName: press kit
              dataGaLocation: body
              icon: Announcement
        - header: Le notizie più recenti riguardo l'andamento azionario e finanziario a beneficio degli investitori.
          type: Rapporti con gli investitori
          image:
            altText: Fallback Cards Infinity
            config:
              src: /images/resources/img-fallback-cards-infinity.png
          link:
            text: Scopri di più
            config:
              href: https://ir.gitlab.com/
              dataGaName: investor relations
              dataGaLocation: body
              icon: Money
  - componentName: CommonNextSteps
