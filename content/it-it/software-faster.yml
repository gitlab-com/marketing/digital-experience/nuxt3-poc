seo:
  title: 'SEO più veloce tramite software'
  description: 'Scegli GitLab per semplificare il tuo processo DevSecOps e dedicarti a ciò che conta davvero. Scopri di più.'
config:
  enableAnimations: true
content:
  - componentName: SoftwareFasterHero
    componentContent:
      hero:
        titleHighlight: 'Sviluppa a una velocità sette volte maggiore'
        secondaryButton:
          text: "Che cos'è GitLab?"
          config:
            dataGaName: 'what is gitlab'
            dataGaLocation: 'hero'
      video:
        text: "Che cos'è GitLab?"
        config:
          videoUrl: 'https://player.vimeo.com/video/799236905?h=4eee39a447&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479'
          videoThumbnail: 'images/software-faster-thumbnail.jpg'
          dataGaName: 'what is gitlab'
          dataGaLocation: 'hero'
      backgroundImage:
        altText: 'Hero image for software faster page.'
        config:
          src: '/images/backgrounds/software-faster-hero.svg'
      config:
        highlightText: true
  - componentName: CommonFeatureCards
    componentContent:
      header: 'Insieme è meglio: grazie a GitLab, i clienti distribuiscono software più velocemente'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '800'
      caseStudies:
        - header: 'Il passaggio rapido e uniforme di Nasdaq verso il cloud'
          description: 'Nasdaq intende passare completamente al cloud e ha scelto GitLab per raggiungere questo obiettivo.'
          showcaseImg:
            altText: 'Realistic cloud with up and down arrows'
            config:
              url: '/images/nasdaq-showcase.png'
          logoImg:
            altText: 'Nasdaq logo'
            config:
              url: '/images/logos/nasdaq.svg'
          link:
            text: 'Riproduci il video'
            config:
              dataGaName: 'nasdaq'
              dataGaLocation: 'body'
              videoUrl: 'https://player.vimeo.com/video/767082285?h=e76c380db4'
              videoThumbnail: 'images/software-faster-thumbnail.jpg'
        - header: 'Esegui il deployment a una velocità cinque volte maggiore'
          description: "Grazie a GitLab Ultimate, Hackerone ha ottimizzato i tempi delle pipeline, la velocità di deployment e l'efficienza degli sviluppatori."
          showcaseImg:
            altText: 'Person with multiple screens of code'
            config:
              url: '/images/hackerone-showcase.png'
          logoImg:
            altText: 'Hackerone logo'
            config:
              url: '/images/logos/hackerone-bw.png'
          link:
            text: 'Scopri di più'
            config:
              href: '/it-it/customers/hackerone/'
              dataGaName: 'hackerone'
              dataGaLocation: 'body'
        - header: 'Distribuisci funzionalità a una velocità 144 volte maggiore'
          description: "Airbus Intelligence ha migliorato il proprio flusso di lavoro e la qualità del codice con una CI basata su un'applicazione singola."
          showcaseImg:
            altText: 'Plane poking out of hangar'
            config:
              url: '/images/airbus-showcase.png'
          logoImg:
            altText: 'Airbus logo'
            config:
              url: '/images/customer_logos/airbus.png'
          link:
            text: 'Lire le témoignage'
            config:
              href: '/it-it/customers/airbus/'
              dataGaName: 'airbus'
              dataGaLocation: 'body'
  - componentName: SoftwareFasterCustomerLogos
    componentContent:
      title:
        text: 'Scelta da:'
      aos:
        config:
          dataAos: 'fade-up'
          duration: '1600'
          offset: '200'
      logos:
        - altText: 'Tmobile'
          config:
            src: '/images/customer_logos/deutsche-telekom-color-logo.jpg'
            href: '/it-it/customers/deutsche-telekom/'
        - altText: 'Goldman Sachs logo'
          config:
            src: '/images/customer_logos/goldman-sachs.svg'
            href: '/it-it/customers/goldman-sachs/'
        - altText: 'Siemens Color logo'
          config:
            src: '/images/customer_logos/siemens-color-logo.png'
            href: '/it-it/customers/siemens/'
        - altText: 'Nvidia logo'
          config:
            src: '/images/customer_logos/nvidia-color-logo.svg'
            href: '/it-it/customers/nvidia/'
        - altText: 'UBS logo'
          config:
            src: '/images/customer_logos/ubs-logo-black.svg'
            href: '/customers/ubs/'
  - componentName: SoftwareFasterFeatureCards
    componentContent:
      hero:
        title: "GitLab centralizza l'intero ciclo di sviluppo software in un'unica applicazione basata sull'IA"
        secondaryButton:
          text: 'Scopri di più'
          config:
            href: '/it-it/platform/'
            dataGaName: 'platform'
            dataGaLocation: 'hero'
        image:
          altText: 'The DevSecOps lifecycle of plan, code, build, test, release, deploy, operate, and monitor arranged in an infinity symbol overlapping the security shield (secure and compliance).'
          config:
            src: '/images/devsecops-infinity-shield.svg'
      cards:
        - title: 'Approfondimenti migliori'
          description: 'Visibilità end-to-end lungo tutto il ciclo di vita della distribuzione del software.'
          config:
            icon: 'CaseStudyAlt'
        - title: 'Maggiore efficienza'
          description: "Supporto integrato per l'automazione e le integrazioni con servizi di terze parti."
          config:
            icon: 'Principles'
        - title: 'Maggiore collaborazione'
          description: 'Un unico flusso di lavoro che unisce team di sviluppo, sicurezza e operativi.'
          config:
            icon: 'Roles'
        - title: 'Time-to-value più breve'
          description: 'Miglioramento continuo attraverso cicli di feedback accelerati.'
          config:
            icon: 'Verification'
  - componentName: CommonCaseStudies
    componentContent:
      charcoalBg: true
      title: "Tutti uniti verso l'obiettivo: esplora le risorse DevSecOps"
      aos:
        config:
          dataAos: 'fade-up'
          duration: '500'
      rows:
        - title: 'Report del sondaggio su DevSecOps 2024'
          subtitle: "Abbiamo chiesto a più di 5.000 professionisti DevSecOps la loro opinione sullo stato attuale della sicurezza, dell'intelligenza artificiale e non solo."
          button:
            text: 'Leggi il report'
            config:
              href: '/developer-survey/'
              dataGaName: 'developer survey'
              dataGaLocation: 'body'
          image:
            alt: '2024 developer survey image'
            config:
              url: '/images/2024-devsecops-survey.png'
          config:
            icon:
              name: 'DocPencilAlt'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
        - title: 'La frequenza è tutto: organizzazioni e professionisti dieci volte più efficienti.'
          subtitle: "Sid Sijbrandij, CEO e cofondatore di GitLab, sull'importanza della frequenza nelle organizzazioni tecniche."
          button:
            text: 'Leggi il blog'
            config:
              href: '/blog/2022/11/03/cadence-is-everything-10x-engineering-organizations-for-10x-engineers/'
              dataGaName: 'cadence'
              dataGaLocation: 'body'
          image:
            alt: 'People running race in city street'
            config:
              url: '/images/athlinks_running.jpg'
          config:
            icon:
              name: 'Blog'
          aos:
            config:
              dataAos: 'fade-right'
              duration: '800'
  - componentName: CommonNextSteps
