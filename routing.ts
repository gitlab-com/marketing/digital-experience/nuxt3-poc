import fs from 'fs';
import path from 'path';

// Helper function to read directory recursively
const readDirectoryRecursively = (dir: string): string[] => {
  let results: string[] = [];
  const list = fs.readdirSync(dir);

  list.forEach((file) => {
    const filePath = path.join(dir, file);
    const stat = fs.statSync(filePath);

    if (stat && stat.isDirectory()) {
      // Recursively read subdirectories
      results = results.concat(readDirectoryRecursively(filePath));
    } else {
      results.push(filePath);
    }
  });

  return results;
};

/**
 * Route builder, it ignores the content from the "shared" folder and clean the routes
 */
export const buildGitlabRoutes = (): string[] => {
  const dir = 'content';
  const files = readDirectoryRecursively(dir);
  const routesRegex = /content|\.yml|index\/?|en-us\//gim;

  return files
    .filter((file) => !file.includes('shared/')) // Remove shared content
    .map((file) => {
      // Extract the relative path and format it for routing
      const relativePath = path.relative(dir, file);
      const route = relativePath.replace(routesRegex, '');
      return `/${route}`.replace(/\/?$/, '/'); // ensure it is returned without a trailing slash
    });
};

// Temporary logic that will be used to rollout the blog in year by year batches.
export const blogRolloutFilter = (routes: string[]) => {
  const allowedYears = ['2012', '2013', '2014', '2015', '2016', '2017'];
  // eslint-disable-next-line no-console
  console.log(`FILTERING BLOGPOSTS FROM YEARS OTHER THAN ${allowedYears.join(', ')}`);
  const dir = 'content/en-us';

  const blogRoutes = routes.filter((route) => route.includes('blog/'));
  const nonBlogRoutes = routes.filter((route) => !route.includes('/blog'));
  const filteredBlogRoutes = blogRoutes.filter((route) => {
    const filePath = path.join(dir, route.replace(/^\/+|\/+$/g, '') + '.yml');
    // Check if the file exists before reading
    if (fs.existsSync(filePath)) {
      const fileContent = fs.readFileSync(filePath, 'utf8');

      const dateMatch = fileContent.match(/date:\s*'(\d{4}-\d{2}-\d{2})'/);
      if (dateMatch) {
        const postDate = new Date(dateMatch[1]);
        return allowedYears.includes(postDate.getFullYear().toString());
      }
    }

    return false;
  });
  // eslint-disable-next-line no-console
  console.log(`total blog posts filtered: ${blogRoutes.length - filteredBlogRoutes.length}`);

  return [...nonBlogRoutes, ...filteredBlogRoutes];
};
