import * as SlippersUI from 'slippers-ui';
import { defineNuxtPlugin } from '#app';

export default defineNuxtPlugin((nuxtApp) => {
  for (const [key, component] of Object.entries(SlippersUI)) {
    if (key.startsWith('Slp')) {
      nuxtApp.vueApp.component(key, component);
    }
  }
});
