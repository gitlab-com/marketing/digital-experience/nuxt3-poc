export default defineNuxtPlugin(async () => {
  try {
    if (import.meta.server) return;

    const module = await import(/* @vite-ignore */ '@gitlab/fonts/gitlab-sans/GitLabSans.woff2');
    const fontUrl = module.default;

    useHead({
      link: [
        {
          rel: 'preload',
          href: fontUrl,
          as: 'font',
          type: 'font/woff2',
          crossorigin: 'anonymous',
        },
      ],
    });
  } catch (error) {
    console.warn('Failed to load font for preloading:', error);
  }
});
