export default defineNuxtPlugin(() => {
  useHead({
    script: [
      {
        type: 'application/ld+json',
        children: JSON.stringify({
          '@context': 'https://schema.org',
          '@type': 'Corporation',
          name: 'GitLab',
          legalName: 'GitLab Inc.',
          tickerSymbol: 'GTLB',
          url: 'https://about.gitlab.com',
          logo: {
            '@type': 'ImageObject',
            url: 'https://images.ctfassets.net/xz1dnu24egyd/KpJoqcRLUFBL1AqKt9x0R/4fd439c21cecca4881106dd0069aa39c/gitlab-logo-extra-whitespace.png',
          },
          description:
            'GitLab is the most comprehensive AI-powered DevSecOps platform for software innovation. GitLab enables organizations to increase developer productivity, improve operational efficiency, reduce security and compliance risk, and accelerate digital transformation.',
          foundingDate: '2011',
          founders: [
            { '@type': 'Person', name: 'Sid Sijbrandij' },
            { '@type': 'Person', name: 'Dmitriy Zaporozhets' },
          ],
          slogan:
            'Our mission is to change all creative work from read-only to read-write so that everyone can contribute.',
          address: {
            '@type': 'PostalAddress',
            streetAddress: '268 Bush Street #350',
            addressLocality: 'San Francisco',
            addressRegion: 'CA',
            postalCode: '94104',
            addressCountry: 'USA',
          },
          awards: [
            { '@type': 'Award', name: "Comparably's Best Engineering Team 2021" },
            { '@type': 'Award', name: '2021 Gartner Magic Quadrant for Application Security Testing - Challenger' },
            { '@type': 'Award', name: 'DevOps Dozen award for the Best DevOps Solution Provider for 2019' },
            { '@type': 'Award', name: '451 Firestarter Award from 451 Research' },
          ],
          knowsAbout: [
            { '@type': 'Thing', name: 'DevOps' },
            { '@type': 'Thing', name: 'CI/CD' },
            { '@type': 'Thing', name: 'DevSecOps' },
            { '@type': 'Thing', name: 'GitOps' },
            { '@type': 'Thing', name: 'DevOps Platform' },
            { '@type': 'Thing', name: 'DevSecOps Platform' },
            { '@type': 'Thing', name: 'AI Code Assistant' },
            { '@type': 'Thing', name: 'Application Security Testing' },
            { '@type': 'Thing', name: 'Enterprise Agile Planning' },
            { '@type': 'Thing', name: 'Source Code Management' },
            { '@type': 'Thing', name: 'AI-Assisted Software Development' },
          ],
          sameAs: [
            'https://www.facebook.com/gitlab',
            'https://en.wikipedia.org/wiki/GitLab',
            'https://www.instagram.com/gitlab/',
            'https://twitter.com/gitlab',
            'https://www.linkedin.com/company/gitlab-com',
            'https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg',
          ],
        }),
      },
    ],
  });
});
