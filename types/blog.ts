import type { BaseSeo } from './base';

// Blog types
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const validTags = [
  'agile',
  'AI/ML',
  'AWS',
  'bug bounty',
  'careers',
  'CI',
  'CD',
  'CI/CD',
  'cloud native',
  'code review',
  'collaboration',
  'community',
  'contributors',
  'customers',
  'demo',
  'design',
  'developer survey',
  'DevOps',
  'DevOps platform',
  'DevSecOps',
  'DevSecOps platform',
  'events',
  'features',
  'frontend',
  'Group Conversations',
  'git',
  'GitOps',
  'GKE',
  'google',
  'growth',
  'inside GitLab',
  'integrations',
  'kubernetes',
  'news',
  'open source',
  'partners',
  'patch releases',
  'performance',
  'product',
  'production',
  'releases',
  'remote work',
  'research',
  'security',
  'security releases',
  'security research',
  'solutions architecture',
  'startups',
  'testing',
  'tutorial',
  'UI',
  'user stories',
  'UX',
  'webcast',
  'workflow',
  'zero trust',
  'automotive',
  'embedded development',
  'public sector',
  'education',
  'financial services',
] as const;

// Create a type from the array values
type ValidTag = (typeof validTags)[number];

export interface BlogCategory {
  content: {
    name: string;
  };
  config: {
    hide: boolean;
    slug: string;
  };
}

export interface BlogPost {
  title: string;
  heroImage: string;
  description: string;
  date: string;
  updatedDate?: string;
  authors: string[];
  body: string;
  category: string;
  tags?: ValidTag;
}
export interface BlogPage {
  config: {
    featured?: boolean;
    slug: string;
    externalUrl?: string;
  };
  seo: BaseSeo;
  content: BlogPost;
}
export interface BlogAuthor {
  content: {
    name: string;
    bio?: string;
    role?: string;
    config: {
      headshot: string;
      gitlabHandle?: string;
      twitter?: string;
      linkedin?: string;
      facebook?: string;
      hackerNews?: string;
      threads?: string;
      ctfId: string;
    };
  };
}
