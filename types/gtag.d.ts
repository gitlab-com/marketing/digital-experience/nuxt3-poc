declare global {
  interface Window {
    dataLayer: unknown[];
    geofeed: (options: { country: string; state?: string }) => void;
  }
}

export {};
