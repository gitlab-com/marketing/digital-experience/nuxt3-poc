export interface BaseSeo {
  title: string;
  description: string;
  ogTitle?: string;
  ogDescription?: string;
  ogImageAlt?: string;
  ogImage?: string;
  ogUrl?: string; // Canonical URL
  twitterDescription?: string;
  twitterImage?: string;
  twitterTitle?: string;
  config?: {
    noIndex?: boolean;
    ogType?: string;
    schema?: never;
    robots?: string;
  };
}

/**
 * Generic Base component, it receives a dynamic type which represents the model/type of the component that will be rendered.
 * Each component will handle their own interface or reuse an existing one
 */
export interface DynamicComponent<Type> {
  componentName: string;
  componentContent: Type;
}

/**
 * Generic Base page data structure used for all pages in the application
 */
export interface BasePage {
  config?: {
    template?: string; // Buyer Experience's available page templates from /components/pages
    layout?: string; // Buyer Experience's available layouts from /layouts
    enableAnimations?: boolean;
  };
  seo: BaseSeo;
  content: Array<DynamicComponent<never>> | never; // Content delivered in a block-driven way
}

/**
 * Generic Title, SubTitle, Description component
 */
export interface BaseCopy {
  title?: string;
  header?: string;
  subTitle?: string;
  subHeader?: string;
  description?: string;
  text?: string;
}

/**
 * Generic Link
 */
export interface BaseLink {
  text: string;
  config: {
    href: string;
    icon?: string;
    iconVariant?: string;
    dataGaName?: string;
    dataGaLocation?: string;
    modal?: boolean;
    variant?: string;
  };
}

/**
 * Generic Copy with CTA component
 */
export interface BaseCTA extends BaseCopy {
  link: BaseLink;
}

/**
 * Generic Copy with image component
 */
export interface BaseCopyImage extends BaseCopy {
  image: BaseImage;
}

/**
 * Generic Image
 */
export interface BaseImage {
  altText: string;
  config: {
    href: string;
    src: string;
    dataGaName?: string;
    dataGaLocation?: string;
  };
}

export interface BaseIcon {
  altText?: string;
  config: {
    name: string;
  };
}

export interface BaseVideo {
  altText: string;
  config: {
    url: string;
    thumbnail: string;
  };
}

export interface BaseForm {
  config: {
    formId: number;
    formName?: string;
  };
  formHeader?: string;
  formRequiredText?: string;
  submittedMessage?: {
    header?: string;
    body?: string;
  };
  externalForm?: {
    url: string;
    width?: string;
    height?: string;
  };
}

/**
 * Generic Hero Interface
 */
export interface BaseHero extends BaseCopy {
  tagline?: string;
  eyebrow?: BaseEyebrow;
  titleHighlight?: string;
  primaryButton?: BaseLink;
  secondaryButton?: BaseLink;
  tertiaryButton?: BaseLink;
  image?: BaseImage;
  backgroundImage?: BaseImage;
  config?: {
    theme?: string;
    background?: string;
    typographyVariants?: {
      description?: string;
    };
  };
  customerLogos?: CustomerLogos;
}

export interface BaseEyebrow {
  text: string;
  config?: {
    backgroundColor?: string;
    tag?: string;
    href?: string;
    typographyVariant?: string;
    dataGaName?: string;
    dataGaLocation?: string;
    icon?: string;
  };
}

/**
 * Generic Card Interface, don't let it grow too much with very specific attributes, for those cases it's better to use type intersection
 */
export interface BaseCard extends BaseCopy {
  button?: BaseLink;
  image?: BaseImage;
  config?: {
    icon: string;
    iconSize: string;
    iconVariant: string;
  };
}

export interface BaseCopyCards extends BaseCopy {
  cards: Array<BaseCard>;
}

export interface BaseNavatticDemo {
  subtitle?: string;
  scheduleButton?: BaseLink;
  config: {
    demoHref: string;
    videoFallbackHref?: string;
  };
}

export interface BaseQuoteCard {
  headline?: string;
  quote: string;
  metrics: Array<{ number: string; text: string }>;
  config: {
    logo?: string;
    headshot?: string;
  };
  author?: {
    name: string;
    title: string;
    company: string;
  };
  cta?: {
    text?: string;
    config: {
      href: string;
      dataGaName: string;
      dataGaLocation: string;
    };
  };
}

/**
 * Builds off of the default Window type, however that does not account for our third-party scripts that write to the window object
 */

export interface MktoFormInstance {
  onSuccess?: (callback: () => boolean | never) => void;
  onError?: (callback: () => void) => void;
  submit?: () => void;
  validate?: () => boolean;
}

export interface Window {
  __vimeoRefresh?: () => void;
  dataLayer: Array<object>;
  MktoForms2?: {
    loadForm: (
      domain: string,
      munchkinId: string,
      formId: string,
      callback: (form: MktoFormInstance | null) => void,
    ) => void;
  };
}

export interface Language {
  label: string;
  code: string;
  value: string;
  path: string;
  default: boolean;
  langLabel: string;
  regional: boolean;
}

export interface CustomerLogos {
  title?: {
    text: string;
  };
  aos?: {
    config: {
      dataAos: string;
      duration: string;
      offset: string;
    };
  };
  logos: BaseImage[];
}

export interface Locale {
  code: string;
  language: string;
  label: string;
  langLabel: string;
  files: string[];
}
