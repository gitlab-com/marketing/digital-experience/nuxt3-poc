import type { BaseLink, BaseImage } from './base';
/**
 * Interface for each navigation item
 */
export interface MainNavigationLevelOneItems {
  text: string;
  config: BaseLink['config'] & { dataNavLevelOne?: string };
  link: BaseLink[];
  cards?: Array<{ title?: string; description?: string; link: BaseLink; items: BaseLink[] }>;
  lists?: Array<{ title: string; items: BaseLink[] }>;
  feature: {
    title?: string;
    backgroundColor: string;
    textColor: string;
    image: BaseImage;
    link: BaseLink;
    text: string;
  };
  footer?: {
    title: string;
    items: BaseLink[];
  };
  left: false;
}

/**
 * Interface for the main top navigation
 */
export interface MainNavigation {
  logo: BaseLink;
  freeTrial: BaseLink;
  sales: BaseLink;
  login: BaseLink;
  items: MainNavigationLevelOneItems[];
  search: SearchData;
}
// Adds additional params needed on footer links
export interface FooterLink extends BaseLink {
  config: BaseLink['config'] & { isOneTrustButton: boolean; id: string };
}
// footer navigation item interface
export interface FooterItem {
  title: string;
  links: FooterLink[];
  subMenu: Array<{ title: string; links: Array<FooterLink> }>;
}

// footer navigation interface
export interface FooterData {
  text?: string;
  sourceCtaText: string;
  editCtaText: string;
  contributeCtaText: string;
  config: string[];
  items?: FooterItem[];
}

export interface SearchData {
  close?: string;
  login?: {
    text: string;
    link: BaseLink;
  };
  suggestions?: {
    text: string;
    default: BaseLink[];
  };
}
