import type { Language } from '@/types/base';

export const SITE_URL = 'https://about.gitlab.com';
export const DEFAULT_META_DESCRIPTION = 'Learn more from GitLab, The One DevOps Platform for software innovation.';
export const DEFAULT_OPENGRAPH_IMAGE = `${SITE_URL}/images/open-graph/open-graph-gitlab.png`;
export const SITE_NAME = 'about.gitlab.com';
export const DEFAULT_PAGE = 'CommonPage';

export enum FreeTrialVariant {
  PRIMARY = 'primary',
  ACCENT = 'accent',
  GLEAM = 'gleam',
  STEEL = 'steel',
}

export const LANG_OPTIONS: readonly Language[] = [
  {
    label: 'English',
    code: 'en',
    value: 'en-us',
    path: '', // In our site the default language is English and does not have a prefix path
    default: true,
    langLabel: 'Language',
    regional: false,
  },
  {
    label: 'Deutsch',
    code: 'de',
    value: 'de-de',
    path: '/de-de',
    langLabel: 'Sprache',
    regional: true,
  },
  {
    label: 'Español',
    code: 'es',
    value: 'es',
    path: '/es',
    langLabel: 'Idioma',
    regional: false,
  },
  {
    label: 'Français',
    code: 'fr',
    value: 'fr-fr',
    path: '/fr-fr',
    langLabel: 'Langue',
    regional: true,
  },
  {
    label: 'Italiano',
    code: 'it',
    value: 'it-it',
    path: '/it-it',
    langLabel: 'Lingua',
    regional: false,
  },
  {
    label: '日本語',
    code: 'ja',
    value: 'ja-jp',
    path: '/ja-jp',
    langLabel: '言語',
    regional: false,
  },
  {
    label: 'Português',
    code: 'pt',
    value: 'pt-br',
    path: '/pt-br',
    langLabel: 'Idioma',
    regional: false,
  },
];
