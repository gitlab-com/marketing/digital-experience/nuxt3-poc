# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# yarn
yarn install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# yarn
yarn dev
```

OR, if running in a container:

```bash
# yarn
yarn dev-container
```

## Production

Build the application for production:

```bash
# yarn
yarn generate
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.

## Developer documentation

Please read through our engineering documentation [here](https://gitlab.com/gitlab-com/marketing/digital-experience/about-gitlab-com/-/tree/main/docs?ref_type=heads) for more information on the inner-workings of our project.

### Local development with Decap CMS

You cannot access Decap at `/admin` without implementing the following steps ([per Decap CMS Proxy Server](https://www.npmjs.com/package/decap-server)):

1. Checkout your branch
2. Update your config.yml to connect to the server:

```yaml
local_backend: true
backend:
  name: git-gateway
# Comment out the production 'backend' property. DO NOT COMMIT OR PUSH THIS UPDATE!
```

3. Run npx decap-server from the root directory of the above repository (if this doesn't work, run `npm i decap-server`)
4. Open a new terminal instance and start your local dev server (`yarn dev`)

Hot reload doesn't work for every type of update. To verify any changes you make to the Decap code works, reset the server.

### File Structure

- /assets: Stores files like images, fonts, and styles that are processed by the build tool.
- /components: Stores reusable Vue components that can be imported and used across pages and other components.
- /common: Holds shared logic (e.g., constants).
- /content: Used to store and manage yml or other structured content files.
- /layouts: Defines layout components that wrap around pages to provide a consistent structure (e.g., header, footer).
- /pages: Contains Vue components mapped to routes; the files inside automatically generate routes for your application.
- /plugins: Used to register and configure plugins globally before the application mounts (e.g., integrating third-party libraries).
- /public: Contains static assets that are directly accessible from the root URL and served without processing.
- /server: Contains API routes and server-side code for rendering server-side data (used in SSR/hybrid mode).
- /services: Contains code that encapsulate and manage specific business logic or functionality in an application, often with dependencies.
- /composable: Contains reusable composables with common logic that needs access to Nuxt Context
