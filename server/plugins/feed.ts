import type { Feed } from 'nuxt-module-feed';
import { serverQueryContent } from '#content/server';
import { createEvent } from 'h3';
import markdownIt from 'markdown-it';
const md = markdownIt();

function markdownToHtml(markdown: string) {
  return md.render(markdown);
}
const BASE_URL = 'https://about.gitlab.com';

export default defineNitroPlugin((nitroApp) => {
  nitroApp.hooks.hook('feed:generate', async ({ feed, options }) => {
    switch (options.path) {
      case '/atom.xml': {
        await generateFeed(feed, 'en-us');
        break;
      }
      case '/es/atom.xml': {
        await generateFeed(feed, 'es');
        break;
      }
      case '/it-it/atom.xml': {
        await generateFeed(feed, 'it-it');
        break;
      }
      case '/pt-br/atom.xml': {
        await generateFeed(feed, 'pt-br');
        break;
      }
      case '/fr-fr/atom.xml': {
        await generateFeed(feed, 'fr-fr');
        break;
      }
      case '/ja-jp/atom.xml': {
        await generateFeed(feed, 'ja-jp');
        break;
      }
      case '/de-de/atom.xml': {
        await generateFeed(feed, 'de-de');
        break;
      }
    }
  });
});

export async function generateFeed(feed: Feed, locale: string) {
  feed.options = {
    title: 'GitLab',
    description: 'My Description',
    id: `${BASE_URL}/blog`,
    link: `${BASE_URL}/blog`,
    language: locale,
    favicon: `${BASE_URL}/favicon.ico`,
    copyright: `All rights reserved ${new Date().getFullYear()},`,
    updated: new Date(),
    feedLinks: {
      atom: `${BASE_URL}/atom.xml`,
    },
    author: {
      name: 'The GitLab Team',
    },
  };

  const mockEvent = createEvent({
    method: 'GET',
    url: '/',
    headers: {},
  });

  const posts = await serverQueryContent(mockEvent, `${locale}/blog`).sort({ 'content.date': -1 }).limit(20).find();
  posts.forEach((post) => {
    try {
      const postPath = `https://about.gitlab.com/blog/${post.config.slug}`;
      const articleDate = post.content.date;
      const formattedDate = new Date(articleDate);
      feed.addItem({
        title: post.content.title,
        link: postPath,
        id: postPath,
        published: formattedDate,
        image: post.content.heroImage,
        date: formattedDate,
        author: post.content.authors.map((author: string) => ({
          name: author,
          link: `https://about.gitlab.com/blog/authors/${author.toLowerCase().replace(' ', '-')}`,
        })),
        content: markdownToHtml(post.content.body),
      });
    } catch (error) {
      console.error(`error generating ${locale} rss feed`, error);
    }
  });
}
