export default defineNitroPlugin((nitroApp) => {
  nitroApp.hooks.hook('sitemap:resolved', async (ctx) => {
    const noRobotRoutes = JSON.parse(process.env.NO_ROBOTS_ROUTES || '[]');

    if (noRobotRoutes && noRobotRoutes.length > 0) {
      // Filter out URLs that match any of the routes in noRobotRoutes
      ctx.urls = ctx.urls.filter((url) => !noRobotRoutes.some((route) => url._path.pathname === `/${route}`));
    }
  });
});
