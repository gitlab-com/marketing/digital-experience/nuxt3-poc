import path from 'path';
import fs from 'fs';

export const generateEventPageRoutes = async (specificRoute = null) => {
  // TODO: For i18n config, this will need to be less specific
  const eventDir = path.resolve('content/en-us/events');
  const files = fs.readdirSync(eventDir);

  const routes = files
    .map((file) => {
      const slug = file.replace(/\.ya?ml$/, '');
      const route = `/events/${slug}`;
      // If a specific route is provided, only return that route
      if (specificRoute && route === specificRoute) {
        return route;
      }
      return specificRoute ? null : route; // Return all routes if no specific route is provided
    })
    .filter((route) => route !== null);

  return routes;
};
