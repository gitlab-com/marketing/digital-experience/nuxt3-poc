/**
 * Pushes an event to the global `dataLayer` for tracking.
 *
 * @param {string} eventAction - The action associated with the event (e.g., 'play', 'pause').
 * @param {string} eventCategory - The category of the event (e.g., 'Video', 'Form Submission').
 * @param {string} eventLabel - A label describing the event (e.g., video title or form name).
 * @param {string} eventName - The name of the event (default is 'event').
 * @param {Record<string, any>} additionalData - Additional data to include with the event.
 */
export const pushToDataLayer = (
  eventAction: string,
  eventCategory: string,
  eventLabel: string,
  eventName = 'event',
  additionalData: Record<string, unknown> = {},
) => {
  if (typeof window !== 'undefined' && window.dataLayer) {
    window.dataLayer = window.dataLayer || [];
    window.dataLayer.push({
      event_action: eventAction,
      event_category: eventCategory,
      event_label: eventLabel,
      event: eventName,
      ...additionalData,
    });
  } else {
    console.error('DataLayer is not available.');
  }
};
