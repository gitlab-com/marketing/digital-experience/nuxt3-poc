import path from 'path';
import fs from 'fs';

export const generateBlogPostRoutes = async (specificRoute = null) => {
  const blogDir = path.resolve('content/en-us/blog');
  const files = fs.readdirSync(blogDir);

  const routes = files
    .map((file) => {
      const match = file.match(/.*\.yml$/);
      if (match) {
        const slug = match[0].split('.')[0];
        const route = `/blog/${slug}/`;
        if (specificRoute && route === specificRoute) {
          return route;
        }
        return specificRoute ? null : route;
      }
      return null;
    })
    .filter((route) => route !== null);

  return routes;
};
