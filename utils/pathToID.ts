export function pathToID(path: string): string {
  return `page-${path
    .replace(/\/[a-z]{2}(-[a-z]{2})?\//i, '/') // Remove language codes
    .replace(/[^a-zA-Z0-9-_]/g, '-') // Replace non-alphanumeric characters with '-'
    .replace(/^-+|-+$/g, '')}`; // Trim leading and trailing hyphens
}
