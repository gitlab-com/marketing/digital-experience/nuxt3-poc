// Being used to convert strings into anchor links, image file names, etc

export function toKebabCase(string: string): string {
  return string
    .toLowerCase()
    .trim()
    .replace(/[^a-z0-9\s-]/g, '') // Remove any character that is not a letter, number, space, or hyphen
    .replace(/\s+/g, '-') // Replace spaces with hyphens
    .replace(/-+/g, '-'); // Replace multiple hyphens with a single hyphen
}
