import type { BaseSeo } from '~/types/base';
import { LANG_OPTIONS } from '@/common/constants';

export const validateSeo = (seoConfig: BaseSeo, hreflangMeta: Array<{ hreflang: string; href: string }>) => {
  const errors: string[] = [];

  if (seoConfig.title && seoConfig.title.length > 60) {
    errors.push(`Title cannot exceed 60 characters. Characters: ${seoConfig.title.length}`);
  }

  if (seoConfig.description && seoConfig.description.length > 170) {
    errors.push(`Description cannot exceed 160 characters. Characters: ${seoConfig.description.length}`);
  }

  if (seoConfig.ogTitle && seoConfig.ogTitle.length > 60) {
    errors.push(`OG Title cannot exceed 60 characters. Characters: ${seoConfig.ogTitle.length}`);
  }

  if (seoConfig.ogDescription && seoConfig.ogDescription.length > 170) {
    errors.push(`OG Description cannot exceed 160 characters. Characters: ${seoConfig.ogDescription.length}`);
  }

  if (seoConfig.ogImage && !/\.(jpg|jpeg|png)$/i.test(seoConfig.ogImage)) {
    errors.push('OG Image must be a valid JPG, JPEG, or PNG file.');
  }

  // Hreflang validation
  const langOptions = LANG_OPTIONS.reduce((acc, langOption) => {
    acc.push(langOption.value.toLowerCase());

    if (langOption.regional) {
      acc.push(langOption.value.split('-')[0]);
    }
    return acc;
  }, []);

  const defaultLanguage = LANG_OPTIONS.find((langOption) => langOption.default);
  if (defaultLanguage) {
    langOptions.push('x-default');
  }

  hreflangMeta.forEach((hreflang, index) => {
    const { hreflang: lang, href } = hreflang;

    if (!langOptions.includes(lang.toLowerCase())) {
      errors.push(`Hreflang at index ${index} must be one of the LANG_OPTIONS values: ${langOptions.join(', ')}.`);
    }

    try {
      new URL(href);
    } catch {
      errors.push(`Href value "${href}" at index ${index} must be a valid URL.`);
    }
  });
  return errors;
};
