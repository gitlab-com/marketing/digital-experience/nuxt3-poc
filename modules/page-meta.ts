import { addTemplate, defineNuxtModule } from 'nuxt/kit';
import path from 'path';
import { promises as fs } from 'fs';

export default defineNuxtModule({
  /**
   * This Nuxt module scans the "components" directory, generates a list of components,
   * and creates a virtual file containing mappings for dynamic imports. The component names
   * are standardized to follow Nuxt's auto-import naming convention (PascalCase).
   * https://nuxt.com/docs/guide/concepts/auto-imports
   * https://nuxt.com/docs/guide/directory-structure/components#component-names
   */
  async setup() {
    async function createVirtualComponentsMapping() {
      const componentsDir = path.resolve(__dirname, '../components');

      function toPascalCase(input) {
        return input.replace(/(^|\/|-)(\w)/g, (_, __, letter) => letter.toUpperCase()).replace(/\//g, '');
      }

      /**
       * Recursively scans a directory to find all Vue component files and generates
       * their names and paths.
       *
       * @param {string} dir - The directory to scan.
       * @param {string} prefix - A prefix to prepend to the component name (used for subdirectories).
       * @returns {Promise<{name: string, path: string}[]>} - A list of components with their names and paths.
       */
      async function getComponentFiles(dir, prefix = '') {
        const entries = await fs.readdir(dir, { withFileTypes: true });
        const components = await Promise.all(
          entries.map(async (entry) => {
            const fullPath = path.join(dir, entry.name);
            if (entry.isDirectory()) {
              return await getComponentFiles(fullPath, `${prefix}${entry.name}/`);
            } else if (entry.isFile() && entry.name.endsWith('.vue')) {
              const componentName = `${prefix}${entry.name.replace(/\.vue$/, '')}`;
              return {
                name: toPascalCase(componentName),
                path: fullPath,
              };
            }
          }),
        );
        return components.flat().filter(Boolean);
      }

      const components = await getComponentFiles(componentsDir);

      /**
       * Creates a mapping object where each key is the PascalCase name of a component
       * and the value is a function for dynamically importing the component.
       */
      const mapping = components.reduce((accumulator, component) => {
        const relativePath = path.relative(path.resolve(__dirname, '../components'), component.path);
        accumulator[component.name] = `() => import('@/components/${relativePath.replace(/\\/g, '/')}')`;
        return accumulator;
      }, {});

      const componentManifest = `
        export default {
          ${Object.entries(mapping)
            .map(([key, value]) => `"${key}": ${value}`)
            .join(',\n')}
        };
      `;
      return componentManifest;
    }

    const componentManifest = await createVirtualComponentsMapping();

    // Register the virtual file in Nuxt's build system
    addTemplate({
      filename: 'component-manifest.mjs', // File available in #build/component-manifest
      getContents: () => componentManifest,
    });
  },
});
