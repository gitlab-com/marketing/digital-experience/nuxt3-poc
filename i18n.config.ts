export default defineI18nConfig(() => ({
  legacy: false,
  fallbackLocale: 'en-us',
  silentFallbackWarn: false,
}));
