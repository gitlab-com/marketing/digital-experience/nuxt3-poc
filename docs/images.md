# Using images on our site

## Img vs NuxtImg

We have [NuxtImg](https://nuxt.com/modules/image) enabled in our project. Please use `<nuxt-img />` as much as possible for images. This ensures that assets can be optimized when the page is built and can have big performance benefits.

Try to output an image as `webp` as much as possible. Please do not use NuxtImg for svgs, as you will then be trying to rasterize a vector file, which can have negative effects on overall quality.

# Using images on our site

## Img vs NuxtImg

We have [NuxtImg](https://nuxt.com/modules/image) enabled in our project. Please use `<nuxt-img />` as much as possible for images. This ensures that assets can be optimized when the page is built and can have big performance benefits.

Try to output an image as `webp` as much as possible. Please do not use NuxtImg for svgs, as you will then be trying to rasterize a vector file, which can have negative effects on overall quality.

### Using `NuxtImg` configuration

[This plugin](https://image.nuxtjs.org/components/nuxt-img) is a built-in image optimization component and automatically sizes the image from within the `<nuxt-img/>` tag. [Screen sizes](https://image.nuxtjs.org/components/nuxt-img) have been predefined and are as listed:

- xs: 320
- sm: 640
- md: 768
- lg: 1024
- xl: 1280
- xxl: 1536
- 2xl: 1536

NuxtImage features includes:

- Modern browser image handling with
- Creating multiple image exports based on viewport size
- Resizing image at build time
- Lazy loading/ image compression
- Using next-gen image formats webp
- Fallback png format for older browsers
- Creating a small filesize placeholder image for really slow connections
