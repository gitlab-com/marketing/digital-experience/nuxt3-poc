## Local development with Decap CMS

# Decap CMS

https://decapcms.org/

Our site has a few areas that are opted-in to using Decap. We may see contributions to these pages from editors that prefer to use the CMS or a local development environment.

When building pages that have Decap enabled, it is important that the collection schema for those entries follow our yaml schema (config, seo, content).

## Local development

You cannot access Decap at `/admin` without implementing the following steps ([per Decap CMS Proxy Server](https://www.npmjs.com/package/decap-server)):

1. Checkout your branch
2. Update your config.yml to connect to the server:

```yaml
local_backend: true
backend:
  name: git-gateway
# Comment out the production 'backend' property. DO NOT COMMIT OR PUSH THIS UPDATE!
```

3. Run `yarn decap` from the root directory of the above repository (if this doesn't work, run `npm i decap-server`)
4. Open a new terminal instance and start your local dev server (`yarn dev`)

Hot reload doesn't work for every type of update. To verify any changes you make to the Decap code works, reset the server.

## Collections and Widgets

Types of content are called 'Collections' with their individual fields called 'Widgets'. Refer to the Decap documentation on how to configure these correctly.

## Editorial Workflows

Decap is integrated into our GitLab instance. Anyone with `maintainer` access to our project as access to Decap, and they can easily log in using OAuth with their GitLab account.

We rely heavily on our `.gitlab/CODEOWNERS` file to manage content governance. Only certain users are allowed to publish (or merge) content into production, but everyone is allowed to create a new draft or propose a new edit.

DEX engineers are allowed to merge in all content to production, but we should be carefully maintaining our CODEOWNERS file to ensure certain page owners or DRIs can merge at will.
