# Content Driven Architecture

Most pages in this project follow a consistent structure. To simplify page creation, the project uses a **Content-Driven Architecture**. This approach enables you to build pages by simply creating YAML files.

This documentation provides a clear guide on how to build pages using this architecture.

## Centralized Page Building

For standardization purposes, the project uses a single page file located at `/pages/[...slug].vue`. This file is responsible for:

- Fetching the content from the YAML files.
- Handling localization.
- Validating the content structure.
- Generating standard SEO metadata.
- Configuring the template and layout to be rendered.

Once the content is fetched and validated, the appropriate template is dynamically imported and rendered using [dynamic components](https://nuxt.com/docs/guide/directory-structure/components#dynamic-components). The content is passed to the selected template. If no template is specified, the **CommonPage** template is used as the default.

Below are the code snippets for each part of this centralized page file:

### Standard Content Fetching

The project uses a standardized composable called **useGitLabContent** to fetch content. This composable manages localization and page routing across the project. By using **useGitLabContent**, there is no need to handle routing, localization, or content fetching in other parts of the codebase.

Below is the implementation of the composable:

```javascript
// composables/useGitLabContent.vue

export const useGitlabContent = async () => {
  const { locale } = useI18n();
  const { params, path } = useRoute();

  const slug = `/${locale.value.toLowerCase()}/${params.slug ? params.slug.join('/') : ''}`;

  const { data } = await useAsyncData('gitlabContent', () => queryContent(slug).findOne());

  if (!data.value) {
    throw createError({
      statusCode: 404,
      message: `Page not found in the content library: ${path}`,
    });
  }

  return data.value;
};
```

### Standard Page Modeling

Our content is stored within the `/content/` directory using yaml/yml files for individual pages or blocks of content.

The content directory is split up into folders by locale (English = en-us, Japanese = ja-jp, German = de-de, etc) with an additional `shared` folder. Files within the individual locale directories are for entire pages, with the full path of that file becoming the URL. For example, /de-de/solutions/security-compliance.

All data under content houses the main text of the page, including information about components. Any non-translatable content should be added under additional config properties within the data schema.

To standardize content across the project, all YAML files inside the `content` folder must follow a **BasePage** interface. This interface defines three main attributes: `config`, `content`, and `seo`. These attributes are validated at build time to ensure that no pages deviate from the architecture.

#### Attributes:

- **`config`**: Defines the layout and template for the page. If not provided, the default layout and the **Common** template are used.
- **`seo`**: Contains SEO configuration for the page. A complete list of attributes can be found in `/types/base.ts` under the `BaseSEO` interface.
- **`content`**: Represents the structured content of the page. It supports modular components such as dynamic content blocks.

#### Example YAML Page

```yaml
## Page config, if not present, the default Layout and the Common template are set as defaults
config:
  layout: default
  template: Common

## SEO configuration, full attributes list is in /types/base.ts under the BaseSEO interface
seo:
  title: 'The most-comprehensive AI-powered DevSecOps platform'
  description: 'From planning to production, bring teams together in one application. Ship secure code more efficiently to deliver value faster.'

## Content of the page
content:
  - componentName: VideoHero
    componentContent:
      title: 'AI throughout the entire software development lifecycle is here.'
      config:
        video: '/videos/hero.mp4'
      primaryButton:
        text: 'GitLab Duo Enterprise'
        config:
          href: '/gitlab-duo/'
          dataGaName: 'primary-button-click'
          dataGaLocation: 'hero'
      secondaryButton:
        text: 'Learn more'
        config:
          href: '/gitlab-duo/'
          dataGaName: 'secondary-button-click'
          dataGaLocation: 'hero'
```

### Interfacing with content

Contributors to about.gitlab.com can edit content locally in a development environment, through GitLab's WebIDE, and through our CMS, Decap.

### Localization efforts

Our marketing site currently draws from English, French, German, Japanese, Portugese, Italian, and Spanish sources. English is the default language used, with new pages being built with English in mind. Our site is continuously being translated.

Questions specific to translated content should be relayed to our [Localization](https://handbook.gitlab.com/handbook/marketing/localization/) team.

### Content module

Our site utilizes the Nuxt Content module. This module allows us to organize and interact with our yaml files like a built-in content management system from within our Page files.

https://content.nuxt.com/get-started/installation

```
const {
  data: { value: data },
} = await useAsyncData(() => queryContent(`${locale}/blog/${filename}`).findOne());

```

### Routing

The project uses a single **slug** file to handle all routes, ensuring consistent route generation and localization.

#### Content-Driven Route Generation

Routes are automatically created based on the structure of the `/content` folder. This approach ensures that any YAML file present in the folder generates a corresponding route, simplifying the process of adding or modifying localized paths.

#### Example: `content` Folder Structure

Below is an example of the `content` folder organization:

```plaintext
-- en-us
  -- platform.yml
  -- solutions
     -- index.yml
-- de-de
  -- platform.yml
-- fr-fr
  -- platform.yml
  -- topics
    -- index.yml
    -- more.yml
-- ja-jp
  -- platform.yml
```

#### Generated routes

Based on the folder structure above, the following routes are automatically created:

```plaintext
/platform/
/solutions/
/de-de/platform/
/fr-fr/platform/
/fr-fr/topics/
/fr-fr/topics/more/
/ja-jp/platform/
```

#### Exclusions

Files stored under /content/shared are ignored during route generation. This ensures that shared resources are not unintentionally exposed as routes.

- **Advantages of Content-Driven Routing Ease of Management:** Adding or modifying routes only requires updating the content folder structure.
- **Localization Support:** Localized paths are seamlessly generated for each language.
- **Standardization:** Using a single function to generate routes ensures consistency across the project.

### Assets

We are hosting site assets (images, PDFs, and small animated video files) within the project within the /public/ directory. Since these can easily take up a lot of storage, which can drastically affect project size and speed, it is important that we keep these files as optimized as possible.

Use .jpg or .svg as much as possible, with .png only being used for images that require transparency. Weekly, we have a scheduled pipeline job that looks for large png files and runs an optimizer from pngquant. In addition to file size optimization, we also want to stick to a naming standard. Please ensure your file name is in all lowercase and kebab-case.

For images uploaded from Decap users, these live at /public/images/uploads.

Videos are handled by our Video and Brand teams. Our company practice is to use Vimeo-hosted videos as much as possible. (See our embedded-videos.md doc for more information)

Icons are hosted in our Storybook instance of our design system, Slippers.

## Templates

The **Common** template renders page content in a modular, block-driven manner. This means that components specified in the YAML file are rendered sequentially. Content editors can reorder, add, or remove components simply by updating the YAML file, enabling flexible and efficient content management.

**Custom Templates** can be created to adjust the needs of a specific page such as the Blog or pages that require specific SEO logic or any other logic.

### Components Manifest File

For performance optimization, the **Common** template avoids importing all components at once. Instead:

- A manifest file is created during build-time by a module located in `/modules/page-meta.ts`.
- The manifest is stored in `#build/component-manifest`.
- The components are stored using [Nuxt 3 Naming convention](https://nuxt.com/docs/guide/directory-structure/components#component-names)

This approach allows the template to dynamically import only the components needed for a specific page, improving build performance.

### Components

When using the **Common** template, components are referenced in the YAML file using their names, adhering to the [Nuxt 3 Naming convention](https://nuxt.com/docs/guide/directory-structure/components#component-names). Each component can be customized with props provided in the componentContent attribute.

#### Attributes

- `componentName`: The name of the component as per Nuxt naming conventions.
- `componentContent`: The props of the component that are passed using `v-bind`

#### Example YAML structure

Suppose the application contains the following components:

- /components/solutions/Hero
- /components/common/accordion
- /components/common/NextSteps

The YAML content would look like this:

```yaml
content:
  - componentName: SolutionsHero
    componentContent:
      title: Lorem Ipsum
      config:
        theme: dark
  - componentName: CommonAccordion
    componentContent:
      header: Accordion
      description: Lorem Ipsum
  ## Static components do not require a componentContent attribute (e.g., NextSteps)
  - componentName: CommonNextSteps
```

### Common Template

The `props` received by the template is the `content` from the YAML file, provided by the centralized **slug** file. Each component has its own props, which are passed using `v-bind` for standardization.

#### Implementation

Below is a snippet demonstrating how the page content is dynamically built:

```vue
<script>
// /components/templates/Common.vue

const pageContent = defineProps({
  content: {
    type: Object as Array<DynamicComponent<never>>,
    required: true,
  },
});

// Fetching components from the manifest file...
</script>

<!-- Common page builder: renders components sequentially -->
<template>
  <template v-for="(component, index) of filteredComponents" :key="index">
    <keep-alive>
      <component :is="component.resolvedComponent.default" v-bind="component.componentContent" />
    </keep-alive>
  </template>
</template>
```

This block-driven design ensures that most pages follow the same pattern while remaining flexible for content customization.

## Custom Templates

**Custom Templates** can be created to meet the specific needs of individual pages. These templates are useful for scenarios such as:

- Pages with unique layouts or designs (e.g., a Blog).
- Pages requiring specialized SEO logic.
- Pages needing custom functionality or logic not covered by the **Common** template or the main **slug** page.

By implementing custom templates, the project maintains flexibility while adhering to its overall content-driven architecture.

### Example of a Custom Page

Below is an example of a custom page template that integrates specific functionality, such as external API requests and custom SEO logic:

```vue
<script lang="ts" setup>
const pageContent = defineProps({
  content: {
    type: Object as CustomPageContentInterface, // Interface tailored to the component's requirements
    required: true,
  },
});

// Custom API requests to fetch additional data
const data = getDataFromExternalSource();

// Define custom SEO metadata
const meta = {
  title: data.title,
  description: data.description,
  config: {
    schema: getCustomSchemaOrg(),
  },
};

// Apply custom SEO logic
useGitlabSeo(meta);
</script>

<template>
  <!-- Render a combination of common and custom components -->
  <CommonHero />
  <CustomContent :data="data" />
</template>
```

This example demonstrates how a custom page can combine reusable components (e.g., `CommonHero`) with
bespoke logic and design (`CustomContent`). It showcases the flexibility of the content-driven architecture in accommodating diverse requirements.
