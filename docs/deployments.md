# Deployments

## Differences between review apps and production

Note that there are subtle differences between review apps and production that should be noted for major important releases. These can burn you if you're not aware of them, and should be closely monitored if there's any functionality that is expected to work.

- OneTrust Cookie Consent - We had originally turned it off since it was leading to errors in review apps ([MR](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/668)). If you have access to the OneTrust UI, you _can_ add your review apps domain individually. It's unknown whether there is a more permanent solution for this. You [can request OneTrust access here](https://handbook.gitlab.com/handbook/marketing/digital-experience/onetrust/#access). From here, you will be able to access Onetrust through Okta, where you [set the URL of the review app](https://gitlab.my.onetrust.com/cookies/websites).
  - Examples: [draft: Load gtag before onetrust](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/3972), [fix(ot): Ot cookie bug](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/3679#note_1870041566)
  - Related: Google Tag Manager does not fully fire in review apps. GTM requires OneTrust cookie consent to fire, and OneTrust is a blocker here.
- Any 3rd party APIs - They have not added to CORS allow header for our review app subdomains.
- Some Vimeo videos have privacy settings making them inaccessible in review apps.

## Build step

Since this site is meant to be a subset of pages built for about.gitlab.com, with the rest of it handled in [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) and any pages remaining in [Buyer Experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience), we disable the [crawler attribute of the generator](https://nitro.build/config#prerender). Missing relative links are assumed to be created elsewhere.

## GitLab CI

Refer to the `.gitlab-ci.yml` file for up to date documentation on the pipeline. Since pipelines can grow in scope and get complex to follow, prefer documenting the `.gitlab-ci.yml` file with inline comments over updating documentation in this markdown file.

## Deploy script

Deployments are handled through a shell script in `scripts/deploy`. As with the `.gitlab-ci.yml` file, prefer documenting decisions in that file directly rather than in this file. At a high level, the deploy script should be able to be called from the CI pipeline and take contextual deployment actions based on variables made available in the CI environment it has been called from. That way we can manage the CI complexity in one place (`.gitlab-ci.yml`), and manage deployment complexity mostly in one place (`scripts/deploy`).
