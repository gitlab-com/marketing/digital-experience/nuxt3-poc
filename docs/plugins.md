# Plugins

We should be mindful of adding new plugins to the project and should only be doing so when necessary. DEX is trying to be as cognizant of page performance as possible and always pushing for higher SEO scores and optimized Core Web Vitals. Plugins are typically globally imported, but we need to consider proper tree-shaking of these functions and scripts.

## Render markdown with `markdown-it`

[This plugin](https://github.com/markdown-it/markdown-it) is a markdown parser that enables markdown syntax, defined in our content files, to be rendered in the page with the help of `$md.render()` method.

In order to use it:

1. Create a markdown attribute inside the `content` folder.

2. Render the content attrbitue created in step 1 inside the `.vue` component in which you want to see the markdown.

### Anchor links with `markdown-it-anchor`

[This plugin](https://www.npmjs.com/package/markdown-it-anchor) will generate an anchor link every time a heading is defined in the markdown attribute defined above.

To use it, define a new heading in the markdown attribute you have setup for your content and is rendered by the `$md.render()` method.

| Markdown                 | Rendered Output                                                                                                        |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------- |
| `# Heading level 1`      | `<h1 id="heading-level-1">Heading level 1 <a class="header-anchor" href="#heading-level-1">...</a></h1>`               |
| `## Heading level 2`     | `<h2 id="heading-level-2" tabindex="-2">Heading level 2 <a class="header-anchor" href="#heading-level-2">...</a></h2>` |
| `### Heading level 3`    | `<h3 id="heading-level-3">Heading level 3 <a class="header-anchor" href="#heading-level-3">...</a></h3>`               |
| `#### Heading level 4`   | `<h4 id="heading-level-4">Heading level 4 <a class="header-anchor" href="#heading-level-4">...</a></h4>`               |
| `##### Heading level 5`  | `<h5 id="heading-level-5">Heading level 5 <a class="header-anchor" href="#heading-level-5">...</a></h5>`               |
| `###### Heading level 6` | `<h6 id="heading-level-6">Heading level 6 <a class="header-anchor" href="#heading-level-6">...</a></h6>`               |

#### Styles

To change styles or the hover behavior, please go to the `base.scss` scss assets file and change the `.header-anchor` selector.

#### Additional `markdown-it-anchor` configuration

To change the icon, position relative to the text and/or additional settings, please go to `markdown-it-anchor` attribute defined in the `markdownit` object, both defined inside the `nuxt.config.js` file.

---

## Render diagrams with `Markdown-It-Mermaid`

Markdown-It-Mermaid will turn Markdown or code into diagrams and visualizations. Further documentation can be found [here](https://github.com/markslides/markslides/tree/main/packages/markdown-it-mermaid#readme).

This is configured to work with all instances of MarkdownIt - or anytime `$md()` is used to render Markdown.
