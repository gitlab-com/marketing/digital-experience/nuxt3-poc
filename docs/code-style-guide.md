# about.gitlab.com code style guide

This project's tech stack includes Vue 3, Typescript, SCSS, Yaml, and Markdown. With many contributors and related marketing site projects, this guide aims to keep all code contributions following dedicated styling rules as agreed upon by the Digital Experience team, or the core maintainers, of this project.

All entries to this style guide should include bad and good examples.

## Vue

### Single-file component element order

[Recommended through the Vue styling guide](https://vuejs.org/style-guide/rules-recommended#single-file-component-top-level-element-order)

Single-File Components should always order `<script>`, `<template>`, and `<style>` tags consistently, with `<style>` last, because at least one of the other two is always necessary.

```javascript
// bad

<!-- ComponentA.vue -->
<template>...</template>
<script>/* ... */</script>
<style>/* ... */</style>

// good

<!-- ComponentB.vue -->
<script>/* ... */</script>
<template>...</template>
<style>/* ... */</style>

```

### Composition API and script tag formatting

Components should utilize the composition API, calling `setup` as an attribute on the script tag. Additionally, Typescript should be added as an attribute.

```javascript
// bad

<!-- ComponentA.vue -->
<script lang="js">
setup() {
   /* ... */
}

</script>

// good

<!-- ComponentB.vue -->
<script lang="ts" setup>
    /* ... */
</script>
```

### V-for

[Vue documentation](https://vuejs.org/api/built-in-special-attributes.html#key)

A `:key` is required in uses of v-for. The key value should not be an object and should be a unique string, number, or symbol. Values should not include the `index` of a v-for loop as this number can be variable if the DOM is changing. This has more side effects in instances for `v-for` is iterating off of a changing array, such as a filtered list, but we should prevent the usage of using the index as a key as much as possible.

```javascript
// bad

<!-- ComponentA.vue -->
<article v-for="(card, index) in filteredCards" :key="index">
  <h3>{{ card.title }}</h3>
</article>

// good

<!-- ComponentB.vue -->
<article v-for="card in filteredCards" :key="card.title">
  <h3>{{ card.title }}</h3>
</article>
```

### Absolute vs relative paths for imports

Use relative paths if the module you are importing is less than two levels up.
If the module you are importing is two or more levels up, use an absolute path instead:

```javascript
// bad
import myUtil from '../../../utils/myUtil';

// good
import myUtil from '~/utils/myUtil';

// or

import myUtil from '@/utils/myUtil';
```

## Components

### Naming conventions

We are trying to minimize duplicate components as much as possible. If a new variant of an existing common component needs to be created, try to find a balance of keeping the name short (for auto-import), yet descriptive.

```html
// bad
<CommonHero200 />

<CommonAnimatedBackgroundImageHero />

// good
<CommonSingleColumnHero />
```

### Page-specific components

To vastly minimize design and functionality regressions across our site, page-specific components should not be used across unrelated pages. If this is a more efficient approach, consider making a shared common component.

```html
// bad - Pricing page
<TeamOpsHero :v-bind="data.hero" />

// good - Pricing page
<CommonHero :v-bind="data.hero" />
```

### Binding props

When passing data to components, avoid using generic catch-all props like `:data` or `:content`. Instead, either bind individual props explicitly or use `v-bind` to pass a properly structured props object. This makes the component's API more transparent and improves maintainability through better Typescript integration and can help with debugging specific prop values that may be causing issues. In addition, this can make our code appear much cleaner by eliminating heavily nested prop objects and having to unnecessarily prefix prop values.

Use `v-bind="object"` when:

- The object's properties exactly match your component's expected props
- You're passing multiple related props from a single data source
- You're building reusable components that may accept different prop combinations

Use explicit prop binding (`:prop="value"`) when:

- Only passing a subset of props from a data source
- Props come from different data sources
- You want to make prop usage more explicit for clarity
- You're transforming data before passing it as props

```javascript
// bad - using a generic data prop
<NavigationDropdown :data="{
  title: item.title,
  lists: item.lists,
  footer: item.footer
}" />

// good - using v-bind with a structured object
<NavigationDropdownList
  v-if="itemData.lists"
  v-bind="itemData"
  :class="{ 'navigation-dropdown-list--w-footer': itemData.footer }"
/>

// also good - explicit prop binding when only passing specific props
<ProductCard
  :name="product.name"
  :price="product.price"
  :image-src="product.image"
  :in-stock="product.inventory > 0"
  @click="handleClick"
/>
```

### DOM Structure

If you need to create a new component that will be used as a section of a page, consider using a wrapping `<section>` tag around a `SlpContainer` component. We can sometimes run the risk of having nested SlpContainers when it is used as the outer wrapper.

```javascript

// bad

<template>
    <SlpContainer class="my-full-width-background-color">
       // Content
    </SlpContainer>
</template>

// good

<template>
  <section class="my-full-width-background-color"> // Useful for styling full-width backgrounds
    <SlpContainer>
      // Content
    </SlpContainer>
  </section>
</template>

```

## Types

Utilize types as much as possible. Component props should always be typed within the `defineProps` method.

Refer to our `/types/base.ts` file for any commonly used type interfaces. There may be something you can use or extend from withinin your own component. If your feature is more extensive and adding new interfaces makes for a messy component file or you do not need to add on to our common types, consider making a new type file within our `/types/` directory.

Refrain from using `any` as a type. You will receive a linter warning. If it is required, disable the lint warning for that line and document in an additional comment why this type is needed.

```javascript
// bad

defineProps({
  data: {
    required: true,
    type: Object,
  }
})

// good

import type { BaseHero } from '@/types/base';

defineProps({
  hero: {
    required: true,
    type: Object as PropType<BaseHero>,
  }
})

OR

defineProps<BaseHero>();
```

## SCSS

### Scoped styles

All `<style>` tags within your components should be scoped by including the `scoped` attribute. We have a linter to check for this and will error for style blocks that are not scoped. If there is a need to globally call these styles, you can explicitly ignore the linter but you must include an additional comment documenting why those styles will not be scoped.

## Accessibility

### Alt text

An alt text attribute is required on all image tags, however a value is not always needed. If the image is purely decorative and does not provide additional contextual information, the alt text value should remain empty. We can be doing more harm than good for screen readers by adding extra information to read through that will not help the user learn more about the content on the page.

```javascript
// bad

<img src="my-image.jpg" />

<img src="lock-icon.svg" alt="Lock Icon" />

// good

<img src="productivity-graph.jpg" alt="A graph showing development teams increasing productivity by 20% after switching to GitLab" />

<img src="lock-icon.svg" alt="" />


```

## Images

### Naming conventions and organization

File names should be kept in lower case using kebab-case. Images should be saved inside an applicable folder within the `/public/images` directory. Refrain from saving images directly inside of the /images/ directory.

Please use due diligence to check if an image you need is already in the project before you add it. It may help to give the image a name that is more descriptive to help others find it.

```
// bad

John_doe-Headshot.jpg

GartnerLogo-purple.jpg

// good

john-doe-headshot.jpg

gartner-logo-purple.jpg


```

### SVG Icons

If you need a commonly used icon, check the Slippers v3 [icon library](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/tree/v3.x/icons?ref_type=heads) to see if the icon has already been added to the project.

If the icon does not exist and is specific to a single page, add it to a page-specific directory within `/public/images` as a `.svg` file. If the icon would most likely be used across the project, save as a `.vue` file within `/assets/icons/`.

```
// bad

/public/images/icons/my-icon.svg

// good

/public/images/homepage/my-unique-icon.svg

/assets/icons/my-icon.vue

import { MyIcon } from 'slippers-ui/icons';

```
