# Embedded video links

Video URLs are different when they are meant to be used as an embedded video in the page. Please try to adjust and adapt to what seems to be working but as a general rule, follow these formats:

## From Youtube

Please try to follow this format when embedding video from YouTube:

```bash
https://www.youtube.com/embed/[videoId]?enablejsapi=1
```

## From Vimeo

Please try to follow this format when embedding video from Vimeo:

```bash
https://player.vimeo.com/video/[videoId]?h=[secondVideoID]&badge=0&autopause=0&player_id=0&app_id=58479
```

## Adding a video to a page

We have a `Video` component (/components/common/video.vue) that can take in a Vimeo or Youtube URL and render it appropriately. Please use this component as much as possible as it provides a performant facade for each player.

### Internationalization, accessibility, and Vimeo

Coming soon once implemented in project

### Capturing analytics from Vimeo

Coming soon once implemented in project
