/**
 * Retrieves the attribute name from a Vue attribute node.
 * This function checks both hard-coded keys (e.g. "data-ga-name") and
 * keys bound with v-bind shorthand (e.g. ":data-ga-name").
 *
 * @param {Object} attr - The AST node representing the attribute.
 * @returns {string} The attribute name.
 */
function getAttributeName(attr) {
  if (attr.directive && attr.key.argument) {
    return attr.key.argument.name;
  }
  return attr.key && attr.key.name;
}

export const dataPropertiesPlugin = {
  rules: {
    'require-data-ga-attributes': {
      meta: {
        type: 'problem',
        docs: {
          description: '<SlpButton> and <a> tags must have the required data-ga-* attributes.',
          category: 'Best Practices',
          recommended: true,
          url: 'https://handbook.gitlab.com/handbook/marketing/digital-experience/analytics/google-tag-manager/#click-tracking',
        },
        fixable: 'code',
      },
      create(context) {
        return context.sourceCode.parserServices.defineTemplateBodyVisitor(
          {
            "VElement[name='slpbutton'], VElement[name='slp-button'], VElement[name='a']"(node) {
              const tagName = node.name;
              const allAttributes = node.startTag.attributes || [];

              const hasHrefAttribute = allAttributes.some((attr) => getAttributeName(attr) === 'href');
              if (!hasHrefAttribute) {
                return;
              }

              const requiredAttributes = ['data-ga-name', 'data-ga-location'];
              const missingAttributes = requiredAttributes.filter(
                (attrName) => !allAttributes.some((attr) => getAttributeName(attr) === attrName),
              );

              if (missingAttributes.length) {
                context.report({
                  node,
                  message: `<${tagName}> tag must have the following data attribute(s): ${missingAttributes.join(', ')}`,
                });
              }
            },
          },
          {},
        );
      },
    },
  },
};
