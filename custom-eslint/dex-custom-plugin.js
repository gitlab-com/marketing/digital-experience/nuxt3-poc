const customRulePlugin = {
  meta: {
    name: 'dex-custom-rules',
    version: '1.0.0',
  },
  rules: {
    'check-yml-page-file-properties': {
      meta: {
        type: 'problem',
        docs: {
          description: 'Ensure required properties are present in YAML mapping',
          category: 'Best Practices',
          recommended: true,
        },
        messages: {
          missingProperty: "The required property '{{property}}' is missing.",
        },
        schema: [],
      },
      create(context) {
        let isRootMapping = true;

        return {
          YAMLMapping(node) {
            // This should only check root, or parent, level required properties
            if (!isRootMapping) return;

            const propertyNames = node.pairs.map((pair) =>
              pair.key && pair.key.type === 'YAMLScalar' ? pair.key.value : null,
            );

            const requiredProperties = ['seo', 'content'];

            requiredProperties.forEach((property) => {
              if (!propertyNames.includes(property)) {
                context.report({
                  node,
                  messageId: 'missingProperty',
                  data: { property },
                });
              }
            });

            isRootMapping = false;
          },
        };
      },
    },

    'check-yml-for-config-properties': {
      meta: {
        type: 'problem',
        docs: {
          description: 'Ensure specified properties are under "config"',
          category: 'Best Practices',
          recommended: true,
        },
        schema: [],
      },
      create(context) {
        const targetProperties = [
          'variant',
          'href',
          'url',
          'dataGaName',
          'dataGaLocation',
          'src',
          'imgSrc',
          'videoSrc',
          'size',
          'cols',
          'tag',
          'formId',
        ];

        return {
          YAMLPair(node) {
            const keyName = node.key && node.key.value;
            if (targetProperties.includes(keyName)) {
              let parent = node.parent;
              while (parent) {
                if (parent.type === 'YAMLPair' && parent.key && parent.key.value === 'config') {
                  return;
                }
                parent = parent.parent;
              }

              context.report({
                node,
                message: `Property "${keyName}" must be under "config" as it should not be a translatable value.`,
              });
            }
          },
        };
      },
    },
  },
};

export default customRulePlugin;
