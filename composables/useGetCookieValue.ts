import { ref } from 'vue';

export function useGetCookieValue(cookieName: string) {
  const cookieValue = ref<string | null>(null);

  const fetchCookieValue = () => {
    const name = `${cookieName}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');

    for (let i = 0; i < ca.length; i++) {
      const c = ca[i].trim();
      if (c.indexOf(name) === 0) {
        cookieValue.value = c.substring(name.length);
        return cookieValue.value;
      }
    }
    cookieValue.value = null;
    return null;
  };

  if (typeof window !== 'undefined') {
    fetchCookieValue();
  }

  const setCookieValue = (name: string, value: string | boolean) => {
    const date = new Date();
    date.setFullYear(date.getFullYear() + 2);
    const expires = `; expires=${date.toUTCString()}`;
    document.cookie = `${name}=${value}${expires}; path=/`;
  };

  return {
    cookieValue,
    fetchCookieValue,
    setCookieValue,
  };
}
