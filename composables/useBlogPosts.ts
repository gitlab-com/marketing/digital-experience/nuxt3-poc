import { ref } from 'vue';
import { useAsyncData } from '#app';
import type { BlogPost } from '~/types/base';

export async function useBlogPosts({
  slug,
  author,
  category,
}: { slug?: string; author?: string; category?: string } = {}) {
  const post = ref<BlogPost | null>(null);
  const posts = ref([]);
  const { locale } = useI18n();
  // Fetch a single post by slug
  const fetchPostBySlug = async (slug: string) => {
    const { data } = await useAsyncData(() => queryContent(locale.value, `blog/${slug}`).findOne());
    return data;
  };

  // Fetch all posts by an author
  const fetchPostsByAuthor = async (author: string) => {
    const { data } = await useAsyncData(() =>
      queryContent(locale.value, 'blog')
        .where({ authors: { $contains: author } })
        .find(),
    );
    return data;
  };

  // Fetch all posts by category
  const fetchPostsByCategory = async (category: string) => {
    const { data } = await useAsyncData(() => queryContent(locale.value, 'blog').where({ category }).find());
    return data;
  };

  // Initial data fetching logic based on provided params
  let response = {};
  if (slug) {
    response = await fetchPostBySlug(slug);
    post.value = response.value;
  } else if (author) {
    response = await fetchPostsByAuthor(author);
    posts.value = response.value;
  } else if (category) {
    response = await fetchPostsByCategory(category);
    posts.value = response.value;
  }

  return {
    post: post.value,
    posts: posts.value,
  };
}
