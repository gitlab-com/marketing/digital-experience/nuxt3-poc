import { LANG_OPTIONS } from '@/common/constants';

// Custom composable that reads routes from runtimePublic, unfortuantely we can't pull
// routes from the native useRouter() because we use a catch-all method [...slug]. The
// runtimePublic "ContentRoutes" array is populated in our nuxt.config.ts during the build
export const useAvailableLanguages = (route: string) => {
  const { public: runtimePublic } = useRuntimeConfig();
  const { contentRoutes } = runtimePublic;
  const { locale } = useI18n();
  const languages = new Set<string>();
  const validLocales = LANG_OPTIONS.map((language) => language.value);
  const defaultLocale = LANG_OPTIONS.find((lang) => lang.default);

  const routeWithoutLocale = route.replace(new RegExp(`^/${locale.value}/?|^/|/$`, 'g'), '');
  const normalizedUrl = `/${routeWithoutLocale}/`;

  const validLocalePattern = validLocales.join('|');
  const localeRegex = new RegExp(`(?:^|\\/)(${validLocalePattern})(?:\\/|$)`);

  contentRoutes
    .filter((contentRoute: string) => contentRoute.endsWith(normalizedUrl))
    .forEach((contentRoute) => {
      const match = contentRoute.match(localeRegex);
      if (match && match[1] && validLocales.includes(match[1])) {
        languages.add(match[1]);
      } else if (!contentRoute.match(localeRegex)) {
        languages.add(defaultLocale?.value); // Default Language
      }
    });
  return Array.from(languages);
};
