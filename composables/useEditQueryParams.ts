import { useRoute } from 'vue-router';

export const useEditQueryParams = () => {
  const route = useRoute();

  const updateFreeTrialGlmContent = (currentString: string): string => {
    let queryString;
    const split = currentString.split('?');

    if (split.length === 1) {
      queryString = '';
    } else {
      queryString = split[split.length - 1];
    }

    const searchParams = new URLSearchParams(queryString);

    if (!searchParams.has('glm_source') || searchParams.get('glm_source') !== 'about.gitlab.com') {
      return currentString;
    }

    // Update `glm_content` based on the current route path
    if (route.path === '/') {
      searchParams.set('glm_content', 'home-page');
    } else {
      searchParams.set('glm_content', route.path.replace(/^\/|\/$/g, '')); // remove leading/trailing slashes
    }

    return `${split[0]}?${decodeURIComponent(searchParams.toString())}`;
  };

  return {
    updateFreeTrialGlmContent,
  };
};
