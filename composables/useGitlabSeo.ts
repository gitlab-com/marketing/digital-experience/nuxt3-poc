import {
  DEFAULT_META_DESCRIPTION,
  DEFAULT_OPENGRAPH_IMAGE,
  LANG_OPTIONS,
  SITE_NAME,
  SITE_URL,
} from '@/common/constants';
import type { BaseSeo } from '@/types/base';
import { validateSeo } from '@/utils/validateSeo';

/**
 * Builds hreflang metadata dynamically based on available languages.
 * @param currentPath Current route path
 * @returns Array of hreflang metadata
 */

const buildHreflangMeta = (currentPath: string): { rel: string; hreflang: string; href: string }[] => {
  const baseUrl = SITE_URL.replace(/\/$/, '');
  const hreflangMeta = [];
  const defaultLocale = LANG_OPTIONS.find((lang) => lang.default);
  const regional = LANG_OPTIONS.map((lang) => lang.regional && lang.value).filter(Boolean);
  const normalizedPath = (currentPath === '/' ? '/index' : currentPath).replace(/\/+$/, '').toLowerCase();
  const availableLanguages = useAvailableLanguages(currentPath === '/' ? currentPath : normalizedPath);

  // Detect language codes in the path
  const regexPathHasLang = /^\/([a-z]{2})(-[a-z]{2})?\//im;

  availableLanguages.forEach((lang) => {
    let localizedPath;

    if (lang === defaultLocale?.value) {
      localizedPath = currentPath.replace(regexPathHasLang, '/');
      localizedPath = localizedPath === '' ? '/' : localizedPath;
      hreflangMeta.push({
        rel: 'alternate',
        hreflang: 'x-default',
        href: `${baseUrl}${localizedPath}`,
      });
    } else {
      // Strip existing locale and prepend the new one
      const strippedPath = currentPath.replace(regexPathHasLang, '/');
      localizedPath = strippedPath === '/' ? `/${lang}/` : `/${lang}${strippedPath}`;

      hreflangMeta.push({
        rel: 'alternate',
        hreflang: lang,
        href: `${baseUrl}${localizedPath.endsWith('/') ? localizedPath : `${localizedPath}/`}`,
      });
    }

    // Handle regional languages (e.g., fr-fr, de-de)
    if (regional.includes(lang)) {
      hreflangMeta.push({
        rel: 'alternate',
        hreflang: lang.split('-')[0], // This adds the language-only variant
        href: `${baseUrl}${localizedPath.endsWith('/') ? localizedPath : `${localizedPath}/`}`,
      });
    }
  });

  return hreflangMeta;
};

const generateCanonical = (path: string) => {
  let canonicalPath;
  if (path === '/') {
    canonicalPath = `${SITE_URL}/`;
  } else if (path.endsWith('/')) {
    canonicalPath = `${SITE_URL}${path}`;
  } else {
    canonicalPath = `${SITE_URL}${path}/`;
  }

  return {
    rel: 'canonical',
    href: canonicalPath,
  };
};

export function generateLayoutMeta(path: string, locale: string) {
  return {
    link: [generateCanonical(path)],
    htmlAttrs: {
      lang: locale,
    },
  };
}

export const useGitlabSeo = (seoConfig: BaseSeo) => {
  const { path } = useRoute();
  // The SEO validation should not run for blog posts since those will often exceed the set character limit for titles and descriptions
  const runValidation = !path.includes('/blog/');
  const hreflangMeta = buildHreflangMeta(path);
  const errors = runValidation && seoConfig ? validateSeo(seoConfig, hreflangMeta) : [];

  if (errors.length) {
    console.error('SEO validation errors:', errors);
    throw new Error(`Invalid SEO configuration for ${path}. ${errors}`);
  }

  const title = seoConfig?.title ? `${seoConfig.title} | GitLab` : 'GitLab';
  const description = seoConfig?.description || DEFAULT_META_DESCRIPTION;
  const { locale } = useI18n();
  const localeLangCode = LANG_OPTIONS.find((lang) => lang.value == locale.value);

  // https://nuxt.com/docs/api/composables/use-head
  useHead({
    title,
    titleTemplate: '%s',
    link: [...hreflangMeta, generateCanonical(path)],
    htmlAttrs: {
      lang: localeLangCode?.code && localeLangCode?.code,
    },
  });

  // Full list of attributes https://github.com/harlan-zw/zhead/blob/main/packages/zhead/src/metaFlat.ts#L90
  useServerSeoMeta({
    title,
    description,
    ogTitle: seoConfig?.ogTitle || title,
    ogDescription: seoConfig?.ogDescription || description,
    ogImage: seoConfig?.ogImage || DEFAULT_OPENGRAPH_IMAGE,
    ogUrl: seoConfig?.ogUrl || `${SITE_URL}${path}`,
    ogSiteName: SITE_NAME,
    ogType: seoConfig?.config?.ogType || 'website',
    twitterTitle: seoConfig?.twitterTitle || title,
    twitterImage: seoConfig?.twitterImage || seoConfig?.ogImage || DEFAULT_OPENGRAPH_IMAGE,
    twitterDescription: seoConfig?.twitterDescription || description,
    twitterCreator: '@GitLab',
  });

  let useRobots = !(seoConfig?.config?.noIndex === true);

  // We don't want google to crawl review apps
  if (
    (process.env.NODE_ENV === 'development' && !process.env.CI_COMMIT_BRANCH) ||
    process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH
  ) {
    useRobots = false;
  }

  useRobotsRule(useRobots);

  // Logic to remove the pages with no "Robots" from the Sitemap on server side
  if (import.meta.server) {
    const noRobotRoutes = JSON.parse(process.env.NO_ROBOTS_ROUTES || '[]');

    if (!useRobots) {
      const route = path.replace(/^\/+|\/+$/g, ''); // Remove leading and trailing slashes
      noRobotRoutes.push(route);
      process.env.NO_ROBOTS_ROUTES = JSON.stringify(noRobotRoutes);
    }
  }
};
