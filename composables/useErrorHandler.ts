import { onErrorCaptured } from 'vue';

export const useErrorHandler = () => {
  const { path } = useRoute();

  onErrorCaptured((err, instance) => {
    let currentInstance = instance;
    const componentTrace = [];

    while (currentInstance) {
      const componentName = currentInstance.$options?.__name;
      if (componentName) {
        componentTrace.push(componentName);
      }
      currentInstance = currentInstance.$parent;
    }

    const cleanTrace = componentTrace
      .reverse()
      .slice(componentTrace.indexOf('[...slug]') + 1)
      .join(' | ');

    const error = `
    [Runtime Error] An error occurred while building a page.
    [Failing Route] ${path}
    [Component Trace] | ${cleanTrace} |
    [Internal Error Stack] ${err.stack}
  `;

    console.error(error);

    throw createError(error);
  });

  return;
};
