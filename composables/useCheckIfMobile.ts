import { ref, onMounted, onUnmounted } from 'vue';

export const useCheckIfMobile = (breakpoint: number = 769) => {
  const isMobile = ref(false);
  const width = ref(0);

  const onResize = () => {
    if (typeof window !== 'undefined') {
      width.value = window.innerWidth;
      isMobile.value = width.value < breakpoint;
    }
  };

  const addResizeListener = () => window.addEventListener('resize', onResize);
  const removeResizeListener = () => window.removeEventListener('resize', onResize);

  onMounted(() => {
    onResize();
    addResizeListener();
  });

  onUnmounted(() => {
    removeResizeListener();
  });

  return { isMobile, width };
};
