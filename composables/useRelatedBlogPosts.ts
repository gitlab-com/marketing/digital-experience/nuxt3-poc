import { useBlogCategories } from './useBlogCategories';

export const useRelatedBlogPosts = async () => {
  const categories = await useBlogCategories();

  const { data: response } = await useAsyncData('relatedBlogPosts', async () => {
    const categoryPromises = categories.map(async (category) => {
      const posts = await queryContent('en-us', '/blog/')
        .where({ 'content.category': category.content.name, 'config.template': 'BlogPost' })
        .sort({ 'content.date': -1 })
        .only(['content', 'config'])
        .limit(3)
        .find();
      return {
        category: category.content.name,
        posts: posts || [],
      };
    });
    return await Promise.all(categoryPromises);
  });
  return response.value;
};
