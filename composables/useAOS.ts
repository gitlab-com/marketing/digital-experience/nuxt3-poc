import 'aos/dist/aos.css';

export async function useAOS() {
  onMounted(async () => {
    const AOS = await import('aos');
    AOS.init({ disable: 'phone', once: true });
  });
}
