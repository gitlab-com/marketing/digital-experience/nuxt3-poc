import { ref } from 'vue';
import { useAsyncData } from '#app';
import type { BlogAuthor } from '~/types/base';

export async function useAuthors(authorNames: string[]): Promise<{ authors: BlogAuthor[] }> {
  const authors = ref<BlogAuthor[]>([]);

  const authorPromises = authorNames.map((author: string) => {
    const authorsData = queryContent('/en-us/blog/authors').where({ 'content.name': author }).findOne();

    return authorsData;
  });

  const { data } = await useAsyncData(async () => {
    return await Promise.all(authorPromises);
  });
  authors.value = data.value;
  return {
    authors: authors.value,
  };
}
