import { useI18n } from 'vue-i18n';
import { useAsyncData, useRoute } from 'nuxt/app';

/**
 * Composable that builds the route based on the locale and the route path to fetch the content
 * If the page is not found, the application is redirected to a 404 page
 */
export const useGitlabContent = async () => {
  const { locale } = useI18n();
  const { params, path } = useRoute();

  const slug = `/${locale.value.toLowerCase()}/${params.slug ? params.slug.join('/') : ''}`;

  const { data } = await useAsyncData('gitlabContent', () => queryContent(slug).findOne());

  if (!data.value) {
    throw createError({
      statusCode: 404,
      message: `Page not found in the content library: ${path}`,
    });
  }

  return data.value;
};
