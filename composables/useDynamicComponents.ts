import componentMappings from '#build/component-manifest';
import { useRoute } from 'nuxt/app';

const buildComponentNotRenderedMessage = (componentName: string, error?) => {
  const { path } = useRoute();

  return `
      [Component Render Error] An error occurred while building a component. The component does not exist on the system or an internal component error occurred.
      [Failing Route] ${path}
      [Component Name] ${componentName}
      ${
        error
          ? `
      [Component Error Name] ${error.name}
      [Component Error Message] ${error.message}
      [Component Error Stack] ${error.stack}
      `
          : ''
      }
    `;
};

export const useDynamicComponents = async (components: { componentName: string; componentContent: never }[]) => {
  const result = await Promise.all(
    components.map(async ({ componentName, componentContent }: { componentName: string; componentContent: never }) => {
      const importComponent = componentMappings[componentName];

      if (!importComponent) {
        console.error(buildComponentNotRenderedMessage(componentName));
        throw createError({
          statusCode: 500,
          message: buildComponentNotRenderedMessage(componentName),
        });
      }

      try {
        const resolvedComponent = await importComponent();
        return { resolvedComponent, componentContent };
      } catch (error) {
        console.error(buildComponentNotRenderedMessage(componentName, error));
        throw createError({
          statusCode: 500,
          message: buildComponentNotRenderedMessage(componentName, error),
        });
      }
    }),
  );

  return result.filter((component) => component);
};
