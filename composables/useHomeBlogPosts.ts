export const useHomeBlogPosts = async () => {
  const { locale } = useI18n();
  // Fetch the most recent featured blog post
  const { data: mainFeaturedPost } = await useAsyncData(() =>
    queryContent(locale.value, '/blog/')
      .where({ 'config.featured': true })
      .only(['content', 'config'])
      .sort({ 'content.date': -1 })
      .limit(1)
      .findOne(),
  );
  // Fetch the most recent 3 featured posts excluding the main featured post
  const { data: recentFeaturedPosts } = await useAsyncData(() =>
    queryContent(locale.value, '/blog/')
      .where({
        'config.featured': true,
        'config.slug': { $ne: mainFeaturedPost.value?.config.slug },
      })
      .sort({ 'content.date': -1 })
      .only(['content', 'config'])
      .limit(3)
      .find(),
  );

  // Fetch another 3 recent posts, skipping the first 4
  const { data: recentPosts } = await useAsyncData(() =>
    queryContent(locale.value, '/blog/')
      .where({
        'config.slug': { $ne: mainFeaturedPost.value?.config.slug },
        'config.template': { $eq: 'BlogPost' }, // specifying this rule because with the updated folder structure where posts live in the root /blog/ folder, this query is returning blog categories as well
      })
      .sort({ 'content.date': -1 })
      .only(['content', 'config'])
      .skip(4)
      .limit(3)
      .find(),
  );
  return {
    mainFeaturedPost: mainFeaturedPost.value,
    recentFeaturedPosts: recentFeaturedPosts.value,
    recentPosts: recentPosts.value,
  };
};
