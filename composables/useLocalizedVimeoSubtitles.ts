import { useRoute } from 'vue-router';
import { LANG_OPTIONS } from '@/common/constants';

export const useLocalizedVimeoSubtitles = () => {
  const route = useRoute();

  const path = route.path.toLowerCase();

  const matchedLang = LANG_OPTIONS.find((lang) => path.includes(lang.value));
  const trackValue = matchedLang ? matchedLang.code : 'en';

  return trackValue;
};
