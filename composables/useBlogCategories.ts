export const useBlogCategories = async () => {
  const { locale } = useI18n();
  const { data: categories } = await useAsyncData(() =>
    queryContent(locale.value, '/blog/categories').where({ 'config.hide': false }).find(),
  );
  return categories.value;
};
