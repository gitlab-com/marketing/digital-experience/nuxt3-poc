import { blogRolloutFilter, buildGitlabRoutes } from './routing';
import { oneTrustScripts, oneTrustPreconnects } from './scripts/onetrust-scripts';
import { getGtagConsentScript } from './scripts/gtm-consent-script';

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ignore: [
    // Ignore all files in the /public/images/blog directory
    'public/images/blog/**',
  ],
  router: {
    options: {
      strict: false, // Treat /page and /page/ as distinct
    },
  },
  app: {
    buildAssetsDir: '/_nuxt-new/',
    head: {
      __dangerouslyDisableSanitizersByTagID: {
        gtagConsent: ['innerHTML'],
      },
      title: 'GitLab The One DevOps Platform',
      meta: [
        {
          hid: 'contentSecurityPolicy',
          'http-equiv': 'Content-Security-Policy',
          content: `
            default-src 'self' https: http:;
            script-src 'self' 'unsafe-inline' 'unsafe-eval' https: http: *.googletagmanager.com *.mutinycdn.com;
            style-src 'self' 'unsafe-inline' https: http:;
            object-src https: http:;
            base-uri 'self';
            connect-src 'self' https: http: wss: ws: *.google-analytics.com *.analytics.google.com *.googletagmanager.com *.mutinyhq.com *.mutinyhq.io *.mutinycdn.com;
            frame-src 'self' https: http:;
            img-src 'self' https: http: data: *.google-analytics.com *.googletagmanager.com *.mutinycdn.com;
            manifest-src 'self'; media-src 'self' https: http:;
            child-src 'self' blob: https: http:;
            font-src 'self' https: http: data:;
          `,
        },
        { name: 'format-detection', content: 'telephone=no' },
        { name: 'twitter:card', content: 'summary_large_image' },
        { name: 'twitter:site', content: '@GitLab' },
      ],
      link: [
        ...oneTrustPreconnects,
        { rel: 'icon', type: 'image/x-icon', href: '/images/ico/favicon.ico' },
        { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/images/ico/favicon-32x32.png' },
        { rel: 'icon', type: 'image/png', sizes: '192x192', href: '/images/ico/favicon-192x192.png' },
        { rel: 'apple-touch-icon', href: '/images/ico/apple-touch-icon.png' },
        { rel: 'alternate', type: 'application/atom+xml', title: 'Blog', href: '/atom.xml' },
        { rel: 'alternate', type: 'application/atom+xml', title: 'All Releases', href: '/all-releases.xml' },
        { rel: 'alternate', type: 'application/atom+xml', title: 'Security Releases', href: '/security-releases.xml' },
        { rel: 'alternate', type: 'application/atom+xml', title: 'Major Releases', href: '/releases.xml' },
      ],
      script: [
        ...oneTrustScripts,
        {
          hid: 'mutiny',
          innerHTML: `
          (function () {
            var a = (window.mutiny = window.mutiny || {});
            if (!window.mutiny.client) {
              a.client = { _queue: {} };
              var b = ['identify', 'trackConversion'];
              var c = [].concat(b, ['defaultOptOut', 'optOut', 'optIn']);
              var d = function factory(c) {
                return function () {
                  for (var d = arguments.length, e = new Array(d), f = 0; f < d; f++) {
                    e[f] = arguments[f];
                  }
                  a.client._queue[c] = a.client._queue[c] || [];
                  if (b.includes(c)) {
                    return new Promise(function (b, d) {
                      a.client._queue[c].push({ args: e, resolve: b, reject: d });
                    });
                  } else {
                    a.client._queue[c].push({ args: e });
                  }
                };
              };
              c.forEach(function (b) {
                a.client[b] = d(b);
              });
            }
          })();
          `,
        },
        {
          src: 'https://client-registry.mutinycdn.com/personalize/client/c18972324098ea25.js',
          async: true,
        },
        {
          hid: 'gtagConsent',
          innerHTML: getGtagConsentScript(),
          defer: true,
        },
        {
          src: 'https://cdn.bizible.com/scripts/bizible.js',
          defer: true,
        },
        {
          src: 'https://munchkin.marketo.net/munchkin.js',
          defer: true,
        },
      ],
    },
  },
  scripts: {
    registry: {
      googleTagManager: {
        id: 'GTM-NJXWQL',
        scriptInput: {
          referrerpolicy: false,
        },
      },
    },
  },
  devtools: { enabled: true },
  modules: [
    '@nuxt/content',
    '@nuxt/eslint',
    '@nuxt/fonts',
    '@nuxt/image',
    '@nuxt/scripts',
    '@nuxtjs/seo',
    '@nuxtjs/i18n',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
  ],
  i18n: {
    vueI18n: './i18n.config.ts', // https://i18n.nuxtjs.org/docs/getting-started/usage
    locales: [
      { code: 'en-us', language: 'en-US', label: 'English', langLabel: 'Language', file: 'en-US.json' },
      { code: 'de-de', language: 'de-DE', label: 'Deutsch', langLabel: 'Sprache', file: 'de-DE.json' },
      { code: 'es', language: 'es', label: 'Español', langLabel: 'Idioma', file: 'es.json' },
      { code: 'fr-fr', language: 'fr-FR', label: 'Français', langLabel: 'Langue', file: 'fr-FR.json' },
      { code: 'it-it', language: 'it-IT', label: 'Italiano', langLabel: 'Lingua', file: 'it-IT.json' },
      { code: 'ja-jp', language: 'ja-JP', label: '日本語', langLabel: '言語', file: 'ja-JP.json' },
      { code: 'pt-br', language: 'pt-BR', label: 'Português', langLabel: 'Idioma', file: 'pt-BR.json' },
    ],
    defaultLocale: 'en-us',
    strategy: 'prefix_except_default',
    langDir: './locales',
    detectBrowserLanguage: {
      useCookie: false,
      redirectOn: 'root',
      fallbackLocale: 'en-us',
    },
  },
  image: {
    domains: ['images.ctfassets.net'], // Remove this domain once we are migrated out of Contentful and all assets have been moved
    format: ['avif', 'webp'], // will default to original format if optimization not possible
    quality: 80, // Set global quality for images - small enough quality change that it should not impact ux
    screens: {
      xs: 320,
      sm: 640,
      md: 768,
      lg: 1024,
      xl: 1280,
      xxl: 1536,
      '2xl': 1536,
    },
  },
  css: ['~/assets/css/base.scss', 'slippers-ui/styles/base.scss', 'slippers-ui/styles/components.css'],
  nitro: {
    prerender: {
      crawlLinks: false,
      ignore: ['/200'],
      concurrency: 500,
    },
  },
  // https://nuxt.com/docs/getting-started/prerendering#prerenderroutes-nuxt-hook
  hooks: {
    'nitro:config'(nitroConfig) {
      const routes = buildGitlabRoutes();

      // Temporary logic while the blog is rolled out
      const filteredRoutes = blogRolloutFilter(routes);

      nitroConfig.runtimeConfig.public['contentRoutes'] = filteredRoutes; // To be used in client-side calling useRuntimeConfig()
      process.env.NUXT_CONTENT_FILES = JSON.stringify(filteredRoutes); // To be used in the "prerender:routes" to avoid calling buildGitlabRoutes() twice
    },
    async 'prerender:routes'(ctx) {
      ctx.routes.clear(); // Removes any route that has been automatically generated by Nuxt
      const routeIdx = process.argv.indexOf('--route');
      const specificRoute = routeIdx !== -1 ? process.argv[routeIdx + 1] : null;
      //const routeArrayIdx = process.argv.indexOf('--route-array');
      //const fileList = routeArrayIdx !== -1 ? process.argv[routeArrayIdx + 1].split('\n') : null;

      // Build a single route
      if (specificRoute) {
        console.info(`🔄 Generating only the specific route: ${specificRoute}`);
        ctx.routes.add(specificRoute.endsWith('/') ? specificRoute : `${specificRoute}/`);
        return;
      }
      // // Build specific changed files
      // // Temporarily disabled while the blog is rolled out!
      // if (fileList?.length) {
      //   const routesRegex = /content|\.yml|index\/?|en-us\//gim;
      //   const updatedRoutes = fileList.map((file) => {
      //     // Extract the relative path and format it for routing
      //     const route = file.replace(routesRegex, '');
      //     return route.endsWith('/') ? route : `${route}/`;
      //   });

      //   // eslint-disable-next-line no-console
      //   console.info(`🔄 Generating only the specific changed routes: ${updatedRoutes}`);
      //   updatedRoutes.forEach((route: string) => ctx.routes.add(route));
      //   return;
      // }
      const routes = JSON.parse(process.env.NUXT_CONTENT_FILES || '[]');

      console.info(`🔄 Generating all routes`);
      for (const route of routes) {
        ctx.routes.add(route.endsWith('/') ? route : `${route}/`);
      }
    },
  },
  site: {
    url: 'https://about.gitlab.com',
    trailingSlash: true,
  },
  sitemap: {
    sitemapName: '/migrated-pages/sitemap.xml',
    xslColumns: [{ label: 'URL', width: '100%' }],
    includeAppSources: true,
    autoI18n: false,
    exclude: ['/'],
  },
  linkChecker: {
    enabled: false,
  },
  schemaOrg: {
    defaults: false,
  },
  vite: {
    $client: {
      build: {
        rollupOptions: {
          output: {
            chunkFileNames: '_nuxt-new/[name].[hash].js',
            entryFileNames: '_nuxt-new/[name].[hash].js',
          },
        },
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use 'slippers-ui/styles/_variables.scss' as *;`,
          api: 'modern',
        },
      },
    },
  },
  compatibilityDate: '2024-08-30',
});
