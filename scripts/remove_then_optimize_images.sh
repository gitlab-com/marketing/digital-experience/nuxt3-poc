#!/bin/bash

IMAGES_DIR="public/images"
CONTENT_DIRS=("content/" "components/" "pages/")
MAX_SIZE=500000
CURRENT_DATE=$(date +"%Y-%m-%d")
BRANCH_NAME="image-cleanup-$CURRENT_DATE"
GITLAB_API_URL="https://gitlab.com/api/v4/projects/55044057/merge_requests"

REVIEWER_IDS=(
    8890879  # Nathan
    7823881  # Laura
    24678449 # Chris
    10032586 # John
    10032580 # Mateo
    1499790  # Javi
    11280894 # Margareth
)

RANDOM_INDEX=$((RANDOM % ${#REVIEWER_IDS[@]}))
REVIEWER_ID=${REVIEWER_IDS[$RANDOM_INDEX]}

get_file_size() {
    local file="$1"
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS uses stat -f%z
        stat -f%z "$file"
    else
        # Linux uses stat -c%s
        stat -c%s "$file"
    fi
}

is_image_referenced() {
    local image_file="$1"
    local found_reference=0

    image_file="${image_file#./}"

    for dir in "${CONTENT_DIRS[@]}"; do
        if grep -q -r "$image_file" "$dir"; then
            found_reference=1
            break
        fi
    done

    return $found_reference
}

# Check if PNGQuant is properly installed
if ! command -v pngquant &> /dev/null; then
    echo "pngquant could not be found. Please install pngquant first."
    exit 1
fi

# Step 1: Find and remove unreferenced images
find "$IMAGES_DIR" -type f \( -name "*.png" -o -name "*.jpg" -o -name "*.jpeg" -o -name "*.gif" -o -name "*.webp" -o -name "*.mp4" \) | while read -r image_file; do
    if ! is_image_referenced "$image_file"; then
        echo "Removing unreferenced image file $image_file"
        rm "$image_file"
    fi
done

# Step 2: Find and compress remaining PNG images over a certain size
find "$IMAGES_DIR" -type f -name "*.png" | while read -r image_file; do
    file_size=$(get_file_size "$image_file")
    if [ "$file_size" -gt $MAX_SIZE ]; then
        echo "Compressing PNG file $image_file (Size: $file_size bytes)"
        pngquant --quality=65-80 --ext=.png --force "$image_file"
    fi
done

# Step 3: Check if there are any changes and commit and create MR
if [ -n "$(git status --porcelain | grep '^ M public/images/')" ]; then
    echo "Changes detected. Creating a new branch, committing, and pushing..."

    git checkout -b "$BRANCH_NAME"
    git config user.name "DigitalExperience-service"
    git config user.email "digitalexperience@gitlab.com"
    git add public/images/*
    git commit -m "Image cleanup and optimization on $CURRENT_DATE"
    git push https://DigitalExperience-service:$BLOG_SLUG_UPDATE_KEY@gitlab.com/$CI_PROJECT_PATH.git 

    echo "Waiting for GitLab to register the push..."
    sleep 5

    response=$(curl --silent --max-time 60 --request POST "$GITLAB_API_URL" \
    --header "PRIVATE-TOKEN: $BLOG_SLUG_UPDATE_KEY" \
    --header "Content-Type: application/json" \
    --data "{
        \"source_branch\": \"$BRANCH_NAME\",
        \"target_branch\": \"main\",
        \"title\": \"Image cleanup and optimization - $CURRENT_DATE\",
        \"reviewer_ids\": [\"$REVIEWER_ID\"]
    }")

    # Check if the MR creation was successful
    if echo "$response" | grep -q '"id"'; then
        echo "Merge request created successfully."
    else
        echo "Failed to create merge request. Response: $response"
        exit 1;
    fi
else
    echo "No changes detected."
fi