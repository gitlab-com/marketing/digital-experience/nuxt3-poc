#!/bin/bash

CI_API_V4_URL="https://gitlab.com/api/v4"

echo "Starting the reviewapp_comment.sh script..."

# Check if required variables are set
if [ -z "$DEX_SERVICE_READ_WRITE_TOKEN" ] || [ -z "$CI_API_V4_URL" ] || [ -z "$CI_PROJECT_ID" ] || [ -z "$CI_MERGE_REQUEST_IID" ]; then
    echo "Missing required environment variables."
    exit 0
fi

# Fetch the merge request details to get the author's username
echo "Fetching merge request details..."
MR_DETAILS=$(curl --silent --header "PRIVATE-TOKEN: $DEX_SERVICE_READ_WRITE_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID")
MR_AUTHOR_USERNAME=$(echo "$MR_DETAILS" | jq -r ".author.username")

# Fetch the target branch to avoid 'unknown revision' issues
echo "Fetching target branch..."
git fetch origin $CI_MERGE_REQUEST_TARGET_BRANCH_NAME

# Fetch the list of modified files between the current commit and the target branch
echo "Finding modified files..."
MODIFIED_FILES=$(git diff --name-only $CI_COMMIT_SHA origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME)

# Extract the first YAML file in the 'content/en-us/blog/posts/' directory
BLOG_FILE=$(echo "$MODIFIED_FILES" | grep -m 1 "^content/en-us/blog/.*\.yaml$")

# Check if a blog post file was found
if [ -n "$BLOG_FILE" ]; then
    BLOG_SLUG=$(basename "$BLOG_FILE" .yaml)
    BLOG_URL_PARTS=$(echo "$BLOG_SLUG" | sed 's/-/\//3' | sed 's/-/\//2' | sed 's/-/\//1')

    DYNAMIC_URL="https://$CI_COMMIT_REF_SLUG.about.gitlab-review.app/blog/$BLOG_URL_PARTS/"
    echo "Dynamic URL: $DYNAMIC_URL"
else
    echo "No blog post YAML file found. Exiting."
    exit 0
fi

# Construct the comment text with the dynamic URL and mention the MR author
COMMENT_TEXT="🚀 @${MR_AUTHOR_USERNAME}, your review app is ready! You can check it out here: $DYNAMIC_URL"
AUTHOR_ID="21175152"  # Replace with the correct service account ID

# Fetch the list of comments (notes) on the merge request
echo "Fetching the list of comments on the merge request..."
NOTES=$(curl --silent --header "PRIVATE-TOKEN: $DEX_SERVICE_READ_WRITE_TOKEN" "$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes")

# Find the comment by the author's ID
LATEST_COMMENT_ID=$(echo "$NOTES" | jq -r ".[] | select(.author.id == $AUTHOR_ID) | .id" | tail -n 1)

# If no comment exists, create a new one
if [ -z "$LATEST_COMMENT_ID" ]; then
    echo "No existing comment found. Creating a new comment..."
    curl --header "PRIVATE-TOKEN: $DEX_SERVICE_READ_WRITE_TOKEN" \
         --header "Content-Type: application/json" \
         --data "{\"body\": \"$COMMENT_TEXT\"}" \
         "$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes"
    echo "New comment created successfully."
else
    echo "Updating the existing comment with ID: $LATEST_COMMENT_ID."
    curl --request PUT --header "PRIVATE-TOKEN: $DEX_SERVICE_READ_WRITE_TOKEN" \
         --header "Content-Type: application/json" \
         --data "{\"body\": \"$COMMENT_TEXT\"}" \
         "$CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes/$LATEST_COMMENT_ID"
    echo "Comment updated successfully."
fi

echo "Script reviewapp_comment.sh finished."
