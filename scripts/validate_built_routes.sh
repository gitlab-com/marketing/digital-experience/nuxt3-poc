#!/bin/bash
echo "List of allowed migrated pages:"

# Alphabetical order
REQUIRED_PAGES=(
  "2024/ai"
  "2024/security-compliance"
  "community"
  "community/co-create" 
  "community/contribute" 
  "community/early-access"
  "community/newsletter"
  "company"
  "company/contact"
  "company/preference-center"
  "company/team/board-of-directors"
  "company/team/e-group"
  "company/visiting"
  "developer-experience"
  "developer-survey"
  "environmental-social-governance"
  "faster-together"
  "get-help"
  "get-started"
  "gitlab-duo"
  "platform"
  "sales"
  "search"
  "security/open-source-resources"
  "services"
  "software-faster"
  "solutions/aerospace"
  "solutions/education"
  "solutions/public-sector"
  "solutions/startups"
  "update"
  "upgrade"
  "teamops"
  "terms"
  "why-gitlab"
)


echo "Recursively validating .html files in .output/public..."

ERROR_FOUND=false

while IFS= read -r FILE; do
  # Skip files with "admin" in their path
  if [[ "$FILE" == *admin* ]]; then
    echo "Skipping file: $FILE"
    continue
  fi

  # Allow blog pages dynamically
  if [[ "$FILE" == */blog/*/index.html ]]; then
    echo "Allowed blog page: $FILE"
    continue
  fi

  # Check if the file's path ends with an allowed pattern
  ALLOWED=false
  for PAGE in "${REQUIRED_PAGES[@]}"; do
    if [[ "$FILE" == *"$PAGE/index.html" ]]; then
      ALLOWED=true
      break
    fi
  done

  if [[ "$ALLOWED" == false ]]; then
    echo "Error: Unexpected file found: $FILE. This route should be added to the REQUIRED_PAGES array in /scripts/validate_built_routes.sh to be deployed."
    ERROR_FOUND=true
  fi
done < <(find .output/public -type f -name "*.html")

# Exit with error if an unexpected file was found
if [[ "$ERROR_FOUND" == true ]]; then
  exit 1
fi

echo "All files are valid."
