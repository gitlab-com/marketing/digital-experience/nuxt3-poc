#!/bin/bash
echo "Checking image sizes in public/ (excluding /public/images/blog/)"
MAX_SIZE=900000 # This is large, but we intend to migrate to cloudinary.
declare -a TOO_LARGE

while read -r img; do
  size=$(ls -l "$img" | awk '{print $5}')
  if [ "$size" -gt "$MAX_SIZE" ]; then
    TOO_LARGE+=("$img ($size bytes)")
  fi
done < <(find public -type f \( -name "*.png" -o -name "*.jpg" -o -name "*.jpeg" -o -name "*.webp" \) -not -path "public/images/blog/*")

# If there are files that are too large, list them and fail the job
if [ "${#TOO_LARGE[@]}" -gt 0 ]; then
  echo -e "\nThe following images exceed the ${MAX_SIZE} byte limit:"
  printf '%s\n' "${TOO_LARGE[@]}"
  exit 1
else
  echo -e "\nAll images are within the size limit."
fi
