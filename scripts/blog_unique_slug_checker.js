import fs from 'fs';
import path from 'path';
import yaml from 'js-yaml';

const locale = process.argv[2];
if (!locale) {
  console.error('Error: Locale is required.');
  process.exit(1);
}

const folderPath = `./content/${locale}/blog`;

const slugs = new Map();
const duplicates = [];

// Function to read and parse the YML files
const readYmlFiles = (folderPath) => {
  const files = fs.readdirSync(folderPath);

  files.forEach((file) => {
    if (file.endsWith('.yml') || file.endsWith('.yaml')) {
      const filePath = path.join(folderPath, file);
      // eslint-disable-next-line no-console
      console.log(`Checking file: ${filePath}`);

      try {
        const fileContent = fs.readFileSync(filePath, 'utf8');
        const data = yaml.load(fileContent);
        const slug = data?.config?.slug;

        if (slug) {
          if (slugs.has(slug)) {
            // If the slug is already in the map, store the file path as a duplicate
            duplicates.push({ slug, file1: slugs.get(slug), file2: filePath });
          } else {
            // Otherwise, add the slug to the map
            slugs.set(slug, filePath);
          }
        } else {
          // eslint-disable-next-line no-console
          console.log(`No slug found in file: ${filePath}`);
        }
      } catch (e) {
        console.error(`Error parsing file ${file}: ${e.message}`);
        process.exit(1);
      }
    }
  });
};

readYmlFiles(folderPath);

if (duplicates.length > 0) {
  console.error('Error: Duplicate slugs found:');
  duplicates.forEach(({ slug, file1, file2 }) => {
    console.error(`Slug "${slug}" found in: ${file1} and ${file2}`);
  });
  process.exit(1);
} else {
  // eslint-disable-next-line no-console
  console.log('Success: All slugs are unique.');
}
