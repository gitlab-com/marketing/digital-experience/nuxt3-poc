interface OneTrustPreconnect {
  rel: 'preconnect';
  href: string;
}

interface OneTrustScript {
  hid?: string;
  src: string;
  type: 'text/javascript';
  charset?: 'utf-8' | 'UTF-8';
  'data-domain-script'?: string;
  defer: boolean;
}

const isDevelopment =
  process.env.NODE_ENV === 'development' || process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH;
const oneTrustDomain = isDevelopment ? process.env.NUXT_STAGING_ONETRUST_DOMAIN : process.env.NUXT_ONETRUST_DOMAIN;

const createOneTrustPreconnects = (): OneTrustPreconnect[] => [
  {
    rel: 'preconnect',
    href: 'https://cdn.cookielaw.org',
  },
  {
    rel: 'preconnect',
    href: 'https://geolocation.onetrust.com',
  },
];

const createOneTrustScripts = (domainScript: string): OneTrustScript[] => [
  {
    hid: 'oneTrustSDK',
    src: 'https://cdn.cookielaw.org/scripttemplates/otSDKStub.js',
    type: 'text/javascript',
    charset: 'utf-8',
    'data-domain-script': domainScript,
    defer: true,
  },
  {
    src: `https://cdn.cookielaw.org/consent/${domainScript}.js`,
    type: 'text/javascript',
    charset: 'UTF-8',
    defer: true,
  },
  {
    hid: 'oneTrustAutoBlocking',
    src: `https://cdn.cookielaw.org/consent/${domainScript}/OtAutoBlock.js`,
    type: 'text/javascript',
    defer: true,
  },
  {
    hid: 'oneTrustGeolocation',
    src: 'https://geolocation.onetrust.com/cookieconsentpub/v1/geo/location/geofeed',
    type: 'text/javascript',
    defer: true,
  },
];

const oneTrustPreconnects = createOneTrustPreconnects();
const oneTrustScripts = isDevelopment ? [] : createOneTrustScripts(oneTrustDomain || '');

export { oneTrustPreconnects, oneTrustScripts };

export type { OneTrustPreconnect, OneTrustScript };
