#!/bin/bash
# Step 1:  Get the files changed

git fetch origin main 
CHANGED_FILES=$(git diff --name-only --diff-filter=ACMRT origin/main | grep '^content/.*\.yml$' || true)

# Step 2:  Run fix-links on the changed files
if [ ! -z "$CHANGED_FILES" ]; then
    echo "Running yarn lint:fix changed files:"
    echo "$CHANGED_FILES"
    yarn lint:fix $CHANGED_FILES
    echo "Running yarn fix-links on changed files:"
    yarn fix-links $CHANGED_FILES
else
    echo "No content YAML files were changed in this MR"
fi