#!/bin/bash
echo "script starting"

# Function to run yarn generate and check for errors
run_generate() {
  yarn generate "$@"
  if [ $? -ne 0 ]; then
    echo "yarn generate failed!"
    exit 1
  fi
}

# If FORCE_GENERATE is set, bypass checks and generate all routes
if [ "$FORCE_GENERATE" == "true" ]; then
  echo "FORCE_GENERATE is set. Bypassing checks and generating all routes."
  yarn generate
  rm -f .output/public/404.html
  exit 0
fi

# Fetch all changed files in the 'content/blog/' folder, excluding deleted files
CHANGED_FILES=$(git diff --name-only --diff-filter=d "$CI_MERGE_REQUEST_DIFF_BASE_SHA" "$CI_COMMIT_SHA" | grep "^content/.*/blog/")

# Check if there are any changes in the 'content/blog/' folder
if [ -z "$CHANGED_FILES" ]; then
  echo "No changes detected in 'content/blog/'. Generating all routes."
  run_generate
else
  # Count the number of changed files
  FILE_COUNT=$(echo "$CHANGED_FILES" | wc -l)

  # Determine whether to generate routes for changed files or all routes
  if [ "$FILE_COUNT" -lt 1000 ]; then
    echo "Changes detected and file count of $FILE_COUNT is below 1000. Generating routes for changed files."
     run_generate --route-array "$CHANGED_FILES"
  else
    echo "Too many changes detected ($FILE_COUNT files). Generating all routes."
    run_generate
  fi
fi

# Clean up unnecessary files
rm -f .output/public/404.html
