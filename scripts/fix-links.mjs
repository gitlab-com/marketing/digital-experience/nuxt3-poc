import fs from 'fs/promises';
import path from 'path';
import yaml from 'js-yaml';

const CONTENT_DIR = 'content';
const BASE_URL = 'https://about.gitlab.com';
const LOCALES = ['ja-jp', 'fr-fr', 'de-de', 'pt-br', 'it-it', 'es'];
// Get file paths from command line arguments
const targetFiles = process.argv.slice(2);
main(targetFiles).catch(console.error);

async function getYamlFiles(dir) {
  const entries = await fs.readdir(dir, { withFileTypes: true });
  let files = [];

  for (const entry of entries) {
    const fullPath = path.join(dir, entry.name);
    if (entry.isDirectory()) {
      files = files.concat(await getYamlFiles(fullPath));
    } else if (entry.name.endsWith('.yml')) {
      files.push(fullPath);
    }
  }
  return files;
}

async function checkUrlExists(url) {
  if (url.startsWith('#') || (!url.startsWith(BASE_URL) && !url.startsWith('/'))) return true;

  try {
    const response = await fetch(url, { method: 'HEAD' });
    return response.ok;
  } catch {
    return false;
  }
}

function getRelativePath(href) {
  if (href.startsWith(BASE_URL)) {
    let relative = href.replace(BASE_URL, '');
    if (!relative.startsWith('/')) {
      relative = '/' + relative;
    }
    return relative;
  }
  return href;
}

function getFileLocale(filePath) {
  const parts = filePath.split(path.sep);
  return parts.reverse().find((part) => LOCALES.includes(part));
}

function addTrailingSlash(path) {
  if (!path) return path;
  const hashIndex = path.indexOf('#');

  if (hashIndex !== -1) {
    // Split into path and hash parts
    const pathPart = path.substring(0, hashIndex);
    const hashPart = path.substring(hashIndex);

    // Add trailing slash to path part, remove any trailing slash from hash part
    const pathWithSlash =
      pathPart.endsWith('/') || pathPart.endsWith('.html') || pathPart.endsWith('.pdf') ? pathPart : `${pathPart}/`;
    const cleanHashPart = hashPart.endsWith('/') ? hashPart.slice(0, -1) : hashPart;

    return `${pathWithSlash}${cleanHashPart}`;
  }

  // Prevent trailing slash after .html and .pdf but add for other cases
  if (path.endsWith('.html/') || path.endsWith('.pdf/')) {
    return path.slice(0, -1);
  }

  // For URLs without hash, add trailing slash unless it ends with .html or .pdf
  return path.endsWith('/') || path.endsWith('.html') || path.endsWith('.pdf') ? path : `${path}/`;
}

async function updateHref(href, fileLocale) {
  if (href.startsWith('#')) return href;
  if (href.includes('mailto:')) return href;

  // Convert to relative path
  let relativePath = getRelativePath(href);
  const parts = relativePath.split('/').filter(Boolean);
  const currentLocale = parts[0];
  const isLocalized = LOCALES.includes(currentLocale);

  // Handle multiple cases
  let updatedPath;

  if (isLocalized) {
    // Check if localized version exists
    if (await checkUrlExists(BASE_URL + relativePath)) {
      updatedPath = relativePath;
    } else {
      // Remove locale if page doesn't exist in that locale
      updatedPath = '/' + parts.slice(1).join('/');
    }
  } else if (fileLocale) {
    // Try localized version first
    const localizedPath = `/${fileLocale}${relativePath}`;
    if (await checkUrlExists(BASE_URL + localizedPath)) {
      updatedPath = localizedPath;
    } else {
      updatedPath = relativePath;
    }
  } else {
    updatedPath = relativePath;
  }

  // Ensure trailing slash (unless  or .pdf)
  return addTrailingSlash(updatedPath);
}

async function processYamlFile(filePath) {
  const originalContent = await fs.readFile(filePath, 'utf8');
  let content = yaml.load(originalContent);
  let modified = false;
  let hrefUpdates = new Map();
  const fileLocale = getFileLocale(filePath);

  async function processObject(obj) {
    if (!obj || typeof obj !== 'object') return;

    if (Array.isArray(obj)) {
      for (const item of obj) {
        await processObject(item);
      }
      return;
    }

    for (const [key, value] of Object.entries(obj)) {
      if (typeof value === 'object') {
        await processObject(value);
        continue;
      }

      if (key === 'href' && typeof value === 'string') {
        const updatedHref = await updateHref(value, fileLocale);
        if (updatedHref !== value) {
          console.log(`${filePath}: ${value} -> ${updatedHref}`);
          obj[key] = updatedHref;
          hrefUpdates.set(value, updatedHref);
          modified = true;
        }
      }
    }
  }

  await processObject(content);

  if (modified) {
    const updatedContent = originalContent
      .split('\n')
      .map((line) => {
        if (!line.includes('href:')) return line;

        const indent = line.match(/^\s*/)[0];
        const originalHref = line.split('href:')[1].trim().replace(/['"]/g, '');
        const newHref = hrefUpdates.get(originalHref);

        return newHref ? `${indent}href: '${newHref}'` : line;
      })
      .join('\n');

    await fs.writeFile(filePath, updatedContent.trimEnd() + '\n', 'utf8');
  }
}

async function main(targetFiles = null) {
  try {
    if (targetFiles && targetFiles.length > 0) {
      await Promise.all(targetFiles.map((file) => processYamlFile(file)));
    } else {
      const yamlFiles = await getYamlFiles(CONTENT_DIR);
      await Promise.all(yamlFiles.map((file) => processYamlFile(file)));
    }
  } catch (error) {
    console.error('Error processing files:', error);
  }
}

export { main };
