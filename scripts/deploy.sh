#!/usr/bin/env bash

echo "Starting a ${DEPLOY_TYPE} deploy."

# Common variables
SRC_DIR='.output/public'

if [ "$DEPLOY_TYPE" = 'staging' ]; then
  GCP_PROJECT=$GCP_PROJECT_REVIEW_APPS
  GCP_BUCKET="gs://$GCP_BUCKET_REVIEW_APPS"
  GCP_SERVICE_ACCOUNT_KEY=$GCP_SERVICE_ACCOUNT_KEY_REVIEW_APPS

  LIVE_DIR="$GCP_BUCKET/$CI_COMMIT_REF_SLUG/"

  # Log deployment details for staging
  echo "------------------------------------------"
  echo "Deployment Type: Staging"
  echo "GCP Project: $GCP_PROJECT"
  echo "GCP Bucket: $GCP_BUCKET"
  echo "Live Directory: $LIVE_DIR"
  echo "------------------------------------------"
elif [ "$DEPLOY_TYPE" = 'production' ]; then
  GCP_PROJECT=$GCP_PROJECT_REVIEW_APPS
  GCP_BUCKET="gs://$GCP_BUCKET_REVIEW_APPS"
  GCP_SERVICE_ACCOUNT_KEY=$GCP_SERVICE_ACCOUNT_KEY_REVIEW_APPS

  LIVE_DIR="$GCP_BUCKET/staging/"

  # Log deployment details for production
  echo "------------------------------------------"
  echo "Deployment Type: Production"
  echo "GCP Project: $GCP_PROJECT"
  echo "GCP Bucket: $GCP_BUCKET"
  echo "Live Directory: $LIVE_DIR"
  echo "------------------------------------------"
else
  echo "Invalid deployment type"
  exit 1
fi

# Authenticate with GCP and run the deployment based on any variables determed above.
gcloud auth activate-service-account --key-file $GCP_SERVICE_ACCOUNT_KEY
gcloud config set project "$GCP_PROJECT"

echo "Starting deployment from $SRC_DIR to $LIVE_DIR..."

gcloud storage rsync -r "$SRC_DIR" "$LIVE_DIR"

echo "Deployment to $LIVE_DIR completed!"
