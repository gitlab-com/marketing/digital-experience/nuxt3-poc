#!/bin/bash
CURRENT_DATE=$(date +"%Y-%m-%d")
set -e  # Exit on error

# Step 1:  Check if there are any changes and commit to existing MR
if [ -n "$(git status --porcelain)" ]; then
    echo "Changes detected. Adding changes, committing, and pushing..."

    # Make sure we're on the right branch
    echo "Checking out branch: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
    git fetch || { echo "Fetching branches branch"; exit 1; }
    git checkout $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME || { echo "Failed to checkout branch"; exit 1; }
    
    # Configure git
    git config user.name "DigitalExperience-service" || { echo "Failed to set git username"; exit 1; }
    git config user.email "digitalexperience@gitlab.com" || { echo "Failed to set git email"; exit 1; }
    
    # Add all changes
    echo "Adding content files changed by lint:fix..."
    git add content/* || { echo "Failed to stage changes"; exit 1; }
    
    # Commit
    echo "Committing changes..."
    git commit -m "Add files changed by lint:fix $CURRENT_DATE for translation MR" || { echo "Failed to commit"; exit 1; }
    
    # Push (fixed URL format)
    echo "Pushing to remote..."
    git push "https://DigitalExperience-service:${ARGO_UPDATE_KEY}@gitlab.com/${CI_PROJECT_PATH}.git" "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" || { echo "Failed to push"; exit 1; }

    echo "Changes successfully committed and pushed"
    
else
    echo "No changes detected."
fi
