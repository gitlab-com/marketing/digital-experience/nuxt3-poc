echo "Checking for file names in specific directories..."

# Directory to check for Pascal case Vue files starting with a capital letter: components
COMPONENTS_DIR="components"

echo "Running git ls-files for components..."
FILES=$(git ls-files $COMPONENTS_DIR)

echo "Filtering for .vue files..."
VUE_FILES=$(echo "$FILES" | grep -E '\.vue$')

echo "Check each file, and make a list of any invalid component names"
for FILE in $VUE_FILES; do
  BASENAME=$(basename "$FILE")

  # Check for PascalCase (starting with a capital letter)
  if ! echo "$BASENAME" | grep -qE '^[A-Z][a-zA-Z0-9]*\.vue$'; then
    BAD_COMPONENT_FILES="$BAD_COMPONENT_FILES $BASENAME"
  fi
done

if [ -n "$BAD_COMPONENT_FILES" ]; then
  echo "Error: The following .vue files in 'components' folder are not PascalCase:"
  echo "$BAD_COMPONENT_FILES"
  exit 1
else
  echo "All .vue files in 'components' are named correctly."
fi

echo "Checking for capital letters or spaces in general directories (pages, public, content)..."

GENERAL_DIRS=("pages" "public" "content")

# Loop through each directory and run git ls-files
for DIR in "${GENERAL_DIRS[@]}"; do
  echo "Running git ls-files on $DIR..."
  FILES=$(git ls-files "$DIR")
  
  # If no files found, continue to the next directory
  if [ -z "$FILES" ]; then
    echo "No files found in $DIR"
    continue
  fi

  # Check for capital letters or spaces
  for FILE in $FILES; do
    if echo "$FILE" | grep -qE '[A-Z]| '; then
      BAD_GENERAL_FILES="$BAD_GENERAL_FILES $FILE"
    fi
  done
done

# Check if any bad files were found
if [ -n "$BAD_GENERAL_FILES" ]; then
  echo "Error: The following files in 'pages', 'public', or 'content' contain capital letters or spaces:"
  echo "$BAD_GENERAL_FILES"
  echo "File naming check for general directories failed!"
  exit 1
else
  echo "All files in general directories are named correctly."
fi

echo "File naming check passed!"
