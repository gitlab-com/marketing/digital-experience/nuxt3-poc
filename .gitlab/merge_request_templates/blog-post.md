### Checklist for writer

- [ ] Link to issue added, and set to closed when this MR is merged
- [ ] Due date added to the merge request title for the desired publish date
- [ ] Review app checked for any formatting issues
- [ ] Any required internal or external approval for this blog post has been granted (please leave a comment with details)
- [ ] Reviews by Legal team if necessary according to [SAFE guidelines](https://about.gitlab.com/handbook/legal/materials-legal-review-process/)
- [ ] **Assign to `@Sgittlen` for final review**

### File structure checklist for manual file creation

- [ ] Blog post should be a `.yml` file under the `/blog/` folder for the appropiate locale, for an English blog post this would be `/content/en-us/blog/`
- [ ] Blog post hero images should be placed in the `public/images/blog/hero-images` folder, and should be a valid image format (png,jpg,jpeg,svg,webp)
- [ ] the yml file has the correct structure:

  ```
  seo:
    title: the blog post title
    description: the blog post description
  content:
    title: the blog post title
    description: the blog post description
    authors:
      - Blog post author
    heroImage: images/blog/hero-images/logoforblogpost.jpg
    date: '2021-03-31'
    body: the blog post body text in markdown
    category: Engineering
    tags:
      - community
  config:
    slug: blog-post-slug
    featured: false
    template: BlogPost

  ```

After the blog has been published:

- [ ] Share on your social media channels
  - Add `?utm_medium=social&utm_campaign=blog&utm_content=advocacy` to the end of the blog URL when you share on social media, for data tracking. Your link should look like this:https://about.gitlab.com/blog/blog-title/?utm_medium=social&utm_campaign=blog&utm_content=advocacy
- [ ] After you've shared on your social media profiles, select one of the posts and link it in the #social_media_action Slack channel for everyone to engage with your post. The GitLab social team may engage or even share your social media post to amplify the work.
- To learn more about how to use your own social media channels for GitLab, [check out our team member social media guidelines here](https://about.gitlab.com/handbook/marketing/team-member-social-media-policy/).

/label ~"blog post"
