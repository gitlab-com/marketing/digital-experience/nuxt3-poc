## 1. Change Summary

[Describe what is changing, why, and the impact]

- Related [ISSUE]

## 2. QA Checklist

[This merge request should follow all best practices utilizing our [team code standards](https://handbook.gitlab.com/handbook/marketing/digital-experience/engineering/code-standards/) and our project's [development documentation](https://gitlab.com/gitlab-com/marketing/digital-experience/about-gitlab-com/-/tree/main/docs?ref_type=heads)]

- [ ] Code Cleanup: Any messages, linter warnings, and/or deprecation warnings are cleaned up in the console
- [ ] Tech Debt: I have created, or documented, any fast follow-up work that needs to be done after this MR is merged
- [ ] Common Component Regression Check: I have verified all impacted pages of my common component updates have been checked for regressions and UX is aware of any wider-scoped visual changes
- [ ] Efficient Code Review: I have tested and reviewed my own changes thoroughly before assigning a reviewer
- [ ] Accessibility: Axe tools run and issues addressed
- [ ] Cross-browser compatibility: Works on Safari, Chrome, and Firefox
- [ ] Analytics and SEO: Compatible with Google Analytics and SEO tools
- [ ] Localization checked for regressions

[Provide concise steps to test the changes]

#### Review App

| Production                | Review app | Screenshot |
| ------------------------- | ---------- | ---------- |
| https://about.gitlab.com/ | WIP        | Screenshot |

## 3. Deployment Steps

[List key deployment steps or configurations]
